import React, { Component } from 'react';
import { StyleSheet, Text, View, 
   Image, TextInput, TouchableOpacity, ImageBackground, 
   ScrollView, Platform} from 'react-native';

import KeyboardWrapperView from '../helpers/KeyboardWrapperView';

import Spinner from 'react-native-loading-spinner-overlay';

import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import firebase from 'react-native-firebase';

export default class ConfirmResetPassword extends Component {
    constructor(props) {
        super(props);

        email = '';
        code='';
        if (this.props.navigation.state.params != undefined) {
            email =
            this.props.navigation.state.params.email;
            code =
            this.props.navigation.state.params.code;
        }
        this.state = {
            code:code,
            email: email, 
            spinner:false,
            spinnerMessage: ''
        };
    }

    static navigationOptions = {
       // header:null
       headerTitle: 'RESET PASSWORD',
       headerStyle: {
        backgroundColor: '#5271FF',
    },
        headerTitleStyle: {
        color: "#fff",
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      headerTintColor: '#fff',
    }

    _onPressButton() {
        Alert.alert('You tapped the button!')
    }
    _onPressButtonSignup= () => {
        this.props.navigation.navigate('Register');
      }
    _onPressButtonSubmit= () => {

        if ((this.state.password.length < 6) || (this.state.confirmPassword.length < 6)) {
            UltraAgentHelpers.showAlertWithOneButton("Password","Password should be minimum 8 characters.");
            return;
        }

        if (this.state.password != this.state.confirmPassword) {
            UltraAgentHelpers.showAlertWithOneButton("Password",
            "Password and Confirm Password should match");
            return;
        }

        this.setState({spinner:true, spinnerMessage: 'Processing...'});
        firebase.auth().confirmPasswordReset(this.state.code, this.state.password)
        .then(() => {
            this.setState({spinner:false, spinnerMessage: ''});
            this.props.navigation.navigate('PasswordChanged');
        })
        .catch((err) => {
            console.log(err);
            this.setState({spinner:false, spinnerMessage: ''});
            UltraAgentHelpers.showAlertWithOneButton("Oops!",err.message);
        });

        
    }


  render() {
    return (
        <KeyboardWrapperView behavior="padding" style={styles.wrapper}>
         <Spinner
                    visible={this.state.spinner}
                    textContent={this.state.spinnerMessage}
                    textStyle={{color: '#FFF'}}
                    />
                 <ImageBackground source={require('../images/registerbg.jpg')} 
                    style={styles.backgroundImage}>
                    <View style={styles.logoContainer}>
                        <Image style={styles.logo} 
                        source={require('../images/logo.png')}/>
                       <Text style={styles.iconText}>
                            {'Reset your password. \n Kindly enter your new password'}
                        </Text>
                    </View>
                        <View style={styles.loginformContainer}>
                
                            <View style={styles.inputContainer}>
                                <Image source={require('../images/lockicon.png')} 
                                style={styles.InputImageIcon} />

                                <TextInput 
                                    style={{flex:1}} 
                                    underlineColorAndroid={'transparent'}
                                    placeholder="New Password" 
                                    placeholderTextColor = "#838383"
                                    secureTextEntry 
                                    autoCorrect={false}
                                    onChangeText={password => this.setState({ password })}
                                    value={this.state.password}
                                />

                            </View>
                            <View style={styles.inputContainer}>
                                <Image source={require('../images/lockicon.png')} 
                                style={styles.InputImageIcon} />

                                <TextInput 
                                    style={{flex:1}} 
                                    underlineColorAndroid={'transparent'}
                                    placeholder="Confirm New Password" 
                                    placeholderTextColor = "#838383"
                                    secureTextEntry 
                                    autoCorrect={false}
                                    onChangeText={confirmPassword => this.setState({ confirmPassword })}
                                    value={this.state.confirmPassword}
                                />

                            </View>
                        
                            <TouchableOpacity 
                                style={styles.button} 
                                onPress={this._onPressButtonSubmit}
                                activeOpacity={0.7} 
                                >
                                <Text style={styles.btntext}>SUBMIT</Text>
                    
                            </TouchableOpacity>
                           
                        </View>
                       
                </ImageBackground>
        </KeyboardWrapperView>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container:{
    flex:1,
    alignItems: 'center',
    alignSelf:'stretch',
    width:null,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  logoContainer:{
    alignItems: 'center',
    alignSelf:'stretch',
    //marginTop:30,
    justifyContent: 'center',
    flexGrow:1,
    marginTop:10,
  },
  logo:{
    width:150,
    height:150,
  },
  loginformContainer:{
    // alignSelf:'stretch',
    // marginTop:40,
    // width:250,
    // paddingLeft:20,
    // paddingRight:20,
    alignSelf:'stretch',
    paddingLeft:20,
    paddingRight:20,
    marginTop:10,
    marginLeft:40,
    marginRight:40,
    marginBottom:180,
  },
  iconText:{
    color:'#000',
    textAlign:'center',
    alignItems: 'center',
    marginTop:10,
    lineHeight:20,
    fontWeight:"bold",
  },
 
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf:'stretch',
    padding:10,
    backgroundColor:'#fff',
    borderRadius:2,
    marginTop:8,
    ...Platform.select({
        ios: {
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.3,
        },
        android: {
            elevation:2,
            height:40,
            paddingTop:0,
            paddingBottom:0,
        },
    })
},
InputImageIcon: {
    marginRight:8,
    height: 20,
    width: 20,
    resizeMode : 'stretch',
    alignItems: 'center'
},
  
  button:{
    alignSelf:'stretch',
    marginTop:20,
    backgroundColor:'#5271FF',
    alignItems:'center',
    padding:10,
    borderRadius:3,
  },
  btntext:{
      color:'#fff',
  },
});
