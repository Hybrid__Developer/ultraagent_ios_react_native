import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Dimensions, Text,
  ImageBackground
} from 'react-native';
import {
  Container, Header, Title, Body, Button,
  Left, Right, Picker, List, ListItem, Content, Badge
} from 'native-base';

export default class TimeSheet extends Component {

  constructor(props) {
    super(props);
    this.state = {
       
    };
  }

  static navigationOptions = {
    header:null
}

_onPressPendingShifts = () => {
  this.props.navigation.navigate('PendingShifts');
}
_onPressCompleteShifts = () => {
  this.props.navigation.navigate('CompleteShifts');
}
_onPressDisputeShifts = () => {
  this.props.navigation.navigate('DisputeShifts');
}
_onPressCancelShifts = () => {
  this.props.navigation.navigate('CancelShifts');
}

  render() {
    let { height, width } = Dimensions.get('window');

    return (
      <ImageBackground
      source={require('../images/timesheetbg.png')}
      style={{ width, height }}
    >
        <Header style={{backgroundColor:'#5271FF'}}>
         
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Image source={require('../images/hamburgericon.png')} style={{height:32, width:32,}} />
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'Time sheet'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
         
       </Header>
      
          <Content>
              <List>
                <ListItem style={{ marginLeft: 0, marginTop:10 }} 
                button onPress={this._onPressPendingShifts}>
                  <View style={styles.IconViewStyle}> 
                    <View style={styles.viewBluebgBox}>
                      <Image source={require('../images/pendingshifticons.png')} style={{height:40, width:40,}} />
                    </View>
                    <Text style={styles.timeSheetText}>{'pending shifts'.toUpperCase()}</Text>
                  </View>
                </ListItem>
                <ListItem style={{ marginLeft: 0}} 
                button onPress={this._onPressCompleteShifts}>
                  <View style={styles.IconViewStyle}> 
                    <View style={styles.viewBluebgBox}>
                      <Image source={require('../images/completedshifticons.png')} style={{height:40, width:40,}} />
                    </View>
                    <Text style={styles.timeSheetText}>{'completed shifts'.toUpperCase()}</Text>
                  </View>
                </ListItem>
                <ListItem style={{ marginLeft: 0}} 
                button onPress={this._onPressDisputeShifts}>
                  <View style={styles.IconViewStyle}> 
                    <View style={styles.viewBluebgBox}>
                      <Image source={require('../images/disputshifticons.png')} style={{height:40, width:40,}} />
                    </View>
                    <Text style={styles.timeSheetText}>{'disputed shifts'.toUpperCase()}</Text>
                  </View>
                </ListItem>
                <ListItem style={{ marginLeft: 0}}  
                button onPress={this._onPressCancelShifts}>
                  <View style={styles.IconViewStyle}> 
                    <View style={styles.viewBluebgBox}>
                      <Image source={require('../images/cancelledicons.png')} style={{height:40, width:40,}} />
                    </View>
                    <Text style={styles.timeSheetText}>{'cancelled shifts'.toUpperCase()}</Text>
                  </View>
                </ListItem>
              </List>
          </Content>

        </ImageBackground>
     );
    }
  }
  const styles = StyleSheet.create({
    backgroundImage:{
      
    },
    HeaderTitle:{
        color:'#fff',
        textAlign:'center',
        alignItems:'center',
    },
    IconViewStyle:{
      flexDirection: 'row',
      flex:1,
    },
    timeSheetText:{
      justifyContent: 'center',
      alignSelf:'stretch',
      flex:1,
      lineHeight:60,
      paddingLeft:20,
    },
    viewBluebgBox:{
      backgroundColor:'#5271ff', 
      padding:10, 
      alignItems:"flex-end", 
      paddingLeft:60, 
      padding:10, 
      paddingRight:20, 
      borderBottomRightRadius:50, 
      borderTopRightRadius:50,
    },
    
})
