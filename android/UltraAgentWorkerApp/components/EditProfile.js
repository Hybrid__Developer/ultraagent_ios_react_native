import React, { Component } from 'react';
import {
  StyleSheet, View, Image, ImageBackground, Text, 
  Platform, ScrollView, TextInput, TouchableOpacity, Dimensions,
  TouchableHighlight, AsyncStorage, Alert
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, Item, Content, Header, Body, Picker, Form, Textarea, Icon
} from 'native-base';
import Dialog, { DialogButton, DialogContent, DialogTitle } from 'react-native-popup-dialog';

import SingletonClass from '../helpers/SingletonClass';
//import Icon from 'react-native-ionicons';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import UploadedObjectAsyncModel from '../asyncstoragemodels/UploadedObjectAsyncModel';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import Spinner from 'react-native-loading-spinner-overlay';

import firebase from 'react-native-firebase';

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

// Because this variable remains in global scope, it should always be a 
// unique name, otherwise, as screen push pop it will get overridden if declared by same name in
// another screen
thisEditProfileInstance = null;

export default class EditProfile extends Component {

  state = {}
  
  constructor(props) {
    super(props);
    const aProf = SingletonClass.get('loggedinprofile');
    //var picUrl = require('../images/setprofilepic.png');
    //var picUrl = {url: 'https://placehold.it/180'};
    var picUrl = require('../images/profiledefault.jpg');
    if ((aProf.profilePicture != undefined) && (aProf.profilePicture.url != undefined) &&
        (aProf.profilePicture.url != null) && (aProf.profilePicture.url.length > 0)) {
        picUrl = aProf.profilePicture;
    }
    this.state = {
        ...aProf,
        oldPassword:'',
        password:'',
        confirmPassword:'',
        workingHours: this.buildWorkingHoursDisplayString(aProf),
        spinner:false,
        spinnerMessage: '',
        avatarSource: picUrl,
        deactivateAccountDialog:false,
        changePasswordDialog:false,
        deactivateReason: "Too much notifications",
        deactivateBrief:''
    };
    
    this.onSavePressed = this.onSavePressed.bind(this);
    //console.log(this.state);

    this._uploadImage = this._uploadImage.bind(this);
    this.onPasswordChangePress = this.onPasswordChangePress.bind(this);
    this.onDeactivatePress = this.onDeactivatePress.bind(this);

    thisEditProfileInstance = this;

    this.citySelectionData = UltraAgentHelpers.cityDropdownData();

  }

  onValueChange(value) {
    this.setState({
        deactivateReason: value
    });
  }

  componentDidMount() {
      //console.log('EditProfile::componentDidMount');
   
  }
  componentWillUnmount() {
    //console.log('EditProfile::componentWillUnmount');
 
  }

  _onPressWorkingHourInput= () => {
    this.props.navigation.navigate('EditProfileWorkingHour',{
        workingHoursUpdatedCallbackForParent: this.childUpdatedWorkingHoursCallback.bind(this),
        parentState: this.state
    });
}

 buildWorkingHoursDisplayString = (aObj) => {
    //console.log('buildWorkingHoursDisplayString');

    var astr = "";
    if ((aObj.fulltime != undefined) && (aObj.fulltime == true)){
        astr = "Full Time";
    } else{
        astr = "Part Time ";
        if ((aObj.parttimeMon != undefined) && (aObj.parttimeMon == true)){
            astr += "Mon,"
        }
        if ((aObj.parttimeTue != undefined) && (aObj.parttimeTue == true)){
            astr += "Tue,"
        }
        if ((aObj.parttimeWed != undefined) && (aObj.parttimeWed == true)){
            astr += "Wed,"
        }
        if ((aObj.parttimeThu != undefined) && (aObj.parttimeThu == true)){
            astr += "Thu,"
        }
        if ((aObj.parttimeFri != undefined) && (aObj.parttimeFri == true)){
            astr += "Fri,"
        }
        if ((aObj.parttimeSat != undefined) && (aObj.parttimeSat == true)){
            astr += "Sat,"
        }
        if ((aObj.parttimeSun != undefined) && (aObj.parttimeSun == true)){
            astr += "Sun,"
        }

        //This will remove the last comma and any whitespace after it:
        astr = astr.replace(/,\s*$/, "");

        astr += "  "
        if ((aObj.workingStartTime != undefined) && (aObj.workingStartTime.length > 0)){
            astr += aObj.workingStartTime;
        }
        if ((aObj.workingStartTimeAmPm != undefined) && (aObj.workingStartTimeAmPm.length > 0)){
            astr += aObj.workingStartTimeAmPm;
        }

        astr += " - "
        if ((aObj.workingEndTime != undefined) && (aObj.workingEndTime.length > 0)){
            astr += aObj.workingEndTime;
        }
        if ((aObj.workingEndTimeAmPm != undefined) && (aObj.workingEndTimeAmPm.length > 0)){
            astr += aObj.workingEndTimeAmPm;
        }
    }
    //console.log(astr);
    return astr;

}

  childUpdatedWorkingHoursCallback = (aObj) => {
    //console.log('childUpdatedWorkingHoursCallback');
    //console.log(aObj);
    const astr = this.buildWorkingHoursDisplayString(aObj);
    this.setState({
        fulltime:aObj.fulltime,
        parttime:aObj.parttime,
        parttimeMon:aObj.parttimeMon,
        parttimeTue:aObj.parttimeTue,
        parttimeWed:aObj.parttimeWed,
        parttimeThu:aObj.parttimeThu,
        parttimeFri:aObj.parttimeFri,
        parttimeSat:aObj.parttimeSat,
        parttimeSun:aObj.parttimeSun,
        workingStartTime:aObj.workingStartTime,
        workingEndTime:aObj.workingEndTime,
        workingStartTimeAmPm:aObj.workingStartTimeAmPm,
        workingEndTimeAmPm:aObj.workingEndTimeAmPm,
        workingHours: astr
    });
}

onSavePressed () {
    //console.log('onSavePressed');


    // if (((this.state.password.length >0) && (this.state.password.length < 6)) || 
    //     ((this.state.confirmPassword.length > 0) && (this.state.confirmPassword.length < 6))) {
    //     UltraAgentHelpers.showAlertWithOneButton("Password","Password should be minimum 8 characters.");
    //     return;
    // }

    // if ((this.state.confirmPassword.length > 0) && (this.state.password != this.state.confirmPassword)) {
    //     UltraAgentHelpers.showAlertWithOneButton("Password",
    //     "Password and Confirm Password should match");
    //     return;
    // }

    if ((this.state.firstName == undefined) || (this.state.firstName.length < 1 )) {
        UltraAgentHelpers.showAlertWithOneButton("First Name","Please enter valid First Name");
        return;
    }
    if ((this.state.lastName == undefined) || (this.state.lastName.length < 1 )) {
        UltraAgentHelpers.showAlertWithOneButton("Last Name","Please enter valid Last Name");
        return;
    }
    if ((this.state.phoneNumber == undefined) || (this.state.phoneNumber.length < 1 )) {
        UltraAgentHelpers.showAlertWithOneButton("Phone Number","Please enter valid Phone Number");
        return;
    }
    if ((this.state.phoneNumber.length < 10) || (isNaN(this.state.phoneNumber))){
        UltraAgentHelpers.showAlertWithOneButton("Invalid",
        "Phone Number should be minimum 10 digits");
        return;
    }
    if ((this.state.city == undefined) || (this.state.city.length < 1 )) {
        UltraAgentHelpers.showAlertWithOneButton("City","Please enter valid city");
        return;
    }
    if ((this.state.address == undefined) || (this.state.address.length < 1 )) {
        UltraAgentHelpers.showAlertWithOneButton("Address","Please enter valid Address");
        return;
    }
    if ((this.state.postcode == undefined) || (this.state.postcode.length < 1 )) {
        UltraAgentHelpers.showAlertWithOneButton("Postcode","Please enter valid Postcode");
        return;
    }
   
    
    if ((this.state.avatarSource != undefined) && 
    (this.state.avatarSource.fileName != undefined) &&
    (this.state.avatarSource.fileName != null))
    {
        //Start spinner
        this.setState({spinner: true, spinnerMessage:'Uploading...'});
        // Profile image has been selected. Upload it first.
        this._uploadImage(this.state.avatarSource.uri, this.state.avatarSource.fileName)
        .then((url) => {
            //console.log("Upload complete")
            //console.log(url)
            profilePicture = thisEditProfileInstance.state.profilePicture;
            profilePicture.setProgress(100);
            profilePicture.setUrl(url);

            thisEditProfileInstance.setState({ profilePicture });
            //console.log(thisEditProfileInstance.state);

            //Start spinner
            this.setState({spinner: true, spinnerMessage:'Updating account...'});
            // Now save data part
            this._performDataSave();
        })
        .catch((error) => {
            //console.log("_onPressButtonBrowse:: Upload failed")
            console.log(error)
            this.setState({spinner: false, spinnerMessage:''});
            UltraAgentHelpers.showAlertWithOneButton("Upload Error",error.message,"Dismiss");
        });
    } else {
        //Start spinner
        this.setState({spinner: true, spinnerMessage:'Updating account...'});
        // Save Data Part
        this._performDataSave();
    }

}

_performDataSave = () => {
    //console.log('_performDataSave');
    //console.log(this.state);
    profileData =  {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        phoneNumber: this.state.phoneNumber,
        address: this.state.address,
        postcode: this.state.postcode,
        city: this.state.city,
        fulltime:this.state.fulltime,
        parttime:this.state.parttime,
        parttimeMon:this.state.parttimeMon,
        parttimeTue:this.state.parttimeTue,
        parttimeWed:this.state.parttimeWed,
        parttimeThu:this.state.parttimeThu,
        parttimeFri:this.state.parttimeFri,
        parttimeSat:this.state.parttimeSat,
        parttimeSun:this.state.parttimeSun,
        workingStartTime:this.state.workingStartTime,
        workingEndTime:this.state.workingEndTime,
        workingStartTimeAmPm:this.state.workingStartTimeAmPm,
        workingEndTimeAmPm:this.state.workingEndTimeAmPm,
    }

    if ((this.state.profilePicture != undefined) && (this.state.profilePicture._url != undefined) &&
        (this.state.profilePicture._url != null) && (this.state.profilePicture._url.length > 0 )) {
        profileData.profilePicture = {url: this.state.profilePicture._url}
    }

    //console.log(profileData);

    firebase.firestore().collection('profiles').doc(this.state.userId).set(
       profileData, {merge: true}
    ). then(() => {
        thisEditProfileInstance.setState({spinner: false, spinnerMessage:''});
        UltraAgentHelpers.showAlertWithOneButton("","Profile updated successfully.");
    })
    .catch((error) => {
        console.log(error);
        thisEditProfileInstance.setState({spinner: false, spinnerMessage:''});
        UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
    });
}

onPasswordChangePress = () => {

    if (this.state.oldPassword.length < 1) {
        UltraAgentHelpers.showAlertWithOneButton("Password",
        "Please enter your existing password correctly");
        return;
    }

    // if ((this.state.password.length < 8) || 
    //     (this.state.confirmPassword.length < 8)) {
    //     UltraAgentHelpers.showAlertWithOneButton("Password","Password should be minimum 8 characters.");
    //     return;
    // }

    if ((!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.state.password)) || 
        (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.state.confirmPassword))) {
        UltraAgentHelpers.showAlertWithOneButton('Password', "Password should be Minimum 8 characters, at least one uppercase letter, one lowercase letter, one digit and one special character.");
        return;
    }

    if ((this.state.confirmPassword.length > 0) && (this.state.password != this.state.confirmPassword)) {
        UltraAgentHelpers.showAlertWithOneButton("Password",
        "Password and Confirm Password should match");
        return;
    }
    
    this.setState({spinner: true, spinnerMessage:'Setting password...'});

    var user = firebase.auth().currentUser;
    var cred = firebase.auth.EmailAuthProvider.credential(user.email, this.state.oldPassword);
    user.reauthenticateWithCredential(cred)
    .then(() => {
        var user = firebase.auth().currentUser;
        user.updatePassword(this.state.confirmPassword)
        .then(() => {
            //console.log("Password updated!");
            this.setState({spinner: false, spinnerMessage:''});
            this.setState({ oldPassword:''});
            this.setState({ password:''});
            this.setState({ confirmPassword:''});
            UltraAgentHelpers.showAlertWithOneButton('','Password updated successfully');
            this.setState({ changePasswordDialog: false });

        })
        .catch((error) => { 
            console.log(error); 
            this.setState({spinner: false, spinnerMessage:''});
            UltraAgentHelpers.showAlertWithOneButton('Error',error.message);
        });
    })
    .catch((error) => { 
        console.log(error); 
        this.setState({spinner: false, spinnerMessage:''});
        UltraAgentHelpers.showAlertWithOneButton('Error',error.message);
    });

}

onDeactivatePress = () => {

    if (this.state.oldPassword.length < 1) {
        UltraAgentHelpers.showAlertWithOneButton("Password",
        "Please enter your existing password correctly");
        return;
    }
    if (this.state.deactivateReason.length < 1) {
        UltraAgentHelpers.showAlertWithOneButton("Deactivate Reason",
        "Please enter your deactivation reason");
        return;
    }
    if (this.state.deactivateBrief.length < 1) {
        UltraAgentHelpers.showAlertWithOneButton("Deactivate Brief",
        "Please enter your deactivation brief");
        return;
    }

    Alert.alert(
        "Confirmation",
        "Do you really want to deactivate?\n\nAll your data will be lost.\n\n",
        [
            {text: 'No, Cancel', onPress: () => {
                console.log('Cancel Pressed');
                this.setState({ deactivateAccountDialog: false });
                this.setState({ deactivateBrief: '' });
                this.setState({ oldPassword: '' });
                
            }, style: 'cancel'},
            {text: "Yes, Deactivate", style:'destructive', onPress: async () => {
              
                this.setState({spinner: true, spinnerMessage:'Deactivating...'});

                var user = firebase.auth().currentUser;
                var cred = firebase.auth.EmailAuthProvider.credential(user.email, this.state.oldPassword);
                user.reauthenticateWithCredential(cred)
                .then(() => {
                    firebase.firestore().collection('deactivation').doc(this.state.userId).set(
                        {
                            email:this.state.email,
                            userId:this.state.userId,
                            firstName: this.state.firstName,
                            lastName:this.state.lastName,
                            reason:this.state.deactivateReason,
                            brief:this.state.deactivateBrief
                        }, {merge:true}
                    )
                    .then(() => {
                        firebase.firestore().collection('profiles').doc(this.state.userId).set(
                            {'approvalStatus':'deactivated'}, {merge: true}
                         ). then(async () => {

                            // Reset fcmToken to empty
                            var aProf = SingletonClass.get('loggedinprofile');
                            let fcmToken = await AsyncStorage.getItem('fcmToken', null);
                            if((aProf != undefined) && (aProf != null) && (aProf.userId != undefined) && (fcmToken != null)) {
                                UltraAgentHelpers.unlinkFcmTokenFromUserUserId(fcmToken, aProf.userId);
                            }

                            firebase.auth().signOut()
                            .then(() => {
                                thisEditProfileInstance.setState({spinner: false, spinnerMessage:''});
                                thisEditProfileInstance.setState({ deactivateAccountDialog: false });
                                UltraAgentHelpers.showAlertWithOneButton("","Profile deactivated successfully.");
                                thisEditProfileInstance.props.navigation.navigate('Auth')
                            })
                            .catch((error) => {
                                console.log(error);
                                thisEditProfileInstance.setState({spinner: false, spinnerMessage:''});
                                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                            });
                         })
                         .catch((error) => {
                             console.log(error);
                             thisEditProfileInstance.setState({spinner: false, spinnerMessage:''});
                             UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                         });
                    })
                    .catch((error) => { 
                        console.log(error); 
                        thisEditProfileInstance.setState({spinner: false, spinnerMessage:''});
                        UltraAgentHelpers.showAlertWithOneButton('Oops!',error.message);
                    });
                })
                .catch((error) => { 
                    console.log(error); 
                    thisEditProfileInstance.setState({spinner: false, spinnerMessage:''});
                    UltraAgentHelpers.showAlertWithOneButton('Oops!',error.message);
                });
            
            }}
        ],
        { cancelable: false }
    );
   
}


  static navigationOptions = {
    header:null
  }


  _uploadImage (uri, fileName, mime='application/octet-stream') {
    return new Promise((resolve, reject) => {
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://','') : uri;
        const sessionId = new Date().getTime();
        let uploadBlob = null;

        //console.log(uploadUri)
        const fileParts = uploadUri.split('/')
        const lastPart = fileParts[fileParts.length -1];
        const nameParts = lastPart.split('.')
        const fileExtension = nameParts[nameParts.length -1] 
        const someRandom = Math.floor(Math.random() * 10000) + 1 
        const uniqFileName = "worker-"+sessionId+"-"+someRandom+"."+fileExtension;
        //console.log("uniqFileName="+uniqFileName);

        const uploadRefObject = new UploadedObjectAsyncModel();
        uploadRefObject.setUniqId(uniqFileName);
        uploadRefObject.setName(fileName);
        this.setState({
            profilePicture: uploadRefObject
        });

        //create a reference in firebase  storage for the file 
        const uploadRef = firebase.storage().ref('docs').child(uniqFileName);

        //encode data with Bse64 prior to uploading
        fs.readFile(uploadUri, 'base64')
        .then((data) => {
            return Blob.build(data, { type: `${mime};BASE64` })
        })
        //place the blob into your storage reference
        .then((blob) => {
            uploadBlob = blob
            uploadTask = uploadRef.put(blob._ref, blob, { contentType: mime });

            // Register three observers:
            // 1. 'state_changed' observer, called any time the state changes
            // 2. Error observer, called on failure
            // 3. Completion observer, called on successful completion
            uploadTask.on('state_changed', function(snapshot){
                // Observe state change events such as progress, pause, and resume
                // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

                //resolve(progress, snapshot.totalBytes, snapshot.bytesTransferred);
                //this.masterDataUpdatedCallbackForParent();

                profilePicture = thisEditProfileInstance.state.profilePicture;
                profilePicture.setProgress(progress);
                profilePicture.setTotalBytes(snapshot.totalBytes);
                profilePicture.setBytesTransferred(snapshot.bytesTransferred);
                
                thisEditProfileInstance.setState({ profilePicture });
                //console.log(thisEditProfileInstance.state);

                //console.log('Upload is ' + progress + '% done');
                switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'
                    //console.log('Upload is paused');
                    break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'
                    //console.log('Upload is running');
                    break;
                }
            },
            function(error) {
                // Handle unsuccessful uploads
                reject(error)
            }, function() {
                // Handle successful uploads on complete
                // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                uploadRef.getDownloadURL().then(function(url) {
                    //console.log('File available at', url);
                    resolve(url)
                    
                });
            });
        });

    });

}

_onPressButtonBrowse = () => {

    ImagePicker.showImagePicker({
        allowsEditing: true,
        aspect:[4,4]
    }, (response) => {
        //console.log('Response = ', response);

        if (response.didCancel) {
            //console.log('User cancelled image picker');
        } else if (response.error) {
            //console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            //console.log('User tapped custom button: ', response.customButton);
        } else {  
            var aFileName = response.fileName;
            if (aFileName == undefined) {
                var pieces = response.uri.split('/');
                aFileName = pieces.pop() || pieces.pop();
            }
            this.setState({avatarSource: {uri: response.uri, fileName:aFileName}})
        }
    });
}

citiesList = () =>{
    console.log('citiesList');
    return( this.citySelectionData.map( (x,i) => { 
        if ((x.city == undefined) || (x.city.length < 1) || (x.city == '-none-')) {
          return( <Picker.Item label={x.display} key={i} value={x.city}  disabled={true}/>)
        } else {
          return( <Picker.Item label={x.display} key={i} value={x.city}  />)
        }
    }));      
}

  render() {
    return (
        <View style={styles.wrapper}>
         <Spinner
                    visible={this.state.spinner}
                    textContent={this.state.spinnerMessage}
                    textStyle={{color: '#FFF'}}
                    />
                <ImageBackground  style={{ flex:1, backgroundColor:'#fff'}}
                source={require('../images/personal-details-bg.jpg')} 
                imageStyle={{ resizeMode: 'cover' }}>
                    <Header style={{backgroundColor:'#5271FF', borderBottomColor:'transparent'}}>
                        <Left style={{flex:1}}>
                            <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                                <Icon type="FontAwesome" name="angle-left" 
                                style={{fontSize: 30, color: '#fff'}} />         
                            </Button>
                        </Left>
                        <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
                            <Title style={styles.HeaderTitle}>{'Edit Profile'.toUpperCase()}</Title>
                        </Body>
                        <Right style={{flex:1}}>
                            <TouchableOpacity 
                                onPress={this.onSavePressed}
                                activeOpacity={0.7} 
                                >
                                <Text style={{fontSize:14,
                                    color:'#FFFFFF',
                                    marginBottom:3,
                                    fontWeight:'bold',}}>Save</Text>
                            </TouchableOpacity>
                        </Right>
                    
                    </Header>
                        <TouchableOpacity onPress={this._onPressButtonBrowse}>
                            <View style={styles.logoContainer}>
                                <View style={styles.profileImageContainer}></View>
                                <View>
                                    <Image style={styles.logo} 
                                    source={this.state.avatarSource}/>
                                    <Image source={require('../images/edit-plusicon.png')} 
                                    style={{width:25, height:25, position:'absolute', 
                                    right:-6, top:-35, zIndex:9}} />
                                </View>
                                <View style={styles.triangleImageStyle}>
                                    <Image style={{width:20, height:20}} 
                                    source={require('../images/triangle50.png')}/>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <ScrollView>
                            <Content>
                                <View style={styles.editProfileContainer}>

                                    <View style={styles.inputContainer}>
                                        <Label style={styles.labelStyle}>First Name</Label>
                                        <TextInput 
                                            style={styles.inputStyleContainer} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="Enter first name" 
                                            outoCorrect={false}
                                            onChangeText={firstName => this.setState({ firstName })}
                                            value={this.state.firstName}
                                        />
                                        <Icon type="FontAwesome" name="angle-right"
                                            style={{fontSize: 25, color: '#5271FF', 
                                            position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                    </View>
                                    <View style={styles.inputContainer}>
                                        <Label style={styles.labelStyle}>Last Name</Label>
                                        <TextInput 
                                            style={styles.inputStyleContainer} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="Enter last name" 
                                            outoCorrect={false}
                                            onChangeText={lastName => this.setState({ lastName })}
                                            value={this.state.lastName}
                                        />
                                        <Icon type="FontAwesome" name="angle-right" 
                                            style={{fontSize: 25, color: '#5271FF', 
                                            position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                    </View>
                                    <View style={styles.inputContainer}>
                                        <Label style={styles.labelStyle}>Email</Label>
                                        <TextInput 
                                            style={styles.inputStyleContainer} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="Enter email address" 
                                            keyboardType='email-address' 
                                            outoCorrect={false}
                                            editable={false}
                                            selectTextOnFocus={false}
                                            value={this.state.email}
                                        />
                                        <Icon type="FontAwesome" name="angle-right" 
                                            style={{fontSize: 25, color: '#5271FF', 
                                            position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                    </View>
                                    <View style={styles.inputContainer}>
                                        <Label style={styles.labelStyle}>Phone Number</Label>
                                        <TextInput 
                                            style={styles.inputStyleContainer} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="e.g. 918-763-2125" 
                                            outoCorrect={false}
                                            keyboardType='numeric'
                                            returnKeyType={ 'done' }
                                            onChangeText={phoneNumber => this.setState({ phoneNumber })}
                                            value={this.state.phoneNumber}
                                        />
                                        <Icon type="FontAwesome" name="angle-right" 
                                            style={{fontSize: 25, color: '#5271FF', 
                                            position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                    </View>
                                    {/* <View style={styles.inputContainer}>
                                        <Label style={styles.labelStyle}>City</Label>
                                        <TextInput 
                                            style={styles.inputStyleContainer} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="London" 
                                            outoCorrect={false}
                                            onChangeText={city => this.setState({ city })}
                                            value={this.state.city}
                                        />
                                        <Icon type="FontAwesome" name="angle-right" 
                                            style={{fontSize: 25, color: '#5271FF', 
                                            position:'absolute', right:10, top:28}}/>
                                    </View> */}
                                    <View style={styles.inputContainer}>
                                    <Label style={styles.labelStyle}>City</Label>
                                    <Picker
                                        mode="dropdown"
                                        iconRight
                                        textStyle={{left:-15}}
                                        style={styles.inputStyleContainerPicker}
                                        placeholderStyle={{ color: "#838383", fontSize:14, left:0 }}
                                        placeholderTextColor = "#838383"
                                        outoCorrect={false}
                                        selectedValue={this.state.city}
                                        onValueChange={(itemValue, itemIndex) => {
                                            // Since android is picking the disabled Item too, 
                                            // hence filter that here
                                            if ((itemValue == undefined) || (itemValue.length < 1) || (itemValue == '-none-')) {
                                            } else {
                                                this.setState({ city:itemValue });
                                            }
                                        }}
                                    >
                                        {/* <Picker.Item label="London" value="London" />
                                        <Picker.Item label="Birmingham" value="Birmingham" />
                                        <Picker.Item label="Leeds" value="Leeds" />
                                        <Picker.Item label="Glasgow" value="Glasgow" />
                                        <Picker.Item label="Sheffield" value="Sheffield" />
                                        <Picker.Item label="Bradford" value="Bradford" /> */}
                                        {this.citiesList()}
                                    </Picker>
                                    <Icon type="FontAwesome" name="angle-down" 
                                            style={{fontSize: 25, color: '#5271FF', 
                                            position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                   
                                </View>
                                    <View style={styles.inputContainer}>
                                        <Label style={styles.labelStyle}>Address</Label>
                                        <TextInput 
                                            style={styles.inputStyleContainer} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="London's Bridge, EC1 XYZ"
                                            placeholderTextColor = "#838383" 
                                            outoCorrect={false}
                                            keyboardType='default' 
                                            onChangeText={address => this.setState({ address })}
                                            value={this.state.address}
                                        />
                                        <Icon type="FontAwesome" name="angle-right" 
                                            style={{fontSize: 25, color: '#5271FF', 
                                            position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                    </View>
                                    <View style={styles.inputContainer}>
                                        <Label style={styles.labelStyle}>Postcode</Label>
                                        <TextInput 
                                            style={styles.inputStyleContainer} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="WC2N 5DU"
                                            placeholderTextColor = "#838383" 
                                            outoCorrect={false}
                                            keyboardType='default' 
                                            onChangeText={postcode => this.setState({ postcode })}
                                            value={this.state.postcode}
                                        />
                                        <Icon type="FontAwesome" name="angle-right" 
                                            style={{fontSize: 25, color: '#5271FF', 
                                            position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                    </View>
                                   
                                    <View style={styles.inputContainer}>
                                        <TouchableOpacity 
                                            onPress={this._onPressWorkingHourInput}
                                            activeOpacity={0.7} 
                                            >
                                            <Label style={styles.labelStyle}>Working Hours</Label>
                                            <Text 
                                                style={[styles.inputStyleContainer, styles.workingInputStyle]} 
                                                underlineColorAndroid={'transparent'}>
                                                {this.state.workingHours}
                                            </Text>
                                            <Icon type="FontAwesome" name="angle-right" 
                                            style={{fontSize: 25, color: '#5271FF', 
                                            position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                        </TouchableOpacity>
                                    </View>

                                     <View style={styles.inputContainer}>
                                        <TouchableOpacity 
                                            onPress={() => {
                                                this.setState({ changePasswordDialog: true });
                                                }}
                                            activeOpacity={0.7} 
                                            >
                                            <Label style={styles.labelStyle}>Change Password</Label>
                                            <Text
                                                style={[styles.inputStyleContainer, styles.inputAndroidDevicePlatform]} 
                                                underlineColorAndroid={'transparent'}
                                                placeholder="**********" 
                                                placeholderTextColor = "#838383"
                                                secureTextEntry 
                                                autoCorrect={false}
                                                // onChangeText={password => this.setState({ password })}
                                                // value={this.state.password}
                                            />
                                            <Icon type="FontAwesome" name="angle-right" 
                                            style={{fontSize: 25, color: '#5271FF', position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                        </TouchableOpacity>

                                        {/* Change password dialog container */}
                                        <Dialog
                                            visible={this.state.changePasswordDialog}
                                            onTouchOutside={() => {
                                                this.setState({ changePasswordDialog: false });
                                                }}
                                            // actions={[
                                            // <DialogButton
                                            //     text="CANCEL"
                                            //     onPress={() => {}}
                                            // />
                                            // ]}
                                            dialogTitle={<DialogTitle title="Change Password" 
                                            textStyle={{ color: '#000', fontSize:18 }}/>}
                                            width={0.9}>
                                            <ScrollView>
                                                <DialogContent>
                                                    <Form>
                                                        <View style={styles.dialogContainer}>
                                                            <View style={styles.dialogInputContainer}>
                                                                <Label style={styles.labelStyle}>Old Password</Label>
                                                                <TextInput 
                                                                    style={styles.dialoginputStyleContainer} 
                                                                    underlineColorAndroid={'transparent'}
                                                                    placeholder="" 
                                                                    secureTextEntry
                                                                    outoCorrect={false}
                                                                    onChangeText={oldPassword => this.setState({ oldPassword })}
                                                                    value={this.state.oldPassword}
                                                                />
                                                            </View>
                                                            <View style={styles.dialogInputContainer}>
                                                                <Label style={styles.labelStyle}>New Password</Label>
                                                                <TextInput 
                                                                    style={styles.dialoginputStyleContainer} 
                                                                    underlineColorAndroid={'transparent'}
                                                                    placeholder="" 
                                                                    secureTextEntry
                                                                    outoCorrect={false}
                                                                    onChangeText={password => this.setState({ password })}
                                                                    value={this.state.password}
                                                                />
                                                            </View>
                                                            <View style={styles.dialogInputContainer}>
                                                                <Label style={styles.labelStyle}>Confirm New Password</Label>
                                                                <TextInput 
                                                                    style={styles.dialoginputStyleContainer} 
                                                                    underlineColorAndroid={'transparent'}
                                                                    placeholder="" 
                                                                    secureTextEntry
                                                                    outoCorrect={false}
                                                                    onChangeText={confirmPassword => this.setState({ confirmPassword })}
                                                                    value={this.state.confirmPassword}
                                                                />
                                                            </View>
                                                            <TouchableHighlight style={styles.button} onPress={() => {
                                                            // this.setState({ changePasswordDialog: false });
                                                            this.onPasswordChangePress();
                                                            }}>
                                                                <Text style={styles.btntext}>
                                                                    Save
                                                                </Text>
                                                            </TouchableHighlight>
                                                        </View>
                                                    </Form>
                                                </DialogContent>
                                            </ScrollView>
                                        </Dialog>

                                    </View>
                                   
                                    <View style={[styles.inputContainer, styles.deactiveStyle]}>
                                        <TouchableOpacity 
                                           onPress={() => {
                                                this.setState({
                                                deactivateAccountDialog: true,
                                                });
                                            }}
                                            activeOpacity={0.7} 
                                            >
                                            <Label style={[styles.labelStyle, styles.labelStyleRed]}>Deactivate Account</Label>
                                            <Text
                                                style={[styles.inputStyleContainer, styles.inputAndroidDevicePlatform]}
                                                underlineColorAndroid={'transparent'}
                                                placeholder="Leave Ultra Agent worker app" 
                                                placeholderTextColor = "#838383"
                                                autoCorrect={false} />
                                            <Icon type="FontAwesome" name="angle-right" 
                                            style={{fontSize: 25, color: '#5271FF', position:'absolute', right:10, top:28, zIndex: 100,
                                            elevation: 100}}/>
                                        </TouchableOpacity>

                                         {/* Deactivate account dialog container */}
                                         <Dialog
                                            visible={this.state.deactivateAccountDialog}
                                            onTouchOutside={() => {
                                                this.setState({ deactivateAccountDialog: false });
                                                }}
                                          
                                            dialogTitle={<DialogTitle title="Deactivate Account" 
                                            textStyle={{ color: '#000', fontSize:18 }}/>}
                                            width={0.9}>
                                            <ScrollView>        
                                                <DialogContent>
                                                    <Form style={styles.deactivateFormStyle}>
                                                        <View style={styles.dialogContainer}>
                                                            <View style={styles.dialogInputContainer}>
                                                                <Label style={styles.labelStyle}>Select Reason for leaving</Label>
                                                                <View style={styles.deactivatePickerStyleContainer}>
                                                                    <Picker
                                                                        mode="dropdown"
                                                                        iconRight
                                                                        iosHeader="Select Reason for leaving"
                                                                        //style={{ width: 280, left:-5, bottom:4}}
                                                                        iosIcon={<Icon type="FontAwesome" name="caret-down" 
                                                                        style={{ color: "#5271FF", fontSize: 15, position:'absolute', right:0, top:10}} />}
                                                                        style={ styles.dropdownPickerStyle}
                                                                        selectedValue={this.state.deactivateReason}
                                                                        onValueChange={this.onValueChange.bind(this)}
                                                                        >
                                                                        <Picker.Item label="Too Much Notifications" value="Too Much Notifications" />
                                                                        <Picker.Item label="Moving To Somewhere" value="Moving To Somewhere" />
                                                                        <Picker.Item label="Not Finding Proper Value" value="Not Finding Proper Value" />
                                                                        <Picker.Item label="Leaving The Application" value="Leaving The Application" />
                                                                    </Picker>
                                                                    {/* <Icon type="FontAwesome" name="caret-down" 
                                                                    style={{fontSize: 15, color: '#5271FF', position:'absolute', right:10, top:10}}/> */}
                                                                </View>
                                                            </View>

                                                            <View style={styles.dialogInputContainer}>
                                                                <Label style={styles.labelStyle}>Brief Explanation</Label>
                                                                <Textarea rowSpan={3} placeholder="" 
                                                                style={styles.dialoginputStyleContainer} 
                                                                onChangeText={deactivateBrief => this.setState({ deactivateBrief })}
                                                                value={this.state.deactivateBrief}/>
                                                            </View>
                                                        
                                                            <View style={styles.dialogInputContainer}>
                                                                <Label style={styles.labelStyle}>Enter Password</Label>
                                                                <TextInput 
                                                                    style={styles.dialoginputStyleContainer} 
                                                                    underlineColorAndroid={'transparent'}
                                                                    placeholder="" 
                                                                    secureTextEntry
                                                                    outoCorrect={false}
                                                                    onChangeText={oldPassword => this.setState({ oldPassword })}
                                                                    value={this.state.oldPassword}
                                                                />
                                                            </View>

                                                            <TouchableHighlight style={[styles.button, styles.redbutton]} onPress={() => {
                                                            //this.setState({ deactivateAccountDialog: false });
                                                            this.onDeactivatePress();
                                                            }}>
                                                                <Text style={styles.btntext}>
                                                                    Permanently Deactivate
                                                                </Text>
                                                            </TouchableHighlight>
                                                        </View>
                                                    </Form>
                                                </DialogContent>
                                            </ScrollView> 
                                        </Dialog>
                                    </View>

                                </View>
                            </Content>
                        </ScrollView>
                </ImageBackground>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  HeaderTitle:{
    color:'#fff',
  },
  logoContainer:{
    alignItems: 'center',
    backgroundColor:'#fff',
    zIndex:0,
  },
  logo:{
      width:100,
      height:100,
      bottom:50,
      borderRadius:50,
      borderColor:'#3c5ffc',
      borderWidth:5,
  },
  profileImageContainer:{
    backgroundColor:'#5271FF', 
    padding:40, 
    width:Dimensions.get('window').width, 
    borderBottomLeftRadius:60,
    borderBottomRightRadius:60
  },
  
  editProfileContainer:{
    alignItems: 'stretch',
    backgroundColor:'transparent',
    padding:20,
    marginBottom:20,
  },
  inputContainer:{
      marginBottom:8,
  },
  triangleImageStyle:{
    position:'absolute', 
    left:0, 
    right:0, 
    bottom:-12, 
    alignItems: 'center', 
  },
  inputStyleContainer: {
    padding:12,
    backgroundColor:'#fff',
    borderRadius:2,
    ...Platform.select({
        ios: {
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.1,
        },
        android: {
            elevation:2,
            height:40,
            paddingBottom:0,
            paddingTop:0,
        },
    })
},
workingInputStyle:{
    paddingRight:18,
    ...Platform.select({
        ios: {
            
        },
        android: {
            paddingTop:10,
            paddingBottom:10,
            color:'#000'
        },
    })
},
inputAndroidDevicePlatform:{
    ...Platform.select({
        android: {
            height:40
        },
    })
    
},
inputStyleContainerPicker:{
    padding:12,
    paddingLeft:0,
    backgroundColor:'#fff',
    borderRadius:2,
    ...Platform.select({
        ios: {
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.1,
            width:'100%',
            height:40,
        },
        android: {
            elevation:2,
            height:40,
            paddingBottom:0,
            paddingTop:0,
        },
    })
},
deactivatePickerStyleContainer: {
    height:40,
    backgroundColor:'#fff',
    borderRadius:3,
    borderColor:"#ccc",
    borderWidth:2,
},

deactiveStyle:{
    marginTop:15,
},
dialogContainer:{
    padding:15,
},
dialogInputContainer:{
    marginBottom:15,
},
dialoginputStyleContainer:{
    paddingTop:8,
    paddingBottom:8,
    paddingLeft:10,
    paddingRight:10,
    backgroundColor:'#fff',
    borderRadius:3,
    borderColor:"#ccc",
    borderWidth:2
},

labelStyle:{
    fontSize:14,
    color:'#5271FF',
    marginBottom:3,
    fontWeight:'bold',
},
labelStyleRed:{
    color:'#f80808',
    fontWeight:'400'
},
button:{
    alignSelf:'stretch',
    marginLeft:60,
    marginRight:60,
    marginTop:10,
    backgroundColor:'#5271FF',
    padding:12,
    borderRadius:3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btntext:{
    textAlign:'center',
    alignSelf:'stretch',
    alignItems: 'center',
    color:'#fff',
    fontWeight:'500',
    fontSize:16,
    letterSpacing:1,
    color:'#fff',
  },
  redbutton:{
    backgroundColor:'#f80808',
    marginLeft:10,
    marginRight:10,
  },
  dropdownPickerStyle:{
    ...Platform.select({
        ios: {
            width: Dimensions.get('window').width-100, 
            left:-5, 
            bottom:4,
        },
        android: {
            width: undefined, 
            bottom:7
        },
    })
    
},
  
});