import React, { Component } from 'react';
import { StyleSheet, Text, View, 
   Image, TextInput, TouchableOpacity, TouchableHighlight, ImageBackground, 
   ScrollView, Platform, Button, Alert, Dimensions} from 'react-native';
import { List, FlatList } from "react-native";
//import  Icon from 'react-native-ionicons';
import { Picker, Icon } from 'native-base';
import DatePicker from 'react-native-datepicker'
import ProfileAsyncModel from "../asyncstoragemodels/ProfileAsyncModel";
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import UploadedObjectAsyncModel from '../asyncstoragemodels/UploadedObjectAsyncModel';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import Spinner from 'react-native-loading-spinner-overlay';
import ProgressCircle from 'react-native-progress-circle';

import firebase from 'react-native-firebase';

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

// Because this variable remains in global scope, it should always be a 
// unique name, otherwise, as screen push pop it will get overridden if declared by same name in
// another screen
thisRegisterUploadInstance = null;

export default class UpdateDocument extends Component {

    state = { }
    profileModelRef = null;
    masterDataUpdatedCallbackForParent = undefined;

    constructor(props) {
        super(props);
        this.state = {
            selectedInsurance: "",
        };
    
        this.profileModelRef = new ProfileAsyncModel();
        this,this.profileModelRef.setErrorMessage("");
        // Use default values of model
        this.state = {
            ...this.profileModelRef.createState(), 
            spinner:false,
            spinnerMessage: '',
            isUploading:false,
        };

        if (this.props.navigation.state.params != undefined) {
            this.masterDataUpdatedCallbackForParent = 
            this.props.navigation.state.params.masterDataUpdatedCallbackForParent;
        }

        this._uploadImage = this._uploadImage.bind(this);

        thisRegisterUploadInstance = this;

        this.uploadProgressUpdate = this.uploadProgressUpdate.bind(this);
    }

    
    onValueChangeInsurance(value) {
        this.setState({
            selectedInsurance: value
        });
    }

    childUpdatedMasterDataCallback = () => {
        //console.log("RegisterUpload::childUpdatedMasterDataCallback");
        this._loadProfileModelFromAsyncStorage();
        if (this.masterDataUpdatedCallbackForParent != undefined) {
            this.masterDataUpdatedCallbackForParent();
        }
    }

    _loadProfileModelFromAsyncStorage() {
        //console.log("RegisterUpload::_loadProfileModelFromAsyncStorage");
        ProfileAsyncModel.restore().then((aRef) => {
            if (aRef!== null) {
                this.profileModelRef = aRef;
                this.setState ({... this.profileModelRef.createState()});
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    componentWillMount() {
        // Required for instancing of models objects.
        UploadedObjectAsyncModel.require(UploadedObjectAsyncModel);
        ProfileAsyncModel.require(ProfileAsyncModel);
 
        this._loadProfileModelFromAsyncStorage();
    }

    componentWillReceiveProps(nextProps){
        this._loadProfileModelFromAsyncStorage(); 
        //console.log("RegisterUpload::componentWillReceiveProps()");
        //console.log(this.state);
    }

    static navigationOptions = {
       // header:null
       headerTitle: 'UPDATE DOCUMENT',
       headerStyle: {
        backgroundColor: '#5271FF',
      },
      headerBackTitle: null,
      headerTitleStyle: {
        color: "#fff",
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      headerTintColor: '#fff',
    }

    uploadProgressUpdate = (progress, transferred, total) => {

        wDocuments = this.state.wDocuments;
        const aLength = wDocuments.length;
        if (aLength > 0) {
            wDocuments[aLength-1].setProgress(progress);
            wDocuments[aLength-1].setTotalBytes(total);
            wDocuments[aLength-1].setBytesTransferred(transferred);

            this.setState({ wDocuments });
            //someRandom = Math.random();
            //this.setState({someRandom})
            //console.log("uploadProgressUpdate:progress="+progress+", transferred="+transferred+", total="+total);
            //console.log(this.state);
        }
    }

    _uploadImage (uri, fileName, mime='application/octet-stream') {
        return new Promise((resolve, reject) => {
            const uploadUri = Platform.OS === 'ios' ? uri.replace('file://','') : uri;
            const sessionId = new Date().getTime();
            let uploadBlob = null;

            //console.log(uploadUri)
            const fileParts = uploadUri.split('/')
            const lastPart = fileParts[fileParts.length -1];
            const nameParts = lastPart.split('.')
            const fileExtension = nameParts[nameParts.length -1] 
            const someRandom = Math.floor(Math.random() * 10000) + 1 
            const uniqFileName = "worker-"+sessionId+"-"+someRandom+"."+fileExtension;
            //console.log("uniqFileName="+uniqFileName);

            const uploadRefObject = new UploadedObjectAsyncModel();
            uploadRefObject.setUniqId(uniqFileName);
            uploadRefObject.setName(fileName);
            this.setState({
                wDocuments: [...this.state.wDocuments,uploadRefObject]
            });

            //create a reference in firebase  storage for the file 
            const uploadRef = firebase.storage().ref('docs').child(uniqFileName);

            //encode data with Bse64 prior to uploading
            fs.readFile(uploadUri, 'base64')
            .then((data) => {
                return Blob.build(data, { type: `${mime};BASE64` })
            })
            //place the blob into your storage reference
            .then((blob) => {
                uploadBlob = blob
                uploadTask = uploadRef.put(blob._ref, blob, { contentType: mime });

                // Register three observers:
                // 1. 'state_changed' observer, called any time the state changes
                // 2. Error observer, called on failure
                // 3. Completion observer, called on successful completion
                uploadTask.on('state_changed', function(snapshot){
                    // Observe state change events such as progress, pause, and resume
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

                    thisRegisterUploadInstance.uploadProgressUpdate(progress, snapshot.bytesTransferred, snapshot.totalBytes);

                    //console.log('Upload is ' + progress + '% done');
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                        //console.log('Upload is paused');
                        break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                        //console.log('Upload is running');
                        break;
                    }
                },
                function(error) {
                    // Handle unsuccessful uploads
                    reject(error)
                }, function() {
                    // Handle successful uploads on complete
                    // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                    uploadRef.getDownloadURL().then(function(url) {
                        //console.log('File available at', url);
                        resolve(url)
                        
                    });
                });
            });

        });

    }

    _onPressButtonBrowse = () => {

        if (this.state.isUploading == true) {
            UltraAgentHelpers.showAlertWithOneButton("Browse not allowed","Please wait for current upload to complete.\n");
            return;
        }

        if ((this.state.wDocuments != undefined) && (this.state.wDocuments != null) &&
            (this.state.wDocuments.length >= 5)) 
        {
            UltraAgentHelpers.showAlertWithOneButton("Upoload Limit Reached",
            "You can upload maximum 5 documents.\n"+
            "You can delete one of the earlier uploads and release quota instead.");
            return;
        }

        ImagePicker.showImagePicker({allowsEditing: true}, (response) => {
            console.log('Response = ', response);
    
            if (response.didCancel) {
                //console.log('User cancelled image picker');
            } else if (response.error) {
                //console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                //console.log('User tapped custom button: ', response.customButton);
            } else {  
                
                this.setState({isUploading: true});
                var aFileName = response.fileName;
                if (aFileName == undefined) {
                    var pieces = response.uri.split('/');
                    aFileName = pieces.pop() || pieces.pop();
                }
                this._uploadImage(response.uri, aFileName)
                .then((url) => {
                    this.setState({isUploading: false});
                    //console.log("Upload complete")
                    //console.log(url)
                    wDocuments = thisRegisterUploadInstance.state.wDocuments;
                    const aLength = wDocuments.length;
                    if (aLength > 0) {
                        wDocuments[aLength-1].setBytesTransferred(wDocuments[aLength-1].getTotalBytes());
                        wDocuments[aLength-1].setProgress(100);
                        wDocuments[aLength-1].setUrl(url);

                        thisRegisterUploadInstance.setState({ wDocuments });

                        //Save to model ref as well
                        this.profileModelRef.setWDocuments(wDocuments);
                        this.profileModelRef.store().then(() => {
                        })
                        .catch((error) => {
                            UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                        }); 
                    }
                    //console.log(thisRegisterUploadInstance.state);
                })
                .catch((error) => {
                    this.setState({isUploading: false});
                    //console.log("_onPressButtonBrowse:: Upload failed")
                    console.log(error)
                    UltraAgentHelpers.showAlertWithOneButton("Upload Error",error,"Dismiss");
                })
                
            }
        });
    }
    _onPressButtonNext = () => {

        if (this.state.isUploading == true) {
            UltraAgentHelpers.showAlertWithOneButton("Next not allowed","Please wait for current upload to complete.\n");
            return;
        }

        this.profileModelRef.setWDocuments(this.state.wDocuments);
           
        //console.log(this.profileModelRef.createState());
        // Create a Profile model object for aync storage based data propagation across screens
        this.profileModelRef.setErrorMessage("");
        this.profileModelRef.store().then(() => {
    
            // Notify Parent on Master Data Change
            if (this.masterDataUpdatedCallbackForParent != undefined) {
                this.masterDataUpdatedCallbackForParent();
            }

            // Works on both iOS and Android
            Alert.alert(
                'Enable Gps Service',
                'To use this App, enable the access to your location and the GPS satellite in phone settings',
                [
                {text: 'Don`t allow', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Allow', onPress: () => {
                    this.props.navigation.navigate('SetProfile', {
                        masterDataUpdatedCallbackForParent: this.childUpdatedMasterDataCallback.bind(this)
                    });
                    }
                },
                ],
                { cancelable: false }
            )
            
        }).catch((error) => {
            console.log(error);
            UltraAgentHelpers.showAlertWithOneButton("Oops!","Unable to save data!\nContact support.");
        });
        
       
    }

    shouldComponentUpdate(nextProps, nextState)
    {
        //console.log("RegisterUpload:shouldComponentUpdate");
        return true;
    }

    _onPressDeleteDocument = (aDocument) => {

        if (this.state.isUploading == true) {
            UltraAgentHelpers.showAlertWithOneButton("Delete not allowed","Please wait for upload to complete.\n");
            return;
        }

        if (((aDocument.getUrl() == null) || (aDocument.getUrl().length < 1)) &&
            (aDocument.getProgress() > 0) && (aDocument.getProgress() < 100) ) {
            UltraAgentHelpers.showAlertWithOneButton("Delete Not Allowed","Please wait for upload to complete.\n");
            return;
        }

        Alert.alert(
            "Are you sure?",
            "You are attempting to delete "+aDocument.getName()+" of size "+
                UltraAgentHelpers.formatBytesToDisplay(aDocument.getTotalBytes())+".\n" ,
            [
                //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                {text: 'No, Cancel', onPress: () => {/*cancelled*/}, style: 'cancel'},
                //{text: 'OK', onPress: () => console.log('OK Pressed')},*/
                {text: "Yes, Delete", style:'destructive', onPress: () => {
                    //Delete now
                    this.setState({spinner: true, spinnerMessage:'Deleting...'});
                    firebase.storage().ref('docs').child(aDocument.getUniqId()).delete()
                    .then(() => {

                        arr = [];
                        wDocuments = this.state.wDocuments;
                        for (var i=0; i < wDocuments.length; i++) {
                            if (wDocuments[i].getUniqId() == aDocument.getUniqId()) {
                                // do not save
                            } else {
                                arr.push(wDocuments[i]);
                            }
                        }
                        this.profileModelRef.setWDocuments(arr);
                        this.setState({wDocuments: arr});

                        this.profileModelRef.store().then(() => {
                            this.setState({spinner: false, spinnerMessage:''});
                            UltraAgentHelpers.showAlertWithOneButton("Deleted","Document has been successfully deleted.\nThank you.");
                        })
                        .catch((error) => {
                            console.log(error);
                            this.setState({spinner: false, spinnerMessage:''});
                            UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                        }); 
                    })
                    .catch((error) => {
                        console.log(error);
                        this.setState({spinner: false, spinnerMessage:''});
                        UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                    });

                }}
            ],
            { cancelable: false }
        );

        
    }

    renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: "100%",
              backgroundColor: "#CED0CE",
              marginLeft: "0%"
            }}
          />
        );
    };

  render() {
    return (
        <View style={styles.wrapper}>

            <Spinner
                visible={this.state.spinner}
                textContent={this.state.spinnerMessage}
                textStyle={{color: '#FFF'}}
            />
                <View style={{ flex:1, backgroundColor:'#fff'}} >
                    <ScrollView>

                        <View style={styles.logoContainer}>
                            <Text style={styles.iconText}>
                                {'Update Your Insurance Document\n Insurance No : 12345678KL'}
                            </Text>

                            <View style={styles.inputContainer}>
                                    {/* <Image source={require('../images/dbs-icon.png')} 
                                    style={styles.InputImageIconsize} /> */}
                                    <TextInput 
                                        style={{flex:1}} 
                                        underlineColorAndroid={'transparent'}
                                        placeholder="12345678KL" 
                                        placeholderTextColor = "#838383"
                                        keyboardType='default' 
                                        outoCorrect={false}
                                    />

                                </View>
                            <View style={[styles.inputContainer, styles.dateInputContainer]}>
                                    
                                <DatePicker
                                    style={{width: '100%', flex:1, height:30, bottom:5, paddingLeft:8}}
                                    date={this.state.date}
                                    mode="date"
                                    placeholder="Expiry Date"
                                    placeholderStyle={{ color: "#838383", fontSize:14, left:0 }}
                                    format="DD-MM-YYYY"
                                    // minDate="2016-05-01"
                                    // maxDate="2016-06-01"                               
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    showIcon={false}
                                    customStyles={{
                                        dateInput: {
                                            alignItems : 'flex-start',
                                            borderColor: 'transparent', 
                                            borderWidth: 0,
                                            paddingLeft: 0,
                                        },
                                        placeholderText: { 
                                            color: '#838383' 
                                        }
                                    }}
                                    onDateChange={(date) => {this.setState({date: date})}}
                                    ref={(ref)=>this.datePickerRef=ref}
                                />
                                    {/* <Image source={require('../images/datepicker-icon.png')} 
                                    style={[styles.InputImageIcon, styles.imageBg]} />    */}
                                    <TouchableHighlight onPress={() => this.datePickerRef.onPressDate()} 
                                        underlayColor='none'>
                                        <Image source={require('../images/datepicker-icon.png')} 
                                        style={[styles.InputImageIcon, styles.imageBg]}/>
                                    </TouchableHighlight>
                            </View>
                            <Image style={styles.logo} 
                            source={require('../images/down50x50.png')}/>
                            <TouchableHighlight onPress={this._onPressButtonBrowse} 
                                style={styles.browseButton}>
                                <Text style={{color:'#2EB2FF', alignItems: 'center', 
                                textAlign:'center', fontWeight:'bold'}}>
                                    BROWSE 
                                </Text>
                            </TouchableHighlight>
                        </View>

                            <View style={styles.uploadContainer}>
                            {/* <ScrollView> */}
                                <FlatList
                                    style={styles.listContentStyle}
                                    extraData={this.state} /*IMPORTANT: This line makes the byte counts update*/
                                    data={this.state.wDocuments}
                                    keyExtractor={(item, index) => index+""}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    renderItem={({ item }) => (
                                        <View style={[styles.rowContainer, styles.GrayBgColor]}>
                                            <View style={styles.textContainer}>
                                            <ProgressCircle
                                                percent={item.getProgress()}
                                                radius={16}
                                                borderWidth={4}
                                                color="#3399FF"
                                                shadowColor="#999"
                                                bgColor="#fff"
                                            >
                                                {/* <Text style={{ fontSize: 18 }}>{'30%'}</Text> */}
                                                <Image source={require('../images/pdf-icon1.png')} 
                                                style={styles.pdfImageIcon} />
                                            </ProgressCircle>
                                                {/* <Image source={require('../images/pdf-icon1.png')} 
                                                style={styles.pdfImageIcon} /> */}
                                                <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                                    <Text style={styles.textColor}>
                                                        { item.getName() }
                                                    </Text>
                                                    <Text style={styles.textColor}>
                                                        { UltraAgentHelpers.formatBytesToDisplay(item.getBytesTransferred()) } / { UltraAgentHelpers.formatBytesToDisplay(item.getTotalBytes()) }
                                                    </Text>
                                                </View>
                                            </View>
                                            <View style={styles.crossIconContainer}>
                                                <Icon style ={styles.crossIcon} type="FontAwesome" name="times-circle-o" 
                                                    style={{fontSize: 20, color: '#f20028'}} onPress={() => this._onPressDeleteDocument(item)}/>
                                            </View>
                                        </View>
                                    )}
                                />
                            {/* </ScrollView> */}
                                {/* <View style={[styles.rowContainer, styles.GrayBgColor]}>
                                    <View style={styles.textContainer}>
                                        <Image source={require('../images/pdf-icon1.png')} 
                                        style={styles.pdfImageIcon} />
                                        <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                            <Text style={styles.textColor}>
                                                CV.pdf1
                                            </Text>
                                            <Text style={styles.textColor}>
                                                1.36mb/1.35mb
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.crossIconContainer}>
                                        <Icon style ={styles.crossIcon} name="md-close-circle" 
                                            size={20} color="#f20028" />
                                    </View>
                                </View> */}
                                {/* <View style={[styles.rowContainer, styles.WhiteBgColor]}>
                                    <View style={styles.textContainer}>
                                        <Image source={require('../images/pdf-icon2.png')} 
                                        style={styles.pdfImageIcon} />
                                        <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                            <Text style={styles.textColor}>
                                                CV.pdf1
                                            </Text>
                                            <Text style={styles.textColor}>
                                                1.36mb/1.35mb
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.crossIconContainer}>
                                        <Icon style ={styles.crossIcon} name="md-close-circle" 
                                            size={20} color="#f20028" />
                                    </View>
                                </View> */}
                                {/* <View style={[styles.rowContainer, styles.GrayBgColor]}>
                                    <View style={styles.textContainer}>
                                        <Image source={require('../images/pdf-icon3.png')} 
                                        style={styles.pdfImageIcon} />
                                        <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                            <Text style={styles.textColor}>
                                                CV.pdf1
                                            </Text>
                                            <Text style={styles.textColor}>
                                                1.36mb/1.35mb
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.crossIconContainer}>
                                        <Icon style ={styles.crossIcon} name="md-close-circle" 
                                            size={20} color="#f20028" />
                                    </View>
                                </View> */}
                               
                                <TouchableHighlight 
                                style={styles.button}>
                                    <Text style={styles.btntext}>
                                        DONE 
                                    </Text>
                                </TouchableHighlight>
                               
                            </View>
                    </ScrollView>

                </View>

        </View>
    );
  }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container:{
        flex:1,
        alignItems: 'center',
        alignSelf:'stretch',
        backgroundColor:'#fff',
        width: null,
        height:null,
    },
    logoContainer:{
        alignItems: 'center',
        alignSelf:'stretch',
        backgroundColor:'#fff',
        marginTop:20,
        marginBottom:10,
        padding:15,
    },
 
    iconText:{
        color:'#000',
        textAlign:'center',
        alignItems: 'center',
        marginTop:10,
        marginBottom:30,
        lineHeight:20,
    },
    logo:{
        marginTop:20
    },
    browseButton:{
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#2EB2FF',
        backgroundColor:'transparent',
        padding:10,
        marginTop:20,
        width:150,
        alignItems: 'center',
    },
    uploadContainer:{
        alignItems: 'stretch',
        backgroundColor:'transparent',
        marginBottom:20,
        paddingBottom:10,
        paddingTop:10,

    },
    GrayBgColor:{
        backgroundColor:'#f5f5f5',
    },
    WhiteBgColor:{
        backgroundColor:'#fff',
    },
    rowContainer:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft:30,
        paddingRight:30,
        paddingTop:10,
        paddingBottom:10,
    },
    pdfImageIcon:{
        width:40,
        height:40
    },
    textContainer: {
        flex: 1, 
        flexDirection: 'row',
        alignSelf:'stretch',
    },
    textColor:{
        color:'#838383',
    },
    crossIconContainer:{
        alignItems: 'center',
        alignSelf:'stretch',
        justifyContent: 'center',
    },

    button:{
        alignSelf:'stretch',
        marginLeft:70,
        marginRight:70,
        marginTop:20,
        marginBottom:50,
        backgroundColor:'#5271FF',
        padding:12,
        borderRadius:3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btntext:{
        textAlign:'center',
        alignSelf:'stretch',
        alignItems: 'center',
        color:'#fff',
        fontWeight:'500',
        fontSize:14,
        letterSpacing:1,
        color:'#fff',
    },
    linkContainer:{
        alignSelf:'stretch',
        alignItems:'center',
        marginBottom:20,
        //marginTop:20,
        justifyContent:'flex-end',
        position:'absolute',
        bottom:0,
        left:0,
        right:0,
    },
    
    regText:{
        marginTop:5,
        color:'#000',
    },
    dateInputContainer:{
        padding:0,
    },
    inputContainer: {
        flex: 1, 
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'stretch',
        padding:8,
        backgroundColor:'#fff',
        borderRadius:2,
        marginBottom:12,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.1,
                height:40,
            },
            android: {
                elevation:2,
                height:40,
                paddingBottom:0,
                paddingTop:0,
            },
        })
    },
    dropdownPickerStyle:{
        ...Platform.select({
            ios: {
                width: Dimensions.get('window').width-55,
                left:-15,
            },
            android: {
                width: undefined, 
                left:-5,
            },
        })
    },
    InputImageIcon: {
        marginRight:10,
        marginLeft:5,
        height: 20,
        width: 20,
        resizeMode : 'stretch',
        alignItems: 'center'
    },
    imageBg:{
        backgroundColor:'#efefef',
        marginRight:0,
        padding:20,
        height: 10,
        width: 10,
    },
  
});