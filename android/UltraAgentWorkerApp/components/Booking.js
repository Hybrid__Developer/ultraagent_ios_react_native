import React, { Component } from 'react';
import {
  StyleSheet, View, Image, TouchableOpacity, Text, FlatList,
  
} from 'react-native';
import {
  Container, Header, Title, Body, Button,
  Left, Right, Picker, List, ListItem, Content, Badge
} from 'native-base';
import { Calendar, calendarTheme } from 'react-native-calendars'; 
import firebase from 'react-native-firebase';

import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

export default class Booking extends Component {

  dayWiseBookingsMap = {};
  shouldShowNoBookingFoundWarning = false;
  constructor(props) {
    super(props);

    shouldShowNoBookingFoundWarning = false;

    const aDate = new Date();
    const aDayKey = aDate.getFullYear()+'-'
      +("0" + (aDate.getMonth()+1)).slice(-2)+'-'
      +("0" + aDate.getDate()).slice(-2);

    this.state = {
        bookingsOnSelectedDay: [],
        selectedDayKey:'',
        markedDates:{},
        todayDate:aDayKey,
      };
    
    this.loggedinprofile = SingletonClass.get('loggedinprofile');
    this.unsubscribeScheduledBookingsCollection = null;

    this.onBookingsCollectionUpdate = this.onBookingsCollectionUpdate.bind(this);
    this.onDateSelectedByUser = this.onDateSelectedByUser.bind(this);
    this._onPressBookingShifts = this._onPressBookingShifts.bind(this);
    this._onPressFullSchedule = this._onPressFullSchedule.bind(this);
    //this.mergeShifts = this.mergeShifts.bind(this);
    }


  calculateEndTime = () => {
    const someDate = item.shiftStartDateTime;
    const dateAfterAdding = someDate.setHours(someDate.getHours() + item.shift.duration )
    //console.log('calculateEndTime',dateAfterAdding.toLocaleTimeString())
    return dateAfterAdding.toLocaleTimeString()
  }

  _onPressBookingShifts = (booking) => {
    this.props.navigation.navigate('BookingShifts',{'booking' : booking});
  }
  _onPressFullSchedule = () => {
    console.log('_onPressFullSchedule called');
    console.log(this.dayWiseBookingsMap);
    console.log(this.state.selectedDayKey);

    if ((this.dayWiseBookingsMap != undefined) && (this.state.selectedDayKey.length > 0)) {
      console.log('_onPressFullSchedule for Day='+this.state.selectedDayKey);
      const dateKeys = Object.keys(this.dayWiseBookingsMap);
      // updated booking might not have for last selected day, hence check nad update
      let found = false;
      for (const aDayKey of dateKeys) {
        if (aDayKey == this.state.selectedDayKey) {
          found = true; break;
        }
      }
      if (found == true) {
        this.props.navigation.navigate('BookingFullSchedule',{
          selectedDayKey: this.state.selectedDayKey,
          bookingsOnSelectedDay: this.dayWiseBookingsMap [this.state.selectedDayKey],
        });
      }
      
    }
  }

  static navigationOptions = {
    header:null
  }

  componentDidMount() {

    this.updateScheduledBookings();
  }

  componentWillUnmount() {
    this.unsubscribeScheduledBookingsCollection();
  }

  updateScheduledBookings() {
    let todayBeginning = new Date();
    todayBeginning.setHours(0,0,0,0);
    this.unsubscribeScheduledBookingsCollection = firebase.firestore().collection('bookings')
    // .where('workerId','==',this.loggedinprofile.userId)
    .where('shiftStartDateTime','>',todayBeginning)
    .onSnapshot(this.onBookingsCollectionUpdate) 

  }


  onBookingsCollectionUpdate = (querySnapshot) => {
    const someBookings = [];
    querySnapshot.forEach((doc) => {
      aObj = doc.data();
      aObj.id = doc.id;
      if ((aObj.workerId == this.loggedinprofile.userId) && 
        (aObj.status == 'active') && 
        ((aObj.workingStatus == undefined) || (aObj.workingStatus != 'completed'))) {
        someBookings.push(aObj);
      }
    });
  
    // Sort Booking by ascending order of date
    someBookings.sort((a,b) => {
      const aDate = a.shiftStartDateTime;
      const bDate = b.shiftStartDateTime;
      if (aDate.getTime() > bDate.getTime()) {
        return 1;
      } else if (aDate.getTime() < bDate.getTime()) {
        return -1;
      }
      return 0;
    });

    /* example
    {
      '2018-10-16': {startingDay: true, color: '#000000', textColor: '#fff',endingDay: true,},
      '2018-10-17': {color: '#5271FF', textColor: '#fff'},
      '2018-10-18': {marked: true, color: '#5271FF', textColor: '#fff',},
      '2018-10-19': {selected: true, color: '#5271FF', textColor: '#fff', },
      '2018-10-20': {selected: true, marked: true, color: '#5271FF', textColor: '#fff',},
      '2018-10-21': {selected: true, color: '#5271FF', textColor: '#fff', },
      '2018-10-22': {selected: true, marked: true, color: '#5271FF', textColor: '#fff',},
      '2018-10-23': {selected: true, marked: true, color: '#5271FF', textColor: '#fff', endingDay: true, },
    }
    */

    this.dayWiseBookingsMap = {};
    for (const aBooking of someBookings) {
      const aDate = aBooking.shiftStartDateTime;
      const aDayKey = aDate.getFullYear()+'-'+("0" + (aDate.getMonth()+1)).slice(-2)+'-'+("0" + aDate.getDate()).slice(-2);
      let arr = this.dayWiseBookingsMap[aDayKey];
      if (arr == undefined) {
        arr = []; 
      } 
      arr.push(aBooking);
      this.dayWiseBookingsMap[aDayKey] = arr;
    }

    console.log(this.dayWiseBookingsMap);

    const dateKeys = Object.keys(this.dayWiseBookingsMap);

    // updated booking might not have for last selected day, hence check nad update
    let found = false;
    for (const aDayKey of dateKeys) {
      if (aDayKey == this.state.selectedDayKey) {
        found = true; break;
      }
    }
    if (found == false) {
      this.state.selectedDayKey = ''; // reset
    }
    // nearest booking date
    if (this.state.selectedDayKey == '') {
      if (dateKeys.length > 0) {
        this.state.selectedDayKey = dateKeys[0]; 
      }
    }

    this.onDateSelectedByUser(this.state.selectedDayKey, true);

  }

  onDateSelectedByUser = (selectedDay) => {
    
    const dateKeys = Object.keys(this.dayWiseBookingsMap);
    found = false;
    for (const aDayKey of dateKeys) {
      if (aDayKey == selectedDay) {
        found = true; break;
      }
    }
    if (found == false) {
      if (this.shouldShowNoBookingFoundWarning == true) {
        UltraAgentHelpers.showAlertWithOneButton('','No bookings found for date '+selectedDay);
      }
    }

    

    let markedDates = {};
    for (const aDayKey of dateKeys) {
      if (aDayKey == selectedDay) {
        markedDates[aDayKey] = {selected:true, color: '#000000', textColor: '#fff',}
      } else {
        markedDates[aDayKey] = {marked: true, color: '#5271FF', textColor: '#fff',}
      }
    }

    let bookingsOnSelectedDay = this.dayWiseBookingsMap [selectedDay];
  
    this.setState({ 
      markedDates: markedDates,
      bookingsOnSelectedDay: bookingsOnSelectedDay,
      selectedDayKey:selectedDay,
    });
    
  }
  
  



  render() {

    return (
      <Container>
        <Header style={{backgroundColor:'#5271FF'}}>
         
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Image source={require('../images/hamburgericon.png')} style={{height:32, width:32,}} />
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'Bookings'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
         
       </Header>
      
        <Content>
        <Text style={styles.calendarTitle}>My Booked dates</Text>

        <View style={{ paddingTop: 10, flex: 1 }}>

          <Calendar
            current={this.state.selectedDayKey}
            minDate={this.state.todayDate}
            monthFormat={'MMMM yyyy'}
            hideExtraDays={true}
            firstDay={1}
            markingType={'period'}
            markedDates={this.state.markedDates}
            disableMonthChange={true}
            onDayPress={(day) => {
              console.log('selected day', day);
              this.shouldShowNoBookingFoundWarning = true;
              this.onDateSelectedByUser(day.dateString);
              this.shouldShowNoBookingFoundWarning = false;
            }}
            onDayLongPress={(day) => {
              console.log('selected day long press', day);
            }}
          theme={{
            ...calendarTheme,
            backgroundColor: '#ffffff',
            selectedDayBackgroundColor: '#000000',
            calendarBackground: '#FBFCFF',
            textSectionTitleColor: '#5271FF',
            todayTextColor: '#5271FF',
            dayTextColor: '#555',
            arrowColor: '#5271FF',
            monthTextColor: '#5271FF',
            textMonthFontWeight: 'bold',
            textSectionTitleFontWeight: 'bold',
            textDayFontSize: 16,
            textMonthFontSize: 16,
            textDayHeaderFontSize: 16
          }}
          />
        </View>
        <View style={styles.calendarBodyText}>
          <FlatList
          keyExtractor={(item, index) => item.id}
          data={this.state.bookingsOnSelectedDay}
          renderItem={({ item }) => (
            <ListItem button onPress={() => this._onPressBookingShifts(item)} style={{borderBottomColor:'transparent'}}>
              <View style={styles.bookingListStyle}>
                <View>
                  <Text style={styles.bookingBadgeStyle}>{item.shiftStartDateTime.getHours()}:{("0" + item.shiftStartDateTime.getMinutes()).slice(-2)} - {item.duration}</Text>
                </View>
                <View style={styles.rightBookingText}>
                  <Text style={styles.bookingidText}>{item.shiftSubCategory} #{item.shiftNo}</Text>
                  <Text style={styles.bookingLocationStyle}>
                    <Text style={styles.locationGrayText}>Location: </Text>
                    <Text style={styles.locationBlackText}>{item.address == undefined?'':item.address}</Text>
                  </Text>
                </View>
              </View>
              <View style={styles.wrappingContainer}></View>
            </ListItem>
            )}
          />
            <TouchableOpacity style={styles.buttonStyle} 
                activeOpacity={0.7} onPress={() => {this._onPressFullSchedule();}}>
                <Text style={styles.btntext}>FULL SCHEDULE</Text>
            </TouchableOpacity>
        </View>
        </Content>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
   
    HeaderTitle:{
      color:'#fff',
      textAlign:'center',
      alignItems:'center',
    },
    calendarTitle:{
      color:'#000',
      textAlign:'center',
      alignItems:'center',
      fontWeight:'bold',
      paddingTop:20,
      fontSize:16,
    },
    bookingBadgeStyle:{
      borderColor:'#5271FF',
      borderWidth:2,
      borderRadius:4,
      textAlign: 'center',
      textAlignVertical: 'center',
      padding:5,
    },
    calendarBodyText:{
      // paddingTop:10,
      // paddingBottom:10,
      padding:10,
    },
    bookingListStyle:{
      flexDirection: 'row',
      flex:1,
      marginBottom:20,
      
    },
    wrappingContainer:{
      flex:0.5, 
      flexDirection: 'row',
    },
   
    bookingLocationStyle:{
      // flexDirection: 'row',
      // flex:1,
      // flexWrap: 'wrap'
    },
    locationBlackText:{
      
    },
    rightBookingText:{
      marginLeft:10,
     // paddingRight:10,
    },
    bookingidText:{
      color:'#5271FF',
      fontWeight:'bold',
    },

    locationGrayText:{
      color:'#ccc',
    },
    buttonStyle:{
      alignSelf:'stretch',
      marginTop:10,
      backgroundColor:'#5271FF',
      alignItems:'center',
      padding:10,
      marginLeft:80,
      marginRight:80,
      marginBottom:20,
      borderRadius:3,
    },
    btntext:{
        color:'#fff',
    },
})
