import React, { Component } from 'react';
import {
  StyleSheet, View, Image, ImageBackground, Text, 
  Dimensions, Switch, ListItem
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, List, Accordion, Content, Header, Body
} from 'native-base';
import Icon from 'react-native-ionicons';

const dataArray = [
  { title: "How to use the App?", 
    content: 
    '\u2022'+" "+" "+'Available shifts – browse for available shifts in shifts tab within your' + 
    'working hours and apply for them. Employer will select from interested workers' + 
    'who have applied for the shift.\n '+
    '\u2022'+" "+" "+'Booking –Track your shift bookings in bookings tab.'+ 
    'Worker can view shift details including navigation path to the work place.\n ' +
    '\u2022'+" "+" "+'Start shift – when on site you are supposed to start the shift on the app before' + 
    'you start working. In case of a break you should pause the time tracker on the app.\n ' +
    '\u2022'+" "+" "+'Complete shift – When the work is done click “complete button” in the app and the' + 
    'timesheet will be submitted to Employer and Ultra Agent Control.\n' +
    '\u2022'+" "+" "+'Timesheet – Track all the shifts you submitted in timesheet tab\n' +
    '\u2022'+" "+" "+'Profile– click profile icon and build your profile and also update your working hours '
  },

  { title: 'Getting Started with Ultra Agent \n' +" "+'Worker App', 
    content: 
    'Ultra Agent worker app is a free-to-download mobile app for ad-hoc workers.\n'+
    'Steps\n'+
    '\u2022 '+' Download Ultra Agent Worker App in Google Play Store for android or in Apple Store for iOS.\n'+
    '\u2022 '+' Register by adding your personal information, profession details and uploading documents.\n'+
    '\u2022 '+' Finish registration process by setting your profile picture and password.\n'+
    '\u2022 '+' Log in and enjoy the app.' 
  },

  { title: "Shifts Disputes", 
    content: 
    'Employer can amend a submitted timesheet by worker, this will require worker' + 
    'approval on any amendments on the shift. A worker can accept amendments or' + 
    'escalate amendments.\n '+'\n'+
    'If a worker escalates amendments, then the shifts will be in dispute.'+ 
    'Ultra Agent control will intervene and resolve the dispute '
  },

  { title: "Shifts Payments", 
    content: 
    'All shifts will be paid for within one week of its completions.' + 
    'Note all payments will be done by Ultra Agent Control outside the app.' + 
    'For any payment issue contact Ultra Agent Control through the provided contact information.'
  },

  
];

export default class Help extends Component {

  constructor(props) {
    super(props);
    this.state = {
    
    };
  }

  
  render() {
    let { height, width } = Dimensions.get('window');
    return (
      <ImageBackground
      source={require('../images/timesheetbg.png')}
      style={{ width, height }}>
        <Header style={{backgroundColor:'#5271FF', borderBottomColor:'transparent'}}>
        
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name="arrow-back" color='#fff' />         
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'help'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
        
        </Header>

        <Content padder>
          <View style={styles.helpListContainer}>
            <Accordion dataArray={dataArray}
            headerStyle={{ backgroundColor: "#f4f4f4", 
            borderColor:'transparent', borderWidth:0, marginTop:10,
            padding:15, borderRadius:2,}}
            contentStyle={{ backgroundColor: "#fafafa", fontSize:15 }}></Accordion>
          </View>
          
        </Content>
     </ImageBackground>
     );
    }
  }
  const styles = StyleSheet.create({
    HeaderTitle:{
      color:'#fff',
    },
    SubHeaderTitle:{
      color:'#fff',
      fontSize:16,
      padding:5,
      paddingTop:6,
    },
    helpListContainer:{
      padding:10,
    },
   
})