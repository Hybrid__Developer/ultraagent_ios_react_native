import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Platform, Text
  
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, List, Content, Textarea
} from 'native-base';
import Icon  from 'react-native-ionicons';
import CountDown from 'react-native-countdown-component';
import moment from 'moment';
import { CheckBox } from 'react-native-elements';
import firebase from 'react-native-firebase';

import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

console.disableYellowBox = true;
console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

export default class BookingCancel extends Component {

    loggedInProfile=null;
    constructor(props) {
        super(props);

        booking = 
            this.props.navigation.state.params.booking;
            console.log('BookingShifts:'+booking);

        this.state = {
            booking:booking,
            emergIssueCheck:false,
            techReasonCheck:false,
            timeCheck:false,
            cancelBrief:''
        };

        this._onPressCancelBooking = this._onPressCancelBooking.bind(this);

        this.loggedInProfile = SingletonClass.get('loggedinprofile');
    }


    static navigationOptions = {
        // header:null
        headerTitle: 'CANCEL BOOKING',
        headerStyle: {
            backgroundColor: '#5271FF',
        },
        headerBackTitle: null,
        headerTitleStyle: {
        color: "#fff",
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTintColor: '#fff',
    }
  
    _onPressCancelBooking = () => {
        if ((this.state.emergIssueCheck == false) && 
            (this.state.techReasonCheck == false) && 
            (this.state.timeCheck == false)) {
            UltraAgentHelpers.showAlertWithOneButton("Reason",'Please select a valid reason from options.');
            return;
        }

        if ((this.state.cancelBrief == undefined) || (this.state.cancelBrief.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("Brief",'Please enter a brief.');
            return;        
        }

        let cancelReasone='';
        if (this.state.emergIssueCheck == true) {
            cancelReasone = 'Emergency issue';
        } else if (this.state.techReasonCheck == true) {
            cancelReasone = 'Personal / Technical reasons';
        } else if (this.state.timeCheck == true) {
            cancelReasone = 'Time coincide with my office time';
        }

        firebase.firestore().collection('bookings').doc(this.state.booking.id).set(
            {
                status:'cancelled',
                cancelledBy:this.loggedInProfile.userId,
                cancelReason:cancelReasone,
                cancelBrief:this.state.cancelBrief,
                cancelledAt:firebase.firestore.FieldValue.serverTimestamp()
            },
            {merge: true}
        )
        .then(() => {
            UltraAgentHelpers.showAlertWithOneButton('Cancelled','Booking has been successfully cancelled.');
            this.props.navigation.popToTop();
        })
        .catch((err) => {
            console.log(err.message);
            UltraAgentHelpers.showAlertWithOneButton('Error',err.message);
        });
    }

  render() {

    return (
      <Container>
        <Content>
          <View style={styles.cancelBookingContainer}>
                <View style={styles.headerTextStyle}>
                    <Text style={styles.topTextStyle}>
                        Shift No: {this.state.booking.shiftSubCategory} #{this.state.booking.shiftNo}
                    </Text>
                    <Text style={styles.secondTopTextStyle}>
                        Select reason for cancelling shift
                    </Text>
                </View>
                <View style={styles.checkboxTextStyle}>
                    <CheckBox
                        title='Emergency issue'
                        containerStyle={{ marginLeft: 0, marginRight:0, marginTop:0,
                            paddingLeft:0, paddingRight:0, marginBottom:0, borderRadius:0, 
                            backgroundColor:'#efefef', borderColor:'transparent',
                            borderWidth:0,
                        }} 
                        checkedColor='#000'
                        uncheckedColor='#000'
                        size={20}
                        checked={this.state.emergIssueCheck}
                        onPress={() => 
                            this.setState({
                                emergIssueCheck: !this.state.emergIssueCheck,
                                techReasonCheck: false,
                                timeCheck:false,
                            })
                        }
                    />
                    <CheckBox
                        title='Personal / Technical reasons'
                        containerStyle={{ marginLeft: 0, marginRight:0, marginTop:0,
                            paddingLeft:0, paddingRight:0, marginBottom:0, borderRadius:0, 
                            backgroundColor:'#fff', borderColor:'transparent',
                            borderWidth:0,
                        }} 
                        checkedColor='#000'
                        uncheckedColor='#000'
                        size={20}
                        checked={this.state.techReasonCheck}
                        onPress={() => this.setState({
                            emergIssueCheck:false,
                            techReasonCheck: !this.state.techReasonCheck,
                            timeCheck:false
                        })}
                    />
                    <CheckBox
                        title='Time coincide with my office time'
                        containerStyle={{ marginLeft: 0, marginRight:0, marginTop:0,
                            paddingLeft:0, paddingRight:0, marginBottom:0, borderRadius:0, 
                            backgroundColor:'#efefef', borderColor:'transparent',
                            borderWidth:0,
                        }}  
                        checkedColor='#000'
                        uncheckedColor='#000'
                        size={20}
                        checked={this.state.timeCheck}
                        onPress={() => this.setState({
                            emergIssueCheck:false,
                            techReasonCheck: false,
                            timeCheck: !this.state.timeCheck
                        })}
                    />
                </View>
                <View style={styles.briefInputContainer}>
                    <Label style={styles.labelStyle}>Brief Explanation</Label>
                    <Textarea rowSpan={4} 
                    style={styles.briefTextareaContainer} 
                    onChangeText={(cancelBrief) => this.setState({ cancelBrief })}
                    value={this.state.cancelBrief}/>
                </View>
                   
                <View style={styles.paragraphText}>
                    <Text style={styles.textStyle}>
                        Please note bookings cannot be cancelled within 24 hours of shift 
                        should the shift have been confirmed for at least 24 hours.
                    </Text>
                    <Text style={styles.textStyle}>
                        Bookings cancelled within 24 hours will be subject to a 50% charge.
                    </Text>
                </View>
                <View style={styles.buttonConatiner}>
                    <Button iconLeft style={[styles.btnOrange]} 
                    onPress={this._onPressCancelBooking}>
                        <Text style={styles.btnText}>{'Cancel booking'.toUpperCase()}</Text>
                    </Button>
                </View>
          </View>
        </Content>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
  
    cancelBookingContainer:{
        padding:30,
       // alignItems:'center',
       // alignSelf:'center',
    },
    headerTextStyle:{
        paddingLeft:10,
        paddingRight:10,
    },
    topTextStyle:{
        fontSize:18,
        color:'#000',
        fontWeight:'800',
        textAlign:'center',
        marginTop:10,
    },
    secondTopTextStyle:{
        fontSize:16,
        color:'#000',
        fontWeight:'700',
        textAlign:'center',
        marginTop:5,
    },
    checkboxTextStyle:{
       borderColor:'#ccc',
       borderWidth:2,
       marginTop:30,
       marginBottom:30,
       marginLeft:10,
       marginRight:10,
    },
    briefInputContainer:{
        marginBottom:20,
        paddingLeft:10,
        paddingRight:10
    },
    briefTextareaContainer:{
        backgroundColor:'#fff',
        borderRadius:3,
        borderColor:"#efefef",
        borderWidth:2,
        ...Platform.select({
            ios: {
                shadowColor: '#efefef',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.4,
            },
            android: {
                elevation:2,
            },
        })
    },
    labelStyle:{
        color:'#000',
        marginBottom:3,
        fontWeight:'bold',
    },
    textStyle:{
        paddingTop:10,
        color:'#f80808'
    },
    buttonConatiner:{
      flex:1,
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
      marginTop:50,
    },
    btnOrange:{
      backgroundColor:'#df8c40',
      paddingTop:10,
      paddingBottom:10,
      paddingLeft:30,
      paddingRight:30,
      borderRadius:3,
    },
    btnText:{
        color:'#fff', 
        marginLeft:8, 
        lineHeight:18
    }
})