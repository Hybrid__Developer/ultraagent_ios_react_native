import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, 
   Image, Button, ImageBackground, TouchableHighlight, Linking} from 'react-native';

export default class Landing extends Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = {
        header:null
    }

    _onPressButton() {
        Alert.alert('You tapped the button!')
    }
    _onPressButtonLogin = () => {
        this.props.navigation.navigate('Login');
      }
    _onPressButtonRegister = () =>{
      this.props.navigation.navigate('Register');
    }
    _onLookingForEmployer = () =>{
      Linking
      .openURL('https://demo3791x5.ultraagents.co.uk/')
      .catch(err => console.error('An error occurred', err));
    }

  render() {
    return (
     
      <View style={styles.wrapper}>
        <ImageBackground style={styles.container} 
        source={require('../images/landingbg.jpg')} imageStyle={{ resizeMode: 'cover' }}>
            <View style={styles.logoContainer}>
                <Image style={styles.logo} 
                source={require('../images/worker_logo.png')}/>
                <View style={styles.buttonContainer}>
                  <TouchableOpacity
                      style={[styles.buttonStyle, styles.btnlogin]}
                      onPress={this._onPressButtonLogin}
                      activeOpacity={0.7} 
                      >
                      <Text style={styles.btntext}> LOGIN </Text>
                  </TouchableOpacity>

                     <View style={styles.horizentalline}>
                        <Text style={styles.linetext}>or</Text>
                     </View>

                  <TouchableOpacity
                      style={[styles.buttonStyle, styles.btnreg]}
                      onPress={this._onPressButtonRegister}
                      activeOpacity={0.7} 
                      >
                      <Text style={styles.btntext}> REGISTER NOW </Text>
                  </TouchableOpacity>
                </View>
            </View>
            <TouchableHighlight onPress={this._onLookingForEmployer} 
            underlayColor='transparent'>
              <View style={styles.footerText}>
                <Text style={styles.bottomtext}>Looking for Employer App?</Text>
              </View>
            </TouchableHighlight>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container:{
    flex:1,
    width:null,
    alignSelf:'stretch',
    height:null,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoContainer:{
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,
  },
  logo:{
    position:'relative',
    bottom:50,
    width:200,
    height:200,
  },
  buttonContainer:{
    width:200,
    marginTop:30,
  },
 
  btnlogin:{
    backgroundColor:'#5271FF',
  },
  btnreg:{
    backgroundColor:'#2EB2FF',
  },

  buttonStyle:{
    alignSelf:'stretch',
    alignItems:'center',
    padding:12,
    borderRadius:3,
    width:200,
  },
  btntext:{
    textAlign:'center',
    alignSelf:'stretch',
    alignItems: 'center',
    color:'#fff',
    fontWeight:'500',
    fontSize:14,
    letterSpacing:1,
  },

  horizentalline:{
    margin:12,
   // borderBottomColor: '#bbb',
   // borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  linetext:{
    textAlign:'center',
    color:'#999',
  },
  footerText:{
    //flex: 1,
    justifyContent: 'flex-end',
  },
  bottomtext:{
    paddingBottom:30,
    color:'#fff',
  },
  
  
});
