import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Text, FlatList,
  
} from 'react-native';
import {
  Container, Header, Title, Body, Button,
  Left, Right, Picker, List, Content, Badge, ListItem
} from 'native-base';
import Icon  from 'react-native-ionicons';

import firebase, { RNFirebase } from 'react-native-firebase';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

export default class PendingShifts extends Component {

  constructor(props) {
    super(props);

    this.loggedinprofile = SingletonClass.get('loggedinprofile');

    this.state = {
       bookings: []
    };
  }

  componentDidMount() {
    this.updateBookings();
  }

  componentWillUnmount() {
    this.unsubscribeBookingsCollection();
  }

  updateBookings() {
    
    this.unsubscribeBookingsCollection = firebase.firestore().collection('bookings')
    .where('workerId','==',this.loggedinprofile.userId)
    .where('status','==','active')
    .onSnapshot(this.onBookingsCollectionUpdate) 

  }

  onBookingsCollectionUpdate = (querySnapshot) => {
    const someBookings = [];
    let todayBeginning = new Date();
    todayBeginning.setHours(0,0,0,0);
    querySnapshot.forEach((doc) => {
      aObj = doc.data();
      aObj.id = doc.id;
      if (aObj.shiftStartDateTime.getTime() > todayBeginning.getTime()) {
          if ((aObj.timesheetStatus == undefined) || (aObj.timesheetStatus == 'pending')) {
             // pending 
             someBookings.push(aObj);
          }
      }
    });
  
    // Sort Booking by ascending order of date
    someBookings.sort((a,b) => {
      const aDate = a.shiftStartDateTime;
      const bDate = b.shiftStartDateTime;
      if (aDate.getTime() > bDate.getTime()) {
        return 1;
      } else if (aDate.getTime() < bDate.getTime()) {
        return -1;
      }
      return 0;
    });

    this.setState({bookings: someBookings});

    console.log(this.state.bookings);
  }


  
static navigationOptions = {
    // header:null
    headerTitle: 'PENDING SHIFTS',
    headerStyle: {
      backgroundColor: '#5271FF',
    },
    headerTitleStyle: {
      color: "#fff",
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
   },
   headerTintColor: '#fff',
 }

  render() {

    return (
        <Container>
            <Content>
                <View style={styles.pendingShiftContainer}>
                    <FlatList 
                    keyExtractor={(item, index) => item.id}
                    data={this.state.bookings}
                    renderItem={({ item }) => (
                        <ListItem>
                            <Left>
                                <View style={styles.styleLeftSatus}>
                                    <View style={styles.shiftTextStyle}>
                                        <Text style={styles.jobIdText}>{item.shiftSubCategory} #{item.shiftNo}</Text>
                                    </View>
                                    <View style={styles.shiftlocationTextStyle}>
                                        <Text style={[styles.locationLeftTextStyle, styles.marginRightStyle]}>Location: </Text>
                                        <Text style={styles.locationRightTextStyle}>{item.address}</Text>
                                    </View>
                                    <View style={styles.startTimeTextStyle}>
                                        <Text style={[styles.startTextStyle, styles.marginRightStyle]}>
                                            Time Started: 
                                        </Text>
                                        <Text style={styles.startTextStyle}>
                                        {item.workStartTime != undefined ? item.workStartTime.getHours() + ':':''}
                                        {item.workStartTime != undefined ? ("0" + item.workStartTime.getMinutes()).slice(-2):''}
                                        </Text>
                                    </View>
                                    <View style={styles.completeTimeTextStyle}>
                                        <Text style={[styles.completeTextStyle, styles.marginRightStyle]}>
                                            Completion Time: 
                                        </Text>
                                        <Text style={styles.completeTextStyle}>
                                        {item.workEndTime != undefined ? item.workEndTime.getHours() + ':':''}
                                        {item.workEndTime != undefined ? ("0" + item.workEndTime.getMinutes()).slice(-2):''}
                                        </Text>
                                    </View>
                                    <View style={styles.shiftBadgeStyle}>
                                        <Badge style={styles.badgeStyle}>
                                        <Text style={styles.badgeText}>£{item.fee}/H</Text>
                                        </Badge>
                                    </View>
                                </View>
                            </Left> 
                            <Right></Right>
                            <View style={styles.styleRightSatus}>
                                <View style={styles.rightTimeStyle}>
                                    <Text style={[styles.timeTextStyle, styles.marginRightStyle]}>
                                        <Icon name="md-clock" size={15} color="#888"/>
                                        {item.shiftStartDateTime.getHours()}:{("0" + item.shiftStartDateTime.getMinutes()).slice(-2)}
                                    </Text>
                                    <Text style={styles.dateTextStyle}>
                                        <Icon name="md-calendar" size={15} color="#888"/>
                                        {item.shiftStartDateTime.getDate()}/{item.shiftStartDateTime.getMonth()+1}/{item.shiftStartDateTime.getFullYear()}</Text>
                                </View>
                                <View style={styles.rightStatusStyle}>
                                    <Text style={styles.statusTextStyle}>
                                       status
                                    </Text>
                                    <Text style={styles.statusBtnTextStyle}>
                                        {'pending'.toUpperCase()}
                                    </Text>
                                </View>
                            </View> 
                        </ListItem>
                    )}
                    />
                </View>
            </Content>
        </Container>
     );
    }
  }
  const styles = StyleSheet.create({
    pendingShiftContainer:{
        paddingTop:10,
        paddingBottom:20,
    },
    jobIdText:{
      fontWeight:'bold',
      color:'#5271ff',
      fontSize:18,
    },
   
    shiftlocationTextStyle:{
      flex:1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginTop:6,
      marginBottom:6,
    },
    startTimeTextStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    completeTimeTextStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    locationLeftTextStyle:{
        color:'#a9a9a9',
        fontWeight:'bold',
    },
    locationRightTextStyle:{
      fontWeight:'bold',
      color:'#303030',
    },
    marginRightStyle:{
      marginRight:5,
    },
    startTextStyle:{
        fontWeight:'bold',
        color:'#03bc07',
    },
    completeTextStyle:{
        fontWeight:'bold',
        color:'#f20028',
    },
    badgeStyle:{
      backgroundColor:'transparent', 
      borderColor:'#df8c40', 
      borderRadius:4, 
      borderWidth:1,
      marginTop:5,
    },
    badgeText:{
      color:'#df8c40',
      paddingLeft:4, 
      paddingRight:4, 
      lineHeight:21,
      textAlign:'center',
      alignItems:'center'
    },
    shiftBadgeStyle:{
      flex:1,
    },
    rightTimeStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    statusTextStyle:{
        color:'#000',
        fontWeight:'bold'
    },
    statusBtnTextStyle:{
        paddingLeft:8,
        paddingRight:8,
        backgroundColor:'#f20028',
        color:'#fff',
        paddingBottom:3,
        textAlign:'center',
        alignItems:'center',
    },
    rightStatusStyle:{
        marginTop:30,
    },
    styleRightSatus:{
        position:'absolute',
        right:10,
        top:15,
        width:100,
        marginLeft:20,
    },
    styleLeftSatus:{
        paddingRight:40,
    }

})
