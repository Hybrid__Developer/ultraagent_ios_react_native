import React, { Component } from 'react';
import { StyleSheet, Text, View, 
   Image, TextInput, TouchableOpacity, ImageBackground, 
   ScrollView, Platform} from 'react-native';

import KeyboardWrapperView from '../helpers/KeyboardWrapperView';

export default class PasswordChanged extends Component {
    constructor(props) {
        super(props);
    }

    static navigationOptions = {
       // header:null
       headerTitle: 'PASSWORD CHANGED',
       headerStyle: {
        backgroundColor: '#5271FF',
      },
      headerTitleStyle: {
        color: "#fff",
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      headerTintColor: '#fff',
      headerLeft: null,
      gesturesEnabled: false,
    }

  
    componentDidMount() {
      setTimeout(() => {
        this.props.navigation.navigate('Landing');
      }, 6000);
    }

  render() {
    return (
        <View style={styles.wrapper}>
                <ImageBackground style={styles.container} 
                source={require('../images/confirmpass-bg.png')} imageStyle={{ resizeMode: 'cover' }}>
                    <View style={styles.logoContainer}>
                        <Image style={styles.logo} 
                        source={require('../images/success-icon.png')}/>
                        <Text style={styles.passwordText}>
                            Password Reset                                
                        </Text>
                        <Text style={styles.passSuccessText}>
                            {'successful'.toUpperCase()}                              
                        </Text>
                    </View>
                </ImageBackground>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container:{
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf:'stretch',
    width:null,
  },
  logoContainer:{
    alignItems: 'center',
    alignSelf:'stretch',
  },
  logo:{
    width:120,
    height:120,
  },
  passwordText:{
    marginTop:20,
    color:'#2e2e2e',
    fontSize:16,
  },
  passSuccessText:{
    fontSize:28,
    color:'#6ac259',
    fontWeight:'bold',
    marginTop:3,
  },

});
