import React, { Component } from 'react';
import { StyleSheet, Text, View, 
   Image, TextInput, TouchableOpacity, ImageBackground, 
   ScrollView, Platform, AsyncStorage, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
import KeyboardWrapperView from '../helpers/KeyboardWrapperView';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

import firebase from 'react-native-firebase';

export default class Login extends Component {

    state = { email: '', password: '', errorMessage: null }

    constructor(props) {
        super(props);
    }

    static navigationOptions = {
       // header:null
       headerTitle: 'LOGIN',
       headerStyle: {
        backgroundColor: '#5271FF',
      },
      headerBackTitle: null,
      headerTitleStyle: {
        color: "#fff",
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      headerTintColor: '#fff',

    }

    _onPressButtonForgot = () => {
        this.props.navigation.navigate('ForgotPassword');
      }
    _onPressButtonRegister = () => {
        this.props.navigation.navigate('Register');
      }
    _onPressButtonLogin = async () =>{
        //await AsyncStorage.setItem('userToken', 'abc');
        //this.props.navigation.navigate('App');
        if ((this.state.email < 1) && (this.state.password.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("Oops",
            "Please enter a valid email and password.");
            return;
        }
        if ((this.state.email == undefined) || (this.state.email.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("Email not specified",
            "Please enter a valid email");
            return;
        }
        if ((this.state.password == undefined) || (this.state.password.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("Password not specified",
            "Please enter a valid password");
            return;
        }
        // if ((!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.state.password))) {
        //   UltraAgentHelpers.showAlertWithOneButton('Invalid', "Password should be Minimum 8 characters, at least one uppercase letter, one lowercase letter, one digit and one special character.");
        //   return;
        // }

        firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(() => {
            // Do not navigate to app here:
            // SPlash is listeneing to auth notifications. Hence it will handle.
            //this.props.navigation.navigate('App');
        })
        .catch(error => {
            console.log(error);
            this.setState({ errorMessage: error.message });

            if ((error.code != undefined) && (error.code == "auth/wrong-password")) {
              UltraAgentHelpers.showAlertWithOneButton("Oops!",'The username or password you entered is incorrect');
            } else if ((error.code != undefined) && (error.code == "auth/user-not-found")) {
              UltraAgentHelpers.showAlertWithOneButton("Oops!",'There is no record corresponding to this username. Please contact your administrator to request access');
            } else {
              UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
            }

        });
      }

  render() {

    return (
           
        <KeyboardWrapperView behavior="padding" style={styles.wrapper}>     
           <ImageBackground source={require('../images/registerbg.jpg')} 
           style={styles.backgroundImage}>
                <View style={styles.logoContainer}>
                    <Image style={styles.logo} 
                    source={require('../images/logo.png')}/>
                </View>

                <View style={styles.loginformContainer}>
                
                    <View style={[styles.inputContainer, styles.inputBorder]}>
                        <Image source={require('../images/mailicon.png')} 
                        style={styles.InputImageIcon} />

                        <TextInput 
                            style={{flex:1}} 
                            underlineColorAndroid={'transparent'}
                            placeholder="Email / User Name" 
                            placeholderTextColor = "#838383"
                            keyboardType='email-address' 
                            autoCapitalize = 'none'
                            outoCorrect={false}
                            onChangeText={email => this.setState({ email })}
                            value={this.state.email}
                        />

                    </View>
                    
                    <View style={[styles.inputContainer, styles.notopradius]}>
                        <Image source={require('../images/lockicon.png')} 
                        style={styles.InputImageIcon} />

                        <TextInput 
                            style={{flex:1}} 
                            underlineColorAndroid={'transparent'}
                            placeholder="Password" 
                            placeholderTextColor = "#838383"
                            autoCapitalize = 'none'
                            secureTextEntry={true}  
                            autoCorrect={false}
                            onChangeText={password => this.setState({ password })}
                            value={this.state.password}
                        />
                    
                    </View>
                    
                    {/* <Text style={{ fontSize: 20}}>Height is {window.height}</Text> */}

                    <TouchableOpacity style={styles.button} onPress={this._onPressButtonLogin}>
                        <Text style={styles.btntext}>LOGIN</Text>
                    </TouchableOpacity>
                    <View style={styles.linkContainer}>
                        <Text style={styles.forgotText} onPress={this._onPressButtonForgot}>Forgot Password?</Text>
                        <Text style={styles.regText}>
                            Don't have an account ? 
                            <Text style={{fontWeight: 'bold', textDecorationLine: 'underline'}} 
                            onPress={this._onPressButtonRegister}>
                                REGISTER NOW
                            </Text>
                        </Text>
                        <Text style={styles.loggingText}>Having trouble logging in?</Text>
                        {/* <Text style={styles.helpText}>GET HELP</Text> */}
                    </View>
                </View>
            </ImageBackground>
       
        </KeyboardWrapperView>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container:{
    // flex:1,
    // alignItems: 'center',
    // //justifyContent: 'center',
    // alignSelf:'stretch',
    width:width,
    height:height,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
},
  logoContainer:{
    alignItems: 'center',
    alignSelf:'stretch',
    //marginTop:30,
    justifyContent: 'center',
    flexGrow:1,
    marginTop:10,
  },
  logo:{
    width:150,
    height:150,
  },
//   loginformContainer:{
//     alignSelf:'stretch',
//     paddingLeft:20,
//     paddingRight:20,
//     ...Platform.select({
//         ios: {
//             marginTop:50,
//         },
//         android: {
//             marginTop:70,
//         },
//     })
//   },
  loginformContainer:{
    alignSelf:'stretch',
    paddingLeft:20,
    paddingRight:20,
    //marginTop:20,
    marginTop:10,
    // maxHeight:150,
    // marginBottom:10,
    // justifyContent: 'center',
    // flex:2,
  },

  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf:'stretch',
    padding:10,
    backgroundColor:'#fff',
    marginLeft:40,
    marginRight:40,
    ...Platform.select({
        ios: {
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.3,
        },
        android: {
            elevation:2,
            height:40,
            paddingTop:0,
            paddingBottom:0,
        },
    })
},
InputImageIcon: {
    marginRight:8,
    height: 20,
    width: 20,
    resizeMode : 'stretch',
    alignItems: 'center'
},
  
  inputBorder:{
    borderBottomColor: '#bbb',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderTopLeftRadius:2,
    borderTopRightRadius:2,
  },
  notopradius:{
    borderBottomLeftRadius:2,
    borderBottomRightRadius:2,
  },
  button:{
    alignSelf:'stretch',
    marginTop:20,
    backgroundColor:'#5271FF',
    alignItems:'center',
    padding:10,
    borderRadius:3,
    marginLeft:40,
    marginRight:40
  },
  btntext:{
      color:'#fff',
  },
  linkContainer:{
    alignSelf:'stretch',
    alignItems:'center',
    // position:'absolute',
    // bottom:0,
    // left:0,
    // right:0,
    marginBottom:20,
    marginTop:40,
  },
  forgotText:{
    color:'#5271FF',
  },
  regText:{
    marginTop:70,
    //marginTop:50,
    color:'#fff',
    marginBottom:10,
  },
  loggingText:{
    marginTop:10,
    color:'#fff',
  },
  helpText:{
    color:'#fff',
    fontWeight:'bold',
    textDecorationLine: 'underline',
  },

});
