import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Alert, Text
  
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, Picker, List, ListItem, Content, Item, Header, Body,
} from 'native-base';
import Icon  from 'react-native-ionicons';
import CountDown from 'react-native-countdown-component';
import moment from 'moment';
import firebase from 'react-native-firebase';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

import Spinner from 'react-native-loading-spinner-overlay';

console.disableYellowBox = true;
console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

export default class BookingCancel extends Component {

    workedSlotsMap = undefined;
    constructor(props) {
        super(props);

        booking = 
            this.props.navigation.state.params.booking;
            console.log('BookingShifts:'+booking);

        this.state = {
            booking:booking,
            totalDuration: 0,
            countUpPaused:false,
            spinner:false,
            spinnerMessage: '',
        };

        this.onBookingCollectionUpdate = this.onBookingCollectionUpdate.bind(this);
        this.onSlotsCollectionUpdate = this.onSlotsCollectionUpdate.bind(this);
        
    }
    componentWillUnmount() {
        this.unsubscribeBookingCollection();
        if (this.unsubscribeSlotsCollection != undefined) {
            this.unsubscribeSlotsCollection();
        }
      }

    componentDidMount() {
        // var that = this;
        // var date = moment()
        //   .utcOffset('+05:30')
        //   .format('YYYY-MM-DD hh:mm:ss');
        // //Getting the current date-time with required formate and UTC   
        
        // var expirydate = '2018-11-03 04:00:45';//You can set your own date-time
        // //Let suppose we have to show the countdown for above date-time 
     
        // var diffr = moment.duration(moment(expirydate).diff(moment(date)));
        // //difference of the expiry date-time given and current date-time
     
        // var hours = parseInt(diffr.asHours());
        // var minutes = parseInt(diffr.minutes());
        // var seconds = parseInt(diffr.seconds());
        
        // var d = hours * 60 * 60 + minutes * 60 + seconds;
        // //converting in seconds
     
        // that.setState({ totalDuration: d });
        // //Settign up the duration of countdown in seconds to re-render

        this.unsubscribeBookingCollection = firebase.firestore().collection('bookings')
        .doc(this.state.booking.id)
        .onSnapshot(this.onBookingCollectionUpdate); 

       

    }

    onSlotsCollectionUpdate = (querySnapshot) => {
        const compDict = {};
        let secsWorked = 0;

        //   const compCount = allCompanies.length;
        querySnapshot.forEach((doc) => {
            aObj = doc.data();
            aObj.id = doc.id;
            compDict[aObj.id] = aObj;

            if (aObj.endTime == undefined) {
                // consider this if it is a active slot, else skip
                if (this.state.booking.workActiveSlotId == aObj.id) {
                    secsWorked += ((new Date()).getTime() - aObj.startTime.getTime())/1000;
                }
            } else {
                secsWorked += (aObj.endTime.getTime() - aObj.startTime.getTime())/1000;
            }
        });
        
        let activeSlot = undefined;

        
        if ((this.state.booking.workActiveSlotId != undefined) && 
            (compDict[this.state.booking.workActiveSlotId] != undefined)) {
            activeSlot = compDict[this.state.booking.workActiveSlotId];
        }

        this.workedSlotsMap = compDict;
        console.log('activeSlot');
        console.log(activeSlot);
        if (activeSlot != undefined) {
            this.setState({ 
                activeSlot: activeSlot,
                totalDuration:secsWorked
            });
        }
    }
      
      
      
    onBookingCollectionUpdate = (querySnapshot) => {
        console.log('onBookingCollectionUpdate: '+querySnapshot);
    
        //   const compCount = allCompanies.length;
        console.log(querySnapshot);
       
        let aBooking = querySnapshot.data();
        if (aBooking != undefined) {
            var shouldPause = false;
            if ((aBooking.workingStatus == undefined) ||
                (aBooking.workingStatus == 'paused') ||
                (aBooking.workingStatus == 'completed')) {
                    shouldPause = true;
                }
          aBooking.id = querySnapshot.id;
          this.setState({ 
            booking: aBooking,
            countUpPaused: shouldPause
          });
        }

        if (this.unsubscribeSlotsCollection != undefined) {
            this.unsubscribeSlotsCollection();
        }
        this.unsubscribeSlotsCollection = firebase.firestore().collection('bookings')
        .doc(this.state.booking.id).collection('workedslots')
        .onSnapshot(this.onSlotsCollectionUpdate); 
    }

    static navigationOptions = {
        header:null
        // headerTitle: 'PLUMBING #1038',
        // headerStyle: {
        //     backgroundColor: '#5271FF',
        // },
        // headerBackTitle: null,
        // headerTitleStyle: {
        // color: "#fff",
        // flex:1,
        // alignItems: 'center',
        // justifyContent: 'center',
    // },
    // headerTintColor: '#fff',
    }
  
    _onPressResumeBooking = () => {
        // Works on both iOS and Android
         Alert.alert(
             'Resume Shift',
             'Do you want to resume shift ?',
             [
             {text: 'No, Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
             {text: 'Yes, Resume', onPress: () => {

                 // Pause active slot
                 firebase.firestore().collection('bookings').doc(this.state.booking.id)
                 .set(
                       {
                        workResumedAt:firebase.firestore.FieldValue.serverTimestamp()
                       }, {merge : true}
                )
                .then(() => {

                    this.setState({
                        countUpPaused:false
                    });

                    //call after two secs to get the server timestamp
                    setTimeout(() => {
                        firebase.firestore().collection('bookings').doc(this.state.booking.id)
                        .get({source: 'server'})
                        .then((doc) => {
                            console.log(doc);
                            if (!doc.exists) { return }
                            let aObj = doc.data();
                            aObj.id = doc.id;
                            console.log('Response: '+aObj);

                            const slotId = aObj.workResumedAt.getTime()+'';
                            firebase.firestore().collection('bookings').doc(this.state.booking.id)
                            .collection('workedslots').doc(slotId).set(
                                  {
                                    startTime:aObj.workResumedAt
                                  }, {merge: true}
                              )
                              .then((doc) => {
                                  console.log('created');
                                  console.log(doc);
                                 firebase.firestore().collection('bookings').doc(this.state.booking.id)
                                 .set(
                                       {
                                         workingStatus:'working',
                                         workActiveSlotId:slotId
                                       }, {merge : true}
                                   )
                              }) 
                              .catch((error) => {
                                 console.log(error);
                                 UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                             });

                        })
                        .catch((error) => {
                            console.log(error);
                            UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                        });

                    }, 2000); 
                })
                .catch((error) => {
                    console.log(error);
                    UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                });

                
                }
             },
             ],
             { cancelable: false }
         )
       }

    _onPressPauseBooking = () => {
  // Works on both iOS and Android
   Alert.alert(
       'Pause Shift',
       'Do you want to take a break ?',
       [
       {text: 'No, Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
       {text: 'Yes, Pause', onPress: () => {
           // Pause active slot
           firebase.firestore().collection('bookings').doc(this.state.booking.id)
           .collection('workedslots').doc(this.state.activeSlot.id).set(
                 {
                   endTime:firebase.firestore.FieldValue.serverTimestamp()
                 }, {merge : true}
             )
             .then(() => {

                this.setState({
                    countUpPaused:true
                });

                firebase.firestore().collection('bookings').doc(this.state.booking.id)
                .set(
                      {
                        workingStatus:'paused'
                      }, {merge : true}
                  )
             }) 
             .catch((error) => {
                console.log(error);
                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
            });
           }
       },
       ],
       { cancelable: false }
   )
 }

 _onPressCompleteBooking = () => {
    // Works on both iOS and Android
     Alert.alert(
         'Complete Shift',
         'Have you completed your Shift ?',
         [
         {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
         {text: 'Yes', onPress: () => {
            // Complete active slot
            this.setState({spinner: true, spinnerMessage:'Completing...'});
           firebase.firestore().collection('bookings').doc(this.state.booking.id)
           .collection('workedslots').doc(this.state.activeSlot.id).set(
                 {
                   endTime:firebase.firestore.FieldValue.serverTimestamp()
                 }, {merge : true}
             )
             .then(() => {

                this.setState({
                    countUpPaused:true
                });

                firebase.firestore().collection('bookings').doc(this.state.booking.id)
                .set(
                      {
                        workingStatus:'completed',
                        timesheetStatus:'pending',
                        workEndTime:firebase.firestore.FieldValue.serverTimestamp()
                      }, {merge : true}
                )
                .then(() => {
                    // Update working hours
                    if (this.unsubscribeSlotsCollection != undefined) {
                        this.unsubscribeSlotsCollection();
                    }
                    let someCollectionUnsub = firebase.firestore().collection('bookings')
                    .doc(this.state.booking.id).collection('workedslots')
                    .onSnapshot((querySnapshot) => {

                        workedSecs = 0;
                        querySnapshot.forEach((doc) => {
                            aObj = doc.data();
                            aObj.id = doc.id;
                            if((aObj.startTime == undefined) || (aObj.endTime == undefined)) { return; }
                            workedSecs += (Math.abs(aObj.endTime.getTime() - aObj.startTime.getTime())/1000); 
                        });

                        workedMins = parseInt(Math.ceil(workedSecs / 60)+'');
                        firebase.firestore().collection('bookings').doc(this.state.booking.id)
                        .set(
                            {
                                workedMins:workedMins
                            }, {merge : true}
                        )
                        .then(() => {
                            this.setState({spinner: false, spinnerMessage:''});
                            this.props.navigation.navigate('BookingComplete',{booking: this.state.booking});
                        })
                        .catch((error) => {
                            console.log(error);
                            this.setState({spinner: false, spinnerMessage:''});
                            UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                        });
                        
                        someCollectionUnsub();
                    }); 

                })
                .catch((error) => {
                    console.log(error);
                    this.setState({spinner: false, spinnerMessage:''});
                    UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                });
             }) 
             .catch((error) => {
                console.log(error);
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
            });
             
            }
         },
         ],
         { cancelable: false }
     )
   }

  render() {

    return (
      <Container>
          <Spinner
                    visible={this.state.spinner}
                    textContent={this.state.spinnerMessage}
                    textStyle={{color: '#FFF'}}
                    />
        <Header style={{backgroundColor:'#5271FF'}}> 
            <Left style={{flex:1}}>
                <Button transparent 
                onPress={() => this.props.navigation.goBack(this.props.navigation.state.key)}>
                    <Icon name="arrow-back" color='#fff' />  
                </Button>
            </Left>
            <Body style={{flex:4, textAlign:'center', alignItems:'center'}}>
                <Title style={styles.HeaderTitle}>{this.state.booking.shiftSubCategory} #{this.state.booking.shiftNo}</Title>
            </Body>
            <Right style={{flex:1}}>
            </Right>  
        </Header>
        <Content>
          <View style={styles.cancelBookingContainer}>
                <View style={styles.locationTextStyle}>
                    <Text style={styles.locTextStyle}>
                        Location : 
                    </Text>
                    <Text style={styles.locUpperTextStyle}>
                    {this.state.booking.address.toUpperCase()}
                    </Text>
                </View>
                <View style={styles.timeTextStyle}>
                    <Text style={styles.dateTextStyle}>
                        <Icon name="md-clock" size={20} color="#888"/>
                        {this.state.booking.shiftStartDateTime.getHours()}:
                        {("0" + this.state.booking.shiftStartDateTime.getMinutes()).slice(-2)}
                    </Text>
                    <Text style={styles.dateTextStyle}>
                        <Icon name="md-calendar" size={20} color="#888"/>
                        {this.state.booking.workStartTime.getDate()}/
                        {this.state.booking.workStartTime.getMonth()+1}/
                        {this.state.booking.workStartTime.getFullYear()}
                    </Text>
                </View>
                <Text style={{fontWeight:'bold', fontSize:16}}>Time Started {}
                {this.state.booking.workStartTime != undefined? this.state.booking.workStartTime.getHours(): '0'}
                :
                {("0" + (this.state.booking.workStartTime != undefined? this.state.booking.workStartTime.getMinutes() : '0')).slice(-2)}
                </Text>
                <View style={styles.countdownTimerStyle}>
                    <Label style={styles.shiftLabel}>Stop Watch</Label>
                    <CountDown
                        countup={true}
                        paused={this.state.countUpPaused}
                        timetoShow={('H', 'M', 'S')}
                        until={this.state.totalDuration}
                        //duration of countdown in seconds
                        onFinish={null}
                        onPress={null}
                        size={30}
                        digitBgColor='transparent'
                        timeTxtColor="#c0c0c0"
                        labelH='Hr'
                        labelM='Min'
                        labelS='Sec'
                        secondColor="#f57df5"
                        />
                </View>
                <View style={styles.buttonConatiner}>
                    { this.state.booking.workingStatus == 'paused' &&
                    <Button iconLeft style={[styles.btnOrange, styles.btnMarginStyle]} 
                    onPress={this._onPressResumeBooking}>
                        <Icon name='play' size={20} color="#fff"/>
                        <Text style={styles.btnText}>{'Resume'.toUpperCase()}</Text>
                    </Button>
                    }
                    { this.state.booking.workingStatus == 'working' &&
                    <Button iconLeft style={[styles.btnOrange, styles.btnMarginStyle]} 
                    onPress={this._onPressPauseBooking}>
                        <Icon name='pause' size={20} color="#fff"/>
                        <Text style={styles.btnText}>{'Pause'.toUpperCase()}</Text>
                    </Button>
                    }
                    { this.state.booking.workingStatus != 'completed' &&
                    <Button iconLeft style={[styles.btnBlue, styles.btnMarginStyle]} 
                    onPress={this._onPressCompleteBooking}>
                        <Icon name='checkmark' size={30} color="#fff"/>
                        <Text style={styles.btnText}>{'Complete'.toUpperCase()}</Text>
                    </Button>
                    }
                </View>
          </View>
        </Content>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
    HeaderTitle:{
        color:'#fff',
      },
    cancelBookingContainer:{
        padding:30,
        marginTop:10,
        marginBottom:50,
        alignItems:'center',
        alignSelf:'center',
    },
    locationTextStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems:'center',
        marginBottom:30,
    },
    timeTextStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems:'center',
        marginBottom:5,
      },
    locUpperTextStyle:{
        fontWeight:'bold',
        fontSize:18
    },
    locTextStyle:{
        fontWeight:'bold',
    },
    dateTextStyle:{
        margin:8
    },
    shiftLabel:{
        color:'#c0c0c0',
    },
    countdownTimerStyle:{
        marginTop:60,
        marginBottom:10,
    },
    buttonConatiner:{
      flex:1,
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
      marginTop:50,
    },
    btnMarginStyle:{
      margin:5,
      marginBottom:10,
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      paddingLeft:30,
      paddingRight:30,
    },
    btnOrange:{
      backgroundColor:'#df8c40',
    },
    btnBlue:{
      backgroundColor:'#5271ff',
    },
    btnText:{
        color:'#fff', 
        marginLeft:8, 
        lineHeight:18
    }
    
})