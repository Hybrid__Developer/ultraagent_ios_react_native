import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  Text,
  Switch,
} from 'react-native';
import {
  Container,
  Header,
  Title,
  Body,
  Button,
  Left,
  Right,
  Content 
} from 'native-base';

import Icon  from 'react-native-ionicons';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

const GOOGLE_MAPS_APIKEY = 'AIzaSyBl0TkFBc9YHTMeuEiky9LmorxPzgbLMJY';

export default class NavigateMpp extends Component {

 
    constructor(props) {
      super(props);

      this.mapView = null;
      booking = 
            this.props.navigation.state.params.booking;
      //console.log('NavigateMpp:'+booking);

      this.state = {
        booking:booking,
        markedLocation: {
          latitude: 51.5074,
          longitude: -0.1278,
          latitudeDelta: 0.0922,
          longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * 0.0922,
        },
      };

      this.updateDistanceDuration = this.updateDistanceDuration.bind(this);
    }

    componentDidMount() {
      navigator.geolocation.getCurrentPosition(
         (position) => {
           console.log("Got current location");
           console.log(position);
           this.setState({
             latitude: position.coords.latitude,
             longitude: position.coords.longitude,
             error: null,
           });


           let minLat=9999; let maxLat=-9999;
           let minLng=9999; let maxLng=-9999;
           if (this.state.booking.lat != undefined) {
            const aLat = parseFloat(this.state.booking.lat+"");
            if (minLat > aLat) { minLat = aLat;}
            if (maxLat < aLat) { maxLat = aLat;}
          }
          if (minLat > position.coords.latitude) { minLat = position.coords.latitude;}
          if (maxLat < position.coords.latitude) { maxLat = position.coords.latitude;}
    
          if (this.state.booking.lng != undefined) {
            const aLng = parseFloat(this.state.booking.lng+"");
            if (minLng > aLng) { minLng = aLng;}
            if (maxLng < aLng) { maxLng = aLng;}
          }
          if (minLng > position.coords.longitude) { minLng = position.coords.longitude;}
          if (maxLng < position.coords.longitude) { maxLng = position.coords.longitude;}

          
          const deltaLat = Math.abs(minLat - maxLat);
          const deltaLng = Math.abs(minLng - maxLng);
          let delta = deltaLat;
          if (deltaLng > deltaLat) { delta = deltaLng; }
          
          delta += (delta*0.1); // 10% more boundary
          console.log('delta='+delta);
          console.log('min: ('+minLat+", "+minLng+")");
          console.log('max: ('+maxLat+", "+maxLng+")");
    
          const midLat = (minLat+maxLat)/2.0;
          const midLng = (minLng+maxLng)/2.0;
          this.setState({
            markedLocation: {
              latitude: midLat,
              longitude: midLng,
              latitudeDelta: delta,
              longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * delta,
            },
          });

          console.log(this.state.markedLocation);

        

         },
         (error) => {
           console.log(error.message);
            this.setState({ error: error.message });
          },
         { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
      );
    }
      
    static navigationOptions = {
      header:null
      //   headerTitle: 'PLUMBING #1038',
      //   headerStyle: {
      //     backgroundColor: '#5271FF',
      //   },
      //   headerBackTitle: null,
      //   headerTitleStyle: {
      //     color: "#fff",
      //     flex:1,
      //     alignItems: 'center',
      //     justifyContent: 'center',
      //  },
      //  headerTintColor: '#fff',
     }

    renderCallout() {
      // Do nothing
    }

    updateDistanceDuration(distance, duration) {
      let distanceInKms = parseInt(Math.ceil(distance));
      let durationInMins = parseInt(Math.ceil(duration));
      this.setState({distanceInKms:distanceInKms, durationInMins:durationInMins});
    }

  render() {

    const { width, height } = Dimensions.get('window');
    const ratio = width / height;

    const coordinates = {
      latitude:59.32932349999999,
      longitude:18.058580800000063,
      latitudeDelta:0.1,
      longitudeDelta:0.1 * ratio,
    };

    return (
      <Container>
       <Header style={{backgroundColor:'#5271FF'}}> 
            <Left style={{flex:1}}>
                <Button transparent 
                onPress={() => this.props.navigation.goBack(this.props.navigation.state.key)}>
                    <Icon name="arrow-back" color='#fff' />  
                </Button>
            </Left>
            <Body style={{flex:4, textAlign:'center', alignItems:'center'}}>
                <Title style={styles.HeaderTitle}>{this.state.booking.shiftSubCategory} #{this.state.booking.shiftNo}</Title>
            </Body>
            <Right style={{flex:1}}>
            </Right>  
        </Header>
        <View style={styles.mapContainer}>
          <MapView style={styles.map}
             ref={map => (this.mapView = map)}
             style={styles.map}
             region={this.state.markedLocation}
             //onRegionChangeComplete={region => this.setState({region: region})}
             onRegionChangeComplete={() => this.renderCallout()}
             zoomEnabled={true}
             pitchEnabled={true}
             showsUserLocation={true}
             showsMyLocationButton={true}
             followUserLocation={true}
             zoomControlEnabled={true}
            >

            <MapView.Marker
                ref={(marker) => {}}
                key={this.state.booking.id} identifier={`${this.state.booking.id}`}
                coordinate={{latitude: this.state.booking.lat, longitude: this.state.booking.lng}}
                tooltip={true}
                style={{width:60, height: 60}}
                image={require('../images/markericon.png')}
                 onPress={() => {}}
                >
            </MapView.Marker>

            {(this.state.latitude != undefined) && (
               <MapViewDirections
               origin={{latitude:this.state.latitude, longitude:this.state.longitude}}
              //  waypoints={ (this.state.coordinates.length > 2) ? this.state.coordinates.slice(1, -1): null}
               destination={{latitude: this.state.booking.lat, longitude: this.state.booking.lng}}
               apikey={GOOGLE_MAPS_APIKEY}
               strokeWidth={3}
               strokeColor="hotpink"
               onStart={(params) => {
                 //console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
               }}
               onReady={result => {
                 
                //console.log('onready');
                //console.log(result);
                //console.log('Distance: ${result.distance} km')
                 //console.log('Duration: ${result.duration} min.')
                 this.updateDistanceDuration(result.distance, result.duration );
                 
                 this.mapView.fitToCoordinates(result.coordinates, {
                   edgePadding: {
                     right: (width / 20),
                     bottom: (height / 20),
                     left: (width / 20),
                     top: (height / 20),
                   }
                 });
               }}
               onError={(errorMessage) => {
                 //console.log('GOT AN ERROR');
                 console.log(errorMessage);
               }}
             />
            )}
            
          </MapView>
          <View style={styles.mapBootomText}>
                <Text style={styles.distanceText}>Distance : {this.state.distanceInKms} {this.state.distanceInKms > 0? 'kms':'km'}</Text>
                <Text style={styles.timeText}>Time : {this.state.durationInMins} {this.state.durationInMins > 0? 'mins':'min'}</Text>
          </View>
        </View>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
    HeaderTitle:{
      color:'#fff',
    },
    mapContainer:{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
  map: {
    marginTop: 1.5,
    ...StyleSheet.absoluteFillObject,
  },
  mapBootomText:{
    backgroundColor:'#ffffffb8',
    position:'absolute',
    bottom:0,
    width:Dimensions.get('window').width,
    padding:6,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  distanceText:{
      textAlign:'left',
      color:'#000',
      fontSize:16,
      fontWeight:'bold',
  },
  timeText:{
    textAlign:'right',
    color:'#000',
    fontSize:16,
    fontWeight:'bold',
}

  
})
