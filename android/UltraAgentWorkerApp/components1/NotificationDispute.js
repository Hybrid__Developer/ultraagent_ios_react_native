import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Alert, Text, Dimensions
  
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, Picker, List, ListItem, Content, Item, Header, Body
} from 'native-base';
import Icon  from 'react-native-ionicons';

import firebase, { RNFirebase } from 'react-native-firebase';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import Spinner from 'react-native-loading-spinner-overlay';

export default class NotificationDispute extends Component {

 
    loggedinprofile={};

  unsubscribeAmendCollection = undefined;
  unsubscribeBookingCollection = undefined;
  unsubscribeShiftCollection = undefined;
  unsubscribeCompanyCollection = undefined;

    constructor(props) {
        super(props);
       
        this.loggedinprofile = SingletonClass.get('loggedinprofile');

        notice = 
            this.props.navigation.state.params.notice;
            console.log('NotificationDispute:'+notice);

        this.state = {
           notice:notice,
           spinner:false,
            spinnerMessage: '',
        };
      }
      
    static navigationOptions = {
        header:null
    }

    componentDidMount() {
       
        if (this.state.notice.bookingId != undefined) {
          this.unsubscribeBookingCollection = firebase.firestore().collection('bookings')
          .doc(this.state.notice.bookingId)
          .onSnapshot(this.onBookingCollectionUpdate) 
        }

        // Mark as read
        if (this.state.notice.isRead == false) {
          firebase.firestore().collection('profiles')
          .doc(this.loggedinprofile.userId)
          .collection('notices').doc(this.state.notice.id).update({isRead: true})
          .then()
          .catch((err) => {console.error(err)});
        }
      }
      
      componentWillUnmount() {
        if (this.unsubscribeAmendCollection != undefined) {
          this.unsubscribeAmendCollection();
        }
        if (this.unsubscribeBookingCollection != undefined) {
          this.unsubscribeBookingCollection();
        }
        if (this.unsubscribeShiftCollection != undefined) {
          this.unsubscribeShiftCollection();
        }
        if (this.unsubscribeCompanyCollection != undefined) {
          this.unsubscribeCompanyCollection();
        }
      }

      onAmendCollectionUpdate = (querySnapshot) => {
        if ((querySnapshot != undefined) && (querySnapshot != null) &&
            (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
        {
          var aObj = querySnapshot._data;
          aObj.id = querySnapshot.id; 
          console.log('onAmendCollectionUpdate');
          console.log(aObj);

          this.setState({amend:aObj});
        }
      }

      onBookingCollectionUpdate = (querySnapshot) => {
        if ((querySnapshot != undefined) && (querySnapshot != null) &&
            (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
        {
          var aObj = querySnapshot._data;
          aObj.id = querySnapshot.id; 
          console.log('onBookingCollectionUpdate');
          console.log(aObj);

          this.setState({booking:aObj});

          this.unsubscribeShiftCollection = firebase.firestore().collection('shifts')
          .doc(aObj.shiftId)
          .onSnapshot(this.onShiftCollectionUpdate) 

          this.unsubscribeCompanyCollection = firebase.firestore().collection('companies')
          .doc(aObj.companyId)
          .onSnapshot(this.onCompanyCollectionUpdate) 

          this.unsubscribeAmendCollection = firebase.firestore().collection('bookings')
          .doc(this.state.notice.bookingId).collection('amends').doc(aObj.activeAmendId)
          .onSnapshot(this.onAmendCollectionUpdate) 

        }
      }

      onShiftCollectionUpdate = (querySnapshot) => {
        if ((querySnapshot != undefined) && (querySnapshot != null) &&
            (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
        {
          var aObj = querySnapshot._data;
          aObj.id = querySnapshot.id; 
          console.log('onShiftCollectionUpdate');
          console.log(aObj);

          this.setState({shift:aObj});
        }
      }

      onCompanyCollectionUpdate = (querySnapshot) => {
        if ((querySnapshot != undefined) && (querySnapshot != null) &&
            (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
        {
          var aObj = querySnapshot._data;
          aObj.id = querySnapshot.id; 
          console.log('onCompanyCollectionUpdate');
          console.log(aObj);

          this.setState({company:aObj});
        }
      }

      _onPressButtonDelete = () => {
        Alert.alert(
          'Confirmation!',
          'Are you sure you want to delete this entry?',
          [
            {text: 'No, Cancel', onPress: () => {/*cancelled*/}, style: 'cancel'},
            //{text: 'OK', onPress: () => console.log('OK Pressed')},*/
            {text: "Yes, Delete", style:'destructive', onPress: () => {
              this.setState({spinner: true, spinnerMessage:'Deleting...'});
              firebase.firestore().collection('profiles').doc(this.loggedinprofile.userId)
              .collection('notices').doc(this.state.notice.id).delete()
              .then(() => {
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Success",'Entry deleted successfully');
                this.props.navigation.navigate('Notifications');
              })
              .catch((error) => {
                console.log(error);
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
              });
            }}
          ],
          { cancelable: false }
        );
        
      }

render() {

    return (
      <Container>
          <Header style={{backgroundColor:'#5271FF'}}>
         
         <Left style={{flex:1}}>
             <Button transparent 
             onPress={() => this.props.navigation.goBack(this.props.navigation.state.key)}>
                <Icon name="arrow-back" color='#fff' />  
             </Button>
         </Left>
         <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
             <Title style={styles.HeaderTitle}>{'Ultra Agent Control'.toUpperCase()}</Title>
         </Body>
         <Right style={{flex:1}}>
            <Button transparent onPress={this._onPressButtonDelete} 
              style={{paddingLeft:0, paddingRight:0,
              marginLeft:0, marginRight:0}}>
                <Image source={require('../images/deleteicon.png')} 
                style={{height:50, width:20, alignItems:'center'}} />
            </Button>
         </Right>
        
      </Header>

        <View style={{backgroundColor:'#5f7bfa'}}>
        <Title style={styles.SubHeaderTitle}>{this.state.notice.shiftTitle} {this.state.notice.header}</Title>
        </View>
        <Content>
          <View style={styles.notifBookingContainer}>

          <Spinner
                visible={this.state.spinner}
                textContent={this.state.spinnerMessage}
                textStyle={{color: '#FFF'}}
            />

            <View style={styles.bookingDetailsContent}>
              <Label style={styles.bookingLabel}>Control Decision:</Label>
              <View style={styles.textBox}>
                <Text style={styles.bookingDetailsChildText}> 
                   {this.amend == undefined? '' : this.amend.adminApprovedBrief}
                </Text>
              </View>
            </View>
            <View style={styles.ntifiDisputeList}>
                <List style={styles.listmarginStyle}>
                    <ListItem noBorder style={[styles.disputeListitem, styles.bgGray]}>
                        <Left>
                            <Text style={styles.fontBoldStyle}>Date :</Text>
                        </Left>
                        <Body style={{paddingLeft:0, marginLeft:0}}>
                            <Text style={styles.fontColorStyle}>
                            {this.state.amend == undefined? '': this.state.amend.adminApprovedStartTime.getDate()+'/'+(this.state.amend.adminApprovedStartTime.getMonth()+1)+'/'+this.state.amend.adminApprovedStartTime.getFullYear()} 
                            </Text>
                        </Body>
                    </ListItem>
                    <ListItem noBorder style={[styles.disputeListitem, styles.bgWhite]}>
                        <Left>
                            <Text style={styles.fontBoldStyle}>Start Time :</Text>
                        </Left>
                        <Body style={{paddingLeft:0, marginLeft:0}}>
                            <Text style={styles.fontColorStyle}>{this.state.amend == undefined? '': this.state.amend.adminApprovedStartTime.getHours() +':'+("0" + this.state.amend.adminApprovedStartTime.getMinutes()).slice(-2)}</Text>
                        </Body>
                    </ListItem>
                    <ListItem noBorder style={[styles.disputeListitem, styles.bgGray]}>
                        <Left>
                            <Text style={styles.fontBoldStyle}>End Time :</Text>
                        </Left>
                        <Body style={{paddingLeft:0, marginLeft:0}}>
                        <Text style={styles.fontColorStyle}>{this.state.amend == undefined? '': this.state.amend.adminApprovedEndTime.getHours() +':'+("0" + this.state.amend.adminApprovedEndTime.getMinutes()).slice(-2)}</Text>
                        </Body>
                    </ListItem>
                    <ListItem noBorder style={[styles.disputeListitem, styles.bgWhite]}>
                        <Left>
                            <Text style={styles.fontBoldStyle}>Duration :</Text>
                        </Left>
                        <Body style={{paddingLeft:0, marginLeft:0}}>
                            <Text style={styles.fontColorStyle}>{this.state.booking == undefined? '': parseInt((parseInt(this.state.booking.approvedMins+'')/60)+'')+':'+parseInt((parseInt(this.state.booking.approvedMins+'')%60)+'')}</Text>
                        </Body>
                    </ListItem>
                    <ListItem noBorder style={[styles.disputeListitem, styles.bgGray]}>
                        <Left>
                            <Text style={styles.fontBoldStyle}>Fee/h :</Text>
                        </Left>
                        <Body style={{paddingLeft:0, marginLeft:0}}>
                            <Text style={styles.fontColorStyle}>&pound;{this.state.booking == undefined? '':parseFloat(this.state.booking.approvedRate+'').toFixed(2)}/h</Text>
                        </Body>
                    </ListItem>
                    <ListItem noBorder style={[styles.disputeListitem, styles.bgWhite]}>
                        <Left>
                            <Text style={styles.fontBoldStyle}>Amount :</Text>
                        </Left>
                        <Body style={{paddingLeft:0, marginLeft:0}}>
                            <Text style={styles.fontColorStyle}>&pound; {this.state.booking == undefined? '':parseFloat((parseInt(this.state.booking.approvedMins+'')*parseFloat(this.state.booking.approvedRate+'')/60)+'').toFixed(2)}</Text>
                        </Body>
                    </ListItem>
                </List>
            </View>
          </View>
        </Content>
        </Container>
    );
  }
}

const styles = StyleSheet.create({
   
    SubHeaderTitle:{
      color:'#fff',
      fontSize:14,
      padding:5,
      paddingTop:8,
      textAlign:'center',
      alignItems:'center',
    },
    HeaderTitle:{
      color:'#fff',
      textAlign:'center',
      alignItems:'center',
    },
    notifBookingContainer:{
      marginTop:15,
      marginBottom:80,
    },
    bookingDetailsContent:{
      marginBottom:40,
      marginTop:40,
      margin:15,
    },
    textBox:{
      backgroundColor:'#f4f4f4',
      padding:15,
    },
    bookingDetailsChildText:{
        paddingBottom:15,
        paddingTop:15,
        lineHeight:22,
    },
    
    bookingLabel:{
      marginBottom:5,
      fontSize:14,
      color:'#000',
      fontWeight:'bold',
    },
    disputeListitem:{
        margin:0,
        marginLeft:0,
        paddingLeft:30,
    },
    bgGray:{
        backgroundColor:'#f4f4f4',
    },
    bgWhite:{
        backgroundColor:'#fff',
    },
    fontColorStyle:{
        color:'#939393',
    },
    fontBoldStyle:{
        fontWeight:'bold',
        alignItems:'flex-end',
    },
    listmarginStyle:{
        width:Dimensions.get('window').width,
        padding:0,
    }
})