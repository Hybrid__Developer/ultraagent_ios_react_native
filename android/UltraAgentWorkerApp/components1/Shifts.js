import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Text, FlatList, 
  Platform
} from 'react-native';
import {
  Container, Header, Title, Body, Button,
  Left, Right, Picker, List, ListItem, Content, Badge, Icon
} from 'native-base';
//import Icon  from 'react-native-ionicons';
import firebase, { RNFirebase } from 'react-native-firebase';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

export default class Shifts extends Component {

  state = {
    textInput: '',
    loading: true,
    shifts: [],
  }

  loggedinprofile={};
  shiftIdToRequestMap={};

  constructor(props) {
    super(props);

    this.loggedinprofile = SingletonClass.get('loggedinprofile');
    
    this.unsubscribeShiftsCollection = null;
    this.unsubscribeBookingRequestsCollection = null;

    this.state = {
      textInput: '',
      loading: true,
      shifts: [],
      sortby:'Date',
      currentLocation: 
      {
        latitude:51.5074,
        longitude:-0.1278
      }
    };

    this.onShiftsCollectionUpdate = this.onShiftsCollectionUpdate.bind(this);
    this.mergeCompanies = this.mergeCompanies.bind(this);
    this.mergeBookingRequests = this.mergeBookingRequests.bind(this);
    
    this.reorderShifts = this.reorderShifts.bind(this);
  }

  static navigationOptions = {
    header:null
}

componentDidMount() {

  

  this.unsubscribeShiftsCollection = firebase.firestore().collection('shifts')
  .where('isAdvanceBooking','==',false)
  .where('subcategory','==',this.loggedinprofile.wSubCategory)
  .onSnapshot(this.onShiftsCollectionUpdate) 

  this.unsubscribeBookingRequestsCollection = firebase.firestore().collection('bookingrequests')
  .where('workerId','==',this.loggedinprofile.userId)
  .onSnapshot(this.onBookingRequestsUpdate);

}


componentWillUnmount() {
  this.unsubscribeShiftsCollection();
  this.unsubscribeBookingRequestsCollection();

}

mergeCompanies = (querySnapshot) => {
  const someShifts = this.state.shifts;
  const compDict = {};
  //   const compCount = allCompanies.length;
  querySnapshot.forEach((doc) => {
    aObj = doc.data();
    aObj.id = doc.id;
    compDict[aObj.id] = aObj;
  });

  const shiftsCount = someShifts.length
  for (var i=0; i < shiftsCount; i++) {
    const companyId = someShifts[i].companyId;
    someShifts[i].company = compDict[companyId];
  }

  this.setState({ 
    shifts: someShifts,
    loading: false,
  });

  //console.log("After: mergeCompanies +", this.state.shifts)
}


onBookingRequestsUpdate = (querySnapshot) => {
  //console.log('onBookingRequestsUpdate');
  const compDict = {};
  //   const compCount = allCompanies.length;
  querySnapshot.forEach((doc) => {
    aObj = doc.data();
    aObj.id = doc.id;
    compDict[aObj.shiftId] = aObj;
  });
  this.shiftIdToRequestMap = compDict;

  //console.log(this.shiftIdToRequestMap);
  this.mergeBookingRequests();
}

mergeBookingRequests = ()=>{
  //console.log('mergeBookingRequests');

  const someShifts = [];
  const shiftsCount = this.state.shifts.length

  for (var i=0; i < shiftsCount; i++) { someShifts.push(this.state.shifts[i]); }

  for (var i=0; i < shiftsCount; i++) {
    const shiftId = someShifts[i].id;
    someShifts[i].bookingrequest = this.shiftIdToRequestMap[shiftId];
  }

  this.setState({ 
    shifts: someShifts,
    loading: false,
  });

  //console.log(this.state.shifts);

  //console.log("After: mergeCompanies +", this.state.shifts)
}

//This function takes in latitude and longitude of two location 
// and returns the distance between them as the crow flies (in km)
calcCrow(lat1, lon1, lat2, lon2) 
{
  var R = 6371; // km
  var dLat = this.toRadians(lat2-lat1);
  var dLon = this.toRadians(lon2-lon1);
  var lat1 = this.toRadians(lat1);
  var lat2 = this.toRadians(lat2);

  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c;
  return d;
}

// Converts numeric degrees to radians
toRadians(Value) 
{
    return Value * Math.PI / 180;
}

onShiftsCollectionUpdate =  (querySnapshot) => {
  //console.log('onShiftsCollectionUpdate');

  let todayBeginning = new Date();
  todayBeginning.setHours(0,0,0,0);
  const todayBeginTimeStamp = todayBeginning.getTime();

  const fromShifts = [];
  const shiftsCount = querySnapshot.length;
  // for (var i=0; i < shiftsCount; i++) {
  //   doc = querySnapshot[i];
  //   aObj = doc.data();
  //   aObj.id = doc.id;
  //   someShifts.push(aObj);
  // }
   querySnapshot.forEach((doc) => {
    aObj = doc.data();
    aObj.id = doc.id;

    if ((aObj.startDateTime != undefined) && (aObj.startDateTime.getTime() > todayBeginTimeStamp)) {
      // Only show those shifts matching the working hours of the loggedinuser
      const distance = this.calcCrow(aObj.lat, aObj.lng, 
        this.state.currentLocation.latitude, 
        this.state.currentLocation.longitude);
      aObj.distance = distance;

      if (distance < 1) {
        aObj.distanceDisplay = parseInt((distance*1000) + '')+" m"; // meters
      } else if (distance < 2) {
        aObj.distanceDisplay = "1 km"; 
      } else {
        aObj.distanceDisplay = parseInt(distance + '')+" kms"; // killometers
      }

      // Bookedshifts are not to be shown
      if((aObj.recruitmentStatus == undefined) || (aObj.recruitmentStatus == 'pending')){
        fromShifts.push(aObj);
      }
    }
  });

  //console.log(fromShifts);

  // a user to receive shifts that fall in their working hours
  let someShifts = 
  UltraAgentHelpers.filterShiftsByMatchingWorkingHours(fromShifts,this.loggedinprofile);

  this.reorderShifts(someShifts, this.state.sortby);
  this.mergeBookingRequests();

  //firebase.firestore().collection('companies').onSnapshot(this.mergeCompanies);
  //console.log("After: onShiftsCollectionUpdate +", this.state.shifts)
}

reorderShifts(shiftsList, sortKey) {
  let someShifts = [];
  someShifts.push(...shiftsList);
  //console.log('reorderShifts called: '+sortKey);
  //console.log('Before');console.log(someShifts);
  // Sort Shifts by descending order of date
  someShifts.sort((a,b) => {
    if ((sortKey != undefined) &&
      (sortKey.toLowerCase() == 'fee')) {
        // sort by fee
      //console.log('sort by fee');
      const aFee = parseInt(a.fee+'');
      const bFee = parseInt(b.fee+'');
      if (aFee > bFee) {
        return -1;
      } else if (aFee < bFee) {
        return 1;
      }
      return 0;
    } else  if ((sortKey != undefined) &&
    (sortKey.toLowerCase() == 'distance')) {
      // sort by distance
      //console.log('sort by distance');
    const aDistance = parseFloat(a.distance+'');
    const bDistance = parseFloat(b.distance+'');
    if (aDistance > bDistance) {
      return 1;
    } else if (aDistance < bDistance) {
      return -1;
    }
    return 0;
  } else {
      // sort by Date
      //console.log('sort by Date');
      const aDate = a.startDateTime;
      const bDate = b.startDateTime;
      if (aDate.getTime() > bDate.getTime()) {
        return -1;
      } else if (aDate.getTime() < bDate.getTime()) {
        return 1;
      }
      return 0;
    }
  });

  //console.log('After');console.log(someShifts);

  this.setState({ 
    shifts: someShifts,
    //loading: false,
  });
  
}
  
  _onPressShiftsDetails = (selectedShift) => {
    this.props.navigation.navigate('ShiftsDetails',{'selectedShift' : selectedShift});
  }

  onSortingSelectionChange = (itemValue, itemIndex) => {
    console.log('onSortingSelectionChange:'+itemValue+': '+itemIndex);
    this.setState({sortby: itemValue});
    this.reorderShifts(this.state.shifts, itemValue);
  }

 
  render() {
    // if (this.state.loading) {
    //   return null; // or render a loading icon
    // }
    return (
      <Container>
        <Header style={{backgroundColor:'#5271FF'}}>
         
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Image source={require('../images/hamburgericon.png')} style={{height:32, width:32,}} />
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'Available shifts'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
         
       </Header>
       <View style={styles.nestedPickerView}>
          <Text style={styles.pickerText}>{this.loggedinprofile.wSubCategory}</Text>
          <View style={styles.pickerIconstyle}>
            <Picker
                mode="dropdown"
                textStyle={{ color: "#fff" }}
                selectedValue={this.state.sortby}
                //style={{height:30}}
                iosIcon={<Icon type="FontAwesome" name="caret-down" 
                  style={{ color: "#fff", fontSize: 15, position:'absolute', right:-15 }} />}
                style={ styles.dropdownPickerStyle}
                placeholder="Sort"
                onValueChange={this.onSortingSelectionChange.bind(this)}
            >
                <Picker.Item label="Date" value="Date" />
                <Picker.Item label="Fee" value="Fee" />
                <Picker.Item label="Distance" value="Distance" />
            </Picker>
            {/* <Icon type="FontAwesome" name="caret-down" 
              style={{fontSize: 15, color: '#fff', position:'absolute', right:0, top:8}}/> */}
          </View>
        </View>
        <Content>
        <FlatList
        keyExtractor={(item, index) => item.id}
        data={this.state.shifts}
        renderItem={({ item }) => (
            <ListItem button onPress={() => this._onPressShiftsDetails(item)}>
              <Left>
              <View>
                <View style={styles.shiftTextStyle}>
                  <Text style={styles.jobIdText}>{item.subcategory} #{item.shiftNo}</Text>
                  <Text style={styles.capBlueText}>{item.companyName.toUpperCase()}</Text>
                </View>
                <View style={styles.shiftlocationTextStyle}>
                  <Text style={[styles.locationTextStyle, styles.marginRightStyle]}>Location: {item.address}</Text>
                  <View style={styles.shiftTimeTextStyle}>
                    <Text style={[styles.timeTextStyle, styles.marginRightStyle]}>
                      <Icon type="FontAwesome" name="clock-o" 
                      style={{fontSize: 15, color: '#888', right:3}}/> {''}
                      {item.startDateTime.getHours()}:{("0" + item.startDateTime.getMinutes()).slice(-2)}
                    </Text>
                    <Text style={styles.dateTextStyle}>
                    {/* {"\n"} */}
                      <Icon type="FontAwesome" name="calendar" 
                      style={{fontSize: 15, color: '#888'}}/> {''}
                      {item.startDateTime.getDate()}/{item.startDateTime.getMonth()+1}/{item.startDateTime.getFullYear()}
                    </Text>
                  </View>
                </View>
                <View style={styles.shiftBadgeStyle}>
                  <Badge style={styles.badgeStyle}>
                    <Text style={styles.badgeText}>£{item.fee}/H</Text>
                  </Badge> 
                  <Text style={{color: '#08c903', lineHeight:31, marginLeft:20, marginRight:20}}>
                  {item.bookingrequest == undefined? '':'applied'}
                  </Text>
                </View>
              </View>
              </Left>
              <Right>
                <Icon type="FontAwesome" name="angle-right" 
                style={{fontSize: 30, color: '#5271ff'}} />
               <Text style={{color: '#df8c40', top:15,
                  textAlign:'right', alignItems:'flex-end'}}>{item.distanceDisplay == undefined? '':item.distanceDisplay}</Text>
              </Right>
            </ListItem>
        )}            
          /> 

           {/* <List>
            <ListItem button onPress={this._onPressShiftsDetails}>
              <Left>
              <View>
                <View style={styles.shiftTextStyle}>
                  <Text style={styles.jobIdText}>Plumbing #1031</Text>
                  <Text style={styles.capBlueText}>{'SKANSKA UK CO'.toUpperCase()}</Text>
                </View>
                <View style={styles.shiftlocationTextStyle}>
                  <Text style={[styles.locationTextStyle, styles.marginRightStyle]}>Location: London city Airport</Text>
                  <Text style={[styles.timeTextStyle, styles.marginRightStyle]}>
                    <Icon name="md-clock" size={15} color="#888"/>
                    15:00
                  </Text>
                  <Text style={styles.dateTextStyle}>
                    <Icon name="md-calendar" size={15} color="#888"/>
                    May 20, 2018
                  </Text>
                </View>
                <View style={styles.shiftBadgeStyle}>
                <Badge style={styles.badgeStyle}>
                  <Text style={styles.badgeText}>£8/H</Text>
                </Badge>
                </View>
              </View>
              </Left>
              <Right>
              <Icon name="ios-arrow-forward" size={30} color="#5271ff" />
              </Right>
            </ListItem>            
          </List>  */}

        </Content>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
   
    HeaderTitle:{
        color:'#fff',
        textAlign:'center',
        alignItems:'center',
    },
    pickerText: {
        color: '#fff',
        alignItems:'flex-end',
        lineHeight:30,
    },
    pickerIconstyle:{
        alignItems:'flex-start',
    },

    nestedPickerView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor:'#6e87fb',
        padding:2,
        paddingLeft:10, 
        paddingRight:10,
    },
    shiftTextStyle:{
      flexDirection: 'row',
      flex:1,
      flexWrap: 'wrap',
    },
    jobIdText:{
      fontWeight:'bold',
      paddingRight:12,
    },
    capBlueText:{
      color:'#5271ff',
    },
    // shiftlocationTextStyle:{
    //   flex:1,
    //   flexDirection: 'row',
    //   flexWrap: 'wrap',
    // },
    shiftTimeTextStyle:{
      flex:1,
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    locationTextStyle:{
      fontWeight:'bold',
      flexWrap: 'wrap',
    },
    marginRightStyle:{
      marginRight:10,
    },
    dateTextStyle:{
    },
    timeTextStyle:{
    },
    badgeStyle:{
      backgroundColor:'transparent', 
      borderColor:'#df8c40', 
      borderRadius:4, 
      borderWidth:1,
      marginTop:5,
    },
    badgeText:{
      color:'#df8c40',
      paddingLeft:4, 
      paddingRight:4, 
      lineHeight:21,
      textAlign:'center',
      alignItems:'center'
    },
    shiftBadgeStyle:{
      flex:1,
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    dropdownPickerStyle:{
      height:30,
      ...Platform.select({
          ios: {
             
          },
          android: {
            width:140,
            alignItems:'flex-end',
            flexDirection:'row',
            color:"#fff",
            flex:1,
            //justifyContent: 'space-between',
            justifyContent: 'center',
            alignItems: 'center',
          },
      })
      
  },
})
