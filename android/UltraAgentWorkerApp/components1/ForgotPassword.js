import React, { Component } from 'react';
import { StyleSheet, Text, View, 
   Image, TextInput, TouchableOpacity, ImageBackground, 
   ScrollView, Platform} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import KeyboardWrapperView from '../helpers/KeyboardWrapperView';

import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import firebase from 'react-native-firebase';

export default class ForgotPassword extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            spinner:false,
            spinnerMessage: ''
        }
    }

    static navigationOptions = {
       // header:null
       headerTitle: 'FORGOT PASSWORD',
       headerStyle: {
        backgroundColor: '#5271FF',
      },
      headerBackTitle: null,
      headerTitleStyle: {
       color: "#fff",
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      headerTintColor: '#fff',
    }

    _onPressButtonSubmit= () => {
        if ((this.state.email.length < 3)) {
            UltraAgentHelpers.showAlertWithOneButton("Oops!","Please enter a valid email address.");
            return;
        }

        this.setState({spinner:true, spinnerMessage: 'Processing...'});
        firebase.auth().sendPasswordResetEmail(this.state.email.trim().toLowerCase())
        .then(() => {
            this.setState({spinner:false, spinnerMessage: ''});
            this.props.navigation.navigate('ResetPassword', {email:this.state.email.trim().toLowerCase()});
        })
        .catch((err) => {
            console.log(err);
            this.setState({spinner:false, spinnerMessage: ''});
            UltraAgentHelpers.showAlertWithOneButton("Oops!",err.message);
        });
    }

    _onPressButtonSignup= () => {
        this.props.navigation.navigate('Register');
      }
    


  render() {
    return (
        <KeyboardWrapperView behavior="padding" style={styles.wrapper}>
            <Spinner
                visible={this.state.spinner}
                textContent={this.state.spinnerMessage}
                textStyle={{color: '#FFF'}}
                />
            <ImageBackground source={require('../images/registerbg.jpg')} 
            style={styles.backgroundImage}>
                <View style={styles.logoContainer}>
                    <Image style={styles.logo} 
                    source={require('../images/logo.png')}/>
                </View>
                <View style={styles.loginformContainer}>
        
                    <View style={styles.inputContainer}>
                        <Image source={require('../images/mailicon.png')} 
                        style={styles.InputImageIcon} />

                        <TextInput 
                            style={{flex:1}} 
                            underlineColorAndroid={'transparent'}
                            placeholder="Enter Your Email id" 
                            placeholderTextColor = "#838383"
                            keyboardType='email-address' 
                            outoCorrect={false}
                            onChangeText={email => this.setState({ email })}
                            value={this.state.email}
                        />

                    </View>
                
                    <TouchableOpacity 
                        style={styles.button} 
                        onPress={this._onPressButtonSubmit}
                        activeOpacity={0.7} 
                        >
                        <Text style={styles.btntext}>SUBMIT</Text>
            
                    </TouchableOpacity>
                    <View style={styles.linkContainer}>
                        <Text style={styles.forgotText}>Don't have an account ?</Text>
                        <Text style={styles.forgotSignupText} 
                        onPress={this._onPressButtonSignup}>SIGNUP NOW</Text>
                    </View>
                </View>
            </ImageBackground>
        </KeyboardWrapperView>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container:{
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf:'stretch',
    width:null,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  logoContainer:{
    alignItems: 'center',
    alignSelf:'stretch',
    //marginTop:30,
    justifyContent: 'center',
    flexGrow:1,
    marginTop:10,
  },
  logo:{
    width:150,
    height:150,
  },
  loginformContainer:{
    // alignSelf:'stretch',
    // marginTop:50,
    // width:250,
    alignSelf:'stretch',
    paddingLeft:20,
    paddingRight:20,
    marginTop:10,
    marginLeft:40,
    marginRight:40,
    marginBottom:20
  },
  /*iconShow:{
    position: 'absolute',
    left: 7,
    bottom: 7
  },*/
 
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf:'stretch',
    padding:10,
    backgroundColor:'#fff',
    borderRadius:2,
    ...Platform.select({
        ios: {
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.3,
        },
        android: {
            elevation:2,
            height:40,
            paddingTop:0,
            paddingBottom:0,
        },
    })
},
InputImageIcon: {
    marginRight:8,
    height: 20,
    width: 20,
    resizeMode : 'stretch',
    alignItems: 'center'
},
  
  button:{
    alignSelf:'stretch',
    marginTop:20,
    backgroundColor:'#5271FF',
    alignItems:'center',
    padding:10,
    borderRadius:3,
  },
  btntext:{
      color:'#fff',
  },
//   linkContainer:{
//     alignSelf:'stretch',
//     alignItems:'center',
//     position:'absolute',
//     bottom:0,
//     left:0,
//     right:0,
//     marginBottom:10,
//   },
linkContainer:{
    alignSelf:'stretch',
    alignItems:'center',
    // position:'absolute',
    // bottom:0,
    // left:0,
    // right:0,
    marginBottom:10,
    marginTop:100,
  },
  forgotText:{
    color:'#fff',
  },
  forgotSignupText:{
    marginTop:10,
    color:'#fff',
  },
});
