import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Dimensions, Text, FlatList,
  
} from 'react-native';
import {
  Container, Header, Title, Body, Button,
  Left, Right, Picker, List, ListItem, Content, Badge
} from 'native-base';

import firebase, { RNFirebase } from 'react-native-firebase';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

export default class Notifications extends Component {

  loggedinprofile={};

  constructor(props) {
    super(props);

    this.loggedinprofile = SingletonClass.get('loggedinprofile');

    this.unsubscribeNoticesCollection = null;

    this.state = {
       
    };

    this.onNoticesCollectionUpdate = this.onNoticesCollectionUpdate.bind(this);
    this._onPressNoticeDetails = this._onPressNoticeDetails.bind(this);
  }

  static navigationOptions = {
    header:null
}


componentDidMount() {

  this.unsubscribeNoticesCollection = firebase.firestore().collection('profiles')
  .doc(this.loggedinprofile.userId).collection('notices')
  .orderBy('createdAt','desc')
  .onSnapshot(this.onNoticesCollectionUpdate) 
}

componentWillUnmount() {
  this.unsubscribeNoticesCollection();
}

onNoticesCollectionUpdate =  (querySnapshot) => {
  const notices = [];
  querySnapshot.forEach((doc) => {
    aObj = doc.data();
    aObj.id = doc.id;
    notices.push(aObj);
  });

  this.setState({notices: notices});
}

_onPressNoticeDetails = (aNotice) => {
  console.log('_onPressNoticeDetails called');
  console.log(aNotice);

  if (aNotice.type == 'workerDisputeResolved') {
    this.props.navigation.navigate('NotificationDispute',{notice:aNotice});
  } else {
    this.props.navigation.navigate('NotificationBooking',{notice:aNotice});
  }


}



  render() {

    return (
      <Container>
        <Header style={{backgroundColor:'#5271FF'}}>
         
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Image source={require('../images/hamburgericon.png')} style={{height:32, width:32,}} />
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'Notifications'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
         
       </Header>
      
        <Content>
        <FlatList
        keyExtractor={(item, index) => item.id}
        data={this.state.notices}
        renderItem={({ item }) => (
          <ListItem style={[styles.bgGray, styles.marginStyle]} button onPress={() => this._onPressNoticeDetails(item)}>
            <Left>
            <View>
              {item.type == 'workerDisputeResolved' && 
                <Text style={styles.notificationTitle}>Ultra Agent Control</Text>
              }
              {item.type != 'workerDisputeResolved' && 
                <Text style={styles.notificationTitle}>{item.companyTitle}</Text>
              }
              <View style={styles.notificationTextStyle}>
                <Text style={styles.jobIdText}>{item.shiftTitle}</Text>
                {item.type == 'workerInvitedToShift' && 
                  <Text style={styles.capBlueText}>{item.header}</Text>
                }
                {item.type == 'employerRejectedBookingRequest' && 
                  <Text style={styles.capRedText}>{item.header}</Text>
                }
                {item.type == 'employerAcceptedBookingRequest' && 
                  <Text style={styles.capGreenText}>{item.header}</Text>
                }
                {item.type == 'workerDisputeResolved' && 
                  <Text style={styles.capGreenText}>{item.header}</Text>
                }
                {item.type == 'employerCancelledBooking' && 
                  <Text style={styles.capRedText}>{item.header}</Text>
                }
                {item.type == 'shiftApproved' && 
                  <Text style={styles.capGreenText}>{item.header}</Text>
                }
                {item.type == 'shiftPaid' && 
                  <Text style={styles.capGreenText}>{item.header}</Text>
                }
              </View>
              <Text style={styles.notificationText}>{item.title}</Text>
            </View>
            </Left>
            <Right>
              {item.isRead == true &&
                <Image source={require('../images/openmailicon.png')} style={{height:30, width:30,}} />
              }
              {item.isRead == false &&
                <Image source={require('../images/messageicon.png')} style={{height:30, width:30,}} />
              }
            </Right>
          </ListItem>
        )}
        />

        </Content>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
   
    HeaderTitle:{
        color:'#fff',
        textAlign:'center',
        alignItems:'center',
    },
    notificationTitle:{
      fontWeight:'bold',
    },
    notificationTextStyle:{
      // flexDirection: 'row',
      // flex:1,
    },
    jobIdText:{
      fontWeight:'bold',
    },
    capRedText:{
      color:'#f80808',
     // paddingLeft:5,
    },
    capBlueText:{
      color:'#5271FF',
      //paddingLeft:5,
    },
    capGreenText:{
      color:'#08c903',
      //paddingLeft:5,
    },
    notificationText:{
      marginTop:5,
    },
    bgGray:{
      backgroundColor:'#f4f4f4',
    },
    bgWhite:{
      backgroundColor:'#fff',
    },
    marginStyle:{
      marginLeft:0,
      paddingLeft:20,
    },

})
