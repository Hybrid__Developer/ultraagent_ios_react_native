import React, { Component } from 'react';
import {
  StyleSheet, View, Image, ImageBackground, Text, AsyncStorage, 
  Dimensions, Switch
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, List, ListItem, Content, Header, Body
} from 'native-base';
import Icon from 'react-native-ionicons';
import GPSState from 'react-native-gps-state';

import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

export default class Settings extends Component {

  loggedinprofile = {};

  constructor(props) {
    super(props);

    this.loggedinprofile = SingletonClass.get('loggedinprofile');

    let sendNotifications = true;
    if (this.loggedinprofile.sendNotifications != undefined) {
      sendNotifications =  this.loggedinprofile.sendNotifications;
    }

    this.state = {
      sendNotifications:sendNotifications,
    };

    this._handleGpsSwitch = this._handleGpsSwitch.bind(this);
    this._handleNotificationsToggleSwitch = this._handleNotificationsToggleSwitch.bind(this);
  }

  componentWillMount(){
    GPSState.addListener((status)=>{
      switch(status){
        case GPSState.NOT_DETERMINED:
          //alert('Please, allow the location, for us to do amazing things for you!');
          this.setState({gps:false});
        break;
  
        case GPSState.RESTRICTED:
          //GPSState.openSettings();
          this.setState({gps:false});
        break;
  
        case GPSState.DENIED:
          //alert('It`s a shame that you do not allowed us to use location :(');
          this.setState({gps:false});
        break;
  
        case GPSState.AUTHORIZED_ALWAYS:
          //TODO do something amazing with you app
          this.setState({gps:true});

        break;
  
        case GPSState.AUTHORIZED_WHENINUSE:
          //TODO do something amazing with you app
          this.setState({gps:true});
        break;
      }
    });
  }

  componentDidMount(){
    //Get the current GPS state
    GPSState.getStatus().then((status)=>{
      switch(status){
        case GPSState.NOT_DETERMINED:
          //alert('Please, allow the location, for us to do amazing things for you!');
          this.setState({gps:false});
        break;
  
        case GPSState.RESTRICTED:
          //GPSState.openSettings();
          this.setState({gps:false});
        break;
  
        case GPSState.DENIED:
          //alert('It`s a shame that you do not allowed us to use location :(');
          this.setState({gps:false});
        break;
  
        case GPSState.AUTHORIZED_ALWAYS:
          //TODO do something amazing with you app
          this.setState({gps:true});

        break;
  
        case GPSState.AUTHORIZED_WHENINUSE:
          //TODO do something amazing with you app
          this.setState({gps:true});
        break;
      }
    });
  }

  componentWillUnmount(){
    GPSState.removeListener();
  }

  _handleToggleSwitch = () => {
    this.setState(state =>({
      switchValue: !state.switchValue 
    }));
  }

_handleGpsSwitch = () => {
  GPSState.openSettings();
}

_handleNotificationsToggleSwitch = async () => {

  let sendNotifications = this.state.sendNotifications;
  if (sendNotifications == true) {
    // User wants to disable
    UltraAgentHelpers.showAlertWithOneButton("Information",
    "\nBy switching OFF notifications, you will not receive new notifications any more.\n\n"+
    "You can turn ON notifications any time and will continue to receive notifications from server.\n");

    let fcmToken = await AsyncStorage.getItem('fcmToken', null);
    if((this.loggedinprofile != undefined) && (this.loggedinprofile != null) && (this.loggedinprofile.userId != undefined) && (fcmToken != null)) {
      UltraAgentHelpers.unlinkFcmTokenFromUserUserId(fcmToken, this.loggedinprofile.userId);
    }

  } else {
    // User wants to enable
    UltraAgentHelpers.showAlertWithOneButton("Information",
    "\nBy switching ON notifications, you will start receiving notifications from server.\n\n"+
    "You can turn OFF notifications any time and will stop receiving notifications from server.\n");

    let fcmToken = await AsyncStorage.getItem('fcmToken', null);
    if((this.loggedinprofile != undefined) && (this.loggedinprofile != null) && (this.loggedinprofile.userId != undefined) && (fcmToken != null)) {
      UltraAgentHelpers.linkFcmTokenWithUserUserId(fcmToken, this.loggedinprofile.userId);
    }
  }

  this.setState({sendNotifications: !sendNotifications});
 
  firebase.firestore().collection('profiles').doc(this.loggedinprofile.userId).set(
      {sendNotifications:!sendNotifications}, {merge: true}
  ).then(() => {})
  .catch((err) => {
    console.log(err);
    UltraAgentHelpers.showAlertWithOneButton("Error",err.message);
  });



}
  
  render() {
    let { height, width } = Dimensions.get('window');
    return (
      <ImageBackground
      source={require('../images/timesheetbg.png')}
      style={{ width, height }}>
        <Header style={{backgroundColor:'#5271FF'}}>
        
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name="arrow-back" color='#fff' />         
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'Settings'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
        
        </Header>
        
        <Content>
          <View style={styles.settingContainer}>
            <List style={styles.listmarginStyle}>
              <Text style={styles.topTitleStyle}>Notifications</Text>
              <ListItem style={styles.settingListitem}>
                <Left>
                  <View style={styles.settingListStyle}>
                    <Text style={styles.fontBoldStyle}>Turn on /off</Text>
                    <Text style={styles.fontSizeStyle}>Recieve notifications or not</Text>
                  </View>
                </Left>
                <Right>
                  <Switch
                  onValueChange = {this._handleNotificationsToggleSwitch}
                  value = {this.state.sendNotifications} 
                  style={{ transform: [{ scaleX: .6 }, { scaleY: .6 }] }} />
                </Right>
              </ListItem>
              {/* <ListItem style={styles.settingListitem}>
                <Left>
                  <View style={styles.settingListStyle}>
                    <Text style={styles.fontBoldStyle}>Turn online /offline</Text>
                    <Text style={styles.fontSizeStyle}>Recieve notifications or not</Text>
                  </View>
                </Left>
                <Right>
                  <Switch
                  onValueChange = {this._handleToggleSwitch}
                  value = {this.state.notSwitch2Value} 
                  style={{ transform: [{ scaleX: .6 }, { scaleY: .6 }] }} />
                </Right>
              </ListItem> */}
              {/* <ListItem style={styles.settingListitem}>
                <Left>
                  <View style={styles.settingListStyle}>
                    <Text style={styles.fontBoldStyle}>Turn online /offline</Text>
                    <Text style={styles.fontSizeStyle}>Recieve notifications or not</Text>
                  </View>
                </Left>
                <Right>
                  <Switch
                  onValueChange = {this._handleToggleSwitch}
                  value = {this.state.notSwitch3Value} 
                  trackColor="#999"
                  style={{ transform: [{ scaleX: .6 }, { scaleY: .6 }] }} />
                </Right>
              </ListItem> */}
            </List>

            {/* <List style={styles.listmarginStyle}>
            <Text style={styles.topTitleStyle}>Location</Text>
              <ListItem style={styles.settingListitem}>
                <Left>
                  <View style={styles.settingListStyle}>
                    <Text style={styles.fontBoldStyle}>GPS</Text>
                    <Text style={styles.fontSizeStyle}>{this.state.gps == undefined? '': (this.state.gps == true? 'Disable GPS': 'Enable GPS')}</Text>
                  </View>
                </Left>
                <Right>
                  <Switch
                  onValueChange = {this._handleGpsSwitch}
                  value = {this.state.gps} 
                  style={{ transform: [{ scaleX: .6 }, { scaleY: .6 }] }} />
                </Right>
              </ListItem>
            </List> */}

            {/* <List style={styles.listmarginStyle}>
            <Text style={styles.topTitleStyle}>System</Text>
              <ListItem style={styles.settingListitem}>
                <Left>
                  <View style={styles.settingListStyle}>
                    <Text style={styles.fontBoldStyle}>Auto Run</Text>
                    <Text style={styles.fontSizeStyle}>Run Ultra Agent When the phone starts</Text>
                  </View>
                </Left>
                <Right>
                  <Switch
                  onValueChange = {this._handleToggleSwitch}
                  value = {this.state.sysSwitchValue} 
                  style={{ transform: [{ scaleX: .6 }, { scaleY: .6 }] }} />
                </Right>
              </ListItem>
            </List> */}
          </View>
        </Content>
     </ImageBackground>
     );
    }
  }
  const styles = StyleSheet.create({
    HeaderTitle:{
      color:'#fff',
    },
    SubHeaderTitle:{
      color:'#fff',
      fontSize:16,
      padding:5,
      paddingTop:6,
    },
    settingContainer:{
      padding:20,
      paddingTop:40,
    },
    settingListStyle:{
      marginLeft:5,
      paddingLeft:5,
    },
    settingListitem:{
      backgroundColor:'#f4f4f4',
      marginLeft:0,
    },
    fontBoldStyle:{
      fontWeight:'bold',
    },
    fontSizeStyle:{
      fontSize:12,
    },
    topTitleStyle:{
      fontWeight:'bold',
      marginBottom:5,
    },
    listmarginStyle:{
      marginBottom:30,
    },
})