import React, { Component } from 'react';
import { StyleSheet, Text, View, 
   Image, TextInput, TouchableOpacity, TouchableHighlight, ImageBackground, 
   ScrollView, Platform, Dimensions} from 'react-native';
//import Icon from 'react-native-ionicons';
import DatePicker from 'react-native-datepicker'
import { Container, Header, Content, Form, Item, Picker, Icon } from 'native-base';
import ProfileAsyncModel from "../asyncstoragemodels/ProfileAsyncModel";
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import UploadedObjectAsyncModel from '../asyncstoragemodels/UploadedObjectAsyncModel';
import KeyboardWrapperView from '../helpers/KeyboardWrapperView';
import Dialog, { DialogButton, DialogContent, DialogTitle, DialogFooter } from 'react-native-popup-dialog';

export default class PersonalDetails extends Component {

    state = { }
    profileModelRef = null;
    //dobDateObject = new Date();
    masterDataUpdatedCallbackForParent = undefined;

    citySelectionData = [];

    constructor(props) {
        super(props);
        // this.state = {
        //     selectedName: undefined,
        //     selectedCity: undefined,
        //     chosenDate: new Date()

        //   };
        let maxDateOfBirth = new Date();
        maxDateOfBirth.setFullYear(maxDateOfBirth.getFullYear() - 20); // 20 years back

        this.profileModelRef = new ProfileAsyncModel();
        this,this.profileModelRef.setErrorMessage("");
        // Use default values of model
        this.state = {
            ...this.profileModelRef.createState(), 
            spinner:false,
            spinnerMessage: '',
            maxDateOfBirth: maxDateOfBirth,
            termsofServiceDialog:false,
        };


        if (this.props.navigation.state.params != undefined) {
            this.masterDataUpdatedCallbackForParent = 
            this.props.navigation.state.params.masterDataUpdatedCallbackForParent;
        }

        this.citySelectionData = UltraAgentHelpers.cityDropdownData();

    }


    childUpdatedMasterDataCallback = () => {
        //console.log("PersonalDetails::childUpdatedMasterDataCallback");
        this._loadProfileModelFromAsyncStorage();
        if (this.masterDataUpdatedCallbackForParent != undefined) {
            this.masterDataUpdatedCallbackForParent();
        }
    }

    _loadProfileModelFromAsyncStorage() {
        //console.log("PersonalDetails::_loadProfileModelFromAsyncStorage");
        ProfileAsyncModel.restore().then((aRef) => {
            if (aRef!== null) {
                this.profileModelRef = aRef;
                this.setState ({... this.profileModelRef.createState()});
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    componentWillMount() {
        // Required for instancing of models objects.
        UploadedObjectAsyncModel.require(UploadedObjectAsyncModel);
        ProfileAsyncModel.require(ProfileAsyncModel);
 
        this._loadProfileModelFromAsyncStorage();
    }

    componentWillReceiveProps(nextProps){
        this._loadProfileModelFromAsyncStorage(); 
        //console.log("PersonalDetails::componentWillReceiveProps()");
        //console.log(this.state);
    }


    static navigationOptions = {
       // header:null
       headerTitle: 'PERSONAL DETAILS',
       headerStyle: {
        backgroundColor: '#5271FF',
      },
      headerBackTitle: null,
      headerTitleStyle: {
        color: "#fff",
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      headerTintColor: '#fff',
    }
    _onPressButtonNext = () => {

        if ((this.state.salutation == undefined) || (this.state.salutation.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("Salutation not specified",
            "Please select a valid Salutation");
            return;
        }
        if ((this.state.firstName == undefined) || (this.state.firstName.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("First Name not specified",
            "Please enter a valid First Name");
            return;
        }
        if ((this.state.lastName == undefined) || (this.state.lastName.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("Surname not specified",
            "Please enter a valid Surname");
            return;
        }
        if ((this.state.dateOfBirth == undefined) || (this.state.dateOfBirth.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("Date of birth not specified",
            "Please enter a valid Date of birth");
            return;
        }
        
        if ((this.state.phoneNumber == undefined) || (this.state.phoneNumber.length < 1)){
            UltraAgentHelpers.showAlertWithOneButton("Phone Number not specified",
            "Please enter a valid Phone Number");
            return;
        }
        if ((this.state.phoneNumber.length < 10) || (isNaN(this.state.phoneNumber))){
            UltraAgentHelpers.showAlertWithOneButton("Invalid",
            "Phone Number should be minimum 10 digits");
            return;
        }
        // if ((this.state.wInsuranceNumber == undefined) || (this.state.wInsuranceNumber.length < 1)) {
        //     UltraAgentHelpers.showAlertWithOneButton("Insurance Number not specified",
        //     "Please enter a valid Insurance Number");
        //     return;
        // }
        if ((this.state.address == undefined) || (this.state.address.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("Address not specified",
            "Please enter a valid Address");
            return;
        }
        if ((this.state.postcode == undefined) || (this.state.postcode.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("Postcode not specified",
            "Please enter a valid Postcode");
            return;
        }
        if ((this.state.city == undefined) || (this.state.city.length < 1)) {
            UltraAgentHelpers.showAlertWithOneButton("City not specified",
            "Please select a valid City");
            return;
        }

        //this.props.navigation.navigate('ProfessionalDetails');

        //console.log(this.dobDateObject);

        this.profileModelRef.setFirstName(this.state.firstName);
        this.profileModelRef.setLastName(this.state.lastName);
        this.profileModelRef.setSalutation(this.state.salutation);
        this.profileModelRef.setDateOfBirth(this.state.dateOfBirth);
        this.profileModelRef.setPhoneNumber(this.state.phoneNumber);
        // this.profileModelRef.setWInsuranceNumber(this.state.wInsuranceNumber);
        this.profileModelRef.setAddress(this.state.address);
        this.profileModelRef.setPostcode(this.state.postcode);
        this.profileModelRef.setCity(this.state.city);
       
        //console.log(this.profileModelRef.createState());
        // Create a Profile model object for aync storage based data propagation across screens
        this.profileModelRef.setErrorMessage("");
        this.profileModelRef.store().then(() => {

            // Notify Parent on Master Data Change
            if (this.masterDataUpdatedCallbackForParent != undefined) {
                this.masterDataUpdatedCallbackForParent();
            }
            this.props.navigation.navigate('ProfessionalDetails', {
                masterDataUpdatedCallbackForParent: this.childUpdatedMasterDataCallback.bind(this)
            });
        }).catch((error) => {
            UltraAgentHelpers.showAlertWithOneButton("Oops!","Unable to save email!\nContact support.");
        });
    

      }

      citiesList = () =>{
        console.log('citiesList');
        return( this.citySelectionData.map( (x,i) => { 
            if ((x.city == undefined) || (x.city.length < 1) || (x.city == '-none-')) {
              return( <Picker.Item label={x.display} key={i} value={x.city}  disabled={true}/>)
            } else {
              return( <Picker.Item label={x.display} key={i} value={x.city}  />)
            }
        }));      
    }

  render() {
    return (
        <View style={styles.wrapper}>
                <ImageBackground  style={{ flex:1, backgroundColor:'#fff'}}
                source={require('../images/personal-details-bg.jpg')} 
                imageStyle={{ resizeMode: 'cover' }}>
                    
                        <View style={styles.logoContainer}>
                            <Image style={styles.logo} 
                            source={require('../images/personaldetails.png')}/>
                            <Text style={styles.iconText}>
                                {'Ultra Agent jobs are so flexible \n Work fulltime or part time'}
                            </Text>
                            <Text style ={styles.iconBulletList}>
                                <Icon type="FontAwesome" name="circle-o" 
                                style={{fontSize: 12, color: '#df8c40', marginRight:3}}/>
                                <Text> </Text>
                                <Icon type="FontAwesome" name="circle" 
                                style={{fontSize: 12, color: '#df8c40', marginRight:3}}/>
                                <Text> </Text>
                                <Icon type="FontAwesome" name="circle-o" 
                                style={{fontSize: 12, color: '#df8c40'}}/>
                            </Text>
                            <View style={styles.triangleImageStyle}>
                                <Image style={{width:20, height:20}} 
                                source={require('../images/triangle50.png')}/>
                            </View>
                        </View>
                        <ScrollView>
                            <View style={styles.personalContainer}>
                    
                                <View style={styles.inputContainer}>
                                    <Image source={require('../images/mailicon.png')} 
                                    style={styles.InputImageIcon} />
                                    <TextInput 
                                        style={{flex:1}} 
                                        underlineColorAndroid={'transparent'}
                                        placeholder="joedoe@gmail.com" 
                                        placeholderTextColor = "#838383"
                                        keyboardType='email-address' 
                                        outoCorrect={false}
                                        onChangeText={email => this.setState({ email })}
                                        value={this.state.email}
                                        editable={false}
                                        selectTextOnFocus={false}
                                    />
                                </View>
                              
                                <View style={styles.InlineInputContainer}>    
                                    <View style={[styles.inputContainer, styles.inputRadiusRight]}>

                                        <Picker
                                            mode="dropdown"
                                            //style={styles.pikerDropdown}
                                            iosIcon={<Icon type="FontAwesome" name="caret-down" 
                                            style={{ color: "#5271FF", fontSize: 15, position:'absolute', left:50 }} />}
                                            style={ styles.saluPickerStyle}
                                            placeholder="Mr"
                                            placeholderStyle={{ color: "#ccc", fontSize:14, left:0 }}
                                            placeholderTextColor = "#ccc"
                                            selectedValue={this.state.salutation}
                                            onValueChange={salutation => this.setState({ salutation })}
                                        >
                                            <Picker.Item label="Mr" value="Mr" />
                                            <Picker.Item label="Mrs" value="Mrs" />
                                            <Picker.Item label="Miss" value="Miss" />
                                        </Picker>
                                        {/* <Icon type="FontAwesome" name="caret-down" 
                                        style={styles.pickerIcon}/> */}
                                        <TextInput 
                                            style={{flex:1}} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="First Name" 
                                            keyboardType='default' 
                                            placeholderTextColor = "#838383"
                                            outoCorrect={false}
                                            onChangeText={firstName => this.setState({ firstName })}
                                            value={this.state.firstName}
                                        />
                                    </View>
                                    <View style={[styles.inputContainer, styles.inputRadiusLeft]}>
                                       
                                        <TextInput 
                                            style={{flex:1}} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="Surname" 
                                            placeholderTextColor = "#838383"
                                            keyboardType='default' 
                                            outoCorrect={false}
                                            onChangeText={lastName => this.setState({ lastName })}
                                            value={this.state.lastName}
                                        />
                                    </View>
                                </View>  
                                <View style={[styles.inputContainer, styles.dateInputContainer]}>
                                <DatePicker
                                    style={{width: '100%', flex:1, height:30, bottom:5, paddingLeft:8}}
                                    date={this.state.dateOfBirth}
                                    mode="date"
                                    placeholder="Date Of Birth"
                                    placeholderTextColor = "#838383"
                                    format="DD-MM-YYYY"
                                    maxDate={this.state.maxDateOfBirth}
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    showIcon={false}
                                    customStyles={{
                                        dateInput: {
                                            alignItems : 'flex-start',
                                            borderColor: 'transparent', 
                                            borderWidth: 0,
                                            paddingLeft:8,
                                        }
                                    }}
                                    onDateChange={dateOfBirth => this.setState({dateOfBirth})} 
                                    ref={(ref)=>this.datePickerRef=ref}
                                />
                                <TouchableHighlight onPress={() => this.datePickerRef.onPressDate()} 
                                underlayColor='transparent'>
                                    <Image source={require('../images/datepicker-icon.png')} 
                                    style={[styles.InputImageIcon, styles.imageBg]}/>
                                </TouchableHighlight>
                                </View>
                                <View style={styles.inputContainer}>
                                    <Image source={require('../images/phone-icon.png')} 
                                    style={styles.InputImageIconsize} />

                                    <TextInput 
                                        style={{flex:1}} 
                                        underlineColorAndroid={'transparent'}
                                        placeholder="Phone Number" 
                                        placeholderTextColor = "#838383"
                                        keyboardType="number-pad"
                                        returnKeyType={ 'done' }
                                        outoCorrect={false}
                                        onChangeText={phoneNumber => this.setState({ phoneNumber })}
                                        value={this.state.phoneNumber}
                                    />

                                </View>
                                {/* <View style={styles.inputContainer}>
                                    <Image source={require('../images/insurance-icon.png')} 
                                    style={styles.InputImageIconsize} />

                                    <TextInput 
                                        style={{flex:1}} 
                                        underlineColorAndroid={'transparent'}
                                        placeholder="Insurance Number" 
                                        placeholderTextColor = "#838383"
                                        keyboardType='default' 
                                        outoCorrect={false}
                                        onChangeText={wInsuranceNumber => this.setState({ wInsuranceNumber })}
                                        value={this.state.wInsuranceNumber}
                                    />

                                </View> */}
                                <View style={styles.inputContainer}>
                                    <Image source={require('../images/address-icon.png')} 
                                    style={styles.InputImageIconsize} />

                                    <TextInput 
                                        style={{flex:1}} 
                                        underlineColorAndroid={'transparent'}
                                        placeholder="Address" 
                                        placeholderTextColor = "#838383"
                                        keyboardType='default' 
                                        outoCorrect={false}
                                        onChangeText={address => this.setState({ address })}
                                        value={this.state.address}
                                    />

                                </View>
                                <View style={styles.inputContainer}>
                                    <TextInput 
                                        style={{flex:1}} 
                                        underlineColorAndroid={'transparent'}
                                        placeholder="Postcode" 
                                        placeholderTextColor = "#838383"
                                        keyboardType='default' 
                                        outoCorrect={false}
                                        onChangeText={postcode => this.setState({ postcode })}
                                        value={this.state.postcode}
                                    />

                                </View>
                                <View style={styles.inputContainer}>
                                    <Picker
                                        mode="dropdown"
                                        iconRight
                                        //style={{ width: 320, left:-10}}
                                        iosIcon={<Icon type="FontAwesome" name="caret-down" 
                                        style={{ color: "#5271FF", fontSize: 15, position:'absolute', right:5 }} />}
                                        style={ styles.dropdownPickerStyle}
                                        itemDisabledTextStyle={{color:'grey'}}
                                        placeholder="City"
                                        placeholderStyle={{ color: "#838383", fontSize:14, left:0 }}
                                        placeholderTextColor = "#838383"
                                        selectedValue={this.state.city}
                                        onValueChange={(itemValue, itemIndex) => {
                                            // Since android is picking the disabled Item too, 
                                            // hence filter that here
                                            if ((itemValue == undefined) || (itemValue.length < 1) || (itemValue == '-none-')) {
                                            } else {
                                                this.setState({ city:itemValue });
                                            }
                                        }}
                                    >
                                        {/* <Picker.Item label="London" value="London" />
                                        <Picker.Item label="Birmingham" value="Birmingham" />
                                        <Picker.Item label="Leeds" value="Leeds" />
                                        <Picker.Item label="Glasgow" value="Glasgow" />
                                        <Picker.Item label="Sheffield" value="Sheffield" />
                                        <Picker.Item label="Bradford" value="Bradford" /> */}
                                        <Picker.Item label='Select a City' key='99999' value='-none-'  disabled={true}/>
                                        {this.citiesList()}

                                    </Picker>
                                    {/* <Icon type="FontAwesome" name="caret-down" 
                                    style={{fontSize: 15, color: '#5271FF', position:'absolute', right:15,}}/> */}
                               
                                </View>
                              
                                <TouchableHighlight onPress={this._onPressButtonNext} style={styles.button}>
                                    <Text style={styles.btntext}>
                                        NEXT
                                    </Text>
                                </TouchableHighlight>
                               
                            </View>

                            <View style={styles.linkContainer}>
                                <Text style={styles.regText}> <Icon type="FontAwesome" name="copyright" 
                                style={{fontSize: 14, color: '#000', marginRight:3}}/> 2019 UA technology .inc</Text>
                                <Text style={styles.regText} onPress={() => {
                                    this.setState({ termsofServiceDialog: true });
                                    }}
                                activeOpacity={0.7}>
                                    Privacy | Terms 
                                </Text>
                            </View>
                    </ScrollView>
                    <View>
              {/* Change password dialog container */}
                <Dialog
                  visible={this.state.termsofServiceDialog}
                  onTouchOutside={() => {
                      this.setState({ termsofServiceDialog: false });
                      }}
                  dialogTitle={
                    <DialogTitle title="Terms of service"
                      textStyle={{ color: '#000', fontSize:18 }}
                    />
                  }
                >
                  <TouchableOpacity onPress={() => {
                      this.setState({ termsofServiceDialog: false });
                      }}  style={{position: 'absolute', right: 15, top:5,
                      justifyContent: 'center'}}>
                    <Icon type="FontAwesome" name='close' 
                    style={{fontSize: 20, color: '#5271FF', marginRight:3, marginTop:10}}/>
                  </TouchableOpacity>
                
                  {/* <ScrollView> */}
                      <DialogContent style={{height:400}}>
                        <ScrollView>
                           <Text style={styles.termsHeading}>
                           Introduction</Text>
                           <Text style={styles.termsText}>
                            These terms and conditions apply between you, 
                            the User of this Website (including any sub-domains, unless expressly 
                            excluded by their own terms and conditions), and Ultra Agent Limited, 
                            the owner and operator of this Website. Please read these terms and 
                            conditions carefully, as they affect your legal rights. Your agreement 
                            to comply with and be bound by these terms and conditions is deemed to 
                            occur upon your first use of the Website. If you do not agree to be 
                            bound by these terms and conditions, you should stop using the Website 
                            immediately.
                           </Text>
                           <Text style={styles.termsText}>
                            In these terms and conditions, User or Users means any third party 
                            that accesses the Website and is not either (i) employed by Ultra 
                            Agent Limited and acting in the course of their employment or (ii) 
                            engaged as a consultant or otherwise providing services to Ultra 
                            Agent Limited and accessing the Website in connection with the 
                            provision of such services.
                           </Text>
                           <Text style={styles.termsText}>
                            You must be at least 16 years of age to use this Website. 
                            By using the Website and agreeing to these terms and conditions, 
                            you represent and warrant that you are at least 16 years of age.
                           </Text>
                           <Text style={styles.termsHeading}>
                            Intellectual property and acceptable use
                           </Text>
                           <Text style={styles.termsText}>
                            1. All Content included on the Website, unless uploaded by Users, is the property of 
                            Ultra Agent Limited, our affiliates or other relevant third parties. In these terms 
                            and conditions, Content means any text, graphics, images, audio, video, software, 
                            data compilations, page layout, underlying code and software and any other form of 
                            information capable of being stored in a computer that appears on or forms part of this
                             Website, including any such content uploaded by Users. By continuing to use 
                             the Website you acknowledge that such Content is protected by copyright, trademarks, 
                             database rights and other intellectual property rights. Nothing on this site shall be 
                             construed as granting, by implication, estoppel, or otherwise, any license or right to 
                             use any trademark, logo or service mark displayed on the site without the owner's prior 
                             written permission
                           </Text>
                           <Text style={styles.termsText}>
                            2. You may, for your own personal, non-commercial use only, do the following :
                           </Text>
                           <View style={{marginLeft:10}}>
                            <Text style={styles.termsText}>
                              a. retrieve, display and view the Content on a computer screen
                            </Text>
                            <Text style={styles.termsText}>
                              b. download and store the Content in electronic form on a disk (but not on any server or other storage device connected to a network)
                            </Text>
                            <Text style={styles.termsText}>
                              c. print one copy of the Content
                            </Text>
                           </View>
                           <Text style={styles.termsText}>
                            3. You must not otherwise reproduce, modify, copy, distribute or use for commercial purposes any Content without the written permission of Ultra Agent Limited.
                           </Text>
                           <Text style={styles.termsText}>
                           4. You acknowledge that you are responsible for any Content you 
                           may submit via the Website, including the legality, reliability, 
                           appropriateness, originality and copyright of any such Content. 
                           You may not upload to, distribute or otherwise publish through the
                            Website any Content that (i) is confidential, proprietary, false, 
                            fraudulent, libellous, defamatory, obscene, threatening, invasive 
                            of privacy or publicity rights, infringing on intellectual property 
                            rights, abusive, illegal or otherwise objectionable; (ii) may 
                            constitute or encourage a criminal offence, violate the rights of 
                            any party or otherwise give rise to liability or violate any law; or 
                            (iii) may contain software viruses, political campaigning, chain 
                            letters, mass mailings, or any form of "spam." You may not use a 
                            false email address or other identifying information, impersonate 
                            any person or entity or otherwise mislead as to the origin of any 
                            content. You may not upload commercial content onto the Website.
                           </Text>
                           <Text style={styles.termsText}>
                            5. You represent and warrant that you own or otherwise control all 
                            the rights to the Content you post; that the Content is accurate; 
                            that use of the Content you supply does not violate any provision 
                            of these terms and conditions and will not cause injury to any 
                            person; and that you will indemnify Ultra Agent Limited for all 
                            claims resulting from Content you supply.
                           </Text>
                           <Text style={styles.termsHeading}>Prohibited use</Text>
                           <Text style={styles.termsText}>6. You may not use the Website 
                           for any of the following purposes:</Text>
                           <View style={{marginLeft:10}}>
                            <Text style={styles.termsText}>a. in any way which causes, or may cause, damage to the 
                            Website or interferes with any other person's use or enjoyment 
                            of the Website;
                            </Text>
                            <Text style={styles.termsText}>b. in any way which is harmful, 
                            unlawful, illegal, abusive, harassing, threatening or otherwise 
                            objectionable or in breach of any applicable law, regulation, 
                            governmental order;
                            </Text>
                            <Text style={styles.termsText}>c. making, transmitting or storing 
                            electronic copies of Content protected by copyright without the 
                            permission of the owner.</Text>
                           </View>
                           <Text style={styles.termsHeading}>Registration</Text>
                           <Text style={styles.termsText}>7. You must ensure that the details 
                           provided by you on registration or at any time are correct and 
                           complete.</Text>
                           <Text style={styles.termsText}>8. You must inform us immediately of 
                           any changes to the information that you provide when registering by 
                           updating your personal details to ensure we can communicate with you
                            effectively.
                          </Text>
                          <Text style={styles.termsText}>9. We may suspend or cancel your 
                          registration with immediate effect for any reasonable purposes or if
                           you breach these terms and conditions.
                          </Text>
                          <Text style={styles.termsText}>10. You may cancel your registration at 
                          any time by informing us in writing to the address at the end of these 
                          terms and conditions. If you do so, you must immediately stop using the 
                          Website. Cancellation or suspension of your registration does not affect 
                          any statutory rights.
                          </Text>
                          <Text style={styles.termsHeading}>Password and security</Text>
                          <Text style={styles.termsText}>11. When you register on this Website, 
                          you will be asked to create a password, which you should keep 
                          confidential and not disclose or share with anyone.
                          </Text>
                          <Text style={styles.termsText}>12. If we have reason to believe that 
                          there is or is likely to be any misuse of the Website or breach of 
                          security, we may require you to change your password or suspend your 
                          account.
                          </Text>
                          <Text style={styles.termsHeading}>Links to other websites</Text>
                          <Text style={styles.termsText}>13. This Website may contain links to 
                          other sites. Unless expressly stated, these sites are not under the 
                          control of Ultra Agent Limited or that of our affiliates.
                          </Text>
                          <Text style={styles.termsText}>14. We assume no responsibility for the 
                          content of such Websites and disclaim liability for any and all forms 
                          of loss or damage arising out of the use of them.
                          </Text>
                          <Text style={styles.termsText}>15. The inclusion of a link to another 
                          site on this Website does not imply any endorsement of the sites 
                          themselves or of those in control of them.
                          </Text>
                          <Text style={styles.termsHeading}>Privacy Policy and Cookies Policy
                          </Text>
                          <Text style={styles.termsText}>16. Use of the Website is also governed by our Privacy 
                          Policy and Cookies Policy, which are incorporated into these terms and conditions by this 
                          reference. To view the Privacy Policy and Cookies Policy, please click on 
                          the following: _______________ and _______________.
                          </Text>
                          <Text style={styles.termsHeading}>Availability of the Website and 
                          disclaimers</Text>
                          <Text style={styles.termsText}>17. Any online facilities, tools, 
                          services or information that Ultra Agent Limited makes available 
                          through the Website (the Service) is provided "as is" and on an 
                          "as available" basis. We give no warranty that the Service will be 
                          free of defects and/or faults. To the maximum extent permitted by the 
                          law, we provide no warranties (express or implied) of fitness for a 
                          particular purpose, accuracy of information, compatibility and 
                          satisfactory quality. Ultra Agent Limited is under no obligation to 
                          update information on the Website.
                          </Text>
                          <Text style={styles.termsText}>18. Whilst Ultra Agent Limited uses 
                          reasonable endeavours to ensure that the Website is secure and free 
                          of errors, viruses and other malware, we give no warranty or guaranty 
                          in that regard and all Users take responsibility for their own 
                          security, that of their personal details and their computers.
                          </Text>
                          <Text style={styles.termsText}>19. Ultra Agent Limited accepts no 
                          liability for any disruption or non-availability of the Website.
                          </Text>
                          <Text style={styles.termsText}>20. Ultra Agent Limited reserves the 
                          right to alter, suspend or discontinue any part (or the whole of) 
                          the Website including, but not limited to, any products and/or 
                          services available. These terms and conditions shall continue to apply 
                          to any modified version of the Website unless it is expressly stated 
                          otherwise.
                          </Text>
                          <Text style={styles.termsHeading}>Limitation of liability</Text>
                          <Text style={styles.termsText}>21. Nothing in these terms and conditions will: (a) 
                          limit or exclude our or your liability for death or personal injury resulting from 
                          our or your negligence, as applicable; (b) limit or exclude our or your liability 
                          for fraud or fraudulent misrepresentation; or (c) limit or exclude any of our or your 
                          liabilities in any way that is not permitted under applicable law.</Text>
                          <Text style={styles.termsText}>22. We will not be liable to you in 
                          respect of any losses arising out of events beyond our reasonable
                           control.
                          </Text>
                          <Text style={styles.termsText}>23. To the maximum extent permitted by 
                          law, Ultra Agent Limited accepts no liability for any of the following:
                          </Text>
                          <View style={{marginLeft:10}}>
                           <Text style={styles.termsText}>a. any business losses, such as loss of 
                           profits, income, revenue, anticipated savings, business, contracts, 
                           goodwill or commercial opportunities;
                            </Text>
                           <Text style={styles.termsText}>b. loss or corruption of any data, 
                           database or software;
                            </Text>
                           <Text style={styles.termsText}>c. any special, indirect or 
                           consequential loss or damage.
                            </Text>
                          </View>
                          <Text style={styles.termsHeading}>General</Text>
                          <Text style={styles.termsText}>24. You may not transfer any of your 
                          rights under these terms and conditions to any other person. We may 
                          transfer our rights under these terms and conditions where we 
                          reasonably believe your rights will not be affected.
                          </Text>
                          <Text style={styles.termsText}>25. These terms and conditions may be 
                          varied by us from time to time. Such revised terms will apply to the 
                          Website from the date of publication. Users should check the terms and 
                          conditions regularly to ensure familiarity with the then current 
                          version.
                          </Text>
                          <Text style={styles.termsText}>26. These terms and conditions together 
                          with the Privacy Policy and Cookies Policy contain the whole agreement 
                          between the parties relating to its subject matter and supersede all 
                          prior discussions, arrangements or agreements that might have taken 
                          place in relation to the terms and conditions.
                          </Text>
                          <Text style={styles.termsText}>27. The Contracts (Rights of Third 
                          Parties) Act 1999 shall not apply to these terms and conditions and no 
                          third party will have any right to enforce or rely on any provision of 
                          these terms and conditions.
                          </Text>
                          <Text style={styles.termsText}>28. If any court or competent 
                          authority finds that any provision of these terms and conditions 
                          (or part of any provision) is invalid, illegal or unenforceable, that 
                          provision or part-provision will, to the extent required, be deemed to
                           be deleted, and the validity and enforceability of the other 
                           provisions of these terms and conditions will not be affected.
                          </Text>
                          <Text style={styles.termsText}>29. Unless otherwise agreed, no delay, 
                          act or omission by a party in exercising any right or remedy will be 
                          deemed a waiver of that, or any other, right or remedy.
                          </Text>
                          <Text style={styles.termsText}>30. This Agreement shall be governed 
                          by and interpreted according to the law of England and Wales and all 
                          disputes arising under the Agreement (including non-contractual 
                          disputes or claims) shall be subject to the exclusive jurisdiction of 
                          the English and Welsh courts.
                          </Text>
                          <Text style={styles.termsHeading}>Ultra Agent Limited details</Text>
                          <Text style={styles.termsText}>31. Ultra Agent Limited is a company 
                          incorporated in England and Wales with registered number 10510063 whose 
                          registered address is 9 Rotherwick hill, Ealing, London, W5 3EQ and it 
                          operates the Website www.ultraagent.co.uk.
                          </Text>
                          <Text style={styles.termsText}>You can contact Ultra Agent Limited by
                           email on Hello@ultragent.co.uk.
                          </Text>
                          <Text style={styles.termsHeading}>Attribution</Text>
                          <Text style={styles.termsText}>32. These terms and conditions were 
                          created using a document from Rocket Lawyer</Text> 
                          {/* <Text onPress={()=>{Linking.openURL('https://www.rocketlawyer.co.uk')}>(https://www.rocketlawyer.co.uk).</Text>
                          <Text onPress={()=>{Linking.openURL('https://www.rocketlawyer.co.uk')}>My Text</Text> */}
                          <TouchableOpacity onPress={() => Linking.openURL('https://www.rocketlawyer.co.uk')} >
                            <Text style={{color:'#5271FF'}}>
                              https://www.rocketlawyer.co.uk                            
                            </Text>
                          </TouchableOpacity>
                        </ScrollView>
                      </DialogContent>
                  {/* </ScrollView> */}
              </Dialog>
            </View> 
                </ImageBackground>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container:{
    flex:1,
    alignItems: 'center',
    alignSelf:'stretch',
    backgroundColor:'#fff',
    width: null,
    height:null,
  },
  logoContainer:{
    alignItems: 'center',
    alignSelf:'stretch',
    paddingLeft:20,
    paddingRight:20,
    backgroundColor:'#fff',
    padding:10,
  },
  logo:{
    width:120,
    height:100,
    padding:50,
  },
  iconText:{
    color:'#000',
    textAlign:'center',
    alignItems: 'center',
    marginTop:10,
    lineHeight:20,
    
    ...Platform.select({
        ios: {
            fontWeight:"500",
        },
        android: {
            fontWeight:"400",
        },
    })
  },
  iconBulletList:{
    marginTop:5,
  },
  iconBullet:{
    marginLeft:5,
  },
  triangleImageStyle:{
    position:'absolute', 
    left:0, 
    right:0, 
    bottom:-12, 
    alignItems: 'center', 
  },
  personalContainer:{
    alignItems: 'stretch',
    backgroundColor:'transparent',
    padding:15,
    paddingBottom:10
  },
  inputContainer: {
    flex: 1, 
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf:'stretch',
    padding:8,
    backgroundColor:'#fff',
    borderRadius:2,
    marginBottom:8,
    ...Platform.select({
        ios: {
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.1,
            height:40,
        },
        android: {
            elevation:2,
            height:40,
            paddingBottom:0,
            paddingTop:0,
        },
    })
},
dropdownPickerStyle:{
    ...Platform.select({
        ios: {
            width: Dimensions.get('window').width-25, 
            left:-10,
        },
        android: {
            width: undefined, 
            left:0,
        },
    })
    
},
dateInputContainer:{
    padding:0,
},
InlineInputContainer:{
    flex: 1,
    flexDirection: "row",
},
inputRadiusLeft:{
    borderTopLeftRadius:0,
    borderBottomLeftRadius:0,
},
inputRadiusRight:{
    borderTopRightRadius:0,
    borderBottomRightRadius:0,
    borderRightColor: '#bbb',
    borderRightWidth: StyleSheet.hairlineWidth,
    ...Platform.select({
        android: {
            borderRightColor: '#bbb',
            borderRightWidth: 1,
        },
    })
},
InputImageIcon: {
    marginRight:10,
    marginLeft:5,
    height: 20,
    width: 20,
    resizeMode : 'stretch',
    alignItems: 'center'
},
InputImageIconsize:{
    marginRight:10,
    marginLeft:5,
    height: 22,
    width: 22,
    resizeMode : 'stretch',
    alignItems: 'center'
},
imageBg:{
    backgroundColor:'#efefef',
    marginRight:0,
    padding:20,
    height: 10,
    width: 10,
},
button:{
    alignSelf:'stretch',
    marginLeft:50,
    marginRight:50,
    marginTop:10,
    backgroundColor:'#5271FF',
    padding:12,
    borderRadius:3,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:0
  },
  btntext:{
    textAlign:'center',
    alignSelf:'stretch',
    alignItems: 'center',
    color:'#fff',
    fontWeight:'500',
    fontSize:14,
    letterSpacing:1,
    color:'#fff',
  },
  
  linkContainer:{
    alignSelf:'stretch',
    alignItems:'center',
    marginBottom:10,
  },
 
  regText:{
    marginTop:5,
    color:'#000',
  },
  
  saluPickerStyle:{
    ...Platform.select({
        ios: {
            width: 70, 
            left:-10,
        },
        android: {
            width: 40, 
            left:0,
        },
    })
    
},
  
});