import React, { Component } from 'react';
import {
  StyleSheet, View, Image, ImageBackground, Text, 
  Dimensions, Switch, Share
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, List, ListItem, Content, Header, Body
} from 'native-base';
import Icon from 'react-native-ionicons';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

import firebase from 'react-native-firebase';


export default class InviteFriend extends Component {

  constructor(props) {
    super(props);

    this.loggedinprofile = SingletonClass.get('loggedinprofile');

    this.state = {
    
    };
    this.onShare = this.onShare.bind(this);

  }

  componentDidMount() {

    
   
    
    /*
    BHARAT: THIS REQUIRES GOOGLE SIGNIN HENCE DROPPED
    var invitation = new firebase.invites.Invitation(title, message);
    firebase.invites().sendInvitation(invitation)
    .then((aObj) => {
      console.log('sendInvitation returned');
      console.log(aObj);
    })
    .catch((error) => {
      console.log(error);
      UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
    });
    */

    this.onShare();

  }

  onShare = async () => {

    var title = this.loggedinprofile.firstName + ' ' + this.loggedinprofile.lastName +
      ' - UltraAgent';
    var message = this.loggedinprofile.firstName + ' ' + this.loggedinprofile.lastName +
      ' invites you to start using UltraAgent';
    var dynamicLinkUrl = 'https://ultraagentworker.page.link/y6N7';
    

    try {
      const result = await Share.share({
        message: message,
        url: dynamicLinkUrl,
        title: title,
        subject:title,
      }, {
        // Android only:
        dialogTitle: 'Invite Friends',
        // iOS only:
        excludedActivityTypes: []
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
          UltraAgentHelpers.showAlertWithOneButton("Your invitations have been sent");
          //this.props.navigation.navigate('Home');
          this.props.navigation.goBack(this.props.navigation.state.key);
        } else {
          // shared
          // UltraAgentHelpers.showAlertWithOneButton("Your invitations have been sent");
          //this.props.navigation.navigate('Home');
          this.props.navigation.goBack(this.props.navigation.state.key);
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
        //this.props.navigation.navigate('Home');
        this.props.navigation.goBack(this.props.navigation.state.key);
      }
    } catch (error) {
      //alert(error.message);
      UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
      //this.props.navigation.navigate('Home');
      this.props.navigation.goBack(this.props.navigation.state.key);
    }
  };

  
  render() {
    let { height, width } = Dimensions.get('window');
    return (
      <ImageBackground
      source={require('../images/timesheetbg.png')}
      style={{ width, height }}>
        <Header style={{backgroundColor:'#5271FF', borderBottomColor:'transparent'}}>
        
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name="arrow-back" color='#fff' />         
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'invite a friend'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
        
        </Header>

        <Content style={{padding:0, margin:0}}>
          <View style={styles.inviteLogoContainer}>
            <Image style={styles.logo} 
            source={require('../images/invitefriendlogo.png')}/>
          </View>
          <View style={styles.inviteIconContainer}>
              <List style={styles.listmarginStyle}>
                <ListItem style={styles.settingListitem}>
                  <Left>
                    <Image style={{width:40, height:40}} 
                      source={require('../images/messageboxicons.png')}/>
                  </Left>
                  <Body>
                      <Text style={[styles.fontBoldStyle, styles.mTextColor]}>Message</Text>
                  </Body>
                  <Right>
                  </Right>
                </ListItem>
                <ListItem style={styles.settingListitem}>
                  <Left>
                    <Image style={{width:40, height:40}} 
                      source={require('../images/fbicons.png')}/>
                  </Left>
                  <Body>
                      <Text style={[styles.fontBoldStyle, styles.fbTextColor]}>Facebook</Text>
                  </Body>
                  <Right>
                  </Right>
                </ListItem>
                <ListItem style={styles.settingListitem}>
                  <Left>
                    <Image style={{width:40, height:40}} 
                      source={require('../images/googleicons.png')}/>
                  </Left>
                  <Body>
                      <Text style={[styles.fontBoldStyle, styles.gTextColor]}>Google+</Text>
                  </Body>
                  <Right>
                  </Right>
                </ListItem>
                <ListItem style={styles.settingListitem}>
                  <Left>
                    <Image style={{width:40, height:40}} 
                      source={require('../images/gmailicons.png')}/>
                  </Left>
                  <Body>
                      <Text style={[styles.fontBoldStyle, styles.gmTextColor]}>Gmail</Text>
                  </Body>
                  <Right>
                  </Right>
                </ListItem>
                <ListItem style={styles.settingListitem}>
                  <Left>
                  <Image style={{width:40, height:40}} 
                      source={require('../images/twittericons.png')}/>
                  </Left>
                  <Body>
                      <Text style={[styles.fontBoldStyle, styles.twTextColor]}>Twitter</Text>
                  </Body>
                  <Right>
                  </Right>
                </ListItem>
                <ListItem style={styles.settingListitem}>
                  <Left>
                  <Image style={{width:40, height:40}} 
                      source={require('../images/whatsappicons.png')}/>
                  </Left>
                  <Body>
                      <Text style={[styles.fontBoldStyle, styles.whTextColor]}>Whatsapp</Text>
                  </Body>
                  <Right>
                  </Right>
                </ListItem>
              </List>
            </View>
        </Content>
     </ImageBackground>
     );
    }
  }
  const styles = StyleSheet.create({
    HeaderTitle:{
      color:'#fff',
    },
    inviteLogoContainer:{
      alignItems: 'center',
      alignSelf:'stretch',
      backgroundColor:'#5271FF',
      paddingBottom:80,
      paddingTop:0,
    },
    logo:{
        width:377,
        height:273,
    },
    inviteIconContainer:{
      backgroundColor:'#f4f4f4',
      bottom:65,
      marginLeft:20,
      marginRight:20,
      borderRadius:4,
    },
    fontColorStyle:{
      color:'#939393',
    },
    fontBoldStyle:{
      fontWeight:'bold',
      alignItems:'flex-end',
    },
    settingListitem:{
      paddingBottom:5,
      paddingTop:5,
    },
    mTextColor:{
      color:'#939393'
    },
    fbTextColor:{
      color:'#4267b2'
    },
    gTextColor:{
      color:'#dc4a38'
    },
    gmTextColor:{
      color:'#f14336'
    },
    twTextColor:{
      color:'#26a6d1'
    },
    whTextColor:{
      color:'#4caf50'
    },
})