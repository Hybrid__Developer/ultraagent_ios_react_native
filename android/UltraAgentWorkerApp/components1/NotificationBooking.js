import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Alert, Text,
  
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, Picker, List, ListItem, Content, Item, Header, Body
} from 'native-base';
import Icon  from 'react-native-ionicons';


import firebase, { RNFirebase } from 'react-native-firebase';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import Spinner from 'react-native-loading-spinner-overlay';

export default class NotificationBooking extends Component {

 
  loggedinprofile={};

  unsubscribeRequestCollection = undefined;
  unsubscribeBookingCollection = undefined;
  unsubscribeShiftCollection = undefined;
  unsubscribeCompanyCollection = undefined;

    constructor(props) {
        super(props);

        this.loggedinprofile = SingletonClass.get('loggedinprofile');

        notice = 
            this.props.navigation.state.params.notice;
            console.log('NotificationBooking:'+notice);

        this.state = {
           notice:notice,
           spinner:false,
            spinnerMessage: '',
           statusMessage:'',
           statusColor:'#08c903', // green color
        };
      }
      
    static navigationOptions = {
        header:null
    //     headerTitle: 'PLUMBING #1038',
    //     headerStyle: {
    //       backgroundColor: '#5271FF',
    //     },
    //     headerTitleStyle: {
    //       color: "#fff",
    //       flex:1,
    //       alignItems: 'center',
    //       justifyContent: 'center',
    //    },
    //    headerTintColor: '#fff',
      }

      componentDidMount() {
        if (this.state.notice.bookingRequestId != undefined) {
          this.unsubscribeRequestCollection = firebase.firestore().collection('bookingrequests')
          .doc(this.state.notice.bookingRequestId)
          .onSnapshot(this.onRequestCollectionUpdate) 
        }
        if (this.state.notice.bookingId != undefined) {
          this.unsubscribeBookingCollection = firebase.firestore().collection('bookings')
          .doc(this.state.notice.bookingId)
          .onSnapshot(this.onBookingCollectionUpdate) 
        }

        // Mark as read
        if (this.state.notice.isRead == false) {
          firebase.firestore().collection('profiles')
          .doc(this.loggedinprofile.userId)
          .collection('notices').doc(this.state.notice.id).update({isRead: true})
          .then()
          .catch((err) => {console.error(err)});
        }
      }
      
      componentWillUnmount() {
        if (this.unsubscribeRequestCollection != undefined) {
          this.unsubscribeRequestCollection();
        }
        if (this.unsubscribeBookingCollection != undefined) {
          this.unsubscribeBookingCollection();
        }
        if (this.unsubscribeShiftCollection != undefined) {
          this.unsubscribeShiftCollection();
        }
        if (this.unsubscribeCompanyCollection != undefined) {
          this.unsubscribeCompanyCollection();
        }
      }

      onRequestCollectionUpdate = (querySnapshot) => {
        if ((querySnapshot != undefined) && (querySnapshot != null) &&
            (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
        {
          var aObj = querySnapshot._data;
          aObj.id = querySnapshot.id; 
          console.log('onRequestCollectionUpdate');
          console.log(aObj);

          this.setState({request:aObj});

          this.unsubscribeShiftCollection = firebase.firestore().collection('shifts')
          .doc(aObj.shiftId)
          .onSnapshot(this.onShiftCollectionUpdate) 

          this.unsubscribeCompanyCollection = firebase.firestore().collection('companies')
          .doc(aObj.companyId)
          .onSnapshot(this.onCompanyCollectionUpdate) 

          if ((this.state.request.approvalStatus != undefined) && (this.state.request.approvalStatus == 'approved')) {
            this.setState({
              statusMessage:'Booking Accepted',
              statusColor:'#08c903', // green color
            });
          } else if ((this.state.request.approvalStatus != undefined) && (this.state.request.approvalStatus == 'cancelled')) {
            this.setState({
              statusMessage:'Booking Cancelled',
              statusColor:'#f80808', // red color
            });
          } else {
            this.setState({
              statusMessage:'',
              statusColor:'#08c903', // green color
            });
          }
        }

      }

      onBookingCollectionUpdate = (querySnapshot) => {
        if ((querySnapshot != undefined) && (querySnapshot != null) &&
            (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
        {
          var aObj = querySnapshot._data;
          aObj.id = querySnapshot.id; 
          console.log('onBookingCollectionUpdate');
          console.log(aObj);

          this.setState({booking:aObj});

          this.unsubscribeShiftCollection = firebase.firestore().collection('shifts')
          .doc(aObj.shiftId)
          .onSnapshot(this.onShiftCollectionUpdate) 

          this.unsubscribeCompanyCollection = firebase.firestore().collection('companies')
          .doc(aObj.companyId)
          .onSnapshot(this.onCompanyCollectionUpdate) 

          if ((this.state.booking.paymentStatus != undefined) && (this.state.booking.paymentStatus == 'paid')) {
            this.setState({
              statusMessage:'Shift Paid',
              statusColor:'#08c903', // green color
            });
          } else if ((this.state.booking.timesheetStatus != undefined) && (this.state.booking.timesheetStatus == 'approved')) {
            this.setState({
              statusMessage:'Timesheet Approved',
              statusColor:'#08c903', // green color
            });
          } else if ((this.state.booking.timesheetStatus != undefined) && (this.state.booking.timesheetStatus == 'amendresolved')) {
            this.setState({
              statusMessage:'Timesheet Accepted',
              statusColor:'#08c903', // green color
            });
          } else if ((this.state.booking.timesheetStatus != undefined) && (this.state.booking.timesheetStatus == 'disputeresolved')) {
            this.setState({
              statusMessage:'Dispute Resolved',
              statusColor:'#08c903', // green color
            });
          } else if ((this.state.booking.status != undefined) && (this.state.booking.status == 'cancelled')) {
            this.setState({
              statusMessage:'Booking Cancelled',
              statusColor:'#f80808', // red color
            });
          } else if ((this.state.booking.workingStatus != undefined) && (this.state.booking.workingStatus == 'completed')) {
            this.setState({
              statusMessage:'Timesheet Pending',
              statusColor:'#08c903', // green color
            });
          } else if ((this.state.booking.workingStatus != undefined) && (this.state.booking.workingStatus != 'completed')) {
            this.setState({
              statusMessage:'In Progress',
              statusColor:'#f80808', // red color
            });
          } else {
            this.setState({
              statusMessage:'Booking Accepted',
              statusColor:'#08c903', // green color
            });
          }
        }
      }

      onShiftCollectionUpdate = (querySnapshot) => {
        if ((querySnapshot != undefined) && (querySnapshot != null) &&
            (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
        {
          var aObj = querySnapshot._data;
          aObj.id = querySnapshot.id; 
          console.log('onShiftCollectionUpdate');
          console.log(aObj);

          this.setState({shift:aObj});
        }
      }

      onCompanyCollectionUpdate = (querySnapshot) => {
        if ((querySnapshot != undefined) && (querySnapshot != null) &&
            (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
        {
          var aObj = querySnapshot._data;
          aObj.id = querySnapshot.id; 
          console.log('onCompanyCollectionUpdate');
          console.log(aObj);

          this.setState({company:aObj});
        }
      }

      _onPressButtonDelete = () => {
        Alert.alert(
          'Confirmation!',
          'Are you sure you want to delete this entry?',
          [
            {text: 'No, Cancel', onPress: () => {/*cancelled*/}, style: 'cancel'},
            //{text: 'OK', onPress: () => console.log('OK Pressed')},*/
            {text: "Yes, Delete", style:'destructive', onPress: () => {
              this.setState({spinner: true, spinnerMessage:'Deleting...'});
              firebase.firestore().collection('profiles').doc(this.loggedinprofile.userId)
              .collection('notices').doc(this.state.notice.id).delete()
              .then(() => {
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Success",'Entry deleted successfully');
                this.props.navigation.navigate('Notifications');
              })
              .catch((error) => {
                console.log(error);
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
              });
            }}
          ],
          { cancelable: false }
        );
        
      }
      _onPressButtonDecline = () => {

        Alert.alert(
          'Confirmation!',
          'Are you sure you want to decline this booking?',
          [
            {text: 'No, Cancel', onPress: () => {/*cancelled*/}, style: 'cancel'},
            //{text: 'OK', onPress: () => console.log('OK Pressed')},*/
            {text: "Yes, Decline", style:'destructive', onPress: () => {
              this.setState({spinner: true, spinnerMessage:'Declining...'});
              firebase.firestore().collection('bookingrequests').doc(this.state.request.id)
              .set({approvalStatus:'rejected'},{merge:true})
              .then(() => {

                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Success",'Booking declined successfully');
                this.props.navigation.navigate('Notifications');

              })
              .catch((error) => {
                console.log(error);
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
              });
            }}
          ],
          { cancelable: false }
        );

      }

      _onPressButtonAccept = () => {
        Alert.alert(
          'Confirmation!',
          'Are you sure you want to accept this booking?',
          [
            {text: 'No, Cancel', onPress: () => {/*cancelled*/}, style: 'cancel'},
            //{text: 'OK', onPress: () => console.log('OK Pressed')},*/
            {text: "Yes, Accept", style:'default', onPress: () => {
              this.setState({spinner: true, spinnerMessage:'Accepting...'});
              firebase.firestore().collection('shifts').doc(this.state.shift.id)
              .set({recruitmentStatus:'recruited'},{merge:true})
              .then(() => {

                firebase.firestore().collection('bookingrequests').doc(this.state.request.id)
                .set({approvalStatus:'approved'},{merge:true})
                .then(() => {

                  firebase.firestore().collection('bookings').doc(this.state.request.id)
                  .set({
                    status:'active',
                    createdBy:this.loggedinprofile.userId,
      
                    shiftId: this.state.shift.id,
                    shiftCategory:this.state.shift.category,
                    shiftSubCategory:this.state.shift.subcategory,
                    shiftNo:this.state.shift.shiftNo,
                    shiftStartDateTime: this.state.shift.startDateTime,
                    fee:this.state.shift.fee,
                    description:this.state.shift.description,
                    city:this.state.shift.city,
                    lat:this.state.shift.lat,
                    lng:this.state.shift.lng,
                    duration:this.state.shift.duration,
                    address:this.state.shift.address,
                    contactpername:this.state.shift.contactpername,
                    contactphonenumber: this.state.shift.phoneNumber,
      
                    workerFirstName:this.loggedinprofile.firstName,
                    workerLastName:this.loggedinprofile.lastName,
                    workerPhoneNumber:this.loggedinprofile.phoneNumber,
                    workerProfilePicture:this.loggedinprofile.profilePicture,
                    workerId: this.loggedinprofile.userId,
      
                    companyProfilePicture:this.state.company.profilePicture,
                    companyName:this.state.company.name,
                    companyId:this.state.company.id,
      
                    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                  },{merge:true})
                  .then(() => {

                    this.setState({spinner: false, spinnerMessage:''});
                    UltraAgentHelpers.showAlertWithOneButton("Success",'Booking accepted successfully');
                    this.props.navigation.navigate('Notifications');
                    
                  })
                  .catch((error) => {
                    console.log(error);
                    this.setState({spinner: false, spinnerMessage:''});
                    UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                  });

                })
                .catch((error) => {
                  console.log(error);
                  this.setState({spinner: false, spinnerMessage:''});
                  UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                });

              })
              .catch((error) => {
                console.log(error);
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
              });
            }}
          ],
          { cancelable: false }
        );
       }

render() {
    return (
      <Container>
          <Header style={{backgroundColor:'#5271FF'}}>
         
         <Left style={{flex:1}}>
             <Button transparent
             onPress={() => this.props.navigation.goBack(this.props.navigation.state.key)}>
                <Icon name="arrow-back" color='#fff' />  
             </Button>
         </Left>
         <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
             <Title style={styles.HeaderTitle}>{this.state.notice.companyTitle.toUpperCase()}</Title>
         </Body>
         <Right style={{flex:1}}>
            <Button transparent onPress={this._onPressButtonDelete} 
            style={{paddingLeft:0, paddingRight:0,
            marginLeft:0, marginRight:0}}>
                <Image source={require('../images/deleteicon.png')} 
                style={{height:50, width:20, alignItems:'center'}} />
            </Button>
         </Right>
        
      </Header>

        <View style={{backgroundColor:'#5f7bfa'}}>
          <Title style={styles.SubHeaderTitle}>{this.state.notice.shiftTitle} {this.state.notice.header}</Title>
        </View>
        <Content>
          <View style={styles.notifBookingContainer}>

          <Spinner
                visible={this.state.spinner}
                textContent={this.state.spinnerMessage}
                textStyle={{color: '#FFF'}}
            />

            <Title style={{marginBottom:15, color:this.state.statusColor}}>{this.state.statusMessage}</Title>

            <View style={styles.bookingDetailsContent}>
              <Label style={styles.bookingLabel}>Shift No:</Label>
              <View style={styles.textBox}>
                <Text style={styles.bookingDetailsChildText}> 
                    {this.state.shift == undefined?'':this.state.shift.subCategory} #{this.state.shift == undefined?'':this.state.shift.shiftNo}
                </Text>
              </View>
            </View>
            <View style={styles.bookingDetailsContent}>
              <Label style={styles.bookingLabel}>Location:</Label>
              <View style={styles.textBox}>
                <Text style={styles.bookingDetailsChildText}> 
                {this.state.shift == undefined?'':this.state.shift.address}
                </Text>
              </View>
            </View>
            <View style={styles.bookingDetailsContent}>
              <Label style={styles.bookingLabel}>Shift Description</Label>
              <View style={styles.textBox}>
                <Text style={styles.bookingDetailsChildText}> 
                    {this.state.shift == undefined?'':this.state.shift.description}
                </Text>
                <Text> 
                    <Icon name="md-clock" size={15} color="#888"/>
                    &nbsp;{this.state.shift == undefined? '': this.state.shift.startDateTime.getHours() +':'+("0" + this.state.shift.startDateTime.getMinutes()).slice(-2)} &nbsp;&nbsp;
                    <Icon name="md-calendar" size={15} color="#888"/>
                    &nbsp;{this.state.shift == undefined? '': this.state.shift.startDateTime.getDate()+'/'+(this.state.shift.startDateTime.getMonth()+1)+'/'+this.state.shift.startDateTime.getFullYear()} 
                </Text>
              </View>
            </View>
            <View style={styles.bookingDetailsContent}>
              <Label style={styles.bookingLabel}>Employer</Label>
              <View style={styles.textBox}>
                <Text style={styles.bookingDetailsChildText}> 
                  {this.state.notice.companyTitle}
                </Text>
              </View>
            </View>
            <View style={styles.bookingDetailsContent}>
              <Label style={styles.bookingLabel}>Address</Label>
              <View style={styles.textBox}>
                <Text style={styles.bookingDetailsChildText}> 
                  {this.state.company == undefined?'':this.state.company.address}
                </Text>
              </View>
            </View>
            <View style={styles.bookingDetailsContent}>
              <Label style={styles.bookingLabel}>Mobile Number</Label>
              <View style={styles.textBox}>
                <Text style={styles.bookingDetailsChildText}> 
                {this.state.shift == undefined?'':this.state.shift.phoneNumber}
                </Text>
              </View>
            </View>
            <View style={styles.bookingDetailsContent}>
              <Label style={styles.bookingLabel}>Duration (HH:mm)</Label>
              <View style={styles.textBox}>
                <Text style={styles.bookingDetailsChildText}> 
                {this.state.shift == undefined?'':this.state.shift.duration}
                </Text>
              </View>
            </View>
            <View style={styles.bookingDetailsContent}>
              <Label style={styles.bookingLabel}>Fees</Label>
              <View style={styles.textBox}>
                <Text style={[styles.bookingDetailsChildText, styles.feeColorBlue]}> 
                    £{this.state.shift == undefined?'':this.state.shift.fee}/h
                </Text>
              </View>
            </View>

            {this.state.notice.type == 'workerInvitedToShift' && this.state.request != undefined && 
              (this.state.request.approvalStatus == undefined || this.state.request.approvalStatus =='pending') &&
            <View style={styles.buttonConatiner}>
              <Button style={[styles.btnOrange, styles.btnMarginStyle]} onPress={this._onPressButtonDecline}>
                <Text style={{color:'#fff'}}>DECLINE</Text>
              </Button>
              <Button style={[styles.btnBlue, styles.btnMarginStyle]} onPress={this._onPressButtonAccept}>
                <Text style={{color:'#fff'}}>ACCEPT</Text>
              </Button>
            </View>
            }
          </View>
        </Content>
        </Container>
    );
  }
}

const styles = StyleSheet.create({
   
    SubHeaderTitle:{
      color:'#fff',
      fontSize:14,
      padding:5,
      paddingTop:8,
      textAlign:'center',
      alignItems:'center',
    },
    bookingAcceptTitle:{
        color:'#08c903',
        marginBottom:15,
    },
    bookingCancelTitle:{
        color:'#f80808',
        marginBottom:15,
    },
    HeaderTitle:{
      color:'#fff',
      textAlign:'center',
      alignItems:'center',
    },
    notifBookingContainer:{
      margin:15,
      marginTop:15,
      marginBottom:15,
    },
    bookingDetailsContent:{
      marginBottom:15,
    },
    textBox:{
      backgroundColor:'#f4f4f4',
      padding:15,
    },
    bookingDetailsChildText:{
      fontWeight:'bold',
    },
    bookingLabel:{
      marginBottom:5,
      fontSize:14,
      color:'#888'
    },
    buttonConatiner:{
      flex:1,
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
    },
    btnMarginStyle:{
      margin:5,
      marginBottom:10,
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      paddingLeft:30,
      paddingRight:30,
    },
    btnOrange:{
      backgroundColor:'#df8c40',
    },
    btnBlue:{
      backgroundColor:'#5271ff',
    },
    feeColorBlue:{
        color:'#5271ff',
    }
})