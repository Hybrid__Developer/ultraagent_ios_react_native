import React, { Component } from 'react';
import { StyleSheet, Text, View, ActivityIndicator, 
   Image, TextInput, TouchableOpacity, ImageBackground, 
   ScrollView, Platform,AsyncStorage, HeaderBackButton, HeaderBackArrow, Button} from 'react-native';

import {StackNavigator, NavigationActions} from 'react-navigation';
import { Icon } from 'native-base';

import Dialog, { DialogButton, DialogContent, DialogTitle, DialogFooter } from 'react-native-popup-dialog';

export default class ThankYou extends Component {
    state = { }
    constructor(props) {
        super(props);

        this._onPressButtonFinish = this._onPressButtonFinish.bind(this);

        this.state = {
          termsofServiceDialog:false,
        };
    }



    static navigationOptions = {
       // header:null
       headerTitle: 'THANK YOU',
       headerStyle: {
        backgroundColor: '#5271FF',
      },
      headerTitleStyle: {
        color: "#fff",
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      headerTintColor: '#fff',
      headerLeft: null,
      gesturesEnabled: false,
    }
    
    _onPressButtonFinish = () => {
        // do something
        //this.props.navigation.navigate('Auth');
        this.props.navigation.navigate('Landing');
        //this.props.navigation.;

        // let navigateAction = NavigationActions.reset({
        //     index: 0,
        //     routeName:'Landing',
        //     actions: [
        //         NavigationActions.navigate({ routeName: 'Landing'})]
        // });
        // this.props.navigation.dispatch(navigateAction);

    }

    componentWillMount(){

        
    }


  render() {
    return (
        <View style={styles.wrapper}>
                <ImageBackground style={styles.container} 
                source={require('../images/thankyou-bg.png')} imageStyle={{ resizeMode: 'cover' }}>
                    
                    <ScrollView>
                        <View style={styles.ThankyouTextContainer}>
                            <Text style={styles.TitleText}> Congratulations </Text>
                            <Text style={styles.thankText}> 
                                You have Successfully completed employee sign up process.
                            </Text>
                            <Text style={styles.thankText}> 
                                You will be notified within the next 24 hours once your account has been approved.
                            </Text>
                            <Text style={styles.thankText}> 
                                While your account is being reviewed, feel free to familiarize yourself with being a UA partner through the video below.
                            </Text>
                            <Image style={styles.videoimage} 
                            source={require('../images/videoimage.png')}/>

                            <TouchableOpacity 
                                style={styles.buttonStyle} 
                                onPress={this._onPressButtonFinish}
                                activeOpacity={0.7} 
                                >
        
                                <Text style={styles.btntext}> GOT IT </Text>
                    
                            </TouchableOpacity>
                        </View>
                        </ScrollView>

                    {/* <View style={styles.loadingIndicator}>
                        <ActivityIndicator style={styles.loadingImg} 
                        animating={true} size="large" color="#5271FF" text="loading"/>
                        <Text style={styles.loadingtext}>Loading</Text>
                    </View> */}
                    <View style={styles.linkContainer}>
                        <Text style={styles.PrivacyText}> 2019 UA technology .inc</Text>
                        <Text style={styles.regText} onPress={() => {
                            this.setState({ termsofServiceDialog: true });
                            }}
                        activeOpacity={0.7}>
                            Privacy | Terms 
                        </Text>
                    </View>

                    <View>
              {/* Change password dialog container */}
                <Dialog
                  visible={this.state.termsofServiceDialog}
                  onTouchOutside={() => {
                      this.setState({ termsofServiceDialog: false });
                      }}
                  dialogTitle={
                    <DialogTitle title="Terms of service"
                      textStyle={{ color: '#000', fontSize:18 }}
                    />
                  }
                >
                  <TouchableOpacity onPress={() => {
                      this.setState({ termsofServiceDialog: false });
                      }}  style={{position: 'absolute', right: 15, top:5,
                      justifyContent: 'center'}}>
                    <Icon type="FontAwesome" name='close' 
                    style={{fontSize: 20, color: '#5271FF', marginRight:3, marginTop:10}}/>
                  </TouchableOpacity>
                
                  {/* <ScrollView> */}
                      <DialogContent style={{height:400}}>
                        <ScrollView>
                           <Text style={styles.termsHeading}>
                           Introduction</Text>
                           <Text style={styles.termsText}>
                            These terms and conditions apply between you, 
                            the User of this Website (including any sub-domains, unless expressly 
                            excluded by their own terms and conditions), and Ultra Agent Limited, 
                            the owner and operator of this Website. Please read these terms and 
                            conditions carefully, as they affect your legal rights. Your agreement 
                            to comply with and be bound by these terms and conditions is deemed to 
                            occur upon your first use of the Website. If you do not agree to be 
                            bound by these terms and conditions, you should stop using the Website 
                            immediately.
                           </Text>
                           <Text style={styles.termsText}>
                            In these terms and conditions, User or Users means any third party 
                            that accesses the Website and is not either (i) employed by Ultra 
                            Agent Limited and acting in the course of their employment or (ii) 
                            engaged as a consultant or otherwise providing services to Ultra 
                            Agent Limited and accessing the Website in connection with the 
                            provision of such services.
                           </Text>
                           <Text style={styles.termsText}>
                            You must be at least 16 years of age to use this Website. 
                            By using the Website and agreeing to these terms and conditions, 
                            you represent and warrant that you are at least 16 years of age.
                           </Text>
                           <Text style={styles.termsHeading}>
                            Intellectual property and acceptable use
                           </Text>
                           <Text style={styles.termsText}>
                            1. All Content included on the Website, unless uploaded by Users, is the property of 
                            Ultra Agent Limited, our affiliates or other relevant third parties. In these terms 
                            and conditions, Content means any text, graphics, images, audio, video, software, 
                            data compilations, page layout, underlying code and software and any other form of 
                            information capable of being stored in a computer that appears on or forms part of this
                             Website, including any such content uploaded by Users. By continuing to use 
                             the Website you acknowledge that such Content is protected by copyright, trademarks, 
                             database rights and other intellectual property rights. Nothing on this site shall be 
                             construed as granting, by implication, estoppel, or otherwise, any license or right to 
                             use any trademark, logo or service mark displayed on the site without the owner's prior 
                             written permission
                           </Text>
                           <Text style={styles.termsText}>
                            2. You may, for your own personal, non-commercial use only, do the following :
                           </Text>
                           <View style={{marginLeft:10}}>
                            <Text style={styles.termsText}>
                              a. retrieve, display and view the Content on a computer screen
                            </Text>
                            <Text style={styles.termsText}>
                              b. download and store the Content in electronic form on a disk (but not on any server or other storage device connected to a network)
                            </Text>
                            <Text style={styles.termsText}>
                              c. print one copy of the Content
                            </Text>
                           </View>
                           <Text style={styles.termsText}>
                            3. You must not otherwise reproduce, modify, copy, distribute or use for commercial purposes any Content without the written permission of Ultra Agent Limited.
                           </Text>
                           <Text style={styles.termsText}>
                           4. You acknowledge that you are responsible for any Content you 
                           may submit via the Website, including the legality, reliability, 
                           appropriateness, originality and copyright of any such Content. 
                           You may not upload to, distribute or otherwise publish through the
                            Website any Content that (i) is confidential, proprietary, false, 
                            fraudulent, libellous, defamatory, obscene, threatening, invasive 
                            of privacy or publicity rights, infringing on intellectual property 
                            rights, abusive, illegal or otherwise objectionable; (ii) may 
                            constitute or encourage a criminal offence, violate the rights of 
                            any party or otherwise give rise to liability or violate any law; or 
                            (iii) may contain software viruses, political campaigning, chain 
                            letters, mass mailings, or any form of "spam." You may not use a 
                            false email address or other identifying information, impersonate 
                            any person or entity or otherwise mislead as to the origin of any 
                            content. You may not upload commercial content onto the Website.
                           </Text>
                           <Text style={styles.termsText}>
                            5. You represent and warrant that you own or otherwise control all 
                            the rights to the Content you post; that the Content is accurate; 
                            that use of the Content you supply does not violate any provision 
                            of these terms and conditions and will not cause injury to any 
                            person; and that you will indemnify Ultra Agent Limited for all 
                            claims resulting from Content you supply.
                           </Text>
                           <Text style={styles.termsHeading}>Prohibited use</Text>
                           <Text style={styles.termsText}>6. You may not use the Website 
                           for any of the following purposes:</Text>
                           <View style={{marginLeft:10}}>
                            <Text style={styles.termsText}>a. in any way which causes, or may cause, damage to the 
                            Website or interferes with any other person's use or enjoyment 
                            of the Website;
                            </Text>
                            <Text style={styles.termsText}>b. in any way which is harmful, 
                            unlawful, illegal, abusive, harassing, threatening or otherwise 
                            objectionable or in breach of any applicable law, regulation, 
                            governmental order;
                            </Text>
                            <Text style={styles.termsText}>c. making, transmitting or storing 
                            electronic copies of Content protected by copyright without the 
                            permission of the owner.</Text>
                           </View>
                           <Text style={styles.termsHeading}>Registration</Text>
                           <Text style={styles.termsText}>7. You must ensure that the details 
                           provided by you on registration or at any time are correct and 
                           complete.</Text>
                           <Text style={styles.termsText}>8. You must inform us immediately of 
                           any changes to the information that you provide when registering by 
                           updating your personal details to ensure we can communicate with you
                            effectively.
                          </Text>
                          <Text style={styles.termsText}>9. We may suspend or cancel your 
                          registration with immediate effect for any reasonable purposes or if
                           you breach these terms and conditions.
                          </Text>
                          <Text style={styles.termsText}>10. You may cancel your registration at 
                          any time by informing us in writing to the address at the end of these 
                          terms and conditions. If you do so, you must immediately stop using the 
                          Website. Cancellation or suspension of your registration does not affect 
                          any statutory rights.
                          </Text>
                          <Text style={styles.termsHeading}>Password and security</Text>
                          <Text style={styles.termsText}>11. When you register on this Website, 
                          you will be asked to create a password, which you should keep 
                          confidential and not disclose or share with anyone.
                          </Text>
                          <Text style={styles.termsText}>12. If we have reason to believe that 
                          there is or is likely to be any misuse of the Website or breach of 
                          security, we may require you to change your password or suspend your 
                          account.
                          </Text>
                          <Text style={styles.termsHeading}>Links to other websites</Text>
                          <Text style={styles.termsText}>13. This Website may contain links to 
                          other sites. Unless expressly stated, these sites are not under the 
                          control of Ultra Agent Limited or that of our affiliates.
                          </Text>
                          <Text style={styles.termsText}>14. We assume no responsibility for the 
                          content of such Websites and disclaim liability for any and all forms 
                          of loss or damage arising out of the use of them.
                          </Text>
                          <Text style={styles.termsText}>15. The inclusion of a link to another 
                          site on this Website does not imply any endorsement of the sites 
                          themselves or of those in control of them.
                          </Text>
                          <Text style={styles.termsHeading}>Privacy Policy and Cookies Policy
                          </Text>
                          <Text style={styles.termsText}>16. Use of the Website is also governed by our Privacy 
                          Policy and Cookies Policy, which are incorporated into these terms and conditions by this 
                          reference. To view the Privacy Policy and Cookies Policy, please click on 
                          the following: _______________ and _______________.
                          </Text>
                          <Text style={styles.termsHeading}>Availability of the Website and 
                          disclaimers</Text>
                          <Text style={styles.termsText}>17. Any online facilities, tools, 
                          services or information that Ultra Agent Limited makes available 
                          through the Website (the Service) is provided "as is" and on an 
                          "as available" basis. We give no warranty that the Service will be 
                          free of defects and/or faults. To the maximum extent permitted by the 
                          law, we provide no warranties (express or implied) of fitness for a 
                          particular purpose, accuracy of information, compatibility and 
                          satisfactory quality. Ultra Agent Limited is under no obligation to 
                          update information on the Website.
                          </Text>
                          <Text style={styles.termsText}>18. Whilst Ultra Agent Limited uses 
                          reasonable endeavours to ensure that the Website is secure and free 
                          of errors, viruses and other malware, we give no warranty or guaranty 
                          in that regard and all Users take responsibility for their own 
                          security, that of their personal details and their computers.
                          </Text>
                          <Text style={styles.termsText}>19. Ultra Agent Limited accepts no 
                          liability for any disruption or non-availability of the Website.
                          </Text>
                          <Text style={styles.termsText}>20. Ultra Agent Limited reserves the 
                          right to alter, suspend or discontinue any part (or the whole of) 
                          the Website including, but not limited to, any products and/or 
                          services available. These terms and conditions shall continue to apply 
                          to any modified version of the Website unless it is expressly stated 
                          otherwise.
                          </Text>
                          <Text style={styles.termsHeading}>Limitation of liability</Text>
                          <Text style={styles.termsText}>21. Nothing in these terms and conditions will: (a) 
                          limit or exclude our or your liability for death or personal injury resulting from 
                          our or your negligence, as applicable; (b) limit or exclude our or your liability 
                          for fraud or fraudulent misrepresentation; or (c) limit or exclude any of our or your 
                          liabilities in any way that is not permitted under applicable law.</Text>
                          <Text style={styles.termsText}>22. We will not be liable to you in 
                          respect of any losses arising out of events beyond our reasonable
                           control.
                          </Text>
                          <Text style={styles.termsText}>23. To the maximum extent permitted by 
                          law, Ultra Agent Limited accepts no liability for any of the following:
                          </Text>
                          <View style={{marginLeft:10}}>
                           <Text style={styles.termsText}>a. any business losses, such as loss of 
                           profits, income, revenue, anticipated savings, business, contracts, 
                           goodwill or commercial opportunities;
                            </Text>
                           <Text style={styles.termsText}>b. loss or corruption of any data, 
                           database or software;
                            </Text>
                           <Text style={styles.termsText}>c. any special, indirect or 
                           consequential loss or damage.
                            </Text>
                          </View>
                          <Text style={styles.termsHeading}>General</Text>
                          <Text style={styles.termsText}>24. You may not transfer any of your 
                          rights under these terms and conditions to any other person. We may 
                          transfer our rights under these terms and conditions where we 
                          reasonably believe your rights will not be affected.
                          </Text>
                          <Text style={styles.termsText}>25. These terms and conditions may be 
                          varied by us from time to time. Such revised terms will apply to the 
                          Website from the date of publication. Users should check the terms and 
                          conditions regularly to ensure familiarity with the then current 
                          version.
                          </Text>
                          <Text style={styles.termsText}>26. These terms and conditions together 
                          with the Privacy Policy and Cookies Policy contain the whole agreement 
                          between the parties relating to its subject matter and supersede all 
                          prior discussions, arrangements or agreements that might have taken 
                          place in relation to the terms and conditions.
                          </Text>
                          <Text style={styles.termsText}>27. The Contracts (Rights of Third 
                          Parties) Act 1999 shall not apply to these terms and conditions and no 
                          third party will have any right to enforce or rely on any provision of 
                          these terms and conditions.
                          </Text>
                          <Text style={styles.termsText}>28. If any court or competent 
                          authority finds that any provision of these terms and conditions 
                          (or part of any provision) is invalid, illegal or unenforceable, that 
                          provision or part-provision will, to the extent required, be deemed to
                           be deleted, and the validity and enforceability of the other 
                           provisions of these terms and conditions will not be affected.
                          </Text>
                          <Text style={styles.termsText}>29. Unless otherwise agreed, no delay, 
                          act or omission by a party in exercising any right or remedy will be 
                          deemed a waiver of that, or any other, right or remedy.
                          </Text>
                          <Text style={styles.termsText}>30. This Agreement shall be governed 
                          by and interpreted according to the law of England and Wales and all 
                          disputes arising under the Agreement (including non-contractual 
                          disputes or claims) shall be subject to the exclusive jurisdiction of 
                          the English and Welsh courts.
                          </Text>
                          <Text style={styles.termsHeading}>Ultra Agent Limited details</Text>
                          <Text style={styles.termsText}>31. Ultra Agent Limited is a company 
                          incorporated in England and Wales with registered number 10510063 whose 
                          registered address is 9 Rotherwick hill, Ealing, London, W5 3EQ and it 
                          operates the Website www.ultraagent.co.uk.
                          </Text>
                          <Text style={styles.termsText}>You can contact Ultra Agent Limited by
                           email on Hello@ultragent.co.uk.
                          </Text>
                          <Text style={styles.termsHeading}>Attribution</Text>
                          <Text style={styles.termsText}>32. These terms and conditions were 
                          created using a document from Rocket Lawyer</Text> 
                          {/* <Text onPress={()=>{Linking.openURL('https://www.rocketlawyer.co.uk')}>(https://www.rocketlawyer.co.uk).</Text>
                          <Text onPress={()=>{Linking.openURL('https://www.rocketlawyer.co.uk')}>My Text</Text> */}
                          <TouchableOpacity onPress={() => Linking.openURL('https://www.rocketlawyer.co.uk')} >
                            <Text style={{color:'#5271FF'}}>
                              https://www.rocketlawyer.co.uk                            
                            </Text>
                          </TouchableOpacity>
                        </ScrollView>
                      </DialogContent>
                  {/* </ScrollView> */}
              </Dialog>
            </View> 
                </ImageBackground>
                
               
        </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container:{
    flex:1,
    alignItems: 'center',
    alignSelf:'stretch',
    width:null,
  },
  ThankyouTextContainer:{
    flex:1,
    alignItems: 'center',
    alignSelf:'center',
    padding:20,
  },
  TitleText:{
      color:'#6cbd68',
      fontSize:20,
      marginBottom:10,
      marginTop:10,
      alignSelf:'stretch',
      alignItems:'center',
      textAlign:'center'
  },
  thankText:{
      color:'#838383',
      padding:10,
      alignSelf:'flex-start',
  },
  videoimage:{
      marginTop:10,
      width:280,
      borderRadius: 4,
      borderWidth: 4,
      borderColor: '#5271FF',
  },
  buttonStyle:{
    alignSelf:'stretch',
    marginLeft:50,
    marginRight:50,
    marginTop:20,
    backgroundColor:'#5271FF',
    padding:12,
    borderRadius:3,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:0
  },
  btntext:{
    textAlign:'center',
    alignSelf:'stretch',
    alignItems: 'center',
    color:'#fff',
    fontWeight:'500',
    fontSize:14,
    letterSpacing:1,
    color:'#fff',
  },
  linkContainer:{
    alignSelf:'stretch',
    alignItems:'center',
    marginBottom:10,
    // position:'absolute',
    // bottom:0,
    // left:0,
    // right:0
  },
 
  PrivacyText:{
    marginTop:5,
    color:'#fff',
  },
  regText:{
    marginTop:5,
    color:'#fff',
  },
  loadingImg:{
    justifyContent: 'space-around',
  },
  loadingIndicator:{
    marginBottom:200,
  },
  loadingtext:{
    color:'#5271FF',
  }
});
