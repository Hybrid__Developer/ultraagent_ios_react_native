
class SingletonClass {
    constructor(){
     if(! SingletonClass.instance){
       this._data = {
            loggedinprofile: null,
            shouldBlockAutoLogin: false,
            documentTypesData:{
                "Insurance":"Insurance",
                "DBSCertificate":"DBS Certificate",
                "DrivingLicense":"Driving License (for drivers only)",
                "ProfessionBodyCertificate":"Profession Body Certificate",
                "IdentificationDocument":"Identification Document",
                "Training":"Training",
                "Other":"Other (e.g CV)",
            },
            categoriesData: {
                "Health and Social Care":[ "Cleaner",
                    "Dental Nurse",
                    "Doctor/GP",
                    "Midwife",
                    "Registered mental health nurse",
                    "Care worker",
                    "Support worker Community",
                    "Project Worker",
                    "HCA Mental health",
                    "Occupational therapist",
                    "Physio therapist",
                    "Social Worker",
                    "Support Worker Young people",
                    "General nurse",
                    "Dentist" ],

                    "Construction": ["Masonry",
                    "Plumbing",
                    "Carpenter",
                    "Electrician",
                    "Management",
                    "Welding",
                    "Plastering",
                    "Glaziers",
                    "Engineering",
                    "Architecture","Labourer"],
            
                "Security and Concierge": ["Door Supervision",
                    "Guard",
                    "CSC Site security",
                    "CCTV security",
                    "Concierge",
                    "Personal Security"],
            
                "Hospitality": ["House keeping",
                    "Kitchen porter & Dishwasher",
                    "Chef",
                    "Bar licence holder",
                    "Travel and tourism",
                    "Catering(Waiter/Waitress)",
                    "Bar staff",
                    "Customer service"]
            },
            citiesData: {
            "ENGLISH CITIES": [
            "Bath",
            "Birmingham",
            "Bradford",
            "Brighton & Hove",
            "Bristol",
            "Cambridge",
            "Canterbury",
            "Carlisle",
            "Chelmsford",
            "Chester",
            "Chichester",
            "Coventry",
            "Derby",
            "Durham",
            "Ely",
            "Exeter",
            "Gloucester",
            "Hereford",
            "Kingston upon Hull",
            "Lancaster",
            "Leeds",
            "Leicester",
            "Lichfield",
            "Lincoln",
            "Liverpool",
            "London",
            "Manchester",
            "Newcastle upon Tyne",
            "Norwich",
            "Nottingham",
            "Oxford",
            "Peterborough",
            "Plymouth",
            "Portsmouth",
            "Preston",
            "Ripon",
            "Salford",
            "Salisbury",
            "Sheffield",
            "Southampton",
            "St Albans",
            "Stoke-on-Trent",
            "Sunderland",
            "Truro",
            "Wakefield",
            "Wells",
            "Westminster",
            "Winchester",
            "Wolverhampton",
            "Worcester",
            "York"],
            
            "SCOTTISH CITIES": [
            "Aberdeen",
            "Dundee",
            "Edinburgh",
            "Glasgow",
            "Inverness",
            "Perth",
            "Stirling",
            "Welsh Cities",
            "Bangor",
            "Cardiff",
            "Newport",
            "St Asaph",
            "St David's",
            "Swansea"],
    
            "NORTHERN IRISH CITIES": [
            "Armagh",
            "Belfast",
            "Londonderry (also known as Derry)",
            "Lisburn",
            "Newry"]
            
        },
       };
       SingletonClass.instance = this;
     }
  
     return SingletonClass.instance;
    }
  
   //rest is the same code as preceding example
   set(key, value){
        this._data[key] = value;
    }

    get(key){
        return this._data[key];
    }
  
}
  
const SingletonInstance = new SingletonClass();
Object.freeze(SingletonInstance);

export default SingletonInstance;