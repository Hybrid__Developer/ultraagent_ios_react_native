/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { ScrollView, 
  Image,Button,AsyncStorage, Dimensions, TouchableOpacity  } from 'react-native';
import {Platform, PushNotificationIOS, StyleSheet, Text, View, Alert} from 'react-native';
import {
  createStackNavigator, createDrawerNavigator, DrawerItems, 
  SafeAreaView, createBottomTabNavigator, createSwitchNavigator
} from 'react-navigation';

import Rating from 'react-native-rating';
import { Easing } from 'react-native';

import firebase from 'react-native-firebase';

import Splash  from './components/Splash';
import Landing  from './components/Landing';
import Login  from './components/Login';
import ForgotPassword  from './components/ForgotPassword';
import Register  from './components/Register';
import PersonalDetails  from './components/PersonalDetails';
import ProfessionalDetails  from './components/ProfessionalDetails';
import RegisterUpload  from './components/RegisterUpload';
import SetProfile  from './components/SetProfile';
import ThankYou  from './components/ThankYou';
import ResetPassword  from './components/ResetPassword';
import ConfirmResetPassword  from './components/ConfirmResetPassword';
import PasswordChanged  from './components/PasswordChanged';

// Drawer navigator screens
import NewDocument  from './components/NewDocument';
import UpdateDocument  from './components/UpdateDocument';
import Documents from './components/Documents';
import Settings from './components/Settings';
import ContactUs from './components/ContactUs';
import InviteFriend from './components/InviteFriend';
import Help from './components/Help';
import About  from './components/About';
import EditProfile  from './components/EditProfile';
import EditProfileWorkingHour  from './components/EditProfileWorkingHour';

// Tab navigator screens
import Home from './components/Home';
import Shifts from './components/Shifts';
import Booking from './components/Booking';
import TimeSheet from './components/TimeSheet';
import Notifications from './components/Notifications';
import ShiftsDetails  from './components/ShiftsDetails';
import BookingShifts  from './components/BookingShifts';
import BookingCancel  from './components/BookingCancel';
import CancelBooking  from './components/CancelBooking';
import BookingComplete  from './components/BookingComplete';
import PendingShifts  from './components/PendingShifts';
import CompleteShifts  from './components/CompleteShifts';
import DisputeShifts  from './components/DisputeShifts';
import DisputeDetails  from './components/DisputeDetails';
import CancelShifts  from './components/CancelShifts';
import BookingFullSchedule  from './components/BookingFullSchedule';
import NavigateMap  from './components/NavigateMap';
import NotificationBooking  from './components/NotificationBooking';
import NotificationDispute  from './components/NotificationDispute';
import UltraAgentHelpers from './helpers/UltraAgentHelpers';

import SingletonClass from './helpers/SingletonClass';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


const AuthStack = createStackNavigator({
  Landing:{
   screen: Landing,
   name: 'Landing'
  },
  Login:{
    screen: Login,
    name: 'Login',
  },
  ForgotPassword:{
    screen: ForgotPassword,
    name: 'ForgotPassword',
  },
  Register:{
    screen: Register,
    name: 'Register',
  },
  PersonalDetails:{
    screen: PersonalDetails,
    name: 'PersonalDetails',
  },
  ProfessionalDetails:{
    screen: ProfessionalDetails,
    name: 'ProfessionalDetails',
  },
  RegisterUpload:{
    screen: RegisterUpload,
    name: 'RegisterUpload',
  },
  
  SetProfile:{
    screen: SetProfile,
    name: 'SetProfile',
  },
  ThankYou:{
    screen: ThankYou,
    name: 'ThankYou',
    // navigationOptions:  {
    //   title: 'Title',
    //   headerLeft: null,
    //   gesturesEnabled: false,
    // }
  },
  ResetPassword:{
    screen: ResetPassword,
    name: 'ResetPassword',
  },
  ConfirmResetPassword:{
    screen: ConfirmResetPassword,
    name: 'ConfirmResetPassword',
  },
  PasswordChanged:{
    screen: PasswordChanged,
    name: 'PasswordChanged',
  },  
 
},
{
  initialRouteName: 'Landing',
  navigationOptions: {
    gesturesEnabled: false,
  },
})

// Home stack navigator
const HomeStackNavigator = createStackNavigator({
  Home: {screen:Home},
  ShiftsDetails: {screen: ShiftsDetails},
},{
  initialRouteName: 'Home',
  navigationOptions: {
    gesturesEnabled: false,
  },
});

// Shifts stack navigator
const ShiftsStackNavigator = createStackNavigator({
  Shifts: {
    screen:Shifts
  },
  ShiftsDetails: {
    screen: ShiftsDetails,
},
},{
  initialRouteName: 'Shifts',
  navigationOptions: {
    gesturesEnabled: false,
  },
});

// Booking stack navigator
const BookingStackNavigator = createStackNavigator({
  Booking: {screen:Booking},
  BookingShifts: {screen: BookingShifts},
  BookingCancel: {screen:BookingCancel},
  CancelBooking: {screen:CancelBooking},
  BookingComplete: {screen:BookingComplete},
  BookingFullSchedule: {screen:BookingFullSchedule},
  NavigateMap: {screen:NavigateMap},
},{
  initialRouteName: 'Booking',
  navigationOptions: {
    gesturesEnabled: false,
  },
});

// TimeSheet stack navigator
const TimeSheetStackNavigator = createStackNavigator({
  TimeSheet: {screen:TimeSheet},
  PendingShifts: {screen:PendingShifts},
  CompleteShifts: {screen:CompleteShifts},
  DisputeShifts: {screen:DisputeShifts},
  DisputeDetails: {screen:DisputeDetails},
  CancelShifts: {screen:CancelShifts},
},{
  initialRouteName: 'TimeSheet',
  navigationOptions: {
    gesturesEnabled: false,
  },
});

// Notifications stack navigator
const NotificationsStackNavigator = createStackNavigator({
  Notifications: {screen:Notifications},
  NotificationBooking: {screen:NotificationBooking},
  NotificationDispute: {screen:NotificationDispute}
},{
  initialRouteName: 'Notifications',
  navigationOptions: {
    gesturesEnabled: false,
  },
});

// Bottom Tab Navigator
const Tabs = createBottomTabNavigator({
  Home: {
    screen:HomeStackNavigator,
    navigationOptions: {
      tabBarIcon: <Image
      source={require('./images/homeicons.png')}
      //resizeMode = {Image.resizeMode.contain}
      style={{ height: 30, width: 30}}
     />,
      tabBarOptions: {
          activeTintColor: '#5271FF',
          //inactiveTintColor: 'green',
          labelStyle: {
            fontSize: 10,
          },
      },
    },
  },
  Shifts: {
    screen:ShiftsStackNavigator,
    navigationOptions: {
      tabBarIcon: <Image
      source={require('./images/shiftsicons.png')}
      //resizeMode = {Image.resizeMode.contain}
      style={{ height: 30, width: 30}}
     />,
      tabBarOptions: {
        activeTintColor: '#5271FF',
        //inactiveTintColor: 'green',
        labelStyle: {
          fontSize: 10,
        },
      },
    },
  },
  Booking: {screen:BookingStackNavigator,
    navigationOptions: {
      tabBarIcon: <Image
      source={require('./images/bookingicons.png')}
      //resizeMode = {Image.resizeMode.contain}
      style={{ height: 30, width: 30}}
     />,
      tabBarOptions: {
        activeTintColor: '#5271FF',
        //inactiveTintColor: 'green',
        labelStyle: {
          fontSize: 10,
        },
      },
    },
  },
  TimeSheet: {screen:TimeSheetStackNavigator,
    navigationOptions: {
      tabBarIcon: <Image
      source={require('./images/clockicons.png')}
      //resizeMode = {Image.resizeMode.contain}
      style={{ height: 30, width: 30}}
     />,
      tabBarOptions: {
        activeTintColor: '#5271FF',
        //inactiveTintColor: 'green',
        labelStyle: {
          fontSize: 10,
        },
      }, 
    },
  },
  Notifications: {screen:NotificationsStackNavigator,
    navigationOptions: {
      tabBarIcon: <Image
      source={require('./images/bellicons.png')}
      //resizeMode = {Image.resizeMode.contain}
      style={{ height: 30, width: 30}}
     />,
      tabBarOptions: {
        activeTintColor: '#5271FF',
        //inactiveTintColor: 'green',
        labelStyle: {
          fontSize: 10,
        },
      },
    },
  },
},
{
  order: ['Home', 'Shifts', 'Booking', 'TimeSheet', 'Notifications']
});

const TabsStackNavigator = createStackNavigator({
  Tabs: {
    screen:Tabs,
    navigationOptions: {
      header: null
    }
  },
},{
  initialRouteName: 'Tabs',
  navigationOptions: {
    gesturesEnabled: false,
  },
});


// export default class App extends Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text style={styles.welcome}>Welcome to React Native!</Text>
//         <Text style={styles.instructions}>To get started, edit App.js</Text>
//         <Text style={styles.instructions}>{instructions}</Text>
//       </View>
//     );
//   }
// }

const CustomDrawerContentComponent = (params) => { 
  //console.log('CustomDrawerContentComponent');
  //console.log(params);
  const loggedInProfile = SingletonClass.get('loggedinprofile');
  console.log(loggedInProfile);
  //var picUrl = require('./images/setprofilepic.png');
  //var picUrl = {url: 'https://placehold.it/180'};
  var picUrl = require('./images/profiledefault.jpg');

  if ((loggedInProfile.profilePicture != undefined) && (loggedInProfile.profilePicture.url != undefined) &&
      (loggedInProfile.profilePicture.url != null) && (loggedInProfile.profilePicture.url.length > 0)) {
    picUrl = loggedInProfile.profilePicture;
  }

  const starImages = {
    starFilled: require('./images/star_filled.png'),
    starUnfilled: require('./images/star_unfilled.png')
  }

  return (
  <SafeAreaView style={{flex:1}}>
    <Image style={{flex: 1 , position : 'absolute' , top : 0, width:'100%', height:'100%'}}
    source={require('./images/drawerbg.png')} />
       <TouchableOpacity onPress={ () => {
         //console.log('PRESSEDEDDEDED');
          params.navigation.navigate('Profile')
        }}
         style = {styles.headerDrawerContainer}>
          <Image source={picUrl} style={styles.drawerImage} />
          <View style = {styles.profileInfo}>
            <Text style={{fontWeight:'bold', paddingLeft:10, paddingRight:10}}>{loggedInProfile.firstName} {loggedInProfile.lastName}</Text>
            <Text style={{flexDirection:'row', flexWrap:'wrap', 
                  paddingLeft:10, paddingRight:10}}>{loggedInProfile.wSubCategory}</Text>
            {/* <Text>{loggedInProfile.phoneNumber}</Text> */}
            <Rating max={5} initial={5} 
              editable={false}
              selectedStar={starImages.starFilled}
              unselectedStar={starImages.starUnfilled}
              config={{
                easing: Easing.inOut(Easing.ease),
                duration: 350
              }}
              stagger={80}
              maxScale={1.1}
              starStyle={{
                width: 20,
                height: 20
              }}
            />
        </View>
      </TouchableOpacity>
    <ScrollView>
      <View style={{marginBottom:50, marginTop:20}}>
        <DrawerItems {...params} />
      </View>
    </ScrollView>
      <TouchableOpacity onPress={ () => {
        // Confirm with user once before logging out
        Alert.alert(
          "Confirmation",
          "Do you really want to logout?",
          [
              {text: 'No, Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: "Yes, Logout", style:'destructive', onPress: async () => {
                //await AsyncStorage.removeItem('userToken')
                //console.log(params.navigation)

                // Reset fcmToken to empty
                var aProf = SingletonClass.get('loggedinprofile');
                let fcmToken = await AsyncStorage.getItem('fcmToken', null);
                if((aProf != undefined) && (aProf != null) && (aProf.userId != undefined) && (fcmToken != null)) {
                  UltraAgentHelpers.unlinkFcmTokenFromUserUserId(fcmToken, aProf.userId, 
                    async () => {
                      // Completion block
                      await firebase.auth().signOut();
                      params.navigation.navigate('Auth')
                    },
                    (err) => {
                      // Error block
                      UltraAgentHelpers.showAlertWithOneButton("Oops!", err.message);
                    });
                }

                
              }}
          ],
          { cancelable: false }
        );
        }} style={{flex:1, flexDirection:'row', 
        bottom: 0, position: 'absolute', width: '100%', paddingLeft:15,
        backgroundColor:'#fff', borderTopColor:'#ccc', borderTopWidth:1, paddingTop:5}}>
        <Image
          source={require('./images/signouticons.png')}
          style={{ height: 35, width: 35}}/>
        <Text style={{marginLeft:20, lineHeight:30, fontWeight: 'bold', 
        color:'#5271FF'}}>
          {'signout'.toUpperCase()}
        </Text>
      </TouchableOpacity>
  </SafeAreaView>
)};


// Profile stack navigator
const ProfileStackNavigator = createStackNavigator({
  Profile: {
    screen:EditProfile
  },
  EditProfileWorkingHour: {
    screen:EditProfileWorkingHour
  },
},{
  initialRouteName: 'Profile',
  navigationOptions: {
    gesturesEnabled: false,
  },
})
// document stack navigator
const DocumentStackNavigator = createStackNavigator({
  Documents:{
    screen: Documents,
    name: 'Documents',
  },
  NewDocument:{
    screen: NewDocument,
    name: 'NewDocument',
  },
  UpdateDocument:{
    screen: UpdateDocument,
    name: 'UpdateDocument',
  },
},{
  initialRouteName: 'Documents',
  navigationOptions: {
    gesturesEnabled: false,
  },
})



// Left Drawer Navigator
const AppdrawerNavigator = createDrawerNavigator({
  Home: { screen: TabsStackNavigator,
    navigationOptions: {
      drawerIcon: <Image
      source={require('./images/drawerhomeicons.png')}
      style={{ height: 40, width: 40}}
     />
    },
  },
  Profile: { screen: ProfileStackNavigator,
    navigationOptions: {
      drawerIcon: <Image
      source={require('./images/editicons.png')}
      style={{ height: 35, width: 35}}
    />
    },
  },
  Documents: { screen: DocumentStackNavigator,
    navigationOptions: {
      drawerIcon: <Image
      source={require('./images/documenticons.png')}
      style={{ height: 38, width: 38}}
    />
    },
  },
//   Documents: { screen: Documents,
//   navigationOptions: {
//     drawerIcon: <Image
//     source={require('./images/documenticons.png')}
//     style={{ height: 35, width: 35}}
//    />
//   },
// },
  Settings: { screen: Settings,
    navigationOptions: {
      drawerIcon: <Image
      source={require('./images/settingicons.png')}
      style={{ height: 35, width: 35}}
     />
    },
  },
  'Contact Us': { screen: ContactUs,
    navigationOptions: {
      drawerIcon: <Image
      source={require('./images/contactusicons.png')}
      style={{ height: 35, width: 35}}
     />
    },
  },
  'Invite a Friend': { screen: InviteFriend,
    navigationOptions: {
      drawerIcon: <Image
      source={require('./images/invitefriendicons.png')}
      style={{ height: 35, width: 35}}
     />
    },
  },
  Help: { screen: Help,
    navigationOptions: {
      drawerIcon: <Image
      source={require('./images/helpicons.png')}
      style={{ height: 35, width: 35}}
     />
    },
  },
  About: { screen: About,
    navigationOptions: {
      drawerIcon: <Image
      source={require('./images/abouticons.png')}
      style={{ height: 35, width: 35}}
     />
    },
  },
},{
  initialRouteName:'Home',
  contentComponent:CustomDrawerContentComponent,
  drawerBackgroundColor : '#fffffff2',
  contentOptions:{
    activeTintColor:'#555',
    labelStyle:{
      marginTop:14,
      marginBottom:14,
    },
  },
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle'
},
)

// Commented out as we have to handle Push Notifications
// export default createSwitchNavigator({
//   AuthLoading: Splash,
//   App: AppdrawerNavigator,
//   Auth: AuthStack,
// },
// {
//   initialRouteName: 'AuthLoading',
// })

const UASwitchNavigator = createSwitchNavigator({
  AuthLoading: Splash,
  App: AppdrawerNavigator,
  Auth: AuthStack,
},
{
  initialRouteName: 'AuthLoading',
})

const navigationDeferred = new Deferred();

function navigateFromPushNotification(navigation, notificationData) {
  // const config = notificationScreenConfig[notificationData.notificationTypeCode]
  // if (!config) return

  // const resetAction = StackActions.reset({
  //   index: 2,
  //   actions: [
  //     NavigationActions.navigate({ routeName: 'Home' }),
  //     NavigationActions.navigate({ routeName: config.parentScreen }),
  //     NavigationActions.navigate({ routeName: 'Notification', params: { notification: notificationData } }),
  //   ],
  // })
  // navigation.dispatch(resetAction)
}

function Deferred() {
  this.promise = new Promise((resolve, reject) => {
    this.reject = reject
    this.resolve = resolve
  })
}

export default class App extends React.Component {

  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners(); //add this line
  }

  //Remove listeners allocated in createNotificationListeners()
  componentWillUnmount() {
    this.notificationListenerUnsubscribe();
    this.notificationOpenedListenerUnsubscribe();
    this.messageListenerUnsubscribe();
  }

   //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
  }

  //2
  async requestPermission() {
    try {
        await firebase.messaging().requestPermission();
        // User has authorised
        this.getToken();
    } catch (error) {
        // User has rejected permissions
        console.log('permission rejected');
    }
  }

    //3
  async getToken() {
    console.log("App: getToken called");
    let fcmToken = await AsyncStorage.getItem('fcmToken', null);
    console.log("fcmToken= "+fcmToken);
    if (!fcmToken) {
        fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
          console.log("fcmToken= "+fcmToken);
            // user has a device token
            await AsyncStorage.setItem('fcmToken', fcmToken);
            this.linkFcmTokenWithUserId(fcmToken);
        }
    } else {
      this.linkFcmTokenWithUserId(fcmToken);
    }
  }

  async linkFcmTokenWithUserId(fcmToken) {
    var aProf = SingletonClass.get('loggedinprofile');
    if((aProf != undefined) && (aProf != null) && (aProf.userId != undefined)) {
      UltraAgentHelpers.linkFcmTokenWithUserUserId(fcmToken, aProf.userId);
    }
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListenerUnsubscribe = firebase.notifications().onNotification((notification) => {
        const { title, body } = notification;
        UltraAgentHelpers.showAlertWithOneButton (title, body);

        console.log('notification received', notification)
        navigationDeferred.promise.then(
          navigation =>
            notification.foreground
              ? // toast the message as to not remove the user from what they are doing
                // they can click it to go to the target or click the x to dismiss
                showToast(navigation, notification)
              : // wait until we have a navigation object available to actually navigate
                // which happens if the app is just starting
                navigateFromPushNotification(navigation, notification.data)
        )

        // if (Platform.OS === 'ios') {
        //   notification.finish(PushNotificationIOS.FetchResult.NoData)
        // }

    });
  
    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListenerUnsubscribe = firebase.notifications().onNotificationOpened((notificationOpen) => {
        const { title, body } = notificationOpen.notification;
        UltraAgentHelpers.showAlertWithOneButton(title, body);
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const { title, body } = notificationOpen.notification;
        UltraAgentHelpers.showAlertWithOneButton(title, body);
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListenerUnsubscribe = firebase.messaging().onMessage((message) => {
      //process data message
      console.log("MESSAGE RECEIVED");
      console.log(message);

    });

   
  }

  render() {
    return <UASwitchNavigator ref={navigatorRef => navigationDeferred.resolve(navigatorRef)} />
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerDrawerContainer:{
    ...Platform.select({
      ios: {
        height:Dimensions.get('window').height/2.87,
      },
      android: {
        height:Dimensions.get('window').height-400,
      },
  }),
     
    alignItems:'center', 
    justifyContent:'center', 
    backgroundColor:'transparent', 
  },
  drawerImage:{
    height:80, 
    width:80, 
    borderRadius:40,
    //top:,
    //bottom:0,
    ...Platform.select({
      ios: {
        right:5,
      },
      android: {
        right:5,
      },
  }),
    // elevation:100,
    // zIndex:100,
  },
  profileInfo:{
    //top:45,
    alignItems:'center', 
    textAlign:'center',
    //marginTop:50
    position:'absolute',
    bottom: 0,
    paddingTop:3,
    // paddingBottom:3,
    zIndex:2,
    // elevation:100,
    // zIndex:100,
  },
});