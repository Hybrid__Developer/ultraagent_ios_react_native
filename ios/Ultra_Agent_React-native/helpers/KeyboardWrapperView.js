import React, { Component } from 'react';
import { StyleSheet, Text, View, KeyboardAvoidingView, 
   Image, TextInput, TouchableOpacity, ImageBackground, 
   ScrollView, Platform, AsyncStorage, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

export default class KeyboardWrapperView extends Component {
    render() {
        if (Platform.OS == 'android') {
            return(
                 <View style={this.props.style}>
                 { this.props.children }
                 </View>
            );
        } else {
            return (
                <KeyboardAvoidingView behavior={this.props.behavior} style={this.props.style}>
                { this.props.children }
                </KeyboardAvoidingView>  
            );
        }
    }
}
