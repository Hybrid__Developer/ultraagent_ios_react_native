import { Alert , Platform} from 'react-native';
import firebase from 'react-native-firebase';
import SingletonClass from '../helpers/SingletonClass';

const UltraAgentHelpers = {
    showAlertWithOneButton: function(title, msg, oklabel="OK"){
        // SHowing after 100 milliseconds delay, so that 
        // react-native-loading-spinner-overlay get dismissed
        // this handles a known bug in iOS of infinity spinner
        // https://www.npmjs.com/package/react-native-loading-spinner-overlay
        setTimeout(() => {
            // Works on both iOS and Android
            Alert.alert(
                title,
                msg,
                [
                    /*{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},*/
                    {text: oklabel, onPress: () => console.log(oklabel+' Pressed')}
                ],
                { cancelable: false }
            );
        }, 100);
    },
    showAlert2: function(param1){

    },
    formatBytesToDisplay: function(bytes,decimals) {
        if(bytes == 0) return '0';
        var k = 1024,
            dm = decimals <= 0 ? 0 : decimals || 2,
            sizes = [/*'Bytes'*/'', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    },
    linkFcmTokenWithUserUserId: function(fcmToken, userId) {
        console.log('linkFcmTokenWithUserUserId called');

        if ((fcmToken == undefined) || (fcmToken == null) || (fcmToken.length < 1)) { return; } 
        if ((userId == undefined) || (userId == null) || (userId.length < 1)) { return; }
        let platform = 'ios';
        if (Platform.OS == 'android') {
            platform = 'android';
        }

        const unsubscribeFcmTokensCollection = 
        firebase.firestore().collection('fcmTokens')
        .doc(userId)
        .onSnapshot((querySnapshot) => {
            console.log(querySnapshot);
            if ((querySnapshot != undefined) && (querySnapshot != null)) 
            {
                var fcmData = querySnapshot._data;
                console.log(fcmData);
                var alreadyExists = false;
                if ((fcmData == undefined) || (fcmData == null)) {
                    fcmData = {};
                    var arr = new Array(); arr.push(fcmToken);
                    fcmData[platform] = arr;
                } else if (fcmData[platform] == undefined) {
                    var arr = new Array(); arr.push(fcmToken);
                    fcmData[platform] = arr;
                } else {
                    var arr = fcmData[platform];
                    console.log(arr);
                    for (var k=0; k< arr.length; k++) {
                        const aToken = arr[k];
                        if (aToken == fcmToken) {
                            alreadyExists = true;
                            break;
                        }
                    }
                    if (alreadyExists == false) {
                        arr.push(fcmToken);
                        fcmData[platform] = arr;
                    }
                }

                if (alreadyExists == false) {
                    console.log('trung to set fcmData to:');
                    console.log(fcmData);
                    firebase.firestore().collection('fcmTokens').doc(userId).set(fcmData, {merge: true})
                    .then((res) => {
                        //console.log(res);
                    })
                    .catch((err) => {console.log(err);});
                }
            }
            unsubscribeFcmTokensCollection();
        });

        // Also subscribe to topic=userId to receive FCM messages from CLoud functions
        var subscribeToTopic = firebase.functions().httpsCallable('subscribeToTopic');
        subscribeToTopic({topic: userId, token:fcmToken})
        .then()
        .catch((err) => {
            console.log(err);
        });
    },
    unlinkFcmTokenFromUserUserId: function(fcmToken, userId, onCompletionCallback = undefined, onErrorCallback=undefined) {
        console.log('unlinkFcmTokenFromUserUserId called');
        if ((fcmToken == undefined) || (fcmToken == null) || (fcmToken.length < 1)) { 
            if (onCompletionCallback != undefined) { onCompletionCallback(); }
            return; 
        } 
        if ((userId == undefined) || (userId == null) || (userId.length < 1)) { 
            if (onCompletionCallback != undefined) { onCompletionCallback(); }
            return; 
        }
        let platform = 'ios';
        if (Platform.OS == 'android') {
            platform = 'android';
        }

        firebase.firestore().collection('fcmTokens')
        .doc(userId)
        .get({source: 'server'})
        .then((querySnapshot) => {
            if ((querySnapshot != undefined) && (querySnapshot != null) &&
                (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
            {
                var fcmData = querySnapshot._data;
                console.log(fcmData);
                var doesExists = false;
                if (fcmData == undefined) {
                    // do nothing
                } else if (fcmData[platform] == undefined) {
                    // do nothing
                } else {
                    var arr = fcmData[platform];
                    for (var k=0; k< arr.length; k++) {
                        const aToken = arr[k];
                        if (aToken == fcmToken) {
                            doesExists = true;
                            break;
                        }
                    }
                    if (doesExists == true) {
                        // delete from array
                        for (var i=arr.length-1; i>=0; i--) {
                            if (arr[i] === fcmToken) {
                                arr.splice(i, 1);
                            }
                        }
                        fcmData[platform] = arr; 
                    }
                }

                if (doesExists == true) {
                    console.log('unlinking fcm token. trying to set fcmData to:');
                    console.log(fcmData);
                    firebase.firestore().collection('fcmTokens').doc(userId).set(fcmData, {merge: true})
                    .then((res) => {
                        console.log(res);
                        UltraAgentHelpers.unsubscribeFromTopicHelper(fcmToken, userId, onCompletionCallback, onErrorCallback);
                    })
                    .catch((err) => {
                        if (onErrorCallback != undefined) { onErrorCallback(err); }
                        console.log(err);
                    });
                } else {
                    UltraAgentHelpers.unsubscribeFromTopicHelper(fcmToken, userId, onCompletionCallback, onErrorCallback);
                }
            } else {
                UltraAgentHelpers.unsubscribeFromTopicHelper(fcmToken, userId, onCompletionCallback, onErrorCallback);
            }
        })
        .catch((err) => {
            if (onErrorCallback != undefined) { onErrorCallback(err); }
            console.log(err);
        });

       
    },
    unsubscribeFromTopicHelper(fcmToken, userId, onCompletionCallback=undefined, onErrorCallback=undefined) {
        if ((fcmToken == undefined) || (fcmToken == null) || (fcmToken.length < 1)) { 
            if (onCompletionCallback != undefined) { onCompletionCallback(); }
            return; 
        } 
        if ((userId == undefined) || (userId == null) || (userId.length < 1)) { 
            if (onCompletionCallback != undefined) { onCompletionCallback(); }
            return;
        }
         // Also unsubscribe from topic=userId to receive FCM messages from CLoud functions
         var unsubscribeFromTopic = firebase.functions().httpsCallable('unsubscribeFromTopic');
         unsubscribeFromTopic({topic: userId, token:fcmToken})
         .then(() => {
             if (onCompletionCallback != undefined) { onCompletionCallback(); }
         })
         .catch((err) => {
            if (onErrorCallback != undefined) { onErrorCallback(err); }
             console.log(err);
         });
    },
    stringToDate(_date,_format,_delimiter)
    {
        var formatLowerCase=_format.toLowerCase();
        var formatItems=formatLowerCase.split(_delimiter);
        var dateItems=_date.split(_delimiter);
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
        return formatedDate;
    },
    filterShiftsByMatchingWorkingHours(fromShifts, userProfile)
    {
        someShifts=[];
        // a user to receive shifts that fall in their working hours
        for (var i=0; i< fromShifts.length; i++) {
            const aShift = fromShifts[i];
            let shouldAddShift = false;
            const aDate = aShift.startDateTime;
            if(aDate == undefined) { continue; }
            if (userProfile.fulltime == true) {
                console.log('fulltime');
                // check if shift date and start-time fall in Monday - Friday, 8:30 AM - 5:00 PM
                // 0: Sunday 1: Monday 
                if ((aDate.getDay() > 0) && (aDate.getDay() < 6)) {
                    // Monday - Friday
                    // Now check timing 8:30 AM to 5:00 PM
                    // const t = aShift.time.toLowerCase();
                    // let arr = t.split(' ');
                    // const hhmm = arr[0];
                    // const ampm = arr[1];
                    // arr = hhmm.split(':');
                    // const hh = arr[0]; 
                    // const mm = arr[1];
                    // console.log(t);
                    const hh = aDate.getHours();// 24 hours format
                    const mm = aDate.getMinutes();
                    const shiftMins = parseInt(hh)*60+parseInt(mm);
                    console.log('shift: '+aDate+': '+hh+':'+mm+', '+shiftMins);
                    // if (ampm == 'am') {
                    //     if ((parseInt(hh)*60+parseInt(mm)) > (8*60+30)) {
                    //         shouldAddShift = true;
                    //     }
                    // } else {
                        if ( (shiftMins >= (8*60+30)) && 
                             (shiftMins <= (12*60+5*60+0)) ) {
                            shouldAddShift = true;
                        }
                    //}
                }
            } else {
                console.log('parttime');
                // 0: Sunday 1: Monday 
                const weekDay = aDate.getDay();
                console.log(weekDay);
                if ( ((weekDay == 0) && (userProfile.parttimeSun == true)) ||
                ((weekDay == 1) && (userProfile.parttimeMon == true)) ||
                ((weekDay == 2) && (userProfile.parttimeTue == true)) ||
                ((weekDay == 3) && (userProfile.parttimeWed == true)) ||
                ((weekDay == 4) && (userProfile.parttimeThu == true)) ||
                ((weekDay == 5) && (userProfile.parttimeFri == true)) ||
                ((weekDay == 6) && (userProfile.parttimeSat == true)) ) {
                    // matches weekday
                    // check if time matches
                    const profileStartHH = userProfile.workingStartTime.split(':')[0];
                    const profileStartMM = userProfile.workingStartTime.split(':')[1];
                    let startMins = parseInt(profileStartHH) * 60 + parseInt(profileStartMM);
                    if (userProfile.workingStartTimeAmPm.toLowerCase() == 'pm') {
                        startMins = parseInt(profileStartHH) * 60 + parseInt(profileStartMM) + 12*60;
                    }
                    console.log('startMins='+startMins);

                    const profileEndHH = userProfile.workingEndTime.split(':')[0];
                    const profileEndMM = userProfile.workingEndTime.split(':')[1];
                    let endMins = parseInt(profileEndHH) * 60 + parseInt(profileEndMM);
                    if (userProfile.workingEndTimeAmPm.toLowerCase() == 'pm') {
                        endMins = parseInt(profileEndHH) * 60 + parseInt(profileEndMM) + 12*60;
                    }
                    console.log('endMins='+endMins);

                    // const t = aShift.time.toLowerCase();
                    // let arr = t.split(' ');
                    // const hhmm = arr[0];
                    // const ampm = arr[1];
                    // arr = hhmm.split(':');
                    // const hh = arr[0]; 
                    // const mm = arr[1];

                    // let shiftStartMins = parseInt(hh)*60+parseInt(mm);
                    // if (ampm == 'pm') {
                    //     shiftStartMins = parseInt(hh)*60+parseInt(mm) + 12*60;
                    // } 

                    let shiftStartMins = aShift.startDateTime.getHours()*60+parseInt(aShift.startDateTime.getMinutes());

                    console.log('shiftStartMins='+shiftStartMins);


                    if ((shiftStartMins >= startMins) && (shiftStartMins <= endMins)) {
                        shouldAddShift = true;
                    }
                }
            }

            if (shouldAddShift == true) {
                someShifts.push(aShift);
            }
        }
        return someShifts;
    },
    cityDropdownData: function() {
        let data = [];
        const citiesData = SingletonClass.get('citiesData');
        const zones = Object.keys(citiesData);
        for (const aZone of zones) {
          data.push({display:aZone,zone:aZone,city:'-none-'});
          const cities = citiesData[aZone];
          for (const aCity of cities) {
            data.push({display:aCity,zone:aZone,city:aCity});
          }
        }
        return data;
      }
}

export default UltraAgentHelpers;