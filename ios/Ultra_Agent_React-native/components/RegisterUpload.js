import React, { Component } from 'react';
import { StyleSheet, Text, View, 
   Image, TextInput, TouchableOpacity, TouchableHighlight, ImageBackground, 
   ScrollView, Platform, Alert, Dimensions} from 'react-native';
import { List, FlatList } from "react-native";
//import  Icon from 'react-native-ionicons';
import { Picker,  Container, Input, Title, Label, Icon,
    Left, Right, ListItem, Content, Item, Header, Body, Button } from 'native-base';
import DatePicker from 'react-native-datepicker'
import ProfileAsyncModel from "../asyncstoragemodels/ProfileAsyncModel";
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import UploadedObjectAsyncModel from '../asyncstoragemodels/UploadedObjectAsyncModel';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import Spinner from 'react-native-loading-spinner-overlay';
import ProgressCircle from 'react-native-progress-circle';
import SingletonClass from '../helpers/SingletonClass';
import Dialog, { DialogButton, DialogContent, DialogTitle, DialogFooter } from 'react-native-popup-dialog';
// import Icon  from 'react-native-ionicons';

import firebase from 'react-native-firebase';

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

// Because this variable remains in global scope, it should always be a 
// unique name, otherwise, as screen push pop it will get overridden if declared by same name in
// another screen
thisRegisterUploadInstance = null;

export default class RegisterUpload extends Component {

    state = { }
    profileModelRef = null;
    masterDataUpdatedCallbackForParent = undefined;

    constructor(props) {
        super(props);
    
        this.documentTypesData = SingletonClass.get('documentTypesData');

        this.profileModelRef = new ProfileAsyncModel();
        this,this.profileModelRef.setErrorMessage("");
        // Use default values of model
        this.state = {
            ...this.profileModelRef.createState(), 
            spinner:false,
            spinnerMessage: '',
            isUploading:false,
            documentType:'',
            documentNumber:'',
            expiryDate: undefined,
            termsofServiceDialog:false,
        };

        if (this.props.navigation.state.params != undefined) {
            this.masterDataUpdatedCallbackForParent = 
            this.props.navigation.state.params.masterDataUpdatedCallbackForParent;
        }

        this._uploadImage = this._uploadImage.bind(this);

        thisRegisterUploadInstance = this;

        this.uploadProgressUpdate = this.uploadProgressUpdate.bind(this);
    }

    onValueChangeDocumentType(itemValue) {

        if ((itemValue == undefined) || (itemValue.length < 1) || (itemValue == '-none-')) {
        } else {
            this.setState({
                documentType: itemValue,
                documentNumber:'',
                expiryDate:undefined,
            });   
        }
    }

    documentTypesList = () =>{
       
        var arrKeys = Object.keys(this.documentTypesData);
        console.log(arrKeys);
        return( arrKeys.map( (x,i) => { 
            if ((x == undefined) || (x.length < 1) || (x == '-none-')) {
                return( <Picker.Item label={this.documentTypesData[x]} key={i} value={x} disabled={true}/>)
            } else {
                return( <Picker.Item label={this.documentTypesData[x]} key={i} value={x} />)
            } 
        }));

    }

    updateState(element) {
        this.setState({value: element});
      }

    childUpdatedMasterDataCallback = () => {
        //console.log("RegisterUpload::childUpdatedMasterDataCallback");
        this._loadProfileModelFromAsyncStorage();
        if (this.masterDataUpdatedCallbackForParent != undefined) {
            this.masterDataUpdatedCallbackForParent();
        }
    }

    _loadProfileModelFromAsyncStorage() {
        //console.log("RegisterUpload::_loadProfileModelFromAsyncStorage");
        ProfileAsyncModel.restore().then((aRef) => {
            if (aRef!== null) {
                this.profileModelRef = aRef;
                this.setState ({... this.profileModelRef.createState()});
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    componentWillMount() {
        // Required for instancing of models objects.
        UploadedObjectAsyncModel.require(UploadedObjectAsyncModel);
        ProfileAsyncModel.require(ProfileAsyncModel);
 
        this._loadProfileModelFromAsyncStorage();
    }

    componentWillReceiveProps(nextProps){
        this._loadProfileModelFromAsyncStorage(); 
        //console.log("RegisterUpload::componentWillReceiveProps()");
        //console.log(this.state);
    }

    static navigationOptions = {
       header:null
    //    headerTitle: 'REGISTER',
    //    headerStyle: {
    //     backgroundColor: '#5271FF',
    //   },
    //   headerBackTitle: null,
    //   headerTitleStyle: {
    //     color: "#fff",
    //     flex:1,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //   },
    //   headerTintColor: '#fff',
    }

    uploadProgressUpdate = (progress, transferred, total) => {

        wDocuments = this.state.wDocuments;
        const aLength = wDocuments.length;
        if (aLength > 0) {
            wDocuments[aLength-1].setProgress(progress);
            wDocuments[aLength-1].setTotalBytes(total);
            wDocuments[aLength-1].setBytesTransferred(transferred);

            this.setState({ wDocuments });
            //someRandom = Math.random();
            //this.setState({someRandom})
            //console.log("uploadProgressUpdate:progress="+progress+", transferred="+transferred+", total="+total);
            //console.log(this.state);
        }
    }

    _uploadImage (uri, fileName, mime='application/octet-stream') {
        return new Promise((resolve, reject) => {
            const uploadUri = Platform.OS === 'ios' ? uri.replace('file://','') : uri;
            const sessionId = new Date().getTime();
            let uploadBlob = null;

            //console.log(uploadUri)
            const fileParts = uploadUri.split('/')
            const lastPart = fileParts[fileParts.length -1];
            const nameParts = lastPart.split('.')
            const fileExtension = nameParts[nameParts.length -1] 
            const someRandom = Math.floor(Math.random() * 10000) + 1 
            const uniqFileName = "worker-"+sessionId+"-"+someRandom+"."+fileExtension;
            //console.log("uniqFileName="+uniqFileName);

            const uploadRefObject = new UploadedObjectAsyncModel();
            uploadRefObject.setUniqId(uniqFileName);
            uploadRefObject.setName(fileName);
            uploadRefObject.setDocumentType(this.state.documentType);
            if (this.state.documentNumber != undefined) {
                uploadRefObject.setDocumentNumber(this.state.documentNumber);
            }
            if (this.state.expiryDate != undefined) {
            
                uploadRefObject.setExpiryDate(this.state.expiryDate);
            }

            this.setState({
                wDocuments: [...this.state.wDocuments,uploadRefObject]
            });

            //create a reference in firebase  storage for the file 
            const uploadRef = firebase.storage().ref('docs').child(uniqFileName);

            //encode data with Bse64 prior to uploading
            fs.readFile(uploadUri, 'base64')
            .then((data) => {
                return Blob.build(data, { type: `${mime};BASE64` })
            })
            //place the blob into your storage reference
            .then((blob) => {
                uploadBlob = blob
                uploadTask = uploadRef.put(blob._ref, blob, { contentType: mime });

                // Register three observers:
                // 1. 'state_changed' observer, called any time the state changes
                // 2. Error observer, called on failure
                // 3. Completion observer, called on successful completion
                uploadTask.on('state_changed', function(snapshot){
                    // Observe state change events such as progress, pause, and resume
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

                    thisRegisterUploadInstance.uploadProgressUpdate(progress, snapshot.bytesTransferred, snapshot.totalBytes);

                    //console.log('Upload is ' + progress + '% done');
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                        //console.log('Upload is paused');
                        break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                        //console.log('Upload is running');
                        break;
                    }
                },
                function(error) {
                    // Handle unsuccessful uploads
                    reject(error)
                }, function() {
                    // Handle successful uploads on complete
                    // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                    uploadRef.getDownloadURL().then(function(url) {
                        //console.log('File available at', url);
                        resolve(url)
                        
                    });
                });
            });

        });

    }

    _onPressButtonBrowse = () => {

        if ((this.state.documentType == undefined) || (this.state.documentType == null) ||
            (this.state.documentType.length < 1)) 
        {
            UltraAgentHelpers.showAlertWithOneButton("Document Type",
            "Please select a document type first");
            return;
        }

        if ((this.state.documentType != 'Other') && (this.state.documentType != 'Training')) {
            if ((this.state.documentNumber == undefined) || (this.state.documentNumber == null) ||
                (this.state.documentNumber.length < 1)) 
            {
                UltraAgentHelpers.showAlertWithOneButton("Document Number",
                "Please specify a document number first");
                return;
            }
        }

       if ((this.state.documentType != 'Other') && 
            (this.state.documentType != 'Training') &&
            (this.state.documentType != 'ProfessionBodyCertificate') && 
            (this.state.documentType != 'IdentificationDocument')) {
            if ((this.state.expiryDate == undefined) || (this.state.expiryDate == null)) 
            {
                UltraAgentHelpers.showAlertWithOneButton("Expiry Date",
                "Please select a expiry date first");
                return;
            }
        }

        if (this.state.isUploading == true) {
            UltraAgentHelpers.showAlertWithOneButton("Browse not allowed","Please wait for current upload to complete.\n");
            return;
        }

        if ((this.state.wDocuments != undefined) && (this.state.wDocuments != null) &&
            (this.state.wDocuments.length >= 5)) 
        {
            UltraAgentHelpers.showAlertWithOneButton("Upload Limit Reached",
            "You can upload maximum 5 documents.\n"+
            "You can delete one of the earlier uploads and release quota instead.");
            return;
        }

        ImagePicker.showImagePicker({allowsEditing: true}, (response) => {
            console.log('Response = ', response);
    
            if (response.didCancel) {
                //console.log('User cancelled image picker');
            } else if (response.error) {
                //console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                //console.log('User tapped custom button: ', response.customButton);
            } else {  
                
                this.setState({isUploading: true});
                var aFileName = response.fileName;
                if (aFileName == undefined) {
                    var pieces = response.uri.split('/');
                    aFileName = pieces.pop() || pieces.pop();
                }
                this._uploadImage(response.uri, aFileName)
                .then((url) => {
                    this.setState({isUploading: false});
                    //console.log("Upload complete")
                    //console.log(url)
                    wDocuments = thisRegisterUploadInstance.state.wDocuments;
                    const aLength = wDocuments.length;
                    if (aLength > 0) {
                        wDocuments[aLength-1].setBytesTransferred(wDocuments[aLength-1].getTotalBytes());
                        wDocuments[aLength-1].setProgress(100);
                        wDocuments[aLength-1].setUrl(url);

                        thisRegisterUploadInstance.setState({ wDocuments });

                        //Save to model ref as well
                        this.profileModelRef.setWDocuments(wDocuments);
                        this.profileModelRef.store().then(() => {
                        })
                        .catch((error) => {
                            UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                        }); 
                    }
                    //console.log(thisRegisterUploadInstance.state);
                })
                .catch((error) => {
                    this.setState({isUploading: false});
                    //console.log("_onPressButtonBrowse:: Upload failed")
                    console.log(error)
                    UltraAgentHelpers.showAlertWithOneButton("Upload Error",error,"Dismiss");
                })
                
            }
        });
    }
    _onPressButtonNext = () => {

        if (this.state.isUploading == true) {
            UltraAgentHelpers.showAlertWithOneButton("Next not allowed","Please wait for current upload to complete.\n");
            return;
        }

        // if ((this.state.wDocuments == undefined) || 
        //     (this.state.wDocuments.length < 1)) {
        //     UltraAgentHelpers.showAlertWithOneButton("Documents Not Submitted","Please select a document type and upload photo.\n");
        //     return;
        // }


        this.profileModelRef.setWDocuments(this.state.wDocuments);
           
        //console.log(this.profileModelRef.createState());
        // Create a Profile model object for aync storage based data propagation across screens
        this.profileModelRef.setErrorMessage("");
        this.profileModelRef.store().then(() => {
    
            // Notify Parent on Master Data Change
            if (this.masterDataUpdatedCallbackForParent != undefined) {
                this.masterDataUpdatedCallbackForParent();
            }

            // Works on both iOS and Android
            Alert.alert(
                'Enable Gps Service',
                'To use this App, enable the access to your location and the GPS satellite in phone settings',
                [
                {text: 'Don`t allow', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Allow', onPress: () => {
                    this.props.navigation.navigate('SetProfile', {
                        masterDataUpdatedCallbackForParent: this.childUpdatedMasterDataCallback.bind(this)
                    });
                    }
                },
                ],
                { cancelable: false }
            )
            
        }).catch((error) => {
            console.log(error);
            UltraAgentHelpers.showAlertWithOneButton("Oops!","Unable to save data!\nContact support.");
        });
        
       
    }

    shouldComponentUpdate(nextProps, nextState)
    {
        //console.log("RegisterUpload:shouldComponentUpdate");
        return true;
    }

    _onPressDeleteDocument = (aDocument) => {

        if (this.state.isUploading == true) {
            UltraAgentHelpers.showAlertWithOneButton("Delete not allowed","Please wait for upload to complete.\n");
            return;
        }

        if (((aDocument.getUrl() == null) || (aDocument.getUrl().length < 1)) &&
            (aDocument.getProgress() > 0) && (aDocument.getProgress() < 100) ) {
            UltraAgentHelpers.showAlertWithOneButton("Delete Not Allowed","Please wait for upload to complete.\n");
            return;
        }

        Alert.alert(
            "Are you sure?",
            "You are attempting to delete "+aDocument.getName()+" of size "+
                UltraAgentHelpers.formatBytesToDisplay(aDocument.getTotalBytes())+".\n" ,
            [
                //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                {text: 'No, Cancel', onPress: () => {/*cancelled*/}, style: 'cancel'},
                //{text: 'OK', onPress: () => console.log('OK Pressed')},*/
                {text: "Yes, Delete", style:'destructive', onPress: () => {
                    //Delete now
                    this.setState({spinner: true, spinnerMessage:'Deleting...'});
                    firebase.storage().ref('docs').child(aDocument.getUniqId()).delete()
                    .then(() => {

                        arr = [];
                        wDocuments = this.state.wDocuments;
                        for (var i=0; i < wDocuments.length; i++) {
                            if (wDocuments[i].getUniqId() == aDocument.getUniqId()) {
                                // do not save
                            } else {
                                arr.push(wDocuments[i]);
                            }
                        }
                        this.profileModelRef.setWDocuments(arr);
                        this.setState({wDocuments: arr});

                        this.profileModelRef.store().then(() => {
                            this.setState({spinner: false, spinnerMessage:''});
                            UltraAgentHelpers.showAlertWithOneButton("Deleted","Document has been successfully deleted.\nThank you.");
                        })
                        .catch((error) => {
                            console.log(error);
                            this.setState({spinner: false, spinnerMessage:''});
                            UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                        }); 
                    })
                    .catch((error) => {
                        console.log(error);
                        this.setState({spinner: false, spinnerMessage:''});
                        UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                    });

                }}
            ],
            { cancelable: false }
        );

        
    }

    renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: "100%",
              backgroundColor: "#CED0CE",
              marginLeft: "0%"
            }}
          />
        );
    };

  render() {
    return (
        <Container>
            <Header style={{backgroundColor:'#5271FF'}}>
            
            <Left style={{flex:1}}>
                <Button transparent
                onPress={() => this.props.navigation.goBack(this.props.navigation.state.key)}>
                    {/* <Icon name="arrow-back" color='#fff' />  */}
                    <Icon type="FontAwesome" name="angle-left" 
                    style={{fontSize: 40, color: '#fff', fontWeight:'600', bottom:5}}/> 
                </Button>
            </Left>
            <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
                <Title style={styles.HeaderTitle}>{'Register'.toUpperCase()}</Title>
            </Body>
            <Right style={{flex:1}}>
                <Button transparent onPress={this._onPressButtonNext} 
                style={{paddingLeft:0, paddingRight:0,
                marginLeft:0, marginRight:0}}>
                    <Text style={{color:'#FFFFFF', fontWeight:'bold',}}>Skip</Text>
                </Button>
            </Right>
            
            </Header>
            <Content>
            {/* <View style={styles.wrapper}> */}

                <Spinner
                    visible={this.state.spinner}
                    textContent={this.state.spinnerMessage}
                    textStyle={{color: '#FFF'}}
                />
                    <View style={{ flex:1, backgroundColor:'#fff'}} >
                        <ScrollView>

                            <View style={styles.logoContainer}>
                                <Text style={styles.iconText}>
                                    {'Upload CV and Professional \n Documents'}
                                </Text>

                                <View style={[styles.inputContainer, styles.yearPickerStyle]}>
                                        
                                    <Picker
                                    mode="dropdown"
                                    iconRight
                                    // style={{borderColor: 'red', borderWidth:1}}
                                    iosIcon={<Icon type="FontAwesome" name="caret-down" 
                                    style={{ color: "#5271FF", fontSize: 15, position:'absolute', right:-30}} />}
                                    style={ styles.dropdownPickerStyle}
                                    itemDisabledTextStyle={{color:'grey'}}
                                    placeholder="Select a Document Type"
                                    placeholderStyle={{ color: "#838383", fontSize:14, left:0 }}
                                    placeholderTextColor = "#838383"
                                    selectedValue={this.state.documentType}
                                    onValueChange={(documentType) => {this.onValueChangeDocumentType(documentType);}}
                                >
                                    {/* <Picker.Item label="WITH EXPIRY DATE" value="Withexpirydate" disabled = {true} /> */}
                                    {/* <Picker.Item label="Insurance" value="Insurance" />
                                    <Picker.Item label="DBS certificate" value="DBSCertificate" />
                                    <Picker.Item label="Driving License (for drivers only)" value="DrivingLicense" /> */}
                                    {/* <Picker.Item label="WITHOUT EXPIRY DATE" value="Withoutexpirydate" disabled = {true} /> */}
                                    {/* <Picker.Item label="Profession body Certificate" value="ProfessionBodyCertificate" />
                                    <Picker.Item label="Identification document" value="IdentificationDocument" />
                                    <Picker.Item label="Other (e.g CV)" value="Other" /> */}

                                    {this.documentTypesList()}
                                </Picker>
                                {/* <Icon type="FontAwesome" name="caret-down" 
                                style={{fontSize: 15, color: '#5271FF', position:'absolute', right:110, zIndex:9}}/> */}
                                
                            </View>
                                { this.state.documentType != 'Other' && 
                                this.state.documentType != 'Training' &&
                                    <View style={styles.inputContainer}>
                                    {/* <Image source={require('../images/dbs-icon.png')} 
                                    style={styles.InputImageIconsize} /> */}
                                    <TextInput 
                                        style={{flex:1}} 
                                        underlineColorAndroid={'transparent'}
                                        placeholder="Enter Document Number" 
                                        placeholderTextColor = "#838383"
                                        keyboardType='default' 
                                        outoCorrect={false}
                                        onChangeText={documentNumber => this.setState({ documentNumber })}
                                        value={this.state.documentNumber}
                                    />

                                    </View>
                                }
                            
                            { this.state.documentType != 'Other' && 
                            this.state.documentType != 'Training' && 
                            this.state.documentType != 'ProfessionBodyCertificate' && 
                            this.state.documentType != 'IdentificationDocument' &&
                                    <View style={[styles.inputContainer, styles.dateInputContainer]}>
                                            
                                        <DatePicker
                                            style={{width: '100%', flex:1, height:30, bottom:5, paddingLeft:8}}
                                            date={this.state.expiryDate}
                                            mode="date"
                                            placeholder="Choose Expiry Date"
                                            placeholderStyle={{ color: "#838383", fontSize:14, left:0 }}
                                            format="DD-MM-YYYY"
                                            minDate={new Date()}
                                            // maxDate="2016-06-01"                               
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            showIcon={false}
                                            customStyles={{
                                                dateInput: {
                                                    alignItems : 'flex-start',
                                                    borderColor: 'transparent', 
                                                    borderWidth: 0,
                                                    paddingLeft: 0,
                                                },
                                                placeholderText: { 
                                                    color: '#838383' 
                                                }
                                            }}
                                            onDateChange={(expiryDate) => {this.setState({expiryDate: expiryDate})}}
                                            ref={(ref)=>this.datePickerRef=ref}
                                        />
                                            {/* <Image source={require('../images/datepicker-icon.png')} 
                                            style={[styles.InputImageIcon, styles.imageBg]} />    */}
                                            <TouchableHighlight onPress={() => this.datePickerRef.onPressDate()} 
                                                underlayColor='transparent'>
                                                <Image source={require('../images/datepicker-icon.png')} 
                                                style={[styles.InputImageIcon, styles.imageBg]}/>
                                            </TouchableHighlight> 
                                    </View>
                                }

                                { this.state.documentType != undefined && this.state.documentType.length > 0 &&
                                this.state.documentNumber != undefined && this.state.documentNumber.length > 0 &&
                                this.state.expiryDate != undefined &&
                                    <View>
                                        {/* <Image style={styles.logo} 
                                        source={require('../images/down50x50.png')}/> */}
                                        <TouchableHighlight onPress={this._onPressButtonBrowse} 
                                            style={styles.browseButton}>
                                            <Text style={{color:'#2EB2FF', alignItems: 'center', 
                                            textAlign:'center', fontWeight:'bold'}}>
                                                SELECT PHOTO 
                                            </Text>
                                        </TouchableHighlight>
                                    </View>
                                }

                                { this.state.documentType != undefined && 
                                    (this.state.documentType == 'ProfessionBodyCertificate' || this.state.documentType == 'IdentificationDocument' || this.state.documentType == 'Other' || this.state.documentType == 'Training') &&
                                    this.state.documentNumber != undefined &&  this.state.documentNumber.length > 0 &&
                                    <View>
                                        {/* <Image style={styles.logo} 
                                        source={require('../images/down50x50.png')}/> */}
                                        <TouchableHighlight onPress={this._onPressButtonBrowse} 
                                            style={styles.browseButton}>
                                            <Text style={{color:'#2EB2FF', alignItems: 'center', 
                                            textAlign:'center', fontWeight:'bold'}}>
                                                SELECT PHOTO
                                            </Text>
                                        </TouchableHighlight>
                                    </View>
                                }

                                { this.state.documentType != undefined && this.state.documentType == 'Training' &&
                                    <View>
                                        {/* <Image style={styles.logo} 
                                        source={require('../images/down50x50.png')}/> */}
                                        <TouchableHighlight onPress={this._onPressButtonBrowse} 
                                            style={styles.browseButton}>
                                            <Text style={{color:'#2EB2FF', alignItems: 'center', 
                                            textAlign:'center', fontWeight:'bold'}}>
                                                SELECT PHOTO
                                            </Text>
                                        </TouchableHighlight>
                                    </View>
                                }

                                { this.state.documentType != undefined && this.state.documentType == 'Other' &&
                                    <View>
                                        {/* <Image style={styles.logo} 
                                        source={require('../images/down50x50.png')}/> */}
                                        <TouchableHighlight onPress={this._onPressButtonBrowse} 
                                            style={styles.browseButton}>
                                            <Text style={{color:'#2EB2FF', alignItems: 'center', 
                                            textAlign:'center', fontWeight:'bold'}}>
                                                SELECT PHOTO
                                            </Text>
                                        </TouchableHighlight>
                                    </View>
                                }

                              
                                

                            </View>

                                <View style={styles.uploadContainer}>
                                {/* <ScrollView> */}
                                    <FlatList
                                        style={styles.listContentStyle}
                                        extraData={this.state} /*IMPORTANT: This line makes the byte counts update*/
                                        data={this.state.wDocuments}
                                        keyExtractor={(item, index) => index+""}
                                        ItemSeparatorComponent={this.renderSeparator}
                                        renderItem={({ item }) => (
                                            <View style={[styles.rowContainer, styles.GrayBgColor]}>
                                                <View style={styles.textContainer}>
                                                <ProgressCircle
                                                    percent={item.getProgress()}
                                                    radius={16}
                                                    borderWidth={4}
                                                    color="#3399FF"
                                                    shadowColor="#999"
                                                    bgColor="#fff"
                                                >
                                                    {/* <Text style={{ fontSize: 18 }}>{'30%'}</Text> */}
                                                    <Image source={require('../images/pdf-icon1.png')} 
                                                    style={styles.pdfImageIcon} />
                                                </ProgressCircle>
                                                    {/* <Image source={require('../images/pdf-icon1.png')} 
                                                    style={styles.pdfImageIcon} /> */}
                                                    <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                                        <Text style={styles.textColor}>
                                                            { this.documentTypesData[item.getDocumentType()] }
                                                        </Text>
                                                        { item.getDocumentNumber().length > 0 && 
                                                            <Text style={styles.textColor}>
                                                                { item.getDocumentNumber() }
                                                            </Text>
                                                        }
                                                        { item.getExpiryDate().length > 0 && 
                                                            <Text style={styles.textColor}>
                                                                Expiry: { item.getExpiryDate() }
                                                            </Text>
                                                        }
                                                        <Text style={styles.textColor}>
                                                            { UltraAgentHelpers.formatBytesToDisplay(item.getBytesTransferred()) } / { UltraAgentHelpers.formatBytesToDisplay(item.getTotalBytes()) }
                                                        </Text>
                                                    
                                                    </View>
                                                </View>
                                                <View style={styles.crossIconContainer}>
                                                    <Icon style ={styles.crossIcon} type="FontAwesome" name="times-circle-o" 
                                                        style={{fontSize: 20, color: '#f20028'}} onPress={() => this._onPressDeleteDocument(item)}/>
                                                </View>
                                            </View>
                                        )}
                                    />
                                {/* </ScrollView> */}
                                    {/* <View style={[styles.rowContainer, styles.GrayBgColor]}>
                                        <View style={styles.textContainer}>
                                            <Image source={require('../images/pdf-icon1.png')} 
                                            style={styles.pdfImageIcon} />
                                            <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                                <Text style={styles.textColor}>
                                                    CV.pdf1
                                                </Text>
                                                <Text style={styles.textColor}>
                                                    1.36mb/1.35mb
                                                </Text>
                                            </View>
                                        </View>
                                        <View style={styles.crossIconContainer}>
                                            <Icon style ={styles.crossIcon} name="md-close-circle" 
                                                size={20} color="#f20028" />
                                        </View>
                                    </View> */}
                                    {/* <View style={[styles.rowContainer, styles.WhiteBgColor]}>
                                        <View style={styles.textContainer}>
                                            <Image source={require('../images/pdf-icon2.png')} 
                                            style={styles.pdfImageIcon} />
                                            <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                                <Text style={styles.textColor}>
                                                    CV.pdf1
                                                </Text>
                                                <Text style={styles.textColor}>
                                                    1.36mb/1.35mb
                                                </Text>
                                            </View>
                                        </View>
                                        <View style={styles.crossIconContainer}>
                                            <Icon style ={styles.crossIcon} name="md-close-circle" 
                                                size={20} color="#f20028" />
                                        </View>
                                    </View> */}
                                    {/* <View style={[styles.rowContainer, styles.GrayBgColor]}>
                                        <View style={styles.textContainer}>
                                            <Image source={require('../images/pdf-icon3.png')} 
                                            style={styles.pdfImageIcon} />
                                            <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                                <Text style={styles.textColor}>
                                                    CV.pdf1
                                                </Text>
                                                <Text style={styles.textColor}>
                                                    1.36mb/1.35mb
                                                </Text>
                                            </View>
                                        </View>
                                        <View style={styles.crossIconContainer}>
                                            <Icon style ={styles.crossIcon} name="md-close-circle" 
                                                size={20} color="#f20028" />
                                        </View>
                                    </View> */}
                                
                                    <TouchableHighlight onPress={this._onPressButtonNext} 
                                    style={styles.button}>
                                        <Text style={styles.btntext}>
                                            NEXT 
                                        </Text>
                                    </TouchableHighlight>
                                
                                
                                </View>
                                <View style={styles.linkContainer}>
                                    <Text style={styles.regText}><Icon type="FontAwesome" name="copyright" 
                                    style={{fontSize: 14, color: '#000', marginRight:3}}/> 2019 UA technology .inc</Text>
                                    <Text style={styles.regText} onPress={() => {
                                        this.setState({ termsofServiceDialog: true });
                                        }}
                                    activeOpacity={0.7}>
                                        Privacy | Terms 
                                    </Text>
                                </View>
                        </ScrollView>
                        <View>
                {/* Change password dialog container */}
                    <Dialog
                    visible={this.state.termsofServiceDialog}
                    onTouchOutside={() => {
                        this.setState({ termsofServiceDialog: false });
                        }}
                    dialogTitle={
                        <DialogTitle title="Terms of service"
                        textStyle={{ color: '#000', fontSize:18 }}
                        />
                    }
                    >
                    <TouchableOpacity onPress={() => {
                        this.setState({ termsofServiceDialog: false });
                        }}  style={{position: 'absolute', right: 15, top:5,
                        justifyContent: 'center'}}>
                        <Icon type="FontAwesome" name='close' 
                        style={{fontSize: 20, color: '#5271FF', marginRight:3, marginTop:10}}/>
                    </TouchableOpacity>
                    
                    {/* <ScrollView> */}
                        <DialogContent style={{height:400}}>
                            <ScrollView>
                            <Text style={styles.termsHeading}>
                            Introduction</Text>
                            <Text style={styles.termsText}>
                                These terms and conditions apply between you, 
                                the User of this Website (including any sub-domains, unless expressly 
                                excluded by their own terms and conditions), and Ultra Agent Limited, 
                                the owner and operator of this Website. Please read these terms and 
                                conditions carefully, as they affect your legal rights. Your agreement 
                                to comply with and be bound by these terms and conditions is deemed to 
                                occur upon your first use of the Website. If you do not agree to be 
                                bound by these terms and conditions, you should stop using the Website 
                                immediately.
                            </Text>
                            <Text style={styles.termsText}>
                                In these terms and conditions, User or Users means any third party 
                                that accesses the Website and is not either (i) employed by Ultra 
                                Agent Limited and acting in the course of their employment or (ii) 
                                engaged as a consultant or otherwise providing services to Ultra 
                                Agent Limited and accessing the Website in connection with the 
                                provision of such services.
                            </Text>
                            <Text style={styles.termsText}>
                                You must be at least 16 years of age to use this Website. 
                                By using the Website and agreeing to these terms and conditions, 
                                you represent and warrant that you are at least 16 years of age.
                            </Text>
                            <Text style={styles.termsHeading}>
                                Intellectual property and acceptable use
                            </Text>
                            <Text style={styles.termsText}>
                                1. All Content included on the Website, unless uploaded by Users, is the property of 
                                Ultra Agent Limited, our affiliates or other relevant third parties. In these terms 
                                and conditions, Content means any text, graphics, images, audio, video, software, 
                                data compilations, page layout, underlying code and software and any other form of 
                                information capable of being stored in a computer that appears on or forms part of this
                                Website, including any such content uploaded by Users. By continuing to use 
                                the Website you acknowledge that such Content is protected by copyright, trademarks, 
                                database rights and other intellectual property rights. Nothing on this site shall be 
                                construed as granting, by implication, estoppel, or otherwise, any license or right to 
                                use any trademark, logo or service mark displayed on the site without the owner's prior 
                                written permission
                            </Text>
                            <Text style={styles.termsText}>
                                2. You may, for your own personal, non-commercial use only, do the following :
                            </Text>
                            <View style={{marginLeft:10}}>
                                <Text style={styles.termsText}>
                                a. retrieve, display and view the Content on a computer screen
                                </Text>
                                <Text style={styles.termsText}>
                                b. download and store the Content in electronic form on a disk (but not on any server or other storage device connected to a network)
                                </Text>
                                <Text style={styles.termsText}>
                                c. print one copy of the Content
                                </Text>
                            </View>
                            <Text style={styles.termsText}>
                                3. You must not otherwise reproduce, modify, copy, distribute or use for commercial purposes any Content without the written permission of Ultra Agent Limited.
                            </Text>
                            <Text style={styles.termsText}>
                            4. You acknowledge that you are responsible for any Content you 
                            may submit via the Website, including the legality, reliability, 
                            appropriateness, originality and copyright of any such Content. 
                            You may not upload to, distribute or otherwise publish through the
                                Website any Content that (i) is confidential, proprietary, false, 
                                fraudulent, libellous, defamatory, obscene, threatening, invasive 
                                of privacy or publicity rights, infringing on intellectual property 
                                rights, abusive, illegal or otherwise objectionable; (ii) may 
                                constitute or encourage a criminal offence, violate the rights of 
                                any party or otherwise give rise to liability or violate any law; or 
                                (iii) may contain software viruses, political campaigning, chain 
                                letters, mass mailings, or any form of "spam." You may not use a 
                                false email address or other identifying information, impersonate 
                                any person or entity or otherwise mislead as to the origin of any 
                                content. You may not upload commercial content onto the Website.
                            </Text>
                            <Text style={styles.termsText}>
                                5. You represent and warrant that you own or otherwise control all 
                                the rights to the Content you post; that the Content is accurate; 
                                that use of the Content you supply does not violate any provision 
                                of these terms and conditions and will not cause injury to any 
                                person; and that you will indemnify Ultra Agent Limited for all 
                                claims resulting from Content you supply.
                            </Text>
                            <Text style={styles.termsHeading}>Prohibited use</Text>
                            <Text style={styles.termsText}>6. You may not use the Website 
                            for any of the following purposes:</Text>
                            <View style={{marginLeft:10}}>
                                <Text style={styles.termsText}>a. in any way which causes, or may cause, damage to the 
                                Website or interferes with any other person's use or enjoyment 
                                of the Website;
                                </Text>
                                <Text style={styles.termsText}>b. in any way which is harmful, 
                                unlawful, illegal, abusive, harassing, threatening or otherwise 
                                objectionable or in breach of any applicable law, regulation, 
                                governmental order;
                                </Text>
                                <Text style={styles.termsText}>c. making, transmitting or storing 
                                electronic copies of Content protected by copyright without the 
                                permission of the owner.</Text>
                            </View>
                            <Text style={styles.termsHeading}>Registration</Text>
                            <Text style={styles.termsText}>7. You must ensure that the details 
                            provided by you on registration or at any time are correct and 
                            complete.</Text>
                            <Text style={styles.termsText}>8. You must inform us immediately of 
                            any changes to the information that you provide when registering by 
                            updating your personal details to ensure we can communicate with you
                                effectively.
                            </Text>
                            <Text style={styles.termsText}>9. We may suspend or cancel your 
                            registration with immediate effect for any reasonable purposes or if
                            you breach these terms and conditions.
                            </Text>
                            <Text style={styles.termsText}>10. You may cancel your registration at 
                            any time by informing us in writing to the address at the end of these 
                            terms and conditions. If you do so, you must immediately stop using the 
                            Website. Cancellation or suspension of your registration does not affect 
                            any statutory rights.
                            </Text>
                            <Text style={styles.termsHeading}>Password and security</Text>
                            <Text style={styles.termsText}>11. When you register on this Website, 
                            you will be asked to create a password, which you should keep 
                            confidential and not disclose or share with anyone.
                            </Text>
                            <Text style={styles.termsText}>12. If we have reason to believe that 
                            there is or is likely to be any misuse of the Website or breach of 
                            security, we may require you to change your password or suspend your 
                            account.
                            </Text>
                            <Text style={styles.termsHeading}>Links to other websites</Text>
                            <Text style={styles.termsText}>13. This Website may contain links to 
                            other sites. Unless expressly stated, these sites are not under the 
                            control of Ultra Agent Limited or that of our affiliates.
                            </Text>
                            <Text style={styles.termsText}>14. We assume no responsibility for the 
                            content of such Websites and disclaim liability for any and all forms 
                            of loss or damage arising out of the use of them.
                            </Text>
                            <Text style={styles.termsText}>15. The inclusion of a link to another 
                            site on this Website does not imply any endorsement of the sites 
                            themselves or of those in control of them.
                            </Text>
                            <Text style={styles.termsHeading}>Privacy Policy and Cookies Policy
                            </Text>
                            <Text style={styles.termsText}>16. Use of the Website is also governed by our Privacy 
                            Policy and Cookies Policy, which are incorporated into these terms and conditions by this 
                            reference. To view the Privacy Policy and Cookies Policy, please click on 
                            the following: _______________ and _______________.
                            </Text>
                            <Text style={styles.termsHeading}>Availability of the Website and 
                            disclaimers</Text>
                            <Text style={styles.termsText}>17. Any online facilities, tools, 
                            services or information that Ultra Agent Limited makes available 
                            through the Website (the Service) is provided "as is" and on an 
                            "as available" basis. We give no warranty that the Service will be 
                            free of defects and/or faults. To the maximum extent permitted by the 
                            law, we provide no warranties (express or implied) of fitness for a 
                            particular purpose, accuracy of information, compatibility and 
                            satisfactory quality. Ultra Agent Limited is under no obligation to 
                            update information on the Website.
                            </Text>
                            <Text style={styles.termsText}>18. Whilst Ultra Agent Limited uses 
                            reasonable endeavours to ensure that the Website is secure and free 
                            of errors, viruses and other malware, we give no warranty or guaranty 
                            in that regard and all Users take responsibility for their own 
                            security, that of their personal details and their computers.
                            </Text>
                            <Text style={styles.termsText}>19. Ultra Agent Limited accepts no 
                            liability for any disruption or non-availability of the Website.
                            </Text>
                            <Text style={styles.termsText}>20. Ultra Agent Limited reserves the 
                            right to alter, suspend or discontinue any part (or the whole of) 
                            the Website including, but not limited to, any products and/or 
                            services available. These terms and conditions shall continue to apply 
                            to any modified version of the Website unless it is expressly stated 
                            otherwise.
                            </Text>
                            <Text style={styles.termsHeading}>Limitation of liability</Text>
                            <Text style={styles.termsText}>21. Nothing in these terms and conditions will: (a) 
                            limit or exclude our or your liability for death or personal injury resulting from 
                            our or your negligence, as applicable; (b) limit or exclude our or your liability 
                            for fraud or fraudulent misrepresentation; or (c) limit or exclude any of our or your 
                            liabilities in any way that is not permitted under applicable law.</Text>
                            <Text style={styles.termsText}>22. We will not be liable to you in 
                            respect of any losses arising out of events beyond our reasonable
                            control.
                            </Text>
                            <Text style={styles.termsText}>23. To the maximum extent permitted by 
                            law, Ultra Agent Limited accepts no liability for any of the following:
                            </Text>
                            <View style={{marginLeft:10}}>
                            <Text style={styles.termsText}>a. any business losses, such as loss of 
                            profits, income, revenue, anticipated savings, business, contracts, 
                            goodwill or commercial opportunities;
                                </Text>
                            <Text style={styles.termsText}>b. loss or corruption of any data, 
                            database or software;
                                </Text>
                            <Text style={styles.termsText}>c. any special, indirect or 
                            consequential loss or damage.
                                </Text>
                            </View>
                            <Text style={styles.termsHeading}>General</Text>
                            <Text style={styles.termsText}>24. You may not transfer any of your 
                            rights under these terms and conditions to any other person. We may 
                            transfer our rights under these terms and conditions where we 
                            reasonably believe your rights will not be affected.
                            </Text>
                            <Text style={styles.termsText}>25. These terms and conditions may be 
                            varied by us from time to time. Such revised terms will apply to the 
                            Website from the date of publication. Users should check the terms and 
                            conditions regularly to ensure familiarity with the then current 
                            version.
                            </Text>
                            <Text style={styles.termsText}>26. These terms and conditions together 
                            with the Privacy Policy and Cookies Policy contain the whole agreement 
                            between the parties relating to its subject matter and supersede all 
                            prior discussions, arrangements or agreements that might have taken 
                            place in relation to the terms and conditions.
                            </Text>
                            <Text style={styles.termsText}>27. The Contracts (Rights of Third 
                            Parties) Act 1999 shall not apply to these terms and conditions and no 
                            third party will have any right to enforce or rely on any provision of 
                            these terms and conditions.
                            </Text>
                            <Text style={styles.termsText}>28. If any court or competent 
                            authority finds that any provision of these terms and conditions 
                            (or part of any provision) is invalid, illegal or unenforceable, that 
                            provision or part-provision will, to the extent required, be deemed to
                            be deleted, and the validity and enforceability of the other 
                            provisions of these terms and conditions will not be affected.
                            </Text>
                            <Text style={styles.termsText}>29. Unless otherwise agreed, no delay, 
                            act or omission by a party in exercising any right or remedy will be 
                            deemed a waiver of that, or any other, right or remedy.
                            </Text>
                            <Text style={styles.termsText}>30. This Agreement shall be governed 
                            by and interpreted according to the law of England and Wales and all 
                            disputes arising under the Agreement (including non-contractual 
                            disputes or claims) shall be subject to the exclusive jurisdiction of 
                            the English and Welsh courts.
                            </Text>
                            <Text style={styles.termsHeading}>Ultra Agent Limited details</Text>
                            <Text style={styles.termsText}>31. Ultra Agent Limited is a company 
                            incorporated in England and Wales with registered number 10510063 whose 
                            registered address is 9 Rotherwick hill, Ealing, London, W5 3EQ and it 
                            operates the Website www.ultraagent.co.uk.
                            </Text>
                            <Text style={styles.termsText}>You can contact Ultra Agent Limited by
                            email on Hello@ultragent.co.uk.
                            </Text>
                            <Text style={styles.termsHeading}>Attribution</Text>
                            <Text style={styles.termsText}>32. These terms and conditions were 
                            created using a document from Rocket Lawyer</Text> 
                            {/* <Text onPress={()=>{Linking.openURL('https://www.rocketlawyer.co.uk')}>(https://www.rocketlawyer.co.uk).</Text>
                            <Text onPress={()=>{Linking.openURL('https://www.rocketlawyer.co.uk')}>My Text</Text> */}
                            <TouchableOpacity onPress={() => Linking.openURL('https://www.rocketlawyer.co.uk')} >
                                <Text style={{color:'#5271FF'}}>
                                https://www.rocketlawyer.co.uk                            
                                </Text>
                            </TouchableOpacity>
                            </ScrollView>
                        </DialogContent>
                    {/* </ScrollView> */}
                </Dialog>
                </View> 

                    </View>

            {/* </View> */}
            </Content>
        </Container>
    );
  }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container:{
        flex:1,
        alignItems: 'center',
        alignSelf:'stretch',
        backgroundColor:'#fff',
        width: null,
        height:null,
    },
    logoContainer:{
        alignItems: 'center',
        alignSelf:'stretch',
        backgroundColor:'#fff',
        marginTop:20,
        marginBottom:10,
        padding:15,
    },
    HeaderTitle:{
        color:'#fff',
        textAlign:'center',
        alignItems:'center',
      },
    iconText:{
        color:'#000',
        textAlign:'center',
        alignItems: 'center',
        marginTop:10,
        marginBottom:30,
        lineHeight:20,
    },
    logo:{
        marginTop:20
    },
    browseButton:{
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#2EB2FF',
        backgroundColor:'transparent',
        padding:10,
        marginTop:20,
        width:150,
        alignItems: 'center',
    },
    uploadContainer:{
        alignItems: 'stretch',
        backgroundColor:'transparent',
        marginBottom:20,
        paddingBottom:10,
        paddingTop:10,

    },
    GrayBgColor:{
        backgroundColor:'#f5f5f5',
    },
    WhiteBgColor:{
        backgroundColor:'#fff',
    },
    rowContainer:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft:30,
        paddingRight:30,
        paddingTop:10,
        paddingBottom:10,
    },
    pdfImageIcon:{
        width:40,
        height:40
    },
    textContainer: {
        flex: 1, 
        flexDirection: 'row',
        alignSelf:'stretch',
    },
    textColor:{
        color:'#838383',
    },
    crossIconContainer:{
        alignItems: 'center',
        alignSelf:'stretch',
        justifyContent: 'center',
    },

    button:{
        alignSelf:'stretch',
        marginLeft:70,
        marginRight:70,
        marginTop:20,
        marginBottom:50,
        backgroundColor:'#5271FF',
        padding:12,
        borderRadius:3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btntext:{
        textAlign:'center',
        alignSelf:'stretch',
        alignItems: 'center',
        color:'#fff',
        fontWeight:'500',
        fontSize:14,
        letterSpacing:1,
        color:'#fff',
    },
    linkContainer:{
        alignSelf:'stretch',
        alignItems:'center',
        marginBottom:20,
        //marginTop:20,
        justifyContent:'flex-end',
        position:'absolute',
        bottom:0,
        left:0,
        right:0,
    },
    
    regText:{
        marginTop:5,
        color:'#000',
    },
    dateInputContainer:{
        padding:0,
    },
    inputContainer: {
        flex: 1, 
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'stretch',
        padding:8,
        backgroundColor:'#fff',
        borderRadius:2,
        marginBottom:12,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.1,
                height:40,
            },
            android: {
                elevation:2,
                height:40,
                paddingBottom:0,
                paddingTop:0,
            },
        })
    },
    dropdownPickerStyle:{
        ...Platform.select({
            ios: {
                width: Dimensions.get('window').width-55,
                left:-15,
            },
            android: {
                width: undefined, 
                left:-5,
            },
        })
    },
    InputImageIcon: {
        marginRight:10,
        marginLeft:5,
        height: 20,
        width: 20,
        resizeMode : 'stretch',
        alignItems: 'center'
    },
    imageBg:{
        backgroundColor:'#efefef',
        marginRight:0,
        padding:20,
        height: 10,
        width: 10,
    },
  
});