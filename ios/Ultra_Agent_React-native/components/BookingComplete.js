import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Alert, Text
  
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, Picker, List, ListItem, Content, Item, Header, Body,
} from 'native-base';
import Icon  from 'react-native-ionicons';
import CountDown from 'react-native-countdown-component';
import moment from 'moment';
import firebase from 'react-native-firebase';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

export default class BookingComplete extends Component {

    constructor(props) {
        super(props);

        booking = 
            this.props.navigation.state.params.booking;

        this.state = {
            booking:booking,
            totalDuration: 0,
        };
    }

    componentWillUnmount() {
        if (this.unsubscribeSlotsCollection != undefined) {
            this.unsubscribeSlotsCollection();
        }
      }

    componentDidMount() {
        // var that = this;
        // var date = moment()
        //   .utcOffset('+05:30')
        //   .format('YYYY-MM-DD hh:mm:ss');
        // //Getting the current date-time with required formate and UTC   
        
        // var expirydate = '2018-11-03 04:00:45';//You can set your own date-time
        // //Let suppose we have to show the countdown for above date-time 
     
        // var diffr = moment.duration(moment(expirydate).diff(moment(date)));
        // //difference of the expiry date-time given and current date-time
     
        // var hours = parseInt(diffr.asHours());
        // var minutes = parseInt(diffr.minutes());
        // var seconds = parseInt(diffr.seconds());
        
        // var d = hours * 60 * 60 + minutes * 60 + seconds;
        // //converting in seconds
     
        // that.setState({ totalDuration: d });
        // //Settign up the duration of countdown in seconds to re-render

        this.unsubscribeSlotsCollection = firebase.firestore().collection('bookings')
        .doc(this.state.booking.id).collection('workedslots')
        .onSnapshot(this.onSlotsCollectionUpdate); 

    }

    onSlotsCollectionUpdate = (querySnapshot) => {
        let secsWorked = 0;

        //   const compCount = allCompanies.length;
        querySnapshot.forEach((doc) => {
            aObj = doc.data();
            aObj.id = doc.id;
            if (aObj.endTime == undefined) {
                // consider this if it is a active slot, else skip
                if (this.state.booking.workActiveSlotId == aObj.id) {
                    secsWorked += ((new Date()).getTime() - aObj.startTime.getTime())/1000;
                }
            } else {
                secsWorked += (aObj.endTime.getTime() - aObj.startTime.getTime())/1000;
            }
        });
       
        this.setState({ 
            totalDuration:secsWorked
        });
    }
      

    static navigationOptions = {
        header:null
    //     headerTitle: 'PLUMBING #1038',
    //     headerStyle: {
    //     backgroundColor: '#5271FF',
    //     },
    //     headerBackTitle: null,
    //     headerTitleStyle: {
    //     color: "#fff",
    //     flex:1,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    // },
    // headerTintColor: '#fff',
    }
  
   

  render() {

    return (
      <Container>
           <Header style={{backgroundColor:'#5271FF'}}> 
            <Left style={{flex:1}}>
                <Button transparent 
                onPress={() => {
                    //this.props.navigation.goBack(this.props.navigation.state.key);
                    this.props.navigation.popToTop();
                }}>
                    <Icon name="arrow-back" color='#fff' />  
                </Button>
            </Left>
            <Body style={{flex:4, textAlign:'center', alignItems:'center'}}>
                <Title style={styles.HeaderTitle}>{this.state.booking.shiftSubCategory} #{this.state.booking.shiftNo}</Title>
            </Body>
            <Right style={{flex:1}}>
            </Right>  
        </Header>
        <Content>
          <View style={styles.cancelBookingContainer}>
                <View style={styles.locationTextStyle}>
                    <Text style={styles.locTextStyle}>
                        Location : 
                    </Text>
                    <Text style={styles.locUpperTextStyle}>
                         {this.state.booking.address.toUpperCase()}
                    </Text>
                </View>
                <View style={styles.timeTextStyle}>
                    <Text style={styles.dateTextStyle}>
                        <Icon name="md-clock" size={20} color="#888"/>
                        {this.state.booking.shiftStartDateTime.getHours()}:
                        {("0" + this.state.booking.shiftStartDateTime.getMinutes()).slice(-2)}
                    </Text>
                    <Text style={styles.dateTextStyle}>
                        <Icon name="md-calendar" size={20} color="#888"/>
                        {this.state.booking.workStartTime.getDate()}/
                        {this.state.booking.workStartTime.getMonth()+1}/
                        {this.state.booking.workStartTime.getFullYear()}
                    </Text>
                </View>
                <Text style={{fontWeight:'bold', fontSize:16}}>Time Started {}
                    {this.state.booking.workStartTime != undefined? this.state.booking.workStartTime.getHours(): '0'}
                    :
                    {("0" + (this.state.booking.workStartTime != undefined? this.state.booking.workStartTime.getMinutes() : '0')).slice(-2)}
                </Text>
                <View style={styles.countdownTimerStyle}>
                    {/* <Label style={styles.shiftLabel}>Stop Watch</Label> */}
                    <CountDown
                        paused={true}
                        timetoShow={('H', 'M', 'S')}
                        until={this.state.totalDuration}
                        //duration of countdown in seconds
                        onFinish={null}
                        onPress={null}
                        size={30}
                        digitBgColor='transparent'
                        timeTxtColor="#c0c0c0"
                        labelH='Hr'
                        labelM='Min'
                        labelS='Sec'
                        secondColor="#f57df5"
                        />
                </View>
                <View style={styles.successTextConatiner}>
                    <Image style={styles.successimage} 
                        source={require('../images/success-icon.png')}/>
                        <Text style={styles.successText}>
                            {'Good news track your \nshift in your Time Sheet'}                                
                        </Text>
                </View>
          </View>
        </Content>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
    HeaderTitle:{
        color:'#fff',
      },
    cancelBookingContainer:{
        padding:30,
        marginTop:5,
        marginBottom:30,
        alignItems:'center',
        alignSelf:'center',
    },
    locationTextStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems:'center',
        marginBottom:30,
    },
    timeTextStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems:'center',
        marginBottom:5,
      },
    locUpperTextStyle:{
        fontWeight:'bold',
        fontSize:18
    },
    locTextStyle:{
        fontWeight:'bold',
    },
    dateTextStyle:{
        margin:8,
        fontSize:16,
    },
    shiftLabel:{
        color:'#c0c0c0',
    },
    countdownTimerStyle:{
        marginTop:30,
        marginBottom:10,
    },
    successTextConatiner:{
      alignItems:'center',
      justifyContent:'center',
      marginTop:40,
    },
    successimage:{
        width:120,
        height:120,
      },
    successText:{
        fontSize:20,
        fontWeight:'bold',
        marginTop:10,
        lineHeight:25
    }
    
})