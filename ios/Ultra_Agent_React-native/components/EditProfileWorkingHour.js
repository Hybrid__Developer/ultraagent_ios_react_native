import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Text, ImageBackground, 
  Dimensions, TouchableOpacity, TextInput, Platform
  
} from 'react-native';
import {
  Container, Header, Title, Label, Button,
  Left, Right, Item, Content, Picker
} from 'native-base';

import { CheckBox } from 'react-native-elements';

import Icon  from 'react-native-ionicons';


export default class EditProfileWorkingHour extends Component {

workingHoursUpdatedCallbackForParent = undefined;

  constructor(props) {
    super(props);
    this.state = {
        selectedTime: undefined,
        fulltime:true,
        parttime:false,
        parttimeMon:true,
        parttimeTue:true,
        parttimeWed:true,
        parttimeThu:true,
        parttimeFri:true,
        parttimeSat:true,
        parttimeSun:true,
        workingStartTime:"12:00",
        workingEndTime:"15:00",
        workingStartTimeAmPm:"AM",
        workingEndTimeAmPm:"PM",
    };


    if (this.props.navigation.state.params != undefined) {
        this.workingHoursUpdatedCallbackForParent = 
        this.props.navigation.state.params.workingHoursUpdatedCallbackForParent;

        if (this.props.navigation.state.params.parentState != undefined) {
            //console.log('parent state');
            //console.log(this.props.navigation.state.params.parentState);
            this.state = {...this.props.navigation.state.params.parentState}
        }
    }
  }

  onValueChangeTime(value) {
    this.setState({
        selectedTime: value
    });
  }

  _onPressButtonSave = () =>{
    if (this.workingHoursUpdatedCallbackForParent != undefined) {
        this.workingHoursUpdatedCallbackForParent(this.state);
    }
    this.props.navigation.goBack();
  }

 
  
static navigationOptions = {
    // header:null
    headerTitle: 'WORKING HOURS',
    headerStyle: {
      backgroundColor: '#5271FF',
    },
    headerTitleStyle: {
      color: "#fff",
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
   },
   headerTintColor: '#fff',
 }

  render() {
    let { height, width } = Dimensions.get('window');
    return (
        <View style={styles.wrapper}>
                <ImageBackground  style={{ flex:1, backgroundColor:'#fff'}}
                source={require('../images/ultrabackground.jpg')} 
                imageStyle={{ resizeMode: 'cover' }}>
                    <Content>
                        <View style={styles.workingHourContainer}>

                            <View style={[styles.workingHourFullTimeText, styles.bgGray]}>
                                <CheckBox
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    containerStyle={{ margin: 0, padding: 0,
                                        backgroundColor:'transparent', borderColor:'transparent',
                                    borderWidth:0}}
                                    checkedColor='#5271FF'
                                    center
                                    checked={this.state.fulltime}
                                    onPress={() => {
                                        //console.log('Fulltime onPress');
                                        this.setState(prevState => ({
                                            fulltime: !prevState.fulltime,
                                            parttime: prevState.fulltime
                                        }));                                          
                                    }}
                                    />
                                   
                                <View>
                                    <Text style={[styles.fullTimeText, styles.fullTimeTitle]}>
                                    Full Time
                                    </Text>
                                    <Text style={styles.fullTimeText}>
                                        Monday To Friday
                                    </Text>
                                    <Text style={styles.fullTimeText}>
                                        8.30am to 5.00 pm
                                    </Text>
                                </View>
                            </View>
                            <View style={[styles.workingHourPartTimeText, styles.bgGray]}>
                                <View style={styles.partTimeText}>
                                    <CheckBox
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        containerStyle={{ margin: 0, padding: 0,
                                            backgroundColor:'transparent', borderColor:'transparent',
                                        borderWidth:0}}
                                        checkedColor='#5271FF'
                                        center
                                        textStyle={{margin: 0, padding: 0,}}
                                        checked={this.state.parttime}
                                        onPress={() => {
                                            this.setState(prevState => ({
                                                parttime: !prevState.parttime,
                                                fulltime: prevState.parttime
                                            })); 
                                        }}
                                    />
                                    <View>
                                        <Text style={[styles.fullTimeText, styles.fullTimeTitle]}>
                                        Part Time
                                        </Text>
                                        <Text style={styles.fullTimeText}>
                                            Select Days
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.checkboxList}>
                                    <View style={styles.checboxListItem}>
                                        <CheckBox
                                            checked={this.state.parttimeMon}
                                            onPress={() => {
                                                if (this.state.parttime) { this.setState({parttimeMon: !this.state.parttimeMon}); }
                                            }}
                                            containerStyle={{ margin: 0, padding: 0,
                                            backgroundColor:'transparent', borderColor:'transparent',
                                            borderWidth:0}}
                                            checkedColor={this.state.parttime == false? 'darkgray' : '#5271FF'}
                                            center
                                            textStyle={{margin: 0, padding: 0,}}
                                            />
                                        <Text style={styles.checkboxText}>MON</Text>
                                    </View>
                                    <View style={styles.checboxListItem}>
                                        <CheckBox  
                                            checked={this.state.parttimeTue}
                                            onPress={() => {
                                                if (this.state.parttime) { this.setState({parttimeTue: !this.state.parttimeTue}); }
                                            }}
                                            containerStyle={{ margin: 0, padding: 0,
                                            backgroundColor:'transparent', borderColor:'transparent',
                                            borderWidth:0}}
                                            checkedColor={this.state.parttime == false? 'darkgray' : '#5271FF'}
                                            center
                                            textStyle={{margin: 0, padding: 0,}}
                                            />
                                        <Text style={styles.checkboxText}>TUE</Text>
                                    </View>
                                    <View style={styles.checboxListItem}>
                                        <CheckBox 
                                            checked={this.state.parttimeWed}
                                            onPress={() => {
                                                if (this.state.parttime) { this.setState({parttimeWed: !this.state.parttimeWed}); }
                                            }}
                                            containerStyle={{ margin: 0, padding: 0,
                                            backgroundColor:'transparent', borderColor:'transparent',
                                            borderWidth:0}}
                                            checkedColor={this.state.parttime == false? 'darkgray' : '#5271FF'}
                                            center
                                            textStyle={{margin: 0, padding: 0,}}
                                        />
                                        <Text style={styles.checkboxText}>WED</Text>
                                    </View>
                                    <View style={styles.checboxListItem}>
                                        <CheckBox 
                                            checked={this.state.parttimeThu}
                                            onPress={() => {
                                                if (this.state.parttime) { this.setState({parttimeThu: !this.state.parttimeThu}); }
                                            }}
                                            containerStyle={{ margin: 0, padding: 0,
                                            backgroundColor:'transparent', borderColor:'transparent',
                                            borderWidth:0}}
                                            checkedColor={this.state.parttime == false? 'darkgray' : '#5271FF'}
                                            center
                                            textStyle={{margin: 0, padding: 0,}}
                                            />
                                        <Text style={styles.checkboxText}>THU</Text>
                                    </View>
                                    <View style={styles.checboxListItem}>
                                        <CheckBox 
                                            checked={this.state.parttimeFri}
                                            onPress={() => {
                                                if (this.state.parttime) { this.setState({parttimeFri: !this.state.parttimeFri}); }
                                            }}
                                            containerStyle={{ margin: 0, padding: 0,
                                            backgroundColor:'transparent', borderColor:'transparent',
                                            borderWidth:0}}
                                            checkedColor={this.state.parttime == false? 'darkgray' : '#5271FF'}
                                            center
                                            textStyle={{margin: 0, padding: 0,}}
                                        />
                                        <Text style={styles.checkboxText}>FRI</Text>
                                    </View>
                                    <View style={styles.checboxListItem}>
                                        <CheckBox 
                                            checked={this.state.parttimeSat}
                                            onPress={() => {
                                                if (this.state.parttime) { this.setState({parttimeSat: !this.state.parttimeSat}); }
                                            }}
                                            containerStyle={{ margin: 0, padding: 0,
                                            backgroundColor:'transparent', borderColor:'transparent',
                                            borderWidth:0}} 
                                            checkedColor={this.state.parttime == false? 'darkgray' : '#5271FF'}
                                            center
                                            textStyle={{margin: 0, padding: 0,}}
                                        />
                                        <Text style={styles.checkboxText}>SAT</Text>
                                    </View>
                                    <View style={styles.checboxListItem}>
                                        <CheckBox 
                                            checked={this.state.parttimeSun}
                                            onPress={() => {
                                                if (this.state.parttime) { this.setState({parttimeSun: !this.state.parttimeSun}); }
                                            }} 
                                            containerStyle={{ margin: 0, padding: 0,
                                            backgroundColor:'transparent', borderColor:'transparent',
                                            borderWidth:0}}
                                            checkedColor={this.state.parttime == false? 'darkgray' : '#5271FF'}
                                            center
                                            textStyle={{margin: 0, padding: 0,}}
                                        />
                                        <Text style={styles.checkboxText}>SUN</Text>
                                    </View>
                                </View>
                                <View style={styles.inputTimeConatiner}>
                                    <Label style={styles.labelStyle}>Enter Working Time</Label>

                                    <View style={styles.InlineInputContainer}>    
                                    <View style={[styles.inputContainer, styles.inputRadiusRight]}>
                                        
                                        <TextInput 
                                            style={{flex:1}} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="" 
                                            keyboardType='default' 
                                            outoCorrect={false}
                                            onChangeText={workingStartTime => this.setState({ workingStartTime })}
                                            value={this.state.workingStartTime}
                                        />
                                          <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name="arrow-down" color="#5271FF" size={12}/>}
                                            style={{ width: undefined, left:-10, width: 50, 
                                                alignItems:'center', justifyContent:'center'}}
                                            placeholder="AM"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.workingStartTimeAmPm}
                                            onValueChange={(itemValue, itemIndex) => this.setState({workingStartTimeAmPm: itemValue})}
                                        >
                                            <Picker.Item label="AM" value="AM" />
                                            <Picker.Item label="PM" value="PM" />
                                        </Picker>
                                    </View>
                                    <View style={[styles.inputContainer, styles.inputRadiusLeft]}>
                                       
                                        <TextInput 
                                            style={{flex:1}} 
                                            underlineColorAndroid={'transparent'}
                                            placeholder="" 
                                            keyboardType='default' 
                                            outoCorrect={false}
                                            onChangeText={workingEndTime => this.setState({ workingEndTime })}
                                            value={this.state.workingEndTime}
                                        />
                                          <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name="arrow-down" color="#5271FF" size={12}/>}
                                            style={{ width: undefined, left:-10, width: 50, 
                                                alignItems:'center', justifyContent:'center'}}
                                            placeholder="PM"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.workingEndTimeAmPm}
                                            onValueChange={(itemValue, itemIndex) => this.setState({workingEndTimeAmPm: itemValue})}
                                        >
                                            <Picker.Item label="AM" value="AM" />
                                            <Picker.Item label="PM" value="PM" />
                                        </Picker>
                                    </View>
                                </View>  
                                </View>
                            </View>
                            <View style={styles.buttonConatiner}>
                                <Button style={styles.btnBlueSave} 
                                onPress={this._onPressButtonSave}>
                                    <Text style={{color:'#fff', fontWeight:'bold', fontSize:16}}>SAVE</Text>
                                </Button>
                            </View>

                        </View>
                    </Content>
                </ImageBackground>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
      },
      HeaderTitle:{
        color:'#fff',
      },
      workingHourContainer:{
          paddingTop:30,
          paddingBottom:40,
      },
      bgGray:{
        backgroundColor:'#f4f4f4',
      },
      workingHourFullTimeText:{
        flex:1,
        flexDirection:'row',
        marginBottom:20,
        padding:30,
      },
      labelStyle:{
        fontSize:14,
        marginBottom:8
      },
      
      fullTimeImageStyle:{
          marginRight:10,
      },
      fullTimeText:{
        color:'#000',
      },
      fullTimeTitle:{
        fontWeight:'600'
      },
      partTimeText:{
        flex:1,
        flexDirection:'row',
        marginBottom:5,
        padding:10,
        paddingTop:20,
        paddingLeft:30,
      },
      checkboxList:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        paddingLeft:5,
        marginTop:20,
        marginBottom:40,
        maxWidth: Dimensions.get('window').width,
        justifyContent:'center'
      },
      checkboxText:{
          textAlign:'left',
          paddingLeft:2,
      },
      workingHourPartTimeText:{
        
      },
      checboxListItem:{
         // marginRight:20,
         maxWidth:45,
         justifyContent:'center',
      },
      inputTimeConatiner:{
          padding:20
      },
    
      InlineInputContainer:{
        flex: 1,
        flexDirection: "row",
    },
    personalContainer:{
        alignItems: 'stretch',
        backgroundColor:'transparent',
        padding:15,
      },
      inputContainer: {
        flex: 1, 
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'stretch',
        padding:8,
        backgroundColor:'#fff',
        borderRadius:2,
        margin:5,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.1,
                height:45,
            },
            android: {
                elevation:2,
                paddingBottom:0,
                paddingTop:0,
            },
        })
    },
    buttonConatiner:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
      },
      btnBlueSave:{
          paddingLeft:50,
          paddingRight:50,
          padding:10,
          margin:50
      }
  });