import React, { Component } from 'react';
import { StyleSheet, Text, View, Platform, 
   Image, ActivityIndicator, ImageBackground, AsyncStorage,
  Linking, Alert} from 'react-native';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import Spinner from 'react-native-loading-spinner-overlay';
import firebase from 'react-native-firebase';

export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };

    this.unsubscribeProfilesCollection = null;
    this.componentDidMountDelayed = this.componentDidMountDelayed.bind(this);
  }


  static navigationOptions = {
    header:null
  }

  /*
    https://facebook.github.io/react-native/docs/linking
    And then on your React component you'll be able to listen to the events on 
    Linking as follows ..
  */
 
 componentDidMount() {
  console.log("Splash::componentDidMount");
  Linking.addEventListener('url', this._handleOpenURL);
  setTimeout(() => {
    this.componentDidMountDelayed();
  }, 1000);
 }
 
 componentDidMountDelayed() {
    
    firebase.auth().onAuthStateChanged(user => {
      console.log("onAuthStateChanged returned");
      console.log(user);
      console.log(SingletonClass.get('shouldBlockAutoLogin'));
      if ((user != undefined) && (user != null) && 
          (user._user != undefined) && (user._user != null) &&
          (user._user.uid != undefined) && (user._user.uid != null) && 
          (user._user.uid.length > 0)
        ) 
      {
        if (SingletonClass.get('shouldBlockAutoLogin') == false) {
          if (this.unsubscribeProfilesCollection != null) {
            this.unsubscribeProfilesCollection();
          }
          this.unsubscribeProfilesCollection = null;

          //Check if email is verified, else prompot user to verify email
          if ((user.emailVerified != undefined) && (user.emailVerified == false)) {
            // Prompt user to send confirmation email or logout
            Alert.alert(
              "Email not verified",
              "Your email address "+user.email+" has not been verified yet.\n"+
              "Choose 'Start Verification' so that we can send you an email with verification link.\n"+
              "You can then open the verification link from your mailbox and follow the instructions.",
              [
                  {text: 'Cancel', onPress: () => {
                    firebase.auth().signOut();
                  }},
                  {text: 'Start Verification', style: 'cancel', onPress: () => {
                    firebase.auth().currentUser.sendEmailVerification().then(()=> {
                      UltraAgentHelpers.showAlertWithOneButton("Verification link sent",
                      "An email with verification link has been sent to your email address "+user.email);
                      firebase.auth().signOut();
                    }).catch(() => {
                      UltraAgentHelpers.showAlertWithOneButton("Error",console.error.message);
                      firebase.auth().signOut();
                    });
                  }}
              ],
              { cancelable: false }
          );
            return;
          }
          

          // force update because sometimes latest data not available with onSnapshot
          firebase.firestore().collection('profiles')
            .doc(user._user.uid)
            .get({source: 'server'})
            .then((obj) => {
              console.log('Request from server');
              console.log(obj);

              if (this.unsubscribeProfilesCollection != null) {
                this.unsubscribeProfilesCollection();
              }
              this.unsubscribeProfilesCollection = null;
    
              this.unsubscribeProfilesCollection = 
              firebase.firestore().collection('profiles')
                .doc(user._user.uid)
                .onSnapshot(this.onProfileQueryCompleted);
    
            })
            .catch();

         
          
        }

      } else {
        SingletonClass.set('loggedinprofile',null);
        if (this.unsubscribeProfilesCollection != null) {
          this.unsubscribeProfilesCollection();
        }
        this.unsubscribeProfilesCollection = null;

        if (SingletonClass.get('shouldBlockAutoLogin') == false) {
          this.props.navigation.navigate('Auth'); 
        }
      }
      
     
    });

    // if (firebase.auth().currentUser == null) {
    //   this.props.navigation.navigate('Auth');
    // } else {
    //   this.props.navigation.navigate('App');
    // }

  }
  componentWillUnmount() {
    console.log("Splash::componentWillUnmount");
    Linking.removeEventListener('url', this._handleOpenURL);

    if (this.unsubscribeProfilesCollection != null) {
      this.unsubscribeProfilesCollection();
    }
    this.unsubscribeProfilesCollection = null;
  }
  _handleOpenURL(event) {
    console.log(event.url);
  }

  async linkFcmTokenWithUserId() {
    var aProf = SingletonClass.get('loggedinprofile');
    if((aProf != undefined) && (aProf != null) && (aProf.userId != undefined)) {
      let fcmToken = await AsyncStorage.getItem('fcmToken', null);
      if (fcmToken) {
        UltraAgentHelpers.linkFcmTokenWithUserUserId(fcmToken, aProf.userId);
      }
    }
  }


  onProfileQueryCompleted = (querySnapshot) => {
    console.log("onProfileQueryCompleted");
    console.log(querySnapshot);

    if ((querySnapshot != undefined) && (querySnapshot != null) &&
        (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
    {
      if ((querySnapshot._data.approvalStatus == 'approved') && 
          (querySnapshot._data.profileType == 'worker')) 
      {
        SingletonClass.set('loggedinprofile',querySnapshot._data);
        this.linkFcmTokenWithUserId();
        if (SingletonClass.get('shouldBlockAutoLogin') == false) {
          this.props.navigation.navigate('App'); 
        }
        return;
      }
      if (querySnapshot._data.approvalStatus != 'approved') {

        if (querySnapshot._data.approvalStatus == 'pending') {
          UltraAgentHelpers.showAlertWithOneButton("Oops!","Your profile is in pending state.\n\n"+
            "We are verifying your documents. Once ready, we will intimate you via email.\n\n"+
            "Please contact admin if you have any queries.\n\nadmin@ultraagent.co.uk");

            // No more access for pending users
            // //Allow access for now to tests and demo
            // SingletonClass.set('loggedinprofile',querySnapshot._data);
            // this.linkFcmTokenWithUserId();
            // if (SingletonClass.get('shouldBlockAutoLogin') == false) {
            //   this.props.navigation.navigate('App'); 
            // }
            //return;


        } else {
          UltraAgentHelpers.showAlertWithOneButton("Oops!","Your profile is in "+querySnapshot._data.approvalStatus
            +" state.\nPlease contact admin to resolve the issue.\n\nadmin@ultraagent.co.uk");
        }
      } else if (querySnapshot._data.profileType != 'admin') {
        /*
        20/05/2019: Albert:
        The following error is generated when an admin tries to log in to the agent link. 
        This is wrong because it lets the attacker know that the username is a valid admin username, 
        and also direct them where to attack. Again ‘The username or password you entered is incorrect’ 
        would be a better message.
        */
        // UltraAgentHelpers.showAlertWithOneButton("Oops!",
        //   "Please login to the Admin portal instead.\nThis app is for worker only.");
        UltraAgentHelpers.showAlertWithOneButton("Oops!",
          "The username or password you entered is incorrect");

      } else if (querySnapshot._data.profileType != 'company') {
        /*
        20/05/2019: Albert:
        The following error is generated when an admin tries to log in to the agent link. 
        This is wrong because it lets the attacker know that the username is a valid admin username, 
        and also direct them where to attack. Again ‘The username or password you entered is incorrect’ 
        would be a better message.
        */
        // UltraAgentHelpers.showAlertWithOneButton("Oops!",
        //   "Please login to the Employer portal instead\.\nThis app is for worker only.");
        UltraAgentHelpers.showAlertWithOneButton("Oops!",
          "The username or password you entered is incorrect");
      }

      SingletonClass.set('loggedinprofile',null);
      if (this.unsubscribeProfilesCollection != null) {
        this.unsubscribeProfilesCollection();
      }
      this.unsubscribeProfilesCollection = null;

      // Throw error and navigate to Auth screens
      firebase.auth().signOut();

      
    } else {
      // Throw error and navigate to Auth screens
      UltraAgentHelpers.showAlertWithOneButton("Oops!",
      "Failed to sync your profile.\n"+
      "Your profile might have been locked or removed.\n"+
      "Please contact support.");
      
      SingletonClass.set('loggedinprofile',null);
      if (this.unsubscribeProfilesCollection != null) {
        this.unsubscribeProfilesCollection();
      }
      this.unsubscribeProfilesCollection = null;

      firebase.auth().signOut();
    }

  }


    componentWillMount(){

      /*
      Commenting out test code after Firebase integration
      setTimeout( async () => {
        try{
          const userToken = await AsyncStorage.getItem('userToken');
          // This will switch to the App screen or Auth screen and this loading
          // screen will be unmounted and thrown away.
          this.props.navigation.navigate(userToken ? 'App' : 'Auth');      
        }
        catch(error){
          console.log(error);
        }
      }, 2000);
      */

    }

  render() {
    return (
      <View style={styles.wrapper}>
        <ImageBackground style={styles.container} 
        source={require('../images/ultrabackground.jpg')} imageStyle={{ resizeMode: 'cover' }}>
        <View style={styles.logoContainer}>
            <Image style={styles.logo} 
            source={require('../images/logo.png')}/>
            <ActivityIndicator style={styles.loadingImg} 
            animating={true} size="large" color="#5271FF" text="loading"/>
            <Text style={styles.loadingtext}>Loading</Text>
        </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container:{
    flex:1,
    width:null,
    alignSelf:'stretch',
    height:null,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoContainer:{
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0, 
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,
  },
  loadingImg:{
    marginTop:100,
    justifyContent: 'space-around',
  },
  logo:{
    position:'relative',
    bottom:30,
    width:200,
    height:200,
  },
  loadingtext:{
    color:'#5271FF',
  }
  
});
