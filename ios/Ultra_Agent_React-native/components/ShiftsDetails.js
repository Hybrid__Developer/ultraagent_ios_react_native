import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Alert, Text,
  
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, Picker, List, ListItem, Content, Item, Header, Body
} from 'native-base';
import Icon  from 'react-native-ionicons';
import firebase from 'react-native-firebase';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

export default class ShiftsDetails extends Component {
  currentShift = null;


  constructor(props) {
    super(props);
    this.state = {
    };

    this.isProcessing=false;

    this.currentShift = 
            this.props.navigation.state.params.selectedShift;

    //console.log("ShiftsDetails Constructor ", this.currentShift)
    
  }
  static navigationOptions = {
    header:null
  //   headerTitle: () => {'PLUMBING #1038 #' + this.currentShift.shiftNo},
  //   headerStyle: {
  //     backgroundColor: '#5271FF',
  //   },
  //   headerTitleStyle: {
  //     color: "#fff",
  //     flex:1,
  //     alignItems: 'center',
  //     justifyContent: 'center',
  //  },
  //  headerTintColor: '#fff',
 }

 _onPressButtonCancel = () => {
  this.props.navigation.navigate('Shifts');
 }
  
 _onPressButtonAccept = () => {

  if (this.isProcessing == true) { return; }
  this.isProcessing = true;

  const reqId = this.currentShift.id+'-'+firebase.auth().currentUser.uid;

  firebase.firestore().collection('bookingrequests').doc(reqId).get()
  .then((aReqSnapshot) => {
    if (aReqSnapshot.exists) {
      this.isProcessing = false;
      UltraAgentHelpers.showAlertWithOneButton("Oops!",'You have already applied.');
      return;
    }

    firebase.firestore().collection('bookingrequests').doc(reqId).set(
      {
        createdBy:firebase.auth().currentUser.uid,
        shiftId: this.currentShift.id,
        workerId: firebase.auth().currentUser.uid,
        companyId:this.currentShift.companyId,
        approvalStatus:'pending',
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        isAdvanceBooking:false,
      },
      {merge: true}
    )           
    .then(() => {
      // Works on both iOS and Android
     Alert.alert(
      'Shift Request sent Successfully.\n',
      'Please wait for confirmation.',
      [
        //{text: 'Don`t allow', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => {
            this.isProcessing = false;
            this.props.navigation.navigate('Shifts');
          }
        },  
        ],
        { cancelable: false }
      );
    })
    .catch((error) => {
      this.isProcessing = false
      console.log(error);
      UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
    });
    

  })
  .catch((error) => {
    this.isProcessing = false
    console.log(error);
    UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
  });


 }

 _onPressButtonCancelApplication = () => {
   console.log('_onPressButtonCancelApplication called');

   if (this.isProcessing == true) { return; }
   this.isProcessing = true;

   const reqId = this.currentShift.id+'-'+firebase.auth().currentUser.uid;

   firebase.firestore().collection('bookingrequests').doc(reqId).delete()        
  .then(() => {
    // Works on both iOS and Android
   Alert.alert(
    'Application cancelled Successfully.\n',
    '',
    [
      //{text: 'Don`t allow', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      {text: 'OK', onPress: () => {
          this.isProcessing = false;
          this.props.navigation.navigate('Shifts');
        }
      },  
      ],
      { cancelable: false }
    );
  })
  .catch((error) => {
    this.isProcessing = false
    console.log(error);
    UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
  });

}

  render() {

    return (
      <Container>
        <Header style={{backgroundColor:'#5271FF'}}> 
        <Left style={{flex:1}}>
             <Button transparent 
             onPress={() => this.props.navigation.goBack(this.props.navigation.state.key)}>
                <Icon name="arrow-back" color='#fff' />  
             </Button>
         </Left>
         <Body style={{flex:4, textAlign:'center', alignItems:'center'}}>
             <Title style={styles.HeaderTitle}>{this.currentShift.subcategory} #{this.currentShift.shiftNo}</Title>
         </Body>
         <Right style={{flex:1}}>
         </Right>  
      </Header>


       <View style={{backgroundColor:'#6e87fb'}}>
          <Title style={styles.SubHeaderTitle}>{this.currentShift.address.toUpperCase()}</Title>
        </View>
        <Content>
          <View style={styles.jobDetailsContainer}>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Shift Description</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}> 
                    {this.currentShift.description}
                </Text>
                <Text> 
                    <Icon name="md-clock" size={15} color="#888"/>
                    &nbsp;{this.currentShift.startDateTime.getHours() +':'+("0" + this.currentShift.startDateTime.getMinutes()).slice(-2)} &nbsp;&nbsp;
                    <Icon name="md-calendar" size={15} color="#888"/>
                    &nbsp;{this.currentShift.startDateTime.getDate()+'/'+(this.currentShift.startDateTime.getMonth()+1)+'/'+this.currentShift.startDateTime.getFullYear()} 
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Employer</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}> 
                  {/* {this.currentShift.company == undefined? '': this.currentShift.company.name} */}
                  {this.currentShift.companyName}
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Address</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}> 
                  {this.currentShift.address}
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Mobile Number</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}> 
                  {this.currentShift.phoneNumber}
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Duration</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}> 
                    {this.currentShift.duration}
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Fees</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}> 
                    £{this.currentShift.fee}/h
                </Text>
              </View>
            </View>
            { this.currentShift.bookingrequest != undefined &&
               <View style={styles.buttonConatiner}>
                <Text style={{color: '#08c903', lineHeight:31, margin:10}}>applied</Text>
              </View>
            }
            { this.currentShift.bookingrequest != undefined &&
               <View style={styles.buttonConatiner}>
               { this.currentShift.bookingrequest.approvalStatus == 'pending' &&
                 <Button style={[styles.btnBlue, styles.btnMarginStyle]} onPress={this._onPressButtonCancelApplication}>
                   <Text style={{color:'#fff'}}>CANCEL APPLICATION</Text>
                 </Button>
               }
             </View>
            }
            
            { this.currentShift.bookingrequest == undefined &&
              <View style={styles.buttonConatiner}>
                <Button style={[styles.btnOrange, styles.btnMarginStyle]} onPress={this._onPressButtonCancel}>
                  <Text style={{color:'#fff'}}>DISMISS</Text>
                </Button>
                <Button style={[styles.btnBlue, styles.btnMarginStyle]} onPress={this._onPressButtonAccept}>
                  <Text style={{color:'#fff'}}>ACCEPT</Text>
                </Button>
              </View>
            }
          </View>
        </Content>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
   
    SubHeaderTitle:{
      color:'#fff',
      fontSize:16,
      padding:5,
      paddingTop:6,
      textAlign:'center',
      alignItems:'center',
    },
    HeaderTitle:{
      color:'#fff',
      textAlign:'center',
      alignItems:'center',
    },
    jobDetailsContainer:{
      margin:15,
      marginTop:15,
      marginBottom:15,
    },
    jobDetailsContent:{
      marginBottom:15,
    },
    textBox:{
      backgroundColor:'#f4f4f4',
      padding:15,
    },
    jobDetailsChildText:{
      fontWeight:'bold',
    },
    shiftLabel:{
      marginBottom:5,
      fontSize:14,
      color:'#888'
    },
    buttonConatiner:{
      flex:1,
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
    },
    btnMarginStyle:{
      margin:5,
      marginBottom:10,
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      paddingLeft:30,
      paddingRight:30,
    },
    btnOrange:{
      backgroundColor:'#df8c40',
    },
    btnBlue:{
      backgroundColor:'#5271ff',
    },
})