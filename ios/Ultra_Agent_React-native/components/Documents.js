import React, { Component } from 'react';
import {
  StyleSheet, View, Image, ImageBackground, Text, 
  Dimensions, FlatList, Linking, TouchableHighlight
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, List, ListItem, Content, Header, Body
} from 'native-base';
import Icon from 'react-native-ionicons';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

import firebase from 'react-native-firebase';

export default class Documents extends Component {

  constructor(props) {
    super(props);

    this.documentTypesData = SingletonClass.get('documentTypesData');

    this.loggedinprofile = SingletonClass.get('loggedinprofile');
    this.state = {
    };

    this.unsubscribeProfilesCollection = 
          firebase.firestore().collection('profiles')
            .doc(this.loggedinprofile.userId)
            .onSnapshot(this.onProfileQueryCompleted);

  }

  static navigationOptions = {
    header:null
  }


  onProfileQueryCompleted = (querySnapshot) => {
    // console.log("onProfileQueryCompleted");
    // console.log(querySnapshot);

    if ((querySnapshot != undefined) && (querySnapshot != null) &&
        (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
    {
      this  .setState({...querySnapshot._data});
      
    } else {
      // Throw error and navigate to Auth screens
      UltraAgentHelpers.showAlertWithOneButton("Oops!",
      "Failed to sync your profile.\n"+
      "Your profile might have been locked or removed.\n"+
      "Please contact support.");
    }

  }

  componentWillUnmount() {

    this.unsubscribeProfilesCollection();

  }

  _onPressAddAnotherDocument = () => {
    this.props.navigation.navigate('NewDocument');
  }
  
  render() {
    let { height, width } = Dimensions.get('window');
    return (
      <ImageBackground
      source={require('../images/timesheetbg.png')}
      style={{ width, height }}>
        <Header style={{backgroundColor:'#5271FF'}}>
        
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name="arrow-back" color='#fff' />         
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'DOCUMENTS'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
        
        </Header>
        <View style={{backgroundColor:'#5f7bfa'}}>
          <Title style={styles.SubHeaderTitle}>{'My Documents'}</Title>
        </View>
        <Content>

            <FlatList
              extraData={this.state} /*IMPORTANT: This line makes the byte counts update*/
              data={this.state.wDocuments}
              keyExtractor={(item, index) => index+""}
              ItemSeparatorComponent={this.renderSeparator}
              renderItem={({ item }) => (
                <ListItem style={styles.pdfListitem}>
                <Left>
                    {/* <Image source={require('../images/pdf_icon.png')} 
                   style={{ height: 80, width: 80}}/> */}
                  <Image source={{uri: item.url }} 
                   style={{ height: 80, width: 80}}/>
                  <View style={styles.documentListStyle}>
                    <Text style={styles.fontBoldStyle}>{this.documentTypesData[item.documentType] != undefined? this.documentTypesData[item.documentType]:item.documentType}</Text>
                    { item.documentNumber != undefined && item.documentNumber.length > 0 &&
                      <Text style={{marginTop:4}}>{item.documentNumber}</Text>
                    }
                    { item.expiryDate != undefined && item.expiryDate.length > 0 &&
                      <Text style={{marginTop:4}}>Expiry: {item.expiryDate}</Text>
                    }
                    <Text style={{marginTop:4}}>Updated on {item.uploadedDateStr}</Text>
                  </View>
                </Left>
                <Right>
                  {/* <Icon name="arrow-forward" size={25} color="#5271ff" /> */}
                </Right>
              </ListItem>
              )}
          />

          {/* <View>
            <Title style={styles.documentTitle}>Professional Documents</Title>
         </View>
         <List>
            <ListItem style={styles.pdfListitem}>
              <Left>
                  <Image source={require('../images/pdf_icon.png')} 
                 style={{ height: 80, width: 80}}/>
                <View style={styles.documentListStyle}>
                  <Text style={styles.fontBoldStyle}>Profession Body Licence</Text>
                  <Text>Updated on 01, JAN 2016</Text>
                </View>
              </Left>
              <Right>
                <View style={{flexDirection: 'row'}}>
                  <Image source={require('../images/redicon.png')} 
                  style={{ height: 15, width: 15, marginRight: 15, marginTop: 5}}/>
                  <Icon name="arrow-forward" size={25} color="#5271ff" />
                </View>
              </Right>
            </ListItem>
         </List> */}
        <View style={styles.addContainer}>
          <TouchableHighlight underlayColor='transparent' onPress={this._onPressAddAnotherDocument}>                
            <View style={styles.addDocumentContainer}>
                <Image source={require('../images/add-icon.png')} 
              style={{ height: 40, width: 40}}/>
                <Text style={styles.addDocumentText}>Add another document</Text>
            </View>
          </TouchableHighlight>

        </View>
        </Content>
     </ImageBackground>
     );
    }
  }
  const styles = StyleSheet.create({
    HeaderTitle:{
      color:'#fff',
    },
    SubHeaderTitle:{
      color:'#fff',
      fontSize:16,
      padding:5,
      paddingTop:6,
    },
    documentListStyle:{
      marginLeft:5,
      paddingLeft:5,
      //paddingTop:20,
    },
    pdfListitem:{
      backgroundColor:'#f4f4f4',
      marginLeft:0,
    },
    fontBoldStyle:{
      fontWeight:'bold',
      paddingRight:40,
    },
    documentTitle:{
      color:'#000',
      marginTop:20,
      marginBottom:20,
      fontSize:18,
    },
    addContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    addDocumentContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop:50
    },
    addImage:{
      width:40,
      height:40,
    },
    addDocumentText: {
      textAlign: 'center',
      fontSize: 20,
      color: '#5271ff',
      marginLeft:10
    },
})