import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Text, ScrollView, Dimensions, FlatList,
  TouchableOpacity, WebView, Platform,
} from 'react-native';
import {
  Container, Header, Title, Body, Button,
  Left, Right, Picker, List, ListItem, Content, Badge
} from 'native-base';

import {Icon as NativeBaseIcon} from 'native-base';

import Icon  from 'react-native-ionicons';
import MapView from 'react-native-maps';

export default class BookingFullSchedule extends Component {

    markersMap = {};

  constructor(props) {
    super(props);

    let selectedDayKey= this.props.navigation.state.params.selectedDayKey;
    let bookingsOnSelectedDay= this.props.navigation.state.params.bookingsOnSelectedDay;
    // bookingsOnSelectedDay.push(this.props.navigation.state.params.bookingsOnSelectedDay[0]);
    // bookingsOnSelectedDay.push(this.props.navigation.state.params.bookingsOnSelectedDay[0]);
    // bookingsOnSelectedDay.push(this.props.navigation.state.params.bookingsOnSelectedDay[0]);
    // bookingsOnSelectedDay.push(this.props.navigation.state.params.bookingsOnSelectedDay[0]);
    // bookingsOnSelectedDay.push(this.props.navigation.state.params.bookingsOnSelectedDay[0]);
    // bookingsOnSelectedDay.push(this.props.navigation.state.params.bookingsOnSelectedDay[0]);

    this.state = {
        selectedDayKey: selectedDayKey,
        bookingsOnSelectedDay: bookingsOnSelectedDay,
        markedLocation: {
            latitude: 51.5074,
            longitude: -0.1278,
            latitudeDelta: 0.0922,
            longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * 0.0922,
        },
    };

    this.onMarkerPressed = this.onMarkerPressed.bind(this);
  }
  
static navigationOptions = {
    header:null
//     headerTitle: 'JULY 18 2018',
//     headerStyle: {
//       backgroundColor: '#5271FF',
//     },
//     headerTitleStyle: {
//       color: "#fff",
//       flex:1,
//       alignItems: 'center',
//       justifyContent: 'center',
//    },
//    headerTintColor: '#fff',
 }

 componentDidMount() {

    // Load map markers after 3 secs delay
    //setTimeout(() => {
        this.calculateAndLoadMapRegion();
    //},3000);
 }

  calculateAndLoadMapRegion () {

    // Calculate map boundary 
    let minLat=0; let maxLat=0;
    let minLng=0; let maxLng=0;
    let shouldUpdateBoundingBox = false;
    const someBookings = this.state.bookingsOnSelectedDay;

    // Initialize bounding box with first lat/lng
    for (aBooking of someBookings) {
      if (aBooking.lat != undefined) {
        const aLat = parseFloat(aBooking.lat+"");
        minLat = aLat - 0.00922; maxLat = aLat + 0.00922;
        shouldUpdateBoundingBox = true;
        break;

      }
    }
    for (aBooking of someBookings) {
      if (aBooking.lng != undefined) {
        const aLng = parseFloat(aBooking.lng+"");
        minLng = aLng - 0.00922; maxLng = aLng + 0.00922;
        shouldUpdateBoundingBox = true;
        break;
      }
    }

    for (aBooking of someBookings) {
        //console.log('shift ('+aBooking.lat+", "+aBooking.lng+")");
        if (aBooking.lat != undefined) {
          const aLat = parseFloat(aBooking.lat+"");
          if (minLat > aLat) { minLat = aLat; shouldUpdateBoundingBox=true; }
          if (maxLat < aLat) { maxLat = aLat; shouldUpdateBoundingBox=true; }
        }
  
        if (aBooking.lng != undefined) {
          const aLng = parseFloat(aBooking.lng+"");
          if (minLng > aLng) { minLng = aLng; shouldUpdateBoundingBox=true; }
          if (maxLng < aLng) { maxLng = aLng; shouldUpdateBoundingBox=true; }
        }
    }

    if (shouldUpdateBoundingBox == true) {
        const deltaLat = Math.abs(minLat - maxLat);
        const deltaLng = Math.abs(minLng - maxLng);
        let delta = deltaLat;
        if (deltaLng > deltaLat) { delta = deltaLng; }
  
        console.log('min: ('+minLat+", "+minLng+")");
        console.log('max: ('+maxLat+", "+maxLng+")");
  
        const midLat = (minLat+maxLat)/2.0;
        const midLng = (minLng+maxLng)/2.0;
        this.setState({
          bookingsOnSelectedDay:this.state.bookingsOnSelectedDay,
          markedLocation: {
            latitude: midLat,
            longitude: midLng,
            latitudeDelta: delta,
            longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * delta,
          },
        });
      }
  
 }

 _onPressBookingShifts = (booking) => {
    this.props.navigation.navigate('BookingShifts',{'booking' : booking});
  }

  onMarkerPressed = (aBookingId) => {
    //console.log('onMarkerPressed: '+aShiftId);

    for (aBooking of this.state.bookingsOnSelectedDay) {
      if (aBooking.id == aBookingId) {
        this._onPressBookingShifts(aBooking);
        break;
      }
    }
    }

  renderCallout() {
    // Do nothing
  }

  render() {
    const { width, height } = Dimensions.get('window');
    const ratio = width / height;

    const coordinates = {
      latitude:59.32932349999999,
      longitude:18.058580800000063,
      latitudeDelta:0.1,
      longitudeDelta:0.1 * ratio,
    };

    return (
        <Container>
            <Header style={{backgroundColor:'#5271FF'}}> 
                <Left style={{flex:1}}>
                    <Button transparent 
                    onPress={() => this.props.navigation.goBack(this.props.navigation.state.key)}>
                        <Icon name="arrow-back" color='#fff' />  
                    </Button>
                </Left>
                <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
                    <Title style={styles.HeaderTitle}>{this.state.selectedDayKey}</Title>
                </Body>
                <Right style={{flex:1}}>
                </Right>  
            </Header>
            <Content contentContainerStyle={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'stretch'}}>
                <View style={styles.bookingScheduleList}>
                    {/* <ScrollView> */}
                    <FlatList
                        keyExtractor={(item, index) => item.id}
                        data={this.state.bookingsOnSelectedDay}
                        renderItem={({ item }) => (
                            <ListItem noBorder style={{ marginLeft: 0, marginTop:10, marginBottom:0, padding:0 }} 
                                button onPress={() => {this._onPressBookingShifts(item);}}>
                                    <View style={styles.timeViewStyle}> 
                                        <View style={[styles.viewLeftbgBox, styles.viewBluebgBox]}>
                                            <Text style={[styles.timeShowtext, styles.whiteTextStyle]}>
                                            {item.shiftStartDateTime.getHours()}:{("0" + item.shiftStartDateTime.getMinutes()).slice(-2)}
                                            </Text>
                                            <Text style={[styles.timeShowtext, styles.whiteTextStyle]}>{item.duration}</Text>
                                        </View>
                                        <View style={styles.bookingScheduleText}>
                                            <Text style={[styles.bookingidText, styles.marginStyle]}>{item.shiftSubCategory} #{item.shiftNo}</Text>
                                            <Text style={[styles.bookingLocationStyle, styles.marginStyle]}>
                                                <Text style={styles.locationGrayText}>Location: </Text>
                                                <Text style={styles.locationBlackText}>{item.address}</Text>
                                            </Text>
                                            <Badge style={[styles.badgeStyle, styles.marginStyle]}>
                                                <Text style={styles.badgeText}>£{item.fee}/H</Text>
                                            </Badge>
                                        </View>
                                    </View>
                            </ListItem>
                        )} 
                    />

                </View>
                <View style={styles.bookingScheduleMap}>
                <MapView
                    ref={map => (this.map = map)}
                    style={styles.map}
                    region={this.state.markedLocation}
                    //onRegionChangeComplete={region => this.setState({region: region})}
                    onRegionChangeComplete={() => this.renderCallout()}
                    zoomEnabled={true}
                    pitchEnabled={true}
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    followUserLocation={true}
                    zoomControlEnabled={true}
                    >

                    {this.state.bookingsOnSelectedDay.map((aBooking, index) => {
                        const coord = {latitude: aBooking.lat, longitude: aBooking.lng};
                        console.log('loading bookimg'+aBooking.id);
                        return (
                        <MapView.Marker
                            ref={(marker) => {this.markersMap[aBooking.id] = marker;}}
                            key={aBooking.id} identifier={`${aBooking.id}`}
                            coordinate={coord}
                            tooltip={true}
                            style={{width:60, height: 60}}
                            image={require('../images/markericon-red.png')}
                            onPress={() => {}}
                            >


                            <MapView.Callout style={{maxWidth:180, maxHeight:80, width:180}} 
                               onPress={() => {this.onMarkerPressed(aBooking.id);}} >
                                <BookingMarkerCalloutView 
                                    category={aBooking.shiftCategory} 
                                    title={aBooking.shiftSubCategory+' #'+aBooking.shiftNo} 
                                    date={aBooking.shiftStartDateTime.getDate()+'/'+
                                        (aBooking.shiftStartDateTime.getMonth()+1)+'/'+
                                        aBooking.shiftStartDateTime.getFullYear()} 
                                    time={aBooking.shiftStartDateTime.getHours() +':'+("0" + 
                                        aBooking.shiftStartDateTime.getMinutes()).slice(-2)} 
                                    bookingId={aBooking.id} 
                                    onMarkerPressed={this.onMarkerPressed}
                                />
                            </MapView.Callout>

                        </MapView.Marker>
                        );
                    })}

                    </MapView>
                </View>
            </Content>
        </Container>
     );
    }
  }

const styles = StyleSheet.create({
    HeaderTitle:{
        color:'#fff',
      },
    bookingScheduleList:{
        height: '50%',
    },
    timeViewStyle:{
        flexDirection: 'row',
        flex:1,
      },
      bookingScheduleText:{
        justifyContent: 'center',
        alignSelf:'stretch',
        flex:1,
        lineHeight:20,
        paddingLeft:10,
      },
      viewLeftbgBox:{
        paddingTop:10, 
        paddingLeft:10,
        paddingBottom:10,
        paddingRight:10,
        borderBottomRightRadius:10, 
        borderTopRightRadius:10,
      },
      viewBluebgBox:{
        backgroundColor:'#5271ff', 
      },
      viewGraybgBox:{
          backgroundColor:'#e5e5e5',
      },
      timeShowtext:{
          color:'#fff',
          lineHeight:40,
      },
      whiteTextStyle:{
        color:'#fff',
      },
      blackTextStyle:{
        color:'#000',
      },
      bookingidText:{
        color:'#5271FF',
        fontWeight:'bold',
        fontSize:16,
      },
      locationGrayText:{
        color:'#ccc',
      },
      // bookingLocationStyle:{
      //   flexDirection: 'row',
      //   flex:1,
      // },
      marginStyle:{
        margin:3,
      },
      badgeStyle:{
        backgroundColor:'transparent', 
        borderColor:'#df8c40', 
        borderRadius:4, 
        borderWidth:1,
      },
      badgeText:{
        color:'#df8c40',
        paddingLeft:4, 
        paddingRight:4, 
        textAlign:'center',
        alignItems:'center'
      },
    bookingScheduleMap:{
        height: '50%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
      height:(Dimensions.get('window').height/2)
    },
    markerContainer:{
      flexDirection: 'row', 
      flex:1,
      paddingRight:5
    },
    markerJobTitle:{
      color:'#000', 
      flexWrap:'wrap',
      fontSize:12,
      fontWeight:'bold',
      lineHeight:14,
    },
    mJobNumbrStyle:{
      color:'#5f7bfa',
      marginTop:5,
      fontSize:12,
      flexWrap:'wrap',
    },
    zoomButton:{
      backgroundColor:'#fff', 
      borderRadius:3, 
      padding:10, 
    },

})


class BookingMarkerCalloutView extends React.Component {

  constructor(props) {
    super(props);

    this.sideIcon = require('../images/plumbingicon.png');

  }

  displayCalloutImage() {

    if(Platform.OS === 'ios') {
      // showing local
      if (this.props.category == "Construction") {
        this.sideIcon = require('../images/cat-construction.png');
      } else if (this.props.category == "Health care") {
        this.sideIcon = require('../images/cat-healthcare.png');
      } else if (this.props.category == "Manufacturing") {
        this.sideIcon = require('../images/cat-manufacturing.png');
      } else if (this.props.category == "Hospitality") {
        this.sideIcon = require('../images/cat-hospitality.png');
      }

      return(
        <View style={styles.markerContainer}>
        <Image style={{width:30, height:30, 
          alignItems: 'center', justifyContent:'center',}} 
          source={this.sideIcon}/>
          <View style={{marginLeft:15, marginRight:10}}>
          <Text style={styles.markerJobTitle}>{this.props.title}</Text>
          <View style={{flexDirection: 'row', flex:1,}}>
            <Text style={{...styles.mJobNumbrStyle, ...{marginRight: 10}}}>{this.props.date}</Text>
            <Text style={styles.mJobNumbrStyle}>
              <NativeBaseIcon type="FontAwesome" name="clock-o" 
                  style={{fontSize: 14, color: '#5f7bfa'}}/> {''}
                  {this.props.time}
            </Text>
          </View>
        </View>
        </View>
      );
    } else {

      // showing from web
      if (this.props.category == "Construction") {
        this.sideIcon = {uri: 'https://ultraagent.co.uk/assets/cat-construction.png'};
      } else if (this.props.category == "Health care") {
        this.sideIcon = {uri: 'https://ultraagent.co.uk/assets/cat-healthcare.png'};
      } else if (this.props.category == "Manufacturing") {
        this.sideIcon = {uri: 'https://ultraagent.co.uk/assets/cat-manufacturing.png'};
      } else if (this.props.category == "Hospitality") {
        this.sideIcon = {uri: 'https://ultraagent.co.uk/assets/cat-hospitality.png'};
      }

      return(
        <View style={styles.markerContainer}>
        <WebView
            style={{width:30, height:30, 
              alignItems: 'center', justifyContent:'center',}} 
              source={{uri:'https://ultraagent.co.uk/assets/cat-manufacturing.png'}}
            injectedJavaScript={'document.getElementsByTagName("BODY")[0].style.backgroundColor = "#ffffff";'}
            javaScriptEnabledAndroid={true}
          /> 
          <View style={{marginLeft:45, marginRight:10}}>
          <Text style={styles.markerJobTitle}>{this.props.title}</Text>
          <View style={{flexDirection: 'row', flex:1,}}>
            <Text style={{...styles.mJobNumbrStyle, ...{marginRight: 10}}}>{this.props.date}</Text>
            <Text style={styles.mJobNumbrStyle}>
              <NativeBaseIcon type="FontAwesome" name="clock-o" 
                  style={{fontSize: 14, color: '#5f7bfa'}}/> {''}
                  {this.props.time}
            </Text>
          </View>
        </View>
        </View>
      );
    }

  }

  render() {
    return (

      <View style={{backgroundColor: '#fff'}}>
        {/* No Need to add TocuableOpacity here. Callout has onPress()
          <TouchableOpacity onPress={() => {this.props.onMarkerPressed(this.props.bookingId);}}> */}
        {this.displayCalloutImage()} 
        {/* </TouchableOpacity> */}
      </View>
    );
  }
}