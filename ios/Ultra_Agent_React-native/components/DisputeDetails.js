import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Text, FlatList, Alert,
  
} from 'react-native';
import {
  Container, Header, Title, Body, Button,
  Left, Right, Picker, List, Content, Badge, ListItem
} from 'native-base';
import Icon  from 'react-native-ionicons';

import firebase, { RNFirebase } from 'react-native-firebase';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import Spinner from 'react-native-loading-spinner-overlay';

export default class DisputeDetails extends Component {

  constructor(props) {
    super(props);

    this.loggedinprofile = SingletonClass.get('loggedinprofile');

    booking = 
            this.props.navigation.state.params.booking;
            console.log('BookingShifts:'+booking);

    this.state = {
       booking: booking,
       amend: {},
       spinner:false,
       spinnerMessage: '',
    };
  }

  componentDidMount() {
    this.updateBooking();
    this.updateAmend();
  }

  componentWillUnmount() {
    this.unsubscribeBookingCollection();
    this.unsubscribeAmendCollection();
  }

  updateAmend() {
    
    this.unsubscribeAmendCollection = firebase.firestore().collection('bookings')
    .doc(this.state.booking.id).collection('amends').doc(this.state.booking.activeAmendId)
    .onSnapshot(this.onAmendCollectionUpdate) 

  }

  onAmendCollectionUpdate = (querySnapshot) => {

    console.log("onAmendCollectionUpdate");
    console.log(querySnapshot);
    aObj = querySnapshot.data();
    aObj.id = querySnapshot.id;

    this.setState({amend: aObj});
  }

  updateBooking() {
    
    this.unsubscribeBookingCollection = firebase.firestore().collection('bookings')
    .doc(this.state.booking.id)
    .onSnapshot(this.onBookingCollectionUpdate) 

  }
  onBookingCollectionUpdate = (querySnapshot) => {

    console.log("onBookingCollectionUpdate");
    console.log(querySnapshot);
    aObj = querySnapshot.data();
    aObj.id = querySnapshot.id;

    this.setState({booking: aObj});
  }

  _onPressEscalate = () =>{
    //this.props.navigation.navigate('');
    Alert.alert(
        'Escalate Proposal',
        'Do you want to escalate proposed changes?',
        [
        {text: 'No, Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Yes, Escalate', onPress: () => {

            this.setState({spinner: true, spinnerMessage:'Escalating...'});
            firebase.firestore().collection('bookings').doc(this.state.booking.id)
            .collection('amends').doc(this.state.booking.activeAmendId).set(
                {
                  escalatedBy:this.loggedinprofile.userId,
                  escalatedAt: firebase.firestore.FieldValue.serverTimestamp()
                }, {merge: true}
            ). then(() => {

                firebase.firestore().collection('bookings').doc(this.state.booking.id)
                .set(
                    {
                      timesheetStatus: 'disputed',
                    }, {merge: true}
                ). then(() => {
                    this.setState({spinner: false, spinnerMessage:''});
                    UltraAgentHelpers.showAlertWithOneButton("Dispute Escalated!!",'Control will be in touch.');
                })  
                .catch((error) => {
                    console.log(error);
                    this.setState({spinner: false, spinnerMessage:''});
                    UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                });  

            })
            .catch((error) => {
                console.log(error);
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
            });

        }
        },
        ],
        { cancelable: false }
    );
  }

  _onPressAccept = () =>{
    //this.props.navigation.navigate('');
    Alert.alert(
        'Accept Proposal',
        'Do you want to accept proposed changes?',
        [
        {text: 'No, Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Yes, Accept', onPress: () => {

            this.setState({spinner: true, spinnerMessage:'Accepting...'});
            firebase.firestore().collection('bookings').doc(this.state.booking.id)
            .collection('amends').doc(this.state.booking.activeAmendId).set(
                {
                  approvalStatus: 'approved',
                  acceptedBy:this.loggedinprofile.userId,
                  acceptedAt: firebase.firestore.FieldValue.serverTimestamp()
                }, {merge: true}
            ). then(() => {

                let approvedMins = (this.state.amend.proposedEndTime.getTime() - 
                    this.state.amend.proposedStartTime.getTime())/(60*1000);
                let approvedRate = this.state.amend.proposedFee;
                firebase.firestore().collection('bookings').doc(this.state.booking.id)
                .set(
                    {
                      timesheetStatus: 'amendresolved',
                      approvedMins: approvedMins,
                      approvedRate: approvedRate
                    }, {merge: true}
                ). then(() => {
                    this.setState({spinner: false, spinnerMessage:''});
                    UltraAgentHelpers.showAlertWithOneButton("Success!",'Dispute resolved');
                })  
                .catch((error) => {
                    console.log(error);
                    this.setState({spinner: false, spinnerMessage:''});
                    UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                });  

            })
            .catch((error) => {
                console.log(error);
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
            });

        }
        },
        ],
        { cancelable: false }
    );
  }

  


  
static navigationOptions = {
    // header:null
    headerTitle: 'DISPUTED DETAILS',
    headerStyle: {
      backgroundColor: '#5271FF',
    },
    headerBackTitle: null,
    headerTitleStyle: {
      color: "#fff",
      flex:3,
      alignItems: 'center',
      justifyContent: 'center',
   },
   headerTintColor: '#fff',
 }

  render() {

    return (
        <Container>
             <Spinner
                    visible={this.state.spinner}
                    textContent={this.state.spinnerMessage}
                    textStyle={{color: '#FFF'}}
                    />
            <Content>
                <View style={styles.pendingShiftContainer}>
                    
                        <ListItem>
                            <Left>
                                <View style={styles.styleLeftSatus}>
                                    <View style={styles.shiftTextStyle}>
                                        <Text style={styles.jobIdText}>
                                        {this.state.booking.shiftSubCategory} #{this.state.booking.shiftNo}</Text>
                                    </View>
                                    <View style={styles.shiftlocationTextStyle}>
                                        <Text style={[styles.locationLeftTextStyle, styles.marginRightStyle]}>Location: </Text>
                                        <Text style={styles.locationRightTextStyle}>{this.state.booking.address}</Text>
                                    </View>
                                    <View style={styles.startTimeTextStyle}>
                                        <Text style={[styles.startTextStyle, styles.marginRightStyle]}>
                                            Time Started: 
                                        </Text>
                                        <Text style={styles.startTextStyle}>
                                        {this.state.booking.workStartTime.getHours()}:{("0" + this.state.booking.workStartTime.getMinutes()).slice(-2)}
                                        </Text>
                                    </View>
                                    <View style={styles.completeTimeTextStyle}>
                                        <Text style={[styles.completeTextStyle, styles.marginRightStyle]}>
                                            Completion Time: 
                                        </Text>
                                        <Text style={styles.completeTextStyle}>
                                        {this.state.booking.workEndTime.getHours()}:{("0" + this.state.booking.workEndTime.getMinutes()).slice(-2)}
                                        </Text>
                                    </View>
                                   
                                </View>
                            </Left> 
                            <Right></Right>
                            <View style={styles.styleRightSatus}>
                                <View style={styles.rightTimeStyle}>
                                    <Text style={[styles.timeTextStyle, styles.marginRightStyle]}>
                                        <Icon name="md-clock" size={15} color="#888"/>
                                        {this.state.booking.shiftStartDateTime.getHours()}
                                        :{("0" + this.state.booking.shiftStartDateTime.getMinutes()).slice(-2)}
                                    </Text>
                                    <Text style={styles.dateTextStyle}>
                                        <Icon name="md-calendar" size={15} color="#888"/>
                                        {this.state.booking.shiftStartDateTime.getDate()}/
                                        {this.state.booking.shiftStartDateTime.getMonth()+1}/
                                        {this.state.booking.shiftStartDateTime.getFullYear()}                                    
                                        </Text>
                                </View>
                                <View style={styles.shiftBadgeStyle}>
                                        <Badge style={styles.badgeStyle}>
                                        <Text style={styles.badgeText}>£{this.state.booking.fee}/H</Text>
                                        </Badge>
                                    </View>
                            </View> 
                        </ListItem>
                        <View style={styles.textContainer}>
                            <Text>Proposed Changes</Text>
                            <View style={styles.datetimeShowConatiner}>
                                <View style={styles.startTimeTextStyle}>
                                    <Text style={[styles.startTimeTStyle, styles.marginRightStyle]}>
                                        Start Time:
                                    </Text>
                                    <Text style={styles.startTimeStyle}>
                                    {this.state.amend.proposedStartTime == undefined? '' : this.state.amend.proposedStartTime.getHours()}:{("0" + (this.state.amend.proposedStartTime == undefined? '' : this.state.amend.proposedStartTime.getMinutes())).slice(-2)}
                                    </Text>
                                </View>
                                <View style={styles.startTimeTextStyle}>
                                    <Text style={[styles.startTimeTStyle, styles.marginRightStyle]}>
                                        End Time:
                                    </Text>
                                    <Text style={styles.startTimeStyle}>
                                    {this.state.amend.proposedEndTime == undefined? '' : this.state.amend.proposedEndTime.getHours()}:{("0" + (this.state.amend.proposedEndTime == undefined? '' : this.state.amend.proposedEndTime.getMinutes())).slice(-2)}
                                    </Text>
                                </View>
                                <View style={styles.startTimeTextStyle}>
                                    <Text style={[styles.startTimeTStyle, styles.marginRightStyle]}>
                                        Fees: 
                                    </Text>
                                    <Text style={styles.startTimeStyle}>
                                        £{this.state.amend.proposedFee}/h                                    
                                    </Text>
                                </View>
                                <View style={styles.startTimeTextStyle}>
                                    <Text style={[styles.startTimeTStyle, styles.marginRightStyle]}>
                                        Brief: 
                                    </Text>
                                    <Text style={styles.startTimeStyle}>
                                        {this.state.amend.brief}                                    
                                    </Text>
                                </View>
                            </View>
                        </View>
                        { this.state.booking.timesheetStatus == 'amended' && 
                        <View style={styles.buttonConatiner}>
                            <Button style={[styles.btnOrange, styles.btnMarginStyle]} 
                            onPress={this._onPressEscalate}>
                                <Text style={{color:'#fff', fontWeight:'bold'}}>ESCALATE</Text>
                            </Button>
                            <Button style={[styles.btnBlue, styles.btnMarginStyle]} 
                            onPress={this._onPressAccept}>
                                <Text style={{color:'#fff'}}>ACCEPT</Text>
                            </Button>
                        </View>
                        }
                        { this.state.booking.timesheetStatus == 'amendresolved' && 
                        <View style={styles.buttonConatiner}>
                            <Text>You have accepted at {} 
                            {this.state.amend.acceptedAt == undefined? '' : 
                            this.state.amend.acceptedAt.getHours()}
                            :{("0" + (this.state.amend.acceptedAt == undefined? '' : 
                            this.state.amend.acceptedAt.getMinutes())).slice(-2)} 
                            {} on {} 
                            {this.state.amend.acceptedAt == undefined? '' : this.state.amend.acceptedAt.getDate()}/
                            {this.state.amend.acceptedAt == undefined? '' : (this.state.amend.acceptedAt.getMonth()+1)}/
                            {this.state.amend.acceptedAt == undefined? '' : this.state.amend.acceptedAt.getFullYear()}  
                            .</Text>
                         </View>
                        }
                        { this.state.booking.timesheetStatus == 'disputed' && 
                        <View style={styles.buttonConatiner}>
                            <Text>Escalated at {} 
                            {this.state.amend.escalatedAt == undefined? '' : 
                            this.state.amend.escalatedAt.getHours()}
                            :{("0" + (this.state.amend.escalatedAt == undefined? '' : 
                            this.state.amend.escalatedAt.getMinutes())).slice(-2)} 
                            {} on {} 
                            {this.state.amend.escalatedAt == undefined? '' : this.state.amend.escalatedAt.getDate()}/
                            {this.state.amend.escalatedAt == undefined? '' : (this.state.amend.escalatedAt.getMonth()+1)}/
                            {this.state.amend.escalatedAt == undefined? '' : this.state.amend.escalatedAt.getFullYear()}  
                            .</Text>
                         </View>
                        }
                         { this.state.booking.timesheetStatus == 'disputeresolved' && 
                        <View style={styles.buttonConatiner}>
                            <Text>Escalated at {} 
                            {this.state.amend.escalatedAt == undefined? '' : 
                            this.state.amend.escalatedAt.getHours()}
                            :{("0" + (this.state.amend.escalatedAt == undefined? '' : 
                            this.state.amend.escalatedAt.getMinutes())).slice(-2)} 
                            {} on {} 
                            {this.state.amend.escalatedAt == undefined? '' : this.state.amend.escalatedAt.getDate()}/
                            {this.state.amend.escalatedAt == undefined? '' : (this.state.amend.escalatedAt.getMonth()+1)}/
                            {this.state.amend.escalatedAt == undefined? '' : this.state.amend.escalatedAt.getFullYear()}  
                            .</Text>
                        </View>
                        }
                        { this.state.booking.timesheetStatus == 'disputeresolved' && 
                        <View style={styles.buttonConatiner}>
                            <Text>Resolved at {} 
                            {this.state.amend.resolvedAt == undefined? '' : 
                            this.state.amend.resolvedAt.getHours()}
                            :{("0" + (this.state.amend.resolvedAt == undefined? '' : 
                            this.state.amend.resolvedAt.getMinutes())).slice(-2)} 
                            {} on {} 
                            {this.state.amend.resolvedAt == undefined? '' : this.state.amend.resolvedAt.getDate()}/
                            {this.state.amend.resolvedAt == undefined? '' : (this.state.amend.resolvedAt.getMonth()+1)}/
                            {this.state.amend.resolvedAt == undefined? '' : this.state.amend.resolvedAt.getFullYear()}  
                            .</Text>
                         </View>
                        }
                </View>
               
            </Content>
        </Container>
     );
    }
  }
  const styles = StyleSheet.create({
    pendingShiftContainer:{
        paddingTop:10,
        paddingBottom:20,
    },
    jobIdText:{
      fontWeight:'bold',
      color:'#5271ff',
      fontSize:18,
    },
   
    shiftlocationTextStyle:{
      flex:1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginTop:6,
      marginBottom:6,
    },
    startTimeTextStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    completeTimeTextStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    locationLeftTextStyle:{
        color:'#a9a9a9',
        fontWeight:'bold',
    },
    locationRightTextStyle:{
      fontWeight:'bold',
      color:'#303030',
    },
    marginRightStyle:{
      marginRight:5,
    },
    startTextStyle:{
        fontWeight:'bold',
        color:'#03bc07',
    },
    completeTextStyle:{
        fontWeight:'bold',
        color:'#f20028',
    },

    startTimeTStyle:{
        color:'#a9a9a9',
    },
    completeTimeTStyle:{
        color:'#a9a9a9',
    },
    startTimeStyle:{
        fontWeight:'500',
        color:'#000',
    },
    completeTimeStyle:{
        fontWeight:'500',
        color:'#000',
    },
    badgeStyle:{
      backgroundColor:'transparent', 
      borderColor:'#df8c40', 
      borderRadius:4, 
      borderWidth:1,
      marginTop:5,
    },
    badgeText:{
      color:'#df8c40',
      paddingLeft:4, 
      paddingRight:4, 
      lineHeight:21,
      textAlign:'center',
      alignItems:'center'
    },
    shiftBadgeStyle:{
      flex:1,
    },
    rightTimeStyle:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    statusTextStyle:{
        color:'#000',
        fontWeight:'bold'
    },
    statusBtnTextStyle:{
        paddingLeft:8,
        paddingRight:8,
        backgroundColor:'#000000',
        color:'#fff',
        paddingBottom:3,
    },
    textContainer:{
        padding:20,
        marginTop:30,
        backgroundColor:'#efefef'
    },
    datetimeShowConatiner:{
        backgroundColor:'#fff',
        padding:10,
        marginTop:5,
    },
    buttonConatiner:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        marginTop:60,
      },
      btnMarginStyle:{
        margin:5,
        marginBottom:10,
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        paddingLeft:30,
        paddingRight:30,
      },
      btnOrange:{
        backgroundColor:'#df8c40',
      },
      btnBlue:{
        backgroundColor:'#5271ff',
      },
      styleRightSatus:{
        position:'absolute',
        right:10,
        top:15,
        width:100,
        marginLeft:20,
    },
    styleLeftSatus:{
        paddingRight:40,
    }

})
