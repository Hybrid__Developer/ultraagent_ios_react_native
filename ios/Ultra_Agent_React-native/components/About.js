import React, { Component } from 'react';
import {
  StyleSheet, View, Image, ImageBackground, Text, 
  Dimensions, Switch, TouchableOpacity, ScrollView, TextInput, TouchableHighlight, Linking
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, List, Content, Header, Body, Form
} from 'native-base';
import Icon from 'react-native-ionicons';
import VersionNumber from 'react-native-version-number';
import Dialog, { DialogButton, DialogContent, DialogTitle, DialogFooter } from 'react-native-popup-dialog';

export default class About extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  
  render() {
    let { height, width } = Dimensions.get('window');

    return (
      
      <Container>
         <ImageBackground 
        source={require('../images/ultrabackground.jpg')} 
        imageStyle={{ resizeMode: 'cover' }}
        style={{ width, height }}>
        <Header style={{backgroundColor:'#5271FF', borderBottomColor:'transparent'}}>
        
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name="arrow-back" color='#fff' />         
              </Button>
          </Left>
          <Body style={{flex:1}}>
              <Title style={styles.HeaderTitle}>{'About'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
        
        </Header>
       
            <View style={styles.container}>
              <View style={styles.logoContainer}>
                <Image style={styles.logo} 
                source={require('../images/logo.png')}/>
                <Text style={styles.iconText}>
                    {'ultra agent'.toUpperCase()}
                </Text>
                <Text style={styles.versionText}>
                    Version {VersionNumber.appVersion}({VersionNumber.buildVersion})
                </Text>
              </View>
                {/* <View style={styles.buttonContainer}>
                  <Button iconRight style={styles.buttonLicence}>
                    <Text style={{color:'#5271FF', fontWeight:'bold'}}>Software licence</Text>
                    <Icon color='#5271FF' name='arrow-forward' />
                  </Button>
                  <Button iconRight style={styles.buttonLicence}>
                    <Text style={{color:'#5271FF', fontWeight:'bold'}}>Google maps licence</Text>
                    <Icon name='arrow-forward' color='#5271FF'/>
                  </Button>
                </View> */}
                <View>
                  <View style={{height:50, width:200,backgroundColor:'#fff',marginTop:10,textAlign:'center',
      alignItems: 'center',justifyContent: 'center',borderStyle:"solid", borderColor:'#1a73e8',borderWidth:2,borderRadius:5}}>
                 <Text style={{color:'#5271FF', fontWeight:'bold'}}
                 onPress={() => {
                  this.setState({ termsofServiceDialog: true });
                  }}
              activeOpacity={0.7}>
                 
                 Terms of service</Text>
                
                  </View>
                  <View style={{height:50, width:200,backgroundColor:'#fff',marginTop:10,textAlign:'center',
      alignItems: 'center',justifyContent: 'center',borderStyle:"solid", borderColor:'#1a73e8',borderWidth:2,borderRadius:5}}>
                  <Text style={{color:'#5271FF', fontWeight:'bold'}}onPress={() => {
                                    this.setState({ privacypolicy: true });
                                    }}
                                activeOpacity={0.7}>
                                  Privacy Policy</Text>
              
      </View>
                
               
                </View>
            </View>

            <View style={styles.inputContainer}>
              {/* Change password dialog container */}
                <Dialog
                  visible={this.state.termsofServiceDialog}
                  onTouchOutside={() => {
                      this.setState({ termsofServiceDialog: false });
                      }}
                  dialogTitle={
                    <DialogTitle title="Terms of service"
                      textStyle={{ color: '#000', fontSize:18 }}
                    />
                  }
                >
                  <TouchableOpacity onPress={() => {
                      this.setState({ termsofServiceDialog: false });
                      }}  style={{position: 'absolute', right: 15, top:5,
                      justifyContent: 'center'}}>
                    <Icon color='#5271FF' name='close' size={35} />
                  </TouchableOpacity>
                
                  {/* <ScrollView> */}
                      <DialogContent style={{height:400}}>
                      <ScrollView>
<Text style={styles.termsHeading}>
Introduction</Text>
<Text style={styles.termsText}>
These terms and conditions apply between you, 
the User of this Website (including any sub-domains, unless expressly 
excluded by their own terms and conditions), and Ultra Agent Limited, 
the owner and operator of this Website. Please read these terms and 
conditions carefully, as they affect your legal rights. Your agreement 
to comply with and be bound by these terms and conditions is deemed to 
occur upon your first use of the Website. If you do not agree to be 
bound by these terms and conditions, you should stop using the Website 
immediately.
</Text>
<Text style={styles.termsText}>
In these terms and conditions, User or Users means any third party 
that accesses the Website and is not either (i) employed by Ultra 
Agent Limited and acting in the course of their employment or (ii) 
engaged as a consultant or otherwise providing services to Ultra 
Agent Limited and accessing the Website in connection with the 
provision of such services.
</Text>
<Text style={styles.termsText}>
You must be at least 16 years of age to use this Website. 
By using the Website and agreeing to these terms and conditions, 
you represent and warrant that you are at least 16 years of age.
</Text>
<Text style={styles.termsHeading}>
Intellectual property and acceptable use
</Text>
<Text style={styles.termsText}>
1. All Content included on the Website, unless uploaded by Users, is the property of 
Ultra Agent Limited, our affiliates or other relevant third parties. In these terms 
and conditions, Content means any text, graphics, images, audio, video, software, 
data compilations, page layout, underlying code and software and any other form of 
information capable of being stored in a computer that appears on or forms part of this
Website, including any such content uploaded by Users. By continuing to use 
the Website you acknowledge that such Content is protected by copyright, trademarks, 
database rights and other intellectual property rights. Nothing on this site shall be 
construed as granting, by implication, estoppel, or otherwise, any license or right to 
use any trademark, logo or service mark displayed on the site without the owner's prior 
written permission
</Text>
<Text style={styles.termsText}>
2. You may, for your own personal, non-commercial use only, do the following :
</Text>
<View style={{marginLeft:10}}>
<Text style={styles.termsText}>
a. retrieve, display and view the Content on a computer screen
</Text>
<Text style={styles.termsText}>
b. download and store the Content in electronic form on a disk (but not on any server or other storage device connected to a network)
</Text>
<Text style={styles.termsText}>
c. print one copy of the Content
</Text>
</View>
<Text style={styles.termsText}>
3. You must not otherwise reproduce, modify, copy, distribute or use for commercial purposes any Content without the written permission of Ultra Agent Limited.
</Text>
<Text style={styles.termsText}>
4. You acknowledge that you are responsible for any Content you 
may submit via the Website, including the legality, reliability, 
appropriateness, originality and copyright of any such Content. 
You may not upload to, distribute or otherwise publish through the
Website any Content that (i) is confidential, proprietary, false, 
fraudulent, libellous, defamatory, obscene, threatening, invasive 
of privacy or publicity rights, infringing on intellectual property 
rights, abusive, illegal or otherwise objectionable; (ii) may 
constitute or encourage a criminal offence, violate the rights of 
any party or otherwise give rise to liability or violate any law; or 
(iii) may contain software viruses, political campaigning, chain 
letters, mass mailings, or any form of "spam." You may not use a 
false email address or other identifying information, impersonate 
any person or entity or otherwise mislead as to the origin of any 
content. You may not upload commercial content onto the Website.
</Text>
<Text style={styles.termsText}>
5. You represent and warrant that you own or otherwise control all 
the rights to the Content you post; that the Content is accurate; 
that use of the Content you supply does not violate any provision 
of these terms and conditions and will not cause injury to any 
person; and that you will indemnify Ultra Agent Limited for all 
claims resulting from Content you supply.
</Text>
<Text style={styles.termsHeading}>Prohibited use</Text>
<Text style={styles.termsText}>6. You may not use the Website 
for any of the following purposes:</Text>
<View style={{marginLeft:10}}>
<Text style={styles.termsText}>a. in any way which causes, or may cause, damage to the 
Website or interferes with any other person's use or enjoyment 
of the Website;
</Text>
<Text style={styles.termsText}>b. in any way which is harmful, 
unlawful, illegal, abusive, harassing, threatening or otherwise 
objectionable or in breach of any applicable law, regulation, 
governmental order;
</Text>
<Text style={styles.termsText}>c. making, transmitting or storing 
electronic copies of Content protected by copyright without the 
permission of the owner.</Text>
</View>
<Text style={styles.termsHeading}>Registration</Text>
<Text style={styles.termsText}>7. You must ensure that the details 
provided by you on registration or at any time are correct and 
complete.</Text>
<Text style={styles.termsText}>8. You must inform us immediately of 
any changes to the information that you provide when registering by 
updating your personal details to ensure we can communicate with you
effectively.
</Text>
<Text style={styles.termsText}>9. We may suspend or cancel your 
registration with immediate effect for any reasonable purposes or if
you breach these terms and conditions.
</Text>
<Text style={styles.termsText}>10. You may cancel your registration at 
any time by informing us in writing to the address at the end of these 
terms and conditions. If you do so, you must immediately stop using the 
Website. Cancellation or suspension of your registration does not affect 
any statutory rights.
</Text>
<Text style={styles.termsHeading}>Password and security</Text>
<Text style={styles.termsText}>11. When you register on this Website, 
you will be asked to create a password, which you should keep 
confidential and not disclose or share with anyone.
</Text>
<Text style={styles.termsText}>12. If we have reason to believe that 
there is or is likely to be any misuse of the Website or breach of 
security, we may require you to change your password or suspend your 
account.
</Text>
<Text style={styles.termsHeading}>Links to other websites</Text>
<Text style={styles.termsText}>13. This Website may contain links to 
other sites. Unless expressly stated, these sites are not under the 
control of Ultra Agent Limited or that of our affiliates.
</Text>
<Text style={styles.termsText}>14. We assume no responsibility for the 
content of such Websites and disclaim liability for any and all forms 
of loss or damage arising out of the use of them.
</Text>
<Text style={styles.termsText}>15. The inclusion of a link to another 
site on this Website does not imply any endorsement of the sites 
themselves or of those in control of them.
</Text>
<Text style={styles.termsHeading}>Privacy Policy and Cookies Policy
</Text>
<Text style={styles.termsText}>16. Use of the Website is also governed by our Privacy 
Policy and Cookies Policy, which are incorporated into these terms and conditions by this 
reference. To view the Privacy Policy and Cookies Policy, please click on 
the following: ______________ and ______________.
</Text>
<Text style={styles.termsHeading}>Availability of the Website and 
disclaimers</Text>
<Text style={styles.termsText}>17. Any online facilities, tools, 
services or information that Ultra Agent Limited makes available 
through the Website (the Service) is provided "as is" and on an 
"as available" basis. We give no warranty that the Service will be 
free of defects and/or faults. To the maximum extent permitted by the 
law, we provide no warranties (express or implied) of fitness for a 
particular purpose, accuracy of information, compatibility and 
satisfactory quality. Ultra Agent Limited is under no obligation to 
update information on the Website.
</Text>
<Text style={styles.termsText}>18. Whilst Ultra Agent Limited uses 
reasonable endeavours to ensure that the Website is secure and free 
of errors, viruses and other malware, we give no warranty or guaranty 
in that regard and all Users take responsibility for their own 
security, that of their personal details and their computers.
</Text>
<Text style={styles.termsText}>19. Ultra Agent Limited accepts no 
liability for any disruption or non-availability of the Website.
</Text>
<Text style={styles.termsText}>20. Ultra Agent Limited reserves the 
right to alter, suspend or discontinue any part (or the whole of) 
the Website including, but not limited to, any products and/or 
services available. These terms and conditions shall continue to apply 
to any modified version of the Website unless it is expressly stated 
otherwise.
</Text>
<Text style={styles.termsHeading}>Limitation of liability</Text>
<Text style={styles.termsText}>21. Nothing in these terms and conditions will: (a) 
limit or exclude our or your liability for death or personal injury resulting from 
our or your negligence, as applicable; (b) limit or exclude our or your liability 
for fraud or fraudulent misrepresentation; or (c) limit or exclude any of our or your 
liabilities in any way that is not permitted under applicable law.</Text>
<Text style={styles.termsText}>22. We will not be liable to you in 
respect of any losses arising out of events beyond our reasonable
control.
</Text>
<Text style={styles.termsText}>23. To the maximum extent permitted by 
law, Ultra Agent Limited accepts no liability for any of the following:
</Text>
<View style={{marginLeft:10}}>
<Text style={styles.termsText}>a. any business losses, such as loss of 
profits, income, revenue, anticipated savings, business, contracts, 
goodwill or commercial opportunities;
</Text>
<Text style={styles.termsText}>b. loss or corruption of any data, 
database or software;
</Text>
<Text style={styles.termsText}>c. any special, indirect or 
consequential loss or damage.
</Text>
</View>
<Text style={styles.termsHeading}>General</Text>
<Text style={styles.termsText}>24. You may not transfer any of your 
rights under these terms and conditions to any other person. We may 
transfer our rights under these terms and conditions where we 
reasonably believe your rights will not be affected.
</Text>
<Text style={styles.termsText}>25. These terms and conditions may be 
varied by us from time to time. Such revised terms will apply to the 
Website from the date of publication. Users should check the terms and 
conditions regularly to ensure familiarity with the then current 
version.
</Text>
<Text style={styles.termsText}>26. These terms and conditions together 
with the Privacy Policy and Cookies Policy contain the whole agreement 
between the parties relating to its subject matter and supersede all 
prior discussions, arrangements or agreements that might have taken 
place in relation to the terms and conditions.
</Text>
<Text style={styles.termsText}>27. The Contracts (Rights of Third 
Parties) Act 1999 shall not apply to these terms and conditions and no 
third party will have any right to enforce or rely on any provision of 
these terms and conditions.
</Text>
<Text style={styles.termsText}>28. If any court or competent 
authority finds that any provision of these terms and conditions 
(or part of any provision) is invalid, illegal or unenforceable, that 
provision or part-provision will, to the extent required, be deemed to
be deleted, and the validity and enforceability of the other 
provisions of these terms and conditions will not be affected.
</Text>
<Text style={styles.termsText}>29. Unless otherwise agreed, no delay, 
act or omission by a party in exercising any right or remedy will be 
deemed a waiver of that, or any other, right or remedy.
</Text>
<Text style={styles.termsText}>30. This Agreement shall be governed 
by and interpreted according to the law of England and Wales and all 
disputes arising under the Agreement (including non-contractual 
disputes or claims) shall be subject to the exclusive jurisdiction of 
the English and Welsh courts.
</Text>
<Text style={styles.termsHeading}>Ultra Agent Limited details</Text>
<Text style={styles.termsText}>31. Ultra Agent Limited is a company 
incorporated in England and Wales with registered number 10510063 whose 
registered address is 9 Rotherwick hill, Ealing, London, W5 3EQ and it 
operates the Website www.ultraagent.co.uk.
</Text>
<Text style={styles.termsText}>You can contact Ultra Agent Limited by
email on hello@ultragent.co.uk.
</Text>
<Text style={styles.termsHeading}>Attribution</Text>
<Text style={styles.termsText}>32. These terms and conditions were 
created using a document from Rocket Lawyer</Text> 
{/* <Text onPress={()=>{Linking.openURL('https://www.rocketlawyer.co.uk')}>(https://www.rocketlawyer.co.uk).</Text>
<Text onPress={()=>{Linking.openURL('https://www.rocketlawyer.co.uk')}>My Text</Text> */}
<TouchableOpacity onPress={() => Linking.openURL('https://www.rocketlawyer.co.uk')} >
<Text style={{color:'#5271FF'}}>
https://www.rocketlawyer.co.uk 
</Text>
</TouchableOpacity>
</ScrollView>
                      </DialogContent>
                  {/* </ScrollView> */}
              </Dialog>
            </View>



            <View style={styles.inputContainer}>
              {/* Change password dialog container */}
                <Dialog
                  visible={this.state.privacypolicy}
                  onTouchOutside={() => {
                      this.setState({ privacypolicy: false });
                      }}
                  dialogTitle={
                    <DialogTitle title="Privacy Policy"
                      textStyle={{ color: '#000', fontSize:18 }}
                    />
                  }
                >
                  <TouchableOpacity onPress={() => {
                      this.setState({ privacypolicy: false });
                      }}  style={{position: 'absolute', right: 15, top:5,
                      justifyContent: 'center'}}>
                    <Icon color='#5271FF' name='close' size={35} />
                  </TouchableOpacity>
                
                  {/* <ScrollView> */}
                      <DialogContent style={{height:400}}>
                       <ScrollView>
                        <Text style={styles.heading}>
                        Privacy policy: 
                           </Text>
<Text style={styles.termsHeading}>
This privacy policy notice is served by Ultra Agent Limited, 9 Rotherwick Hill, London, W5 3EQ, United Kingdom for the app; </Text>
<TouchableOpacity onPress={() => Linking.openURL('https://ultraagent.co.uk')} >
                            <Text style={{color:'#5271FF'}}>
https://ultraagent.co.uk 
</Text></TouchableOpacity>
<Text style={styles.termsHeading}>
and the associated website. The app is used to link and connect employers with full vetted employees at a cost which is less than traditional agency establishments.

The purpose of this policy is to explain to you how we control, process, handle and protect your personal information through the business and while you use the app and browse on the website. If you do not agree to the following policy you may wish to cease viewing / using the app or website, and or refrain from submitting your personal data to us.
</Text>
<Text style={styles.heading}>
Policy key definitions:
                           </Text>
<Text style={styles.termsHeading}>

• "I", "our", "us", or "we" refer to the business, Ultra Agent Limited.
• "you", "the user" refer to the person(s) using this website or app.
• GDPR means General Data Protection Act.
• PECR means Privacy & Electronic Communications Regulation.
• ICO means Information Commissioner's Office.
• Cookies mean small files stored on a user’s computer or device.
</Text>
<Text style={styles.heading}>
Key principles of GDPR:
</Text>
<Text style={styles.termsHeading}>


Our privacy policy embodies the following key principles; (a) Lawfulness, fairness and transparency, (b) Purpose limitation, (c) Data minimisation, (d) Accuracy, (e) Storage limitation, (f) Integrity and confidence, (g) Accountability.
</Text>
<Text style={styles.heading}>

Collection and Processing of your personal data
</Text>
<Text style={styles.termsHeading}>

Under the GDPR (General Data Protection Regulation) we control and / or process any personal information about you electronically using lawful bases.

We are registered with the ICO under the Data Protection Register with registration number ZA528253.

We collect different types of information depending on whether you are using the app as an employer or jobseeker.

The information we collect for job seekers may include name, email address, postal addresses, availability dates, photograph, profile description, contact details, previous employment experience, academic education, training certification copies of any ‘right to work’ photo documentation which shows your ongoing entitlement to work in the UK (such as passport, visa documentation, national ID and/or resident’s permit), date of birth, Disclosure and Barring Service (DBS) checks for criminal history and any other information required on the registration form. DBS documentation will be destroyed immediately after registration.

The app has a feature for uploading capturing and uploading soft copies of supporting documents for registration, and your consent will be requested for access to the camera or photo library to facilitate the registration process.

The information we collect for employers may include name, employer name, employer address, email address, job title and other business contact details of your staffing managers as well as the following information relating to any jobseekers whom you engage: (a) rates of pay, (b) shifts worked, (c) skills set, (d) employer feedback and ratings (e) timesheet information

Regarding each visit to the app, we automatically collect technical information including the Internet protocol (IP) address used to connect your device to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;

We also collect information about your visit, including the dates and times you use the app for jobseekers, work opportunities you view or search for page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), methods used to browse away from the page, and any phone number used to call our customer service number; and

We receive information from other sources; and for employers this may include information from credit rating agencies. We will notify you when we receive information about you from them and the purposes for which we intend to use that data.

We will continue to process your information under established legal basis until you withdraw consent or it is determined your consent no longer exists.

If, as determined by us, the lawful basis upon which we process your personal information changes, we will notify you about the change and any new lawful basis to be used if required. We shall stop processing your personal information if the lawful basis used is no longer relevant.

Your individual rights
Under the GDPR your rights are as follows. You can read more about your rights in details here;

• the right to be informed;
• the right of access;
• the right to rectification;
• the right to erasure;
• the right to restrict processing;
• the right to data portability;
• the right to object; and
• the right not to be subject to automated decision-making including profiling.
You also have the right to complain to the ICO www.ico.org.uk if you feel there is a problem with the way we are handling your data.

We handle subject access requests in accordance with the GDPR.

Internet cookies
We use cookies on the website and app to provide you with a better user experience. We do this by placing a small text file on your device / computer hard drive to track how you use the website, to record or log whether you have seen particular messages that we display, to keep you logged into the website where applicable, to display relevant adverts or content, referred you to a third party website.

Some cookies are required to enjoy and use the full functionality of this website or app.

We use a cookie control system which allows you to accept the use of cookies, and control which cookies are saved to your device / computer. Some cookies will be saved for specific time periods, where others may last indefinitely. Your web browser should provide you with the controls to manage and delete cookies from your device, please see your web browser options.

Cookies that we use are;

• Session cookies are used to remember selections made when you use the app and website. Session cookies are deleted automatically when you leave the app or close your browser.
• Persistent cookies are used to identify you when you return to the app/ browser and/or to remember specific information about your preferences. These cookies also help us understand browsing behaviour within the app, which can help us develop the app in a way we think will be most relevant to your interests. Persistent cookies are stored on your device and are not deleted when the browser is closed – these cookies must be ‘manually’ deleted if you want to remove them.
Data security and protection
We ensure the security of any personal information we hold by using secure data storage technologies and precise procedures in how we store, access and manage that information. The data that we collect from you will be transferred to, and stored at, a destination within the European Economic Area (“EEA”). We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.

Our security methods meet the GDPR compliance requirements.
</Text>
<Text style={styles.heading}>
Fair & Transparent Privacy Explained
</Text>
<Text style={styles.termsHeading}>

We have provided some further explanations about user privacy and the way we use this website or app to help promote a transparent and honest user privacy methodology.
</Text>
<Text style={styles.heading}>
Sponsored links, affiliate tracking & commissions
</Text>
<Text style={styles.termsHeading}>


Our website may contain adverts, sponsored and affiliate links on some pages. These are typically served through our advertising partners; Google AdSense, eBay Partner Network, Amazon Affiliates, or are self-served through our own means. We only use trusted advertising partners who each have high standards of user privacy and security. However, we do not control the actual adverts seen / displayed by our advertising partners. Our ad partners may collect data and use cookies for ad personalisation and measurement. Where ad preferences are requested as 'non-personalised' cookies may still be used for frequency capping, aggregated ad reporting and to combat fraud and abuse.

Clickable sponsored or affiliate links may be displayed as a website URL. Clicking on any adverts, sponsored or affiliate links may track your actions by using a cookie saved to your device. You can read more about cookies on this website above. Your actions are usually recorded as a referral from our website by this cookie. In most cases we earn a very small commission from the advertiser or advertising partner, at no cost to you, whether you make a purchase on their website or not.

We use advertising partners in these ways to help generate an income from the website, which allows us to continue our work and provide you with the best overall experience and valued information.

If you have any concerns about this, we suggest you do not click on any adverts, sponsored or affiliate links found throughout the website.
</Text>
<Text style={styles.heading}>

Email marketing messages & subscription
</Text>
<Text style={styles.termsHeading}>

Under the GDPR we use the consent lawful basis for anyone subscribing to our newsletter or marketing mailing list. We only collect certain data about you, as detailed in the "Processing of your personal data" above. Any email marketing messages we send are done so through an EMS, email marketing service provider. An EMS is a third-party service provider of software / applications that allows marketers to send out email marketing campaigns to a list of users.

Email marketing messages that we send may contain tracking beacons / tracked clickable links or similar server technologies in order to track subscriber activity within email marketing messages. Where used, such marketing messages may record a range of data such as; times, dates, I.P addresses, opens, clicks, forwards, geographic and demographic data. Such data, within its limitations will show the activity each subscriber made for that email campaign.

Any email marketing messages we send are in accordance with the GDPR and the PECR. We provide you with an easy method to withdraw your consent (unsubscribe) or manage your preferences / the information we hold about you at any time. See any marketing messages for instructions on how to unsubscribe or manage your preferences.

 
</Text>
<Text style={styles.heading}>
Contact
</Text>
<Text style={styles.termsHeading}>


Questions, comments and requests regarding this privacy policy should be addressed to hello@ultraagent.co.uk

Should you have cause to complain about how we handle your personal data, please contact us in the first instance. We will do our best to resolve your concern. Alternatively, you may prefer to submit a complaint directly to the Information Commissioner’s Office (ICO).

For further details on how to complain to the ICO, please follow the links below: </Text>
<TouchableOpacity onPress={() => Linking.openURL('https://ico.org.uk/concerns/')} >
                            <Text style={{color:'#5271FF'}}>
https://ico.org.uk/concerns/
</Text></TouchableOpacity><Text>or</Text>
<TouchableOpacity onPress={() => Linking.openURL(' https://ico.org.uk/global/contact-us/')} >
                            <Text style={{color:'#5271FF'}}>
  https://ico.org.uk/global/contact-us/.
</Text></TouchableOpacity>
                        </ScrollView> 
                      </DialogContent>
                  {/* </ScrollView> */}
              </Dialog>
            </View>
        </ImageBackground>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
    HeaderTitle:{
      color:'#fff',
    },
    SubHeaderTitle:{
      color:'#fff',
      fontSize:16,
      padding:5,
      paddingTop:6,
    },
   
    container:{
      flex:1,
      alignItems: 'center',
    },
    logoContainer:{
      alignItems: 'center',
      alignSelf:'stretch',
      marginTop:80,
    },
    iconText:{
      color:'#000',
      textAlign:'center',
      alignItems: 'center',
      marginTop:10,
      fontWeight:"bold",
      fontSize:20,
    },
    versionText:{
      color:'#000',
      marginTop:10,
      textAlign:'center',
      alignItems: 'center',
      fontWeight:"bold",
    },

    logo:{
      width:180,
      height:180,
    },
   
    buttonContainer:{
      marginTop:50,
      width:260,
      textAlign:'center',
      alignItems: 'center',
      textAlignVertical:'center'
    },
    // buttonLicence:{
    //   width:260,
    //   marginTop:10,
    //   backgroundColor:'#fff',
    //   borderRadius:5,
    //   borderWidth:2,
    //   borderColor:'#5271FF',
    //   padding:10,
    // }
    buttonLicence:{
      marginTop:50,
      backgroundColor:'#fff',
      borderRadius:5,
      borderWidth:2,
      borderColor:'#5271FF',
      padding:10,
      paddingLeft:20,
      paddingRight:20
    },
    termsHeading:{
      color:'#000', 
      fontWeight:'bold', 
      fontSize:16, 
      marginTop:10
    },
    termsText:{
      fontSize:14,
      marginTop:5
    },
    heading:{
      marginTop:5,
      fontSize:25,
      color:'#000',
  },
    
})