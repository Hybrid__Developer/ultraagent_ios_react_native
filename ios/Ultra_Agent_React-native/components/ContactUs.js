import React, { Component } from 'react';
import {
  StyleSheet, View, Image, ImageBackground, Text, 
  Dimensions, Switch,
} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, List, ListItem, Content, Header, Body
} from 'native-base';
import Icon from 'react-native-ionicons';


export default class ContactUs extends Component {

  constructor(props) {
    super(props);
    this.state = {
    
    };
  }

  
  render() {
    let { height, width } = Dimensions.get('window');
    return (
      <ImageBackground
      source={require('../images/timesheetbg.png')}
      style={{ width, height }}>
        <Header style={{backgroundColor:'#5271FF', borderBottomColor:'transparent'}}>
        
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name="arrow-back" color='#fff' />         
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'contact us'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
        
        </Header>

        <Content>
          <View style={styles.contactLogoContainer}>
            <Image style={styles.logo} 
            source={require('../images/contactuslogo.png')}/>
          </View>
          <View style={styles.contactFormContainer}>
              <List style={styles.listmarginStyle}>
                <ListItem style={styles.settingListitem}>
                  {/* <Left>
                      <Text style={styles.fontColorStyle}>Call</Text>
                  </Left>
                  <Body style={{paddingLeft:0, marginLeft:0}}>
                      <Text style={styles.fontBoldStyle}>+447852317228</Text>
                  </Body> */}
                  <View style={{flex:1, flexDirection:'row'}}>
                    <Text style={styles.fontColorStyle}>Call</Text>
                  
                    <Text style={[styles.fontBoldStyle, styles.leftSpace]}>07450 224 064</Text>
                  </View>
                </ListItem>
                <ListItem style={styles.settingListitem}>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <Text style={styles.fontColorStyle}>Text</Text>
                  
                    <Text style={[styles.fontBoldStyle, styles.leftSpace]}>07450 224 064</Text>
                  </View>
                 
                </ListItem>
                <ListItem style={styles.settingListitem}>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <Text style={styles.fontColorStyle}>Email</Text>
                  
                    <Text style={[styles.fontBoldStyle, styles.leftSpace]}>hello@ultraagent.co.uk</Text>
                  </View>
                </ListItem>
                <ListItem style={styles.settingListitem}>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <Text style={styles.fontColorStyle}>Address</Text>
                  
                    <Text style={styles.fontBoldStyle}>{'First Floor F12 \n25 Finsbury Circus \nLondon \nEC2M 7EE'}</Text>
                  </View>
                </ListItem>
                <ListItem style={styles.settingListitem}>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <Text style={styles.fontColorStyle}>Website</Text>
                  
                    <Text style={styles.fontBoldStyle}>www.ultraagent.co.uk</Text>
                  </View>
                </ListItem>
              </List>
            </View>
        </Content>
     </ImageBackground>
     );
    }
  }
  const styles = StyleSheet.create({
    HeaderTitle:{
      color:'#fff',
    },
    SubHeaderTitle:{
      color:'#fff',
      fontSize:16,
      padding:5,
      paddingTop:6,
    },
    contactLogoContainer:{
      alignItems: 'center',
      alignSelf:'stretch',
      backgroundColor:'#5271FF',
      paddingTop:80,
      paddingBottom:60,
    },
    logo:{
        width:300,
        height:200,
    },
    contactFormContainer:{
      backgroundColor:'#f4f4f4',
      bottom:70,
      marginLeft:20,
      marginRight:20,
      borderRadius:4,
    },
    fontColorStyle:{
      color:'#939393',
      // alignItems:'flex-start',
      // textAlign:'left'
    },
    fontBoldStyle:{
      fontWeight:'bold',
      left:50,
      paddingRight:20,
      flexWrap:'wrap'
      // alignItems:'flex-end',
      // textAlign:'right',
      // justifyContent:'flex-end',
      //marginLeft:80
    },
    leftSpace:{
      left:70,
    }
})