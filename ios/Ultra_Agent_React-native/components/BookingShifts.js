import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Alert, Text,

} from 'react-native';
import {
  Container, Input, Title, Label, Button,
  Left, Right, Picker, List, ListItem, Content, Item, Header, Body,
} from 'native-base';
import Icon from 'react-native-ionicons';
import CountDown from 'react-native-countdown-component';
import moment from 'moment';
import firebase from 'react-native-firebase';

import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import Spinner from 'react-native-loading-spinner-overlay';

console.disableYellowBox = true;
console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

export default class BookingShifts extends Component {

  constructor(props) {
    super(props);

    this.unsubscribeBookingCollection = null;

    booking =
      this.props.navigation.state.params.booking;
    // console.log('BookingShifts:' + booking);
    this.state = {
      totalDuration: 0,
      hideCountDown: false,
      booking: booking,
      startEndShiftButtonShouldShow: true,
      startEndShiftButtonTitle: 'START SHIFT',
      spinner: false,
      onLoad: true,
      updateTotalDuration: true,
      onStop: false,
      spinnerMessage: '',
    };
    this.onBookingUpdate()
  }
  onBookingUpdate() {
    this.onBookingCollectionUpdate = this.onBookingCollectionUpdate.bind(this);
  }

  componentWillUnmount() {
    this.unsubscribeBookingCollection();
  }
  onLoad = (stop) => {
    if (!this.state.onStop) {
      this._interval = setInterval(() => this.onBookingUpdate(), 1000);
    }
    if (stop && (this.state.onStop)) {
      this.onStopLoad()
    }
  }
  onStopLoad = () => {
    clearInterval(this._interval);
  }

  componentDidMount() {
    // var that = this;
    // var date = moment()
    //   .utcOffset('+05:30')
    //   .format('YYYY-MM-DD hh:mm:ss');
    // //Getting the current date-time with required formate and UTC   

    // var expirydate = '2018-11-03 04:00:45';//You can set your own date-time
    // //Let suppose we have to show the countdown for above date-time 

    // var diffr = moment.duration(moment(expirydate).diff(moment(date)));
    // //difference of the expiry date-time given and current date-time

    // var hours = parseInt(diffr.asHours());
    // var minutes = parseInt(diffr.minutes());
    // var seconds = parseInt(diffr.seconds());

    // var d = hours * 60 * 60 + minutes * 60 + seconds;
    // //converting in seconds

    // that.setState({ totalDuration: d });
    // //Settign up the duration of countdown in seconds to re-render

    this.unsubscribeBookingCollection = firebase.firestore().collection('bookings')
      .doc(this.state.booking.id)
      .onSnapshot(this.onBookingCollectionUpdate);
  }

  onBookingCollectionUpdate = (querySnapshot) => {
    // console.log(querySnapshot);
    let aBooking = querySnapshot.data();
    if (aBooking != undefined) {
      aBooking.id = querySnapshot.id;
      console.log(aBooking);
      let buttShow = true;
      let buttTitle = '';

      var hideCountDown = false;
      var secsToGo = (aBooking.shiftStartDateTime.getTime() - (new Date()).getTime()) / 1000;

      if ((aBooking.workingStatus == undefined) || (aBooking.workingStatus == 'pending')) {
        buttShow = true; buttTitle = 'START SHIFT';
      } else if ((aBooking.workingStatus == 'working') || (aBooking.workingStatus == 'paused')) {
        buttShow = true; buttTitle = 'UPDATE SHIFT';
        hideCountDown = true;
      } else {
        buttShow = false; buttTitle = '';
        hideCountDown = true;
      }


      if (this.state.onLoad) {
        this.setState({
          booking: aBooking,
          startEndShiftButtonShouldShow: buttShow,
          startEndShiftButtonTitle: buttTitle,
          totalDuration: secsToGo,
          hideCountDown: hideCountDown,
          onLoad: false
        });
        this.onLoad(false)
      }
      // onLoad = onLoad + 1
      if ((aBooking.shiftStartDateTime.getTime()) / 1000 <= (new Date()).getTime() / 1000) {
        if (this.state.updateTotalDuration) {
          this.setState({
            w: 'Alert! Start shift before your time elapses',
            totalDuration: secsToGo + 1800,
            updateTotalDuration: false
          })
        }
        if ((aBooking.shiftStartDateTime.getTime()) / 1000 + 1800 <= (new Date()).getTime() / 1000) {
          this.setState({
            w: 'Sorry you are late for this shift, Please cancel booking to add reasons for not starting shift in time.',
            onStop: true
          })
          this.setState({ startEndShiftButtonShouldShow: false })
          this.onLoad(true)
        }
      }
    }

    //console.log("After: mergeCompanies +", this.state.shifts)
  }


  static navigationOptions = {
    header: null
    //   headerTitle: 'PLUMBING #1038',
    //   headerStyle: {
    //     backgroundColor: '#5271FF',
    //   },
    //   headerBackTitle: null,
    //   headerTitleStyle: {
    //     color: "#fff",
    //     flex:1,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //  },
    //  headerTintColor: '#fff',
  }

  _onPressNavigate = () => {
    this.props.navigation.navigate('NavigateMap', { booking: this.state.booking });
  }

  _onPressCancelBooking = () => {
    // Works on both iOS and Android
    Alert.alert(
      'Cancel Booking',
      'Do you want to cancel booking?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: 'Yes', onPress: () => {
            this.props.navigation.navigate('CancelBooking', { booking: this.state.booking });
          }
        },
      ],
      { cancelable: false }
    )
  }

  _onPressStartShift = () => {
    // Works on both iOS and Android
    if (this.state.startEndShiftButtonTitle == 'START SHIFT') {

      const now = new Date();
      if (now.getTime() < this.state.booking.shiftStartDateTime.getTime()) {
        UltraAgentHelpers.showAlertWithOneButton("Before time not allowed", "Shift is scheduled to start at " +
          this.state.booking.shiftStartDateTime.getDate() + '/' + (this.state.booking.shiftStartDateTime.getMonth() + 1) + '/' + this.state.booking.shiftStartDateTime.getFullYear() +
          " " + this.state.booking.shiftStartDateTime.getHours() + ':' + ("0" + this.state.booking.shiftStartDateTime.getMinutes()).slice(-2) +
          "\n\n" + "Please try to start shift on or after the scheduled start time.");
        return;
      }

      Alert.alert(
        'Start Shift',
        'Are you Ready \n to start Shift ?',
        [
          { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          {
            text: 'Yes', onPress: () => {
              // Start Shift here
              this.setState({ spinner: true, spinnerMessage: 'Updating...' });
              firebase.firestore().collection('bookings').doc(this.state.booking.id).set(
                {
                  workingStatus: 'working',
                  workStartTime: firebase.firestore.FieldValue.serverTimestamp()
                }, { merge: true }
              ).then(() => {

                setTimeout(() => {
                  firebase.firestore().collection('bookings').doc(this.state.booking.id)
                    .get({ source: 'server' })
                    .then((doc) => {
                      console.log(doc);
                      if (!doc.exists) { return }
                      let aObj = doc.data();
                      aObj.id = doc.id;
                      console.log('Response: ' + aObj);
                      console.log(aObj);
                      console.log(aObj.workStartTime.getTime());
                      firebase.firestore().collection('bookings').doc(this.state.booking.id)
                        .collection('workedslots').doc(aObj.workStartTime.getTime() + '').set(
                          {
                            startTime: aObj.workStartTime
                          }, { merge: true }
                        ).then(() => {
                          firebase.firestore().collection('bookings').doc(this.state.booking.id).set(
                            {
                              workActiveSlotId: aObj.workStartTime.getTime() + ''
                            }, { merge: true }
                          ).then(() => {
                            this.setState({ spinner: false, spinnerMessage: '' });
                            this.props.navigation.navigate('BookingCancel', { booking: this.state.booking });
                          })
                            .catch((error) => {
                              console.log(error);
                              this.setState({ spinner: false, spinnerMessage: '' });
                              UltraAgentHelpers.showAlertWithOneButton("Oops!", error.message);
                            });
                        })
                        .catch((error) => {
                          console.log(error);
                          this.setState({ spinner: false, spinnerMessage: '' });
                          UltraAgentHelpers.showAlertWithOneButton("Oops!", error.message);
                        });
                    })
                    .catch((error) => {
                      console.log(error);
                      this.setState({ spinner: false, spinnerMessage: '' });
                      UltraAgentHelpers.showAlertWithOneButton("Oops!", error.message);
                    });

                }, 2000);
              })
                .catch((error) => {
                  console.log(error);
                  this.setState({ spinner: false, spinnerMessage: '' });
                  UltraAgentHelpers.showAlertWithOneButton("Oops!", error.message);
                });
            }
          },
        ],
        { cancelable: false }
      );

    } else if (this.state.startEndShiftButtonTitle == 'UPDATE SHIFT') {
      this.props.navigation.navigate('BookingCancel', { booking: this.state.booking });
    }
  }
  //  componentDidMount() {
  //  this.setState({ totalDuration: 15 });
  //  }
  // abc(){

  //   this.setState({ totalDuration: 20 });

  //   this.setState({startEndShiftButtonShouldShow: false})
  // }
  render() {

    return (
      <Container>
        <Spinner
          visible={this.state.spinner}
          textContent={this.state.spinnerMessage}
          textStyle={{ color: '#FFF' }}
        />
        <Header style={{ backgroundColor: '#5271FF' }}>
          <Left style={{ flex: 1 }}>
            <Button transparent
              onPress={() => this.props.navigation.goBack(this.props.navigation.state.key)}>
              <Icon name="arrow-back" color='#fff' />
            </Button>
          </Left>
          <Body style={{ flex: 4, textAlign: 'center', alignItems: 'center' }}>
            <Title style={styles.HeaderTitle}>{this.state.booking.shiftSubCategory} #{this.state.booking.shiftNo}</Title>
          </Body>
          <Right style={{ flex: 1 }}>
          </Right>
        </Header>

        <View style={{ backgroundColor: '#5f7bfa' }}>
          <Title style={styles.SubHeaderTitle}>Location : {this.state.booking.address.toUpperCase()}</Title>
        </View>
        <Content>
          <View style={styles.jobDetailsContainer}>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Shift Description</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}>
                  {this.state.booking.description}
                </Text>
                <Text>
                  <Icon name="md-clock" size={15} color="#888" />
                  &nbsp;{this.state.booking.shiftStartDateTime.getHours() + ':' + ("0" + this.state.booking.shiftStartDateTime.getMinutes()).slice(-2)} &nbsp;&nbsp;
                    <Icon name="md-calendar" size={15} color="#888" />
                  &nbsp;{this.state.booking.shiftStartDateTime.getDate() + '/' + (this.state.booking.shiftStartDateTime.getMonth() + 1) + '/' + this.state.booking.shiftStartDateTime.getFullYear()}
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Employer</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}>
                  {this.state.booking.companyName}
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Address</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}>
                  {this.state.booking.address}
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Mobile Number</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}>
                  {this.state.booking.contactphonenumber}
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Duration (HH:mm)</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}>
                  {this.state.booking.duration}
                </Text>
              </View>
            </View>
            <View style={styles.jobDetailsContent}>
              <Label style={styles.shiftLabel}>Fees</Label>
              <View style={styles.textBox}>
                <Text style={styles.jobDetailsChildText}>
                  £{this.state.booking.fee}/h
                </Text>
              </View>
            </View>
            {this.state.hideCountDown != true &&
              <View style={styles.jobDetailsContent}>
                <Label style={styles.shiftLabel}>Count Down Timer</Label>
                <Text style={{ color: '#FF5733', flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>{this.state.w}</Text>
                <CountDown
                  timetoShow={('H', 'M', 'S')}
                  until={this.state.totalDuration}
                  //duration of countdown in seconds
                  onFinish={this.onLoad(false)}
                  onPress={null}
                  size={30}
                  digitBgColor='transparent'
                  timeTxtColor="#000"
                  labelD='Days'
                  labelH='Hr'
                  labelM='Min'
                  labelS='Sec'
                />

              </View>
            }

            <View style={styles.buttonNavigateConatiner}>
              <Button style={styles.navigateButton}
                onPress={this._onPressNavigate}>
                <Image source={require('../images/navigateicon.png')}
                  style={styles.buttonImageIcon} />
                <Text style={{ color: '#000' }}>NAVIGATE</Text>
              </Button>
            </View>
            <View style={styles.buttonConatiner}>
              <Button style={[styles.btnOrange, styles.btnMarginStyle]}
                onPress={this._onPressCancelBooking} >
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>CANCEL BOOKING</Text>
              </Button>
              {this.state.startEndShiftButtonShouldShow == true &&
                <Button style={[styles.btnBlue, styles.btnMarginStyle]}
                  onPress={this._onPressStartShift} >
                  <Text style={{ color: '#fff' }}>{this.state.startEndShiftButtonTitle}</Text>
                </Button>
              }
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  HeaderTitle: {
    color: '#fff',
  },
  SubHeaderTitle: {
    color: '#fff',
    fontSize: 16,
    padding: 5,
    paddingTop: 6,
    textAlign: 'center',
    alignItems: 'center',
  },
  jobDetailsContainer: {
    margin: 15,
    marginTop: 15,
    marginBottom: 15,
  },
  jobDetailsContent: {
    marginBottom: 15,
  },
  textBox: {
    backgroundColor: '#f4f4f4',
    padding: 15,
  },
  jobDetailsChildText: {
    fontWeight: 'bold',
  },
  shiftLabel: {
    marginBottom: 5,
    fontSize: 14,
    color: '#c0c0c0'
  },
  buttonConatiner: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30,
  },
  buttonNavigateConatiner: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
  btnMarginStyle: {
    margin: 5,
    marginBottom: 10,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },
  btnOrange: {
    backgroundColor: '#df8c40',
  },
  btnBlue: {
    backgroundColor: '#5271ff',
  },
  buttonImageIcon: {
    width: 30,
    height: 30,
  },
  navigateButton: {
    backgroundColor: 'transparent',
    borderColor: '#000',
    borderWidth: 2,
    paddingLeft: 20,
    paddingRight: 20,
    padding: 10,
  }
})