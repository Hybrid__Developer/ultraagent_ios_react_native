import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  Text,
  Switch,
  TouchableOpacity,
  WebView, Platform, AsyncStorage
} from 'react-native';
import {
  Container,
  Header,
  Title,
  Body,
  Button,
  Left,
  Right,
  Content,
  Icon
} from 'native-base';
import {Icon as NativeBaseIcon} from 'native-base';

import MapView from 'react-native-maps';
import SingletonClass from '../helpers/SingletonClass';
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';

import firebase from 'react-native-firebase';

export default class Home extends Component {

  markersMap = {};
  todayShiftsCache = [];

  constructor(props) {
    super(props);
    this.state = {
      ...SingletonClass.get('loggedinprofile'),
      loading: true,
      // places: [
      //   {
      //     title:'The Brithish Library Plumbing',
      //     latitude: -31.433333,
      //     longitude: 152.900000,
      //   },
      // ],
      markedLocation: {
        latitude: 51.5074,
        longitude: -0.1278,
        latitudeDelta: 0.0922,
        longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * 0.0922,
      },
      shiftsToday: [],
      hasBookedShiftsToday:true
    };

    if (this.state.wAvailable == undefined) {
      this.state = {...this.state, wAvailable: true}
    }

    this.loggedinprofile = SingletonClass.get('loggedinprofile');

    this.onMarkerPressed = this.onMarkerPressed.bind(this);
  }

  componentDidMount() {
    // Keep listening to profile change and update in Singleton reference
    // DO NOT UNSUBSCRIBE TO PROFILE UPDATES. WE NEED THIS GO ON.
       // DO NOT REFERENCE this pointer. THIS BLOCK OF CODE WILL RUN 
       // EVEN AFTER THIS COMPONENT HAS UNMOUNTED
       // SOMONE HAS TO DO THIS TO UPDATE THE LOGGED IN PROFILE
    aUser = SingletonClass.get('loggedinprofile');

    if ((this.unsubscribeProfilesCollection != undefined) && (this.unsubscribeProfilesCollection != null)) {
      this.unsubscribeProfilesCollection();
    }
    this.unsubscribeProfilesCollection = null;

    this.unsubscribeProfilesCollection = 
          firebase.firestore().collection('profiles')
            .doc(aUser.userId)
            .onSnapshot(this.onProfileQueryCompleted);

    
   this.updateAvailableShiftsToday();


  }

  updateScheduledBookingsToday() {
    let todayBeginning = new Date();
    todayBeginning.setHours(0,0,0,0);
    let todayEnd = new Date();
    todayEnd.setHours(23,59,59,999);
    this.unsubscribeScheduledBookingsTodayCollection = firebase.firestore().collection('bookings')
    .where('shiftStartDateTime','>',todayBeginning)
    .where('shiftStartDateTime','<',todayEnd)
    // .where('subcategory','==',this.loggedinprofile.wSubCategory)
    // .where('isAdvanceBooking','==',false)
    .onSnapshot(this.onScheduledBookingsTodayCollectionUpdate) 

  }

  updateAvailableShiftsToday() {
    let todayBeginning = new Date();
    todayBeginning.setHours(0,0,0,0);
    let todayEnd = new Date();
    todayEnd.setHours(23,59,59,999);
    this.unsubscribeShiftsTodayCollection = firebase.firestore().collection('shifts')
    .where('startDateTime','>',todayBeginning)
    .where('startDateTime','<',todayEnd)
    // .where('subcategory','==',this.loggedinprofile.wSubCategory)
    // .where('isAdvanceBooking','==',false)
    .onSnapshot(this.onShiftsTodayCollectionUpdate) 

  }

  componentWillUnmount() {
    console.log('Home::componentWillUnmount');

    // 9/4/2019: 
    // DO NOT UNSUBSCRIBE TO PROFILE UPDATES. WE NEED THIS GO ON.
    if (this.unsubscribeProfilesCollection != null) {
      this.unsubscribeProfilesCollection();
    }
    this.unsubscribeProfilesCollection = null;

    
    this.unsubscribeShiftsTodayCollection();
    if (this.unsubscribeScheduledBookingsTodayCollection != undefined) {
      this.unsubscribeScheduledBookingsTodayCollection();
    }
  }

  onMarkerPressed = (aShiftId) => {
    //console.log('onMarkerPressed: '+aShiftId);

    for (shift of this.state.shiftsToday) {
      if (shift.id == aShiftId) {
        this.props.navigation.navigate('ShiftsDetails',{'selectedShift' : shift});
        break;
      }
    }


  }

  onShiftsTodayCollectionUpdate =  (querySnapshot) => {
    const fromShifts = [];
    querySnapshot.forEach((doc) => {
      aObj = doc.data();
      aObj.id = doc.id;
      // Only show those shifts matching the working hours of the loggedinuser
      if(aObj.subcategory==this.loggedinprofile.wSubCategory) {

        // Bookedshifts are not to be shown
        if((aObj.recruitmentStatus == undefined) || (aObj.recruitmentStatus == 'pending')){
          fromShifts.push(aObj);
        }
      }
    });
  
    this.todayShiftsCache = fromShifts;
    this.updateScheduledBookingsToday();

  }

  onScheduledBookingsTodayCollectionUpdate =  (querySnapshot) => {
    const fromBookings = [];
    querySnapshot.forEach((doc) => {
      aObj = doc.data();
      aObj.id = doc.id;
      // Only show those shifts matching the working hours of the loggedinuser
      if((aObj.status=='active') && (aObj.workerId == this.loggedinprofile.userId)) {
        fromBookings.push(aObj);
      }
    });

    let someShifts = [];

    if ((fromBookings.length > 0) && (this.todayShiftsCache.length > 0))  {
      // Show my bookings today
     // alert('1')
    
      this.state.hasBookedShiftsToday = true;
      //'My booked shifts today';
      for(let i=0; i < fromBookings.length; i++) {
       // alert(fromBookings[i])
      for(let j=0; j < this.todayShiftsCache.length; j++) {
       
          if (fromBookings[i].shiftId == this.todayShiftsCache[j].id) {
           
            this.todayShiftsCache[j].booking = fromBookings[i];
            someShifts.push(this.todayShiftsCache[i]);
        
          }
       }
      }
    } else {
     // alert('2')
     this.state.hasBookedShiftsToday = false;//'Trending shifts today';
        // a user to receive shifts that fall in their working hours
      someShifts = 
      UltraAgentHelpers.filterShiftsByMatchingWorkingHours(this.todayShiftsCache,this.loggedinprofile);
    }

    // Sort Shifts by descending order of date
    someShifts.sort((a,b) => {
      console.log(a.startDateTime);
      const aDate = a.startDateTime;
      const bDate = b.startDateTime;
      if (aDate.getTime() > bDate.getTime()) {
        return -1;
      } else if (aDate.getTime() < bDate.getTime()) {
        return 1;
      }
      return 0;
    });

    // Calculate map boundary 
    let minLat=9999; let maxLat=-9999;
    let minLng=9999; let maxLng=-9999;
    let shouldUpdateBoundingBox = false;

    // Initialize bounding box with first lat/lng
    for (aShift of someShifts) {
      if (aShift.lat != undefined) {
        const aLat = parseFloat(aShift.lat+"");
        minLat = aLat - 0.00922; maxLat = aLat + 0.00922;
        shouldUpdateBoundingBox = true;
        break;

      }
    }
    for (aShift of someShifts) {
      if (aShift.lng != undefined) {
        const aLng = parseFloat(aShift.lng+"");
        minLng = aLng - 0.00922; maxLng = aLng + 0.00922;
        shouldUpdateBoundingBox = true;
        break;
      }
    }
    
    for (aShift of someShifts) {
      //console.log('shift ('+aShift.lat+", "+aShift.lng+")");
      if (aShift.lat != undefined) {
        const aLat = parseFloat(aShift.lat+"");
        if (minLat > aLat) { minLat = aLat; shouldUpdateBoundingBox=true; }
        if (maxLat < aLat) { maxLat = aLat; shouldUpdateBoundingBox=true; }
      }

      if (aShift.lng != undefined) {
        const aLng = parseFloat(aShift.lng+"");
        if (minLng > aLng) { minLng = aLng; shouldUpdateBoundingBox=true; }
        if (maxLng < aLng) { maxLng = aLng; shouldUpdateBoundingBox=true; }
      }
    }

    if (shouldUpdateBoundingBox == true) {
      const deltaLat = Math.abs(minLat - maxLat);
      const deltaLng = Math.abs(minLng - maxLng);
      let delta = deltaLat;
      if (deltaLng > deltaLat) { delta = deltaLng; }

      //console.log('min: ('+minLat+", "+minLng+")");
      //console.log('max: ('+maxLat+", "+maxLng+")");

      const midLat = (minLat+maxLat)/2.0;
      const midLng = (minLng+maxLng)/2.0;
      this.setState({
        markedLocation: {
          latitude: midLat,
          longitude: midLng,
          latitudeDelta: delta,
          longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * delta,
        },
      });
    }
  
  
    this.setState({ 
      shiftsToday: someShifts,
      //loading: false,
    });
  
    
  }
    

  static navigationOptions = {
    header:null
  }

  onProfileQueryCompleted = async (querySnapshot) => {
    //console.log("Home::onProfileQueryCompleted");
    //console.log(querySnapshot);
       // DO NOT UNSUBSCRIBE TO PROFILE UPDATES. WE NEED THIS GO ON.
       // DO NOT REFERENCE this pointer. THIS BLOCK OF CODE WILL RUN 
       // EVEN AFTER THIS COMPONENT HAS UNMOUNTED
       // SOMONE HAS TO DO THIS TO UPDATE THE LOGGED IN PROFILE
    if ((querySnapshot != undefined) && (querySnapshot != null) &&
        (querySnapshot._data != undefined) && (querySnapshot._data != null)) 
    {
      //console.log(querySnapshot._data);

      // On profile update, if sendNotifications have been turned ON or OFF, then accordingly,
      // link or unlink FCM
      const oldProf = SingletonClass.get('loggedinprofile');
      const newProf = querySnapshot._data;
      if ((oldProf == undefined) || (oldProf == null) || (oldProf.sendNotifications != newProf.sendNotifications)) {
        // Update notifications
        let fcmToken = await AsyncStorage.getItem('fcmToken', null);
        if((newProf != undefined) && (newProf != null) && (newProf.userId != undefined) && (fcmToken != null)) {
          if (newProf.sendNotifications == true) {
            UltraAgentHelpers.linkFcmTokenWithUserUserId(fcmToken, newProf.userId);
          } else {
            UltraAgentHelpers.unlinkFcmTokenFromUserUserId(fcmToken, newProf.userId);
          }
        }
      }

      SingletonClass.set('loggedinprofile',querySnapshot._data);
    }
  }

  _handleToggleSwitch = () => {
    //console.log('_handleToggleSwitch called');
    //console.log(this.state);
    const val = this.state.wAvailable;
    this.setState({wAvailable: !val});
    if (val == true) {
      // switched off
      UltraAgentHelpers.showAlertWithOneButton("Information",
      "\nBy switching OFF availability, you will not receive new shift notifications any more.\n\n"+
      "You can turn ON availability any time and will continue to receive notifications of new shift postings.\n");
    } else {
      UltraAgentHelpers.showAlertWithOneButton("Information",
      "\nBy switching ON availability, you will start receiving notifications on new shift postings.\n\n"+
      "You can turn OFF availability any time and will stop receiving notifications of new shift postings.\n");

    }
   
    firebase.firestore().collection('profiles').doc(this.state.userId).set(
        {wAvailable:!val}, {merge: true}
    ).then(() => {})
    .catch((err) => {
      console.log(err);
      UltraAgentHelpers.showAlertWithOneButton("Error",err.message);
    });

  }


  renderCallout() {
    //console.log('renderCallout called');
    // Bharat, give 2 seconds gap for regions to load complete 
    // before callout is rendered
    setTimeout(() => {
      if(this.state.calloutIsRendered === true) { return; }
      this.setState({calloutIsRendered: true});
      //console.log(this.markersMap);
      for (const [k, v] of Object.entries(this.markersMap)) {
        v.showCallout();
        break;
      }
    }, 2000);
  }

  onPressZoomIn() {
    this.region = {
      latitude: this.state.markedLocation.latitude,
      longitude: this.state.markedLocation.longitude,
      latitudeDelta: this.state.markedLocation.latitudeDelta * 10,
      longitudeDelta: this.state.markedLocation.longitudeDelta * 10
    }

    this.setState({
      markedLocation: {
        latitudeDelta: this.region.latitudeDelta,
        longitudeDelta: this.region.longitudeDelta,
        latitude: this.region.latitude,
        longitude: this.region.longitude
      }
    })
    this.map.animateToRegion(this.region, 100);
  }

  onPressZoomOut() {
    this.region = {
      latitude: this.state.markedLocation.latitude,
      longitude: this.state.markedLocation.longitude,
      latitudeDelta: this.state.markedLocation.latitudeDelta / 10,
      longitudeDelta: this.state.markedLocation.longitudeDelta / 10
    }
    this.setState({
      markedLocation: {
        latitudeDelta: this.region.latitudeDelta,
        longitudeDelta: this.region.longitudeDelta,
        latitude: this.region.latitude,
        longitude: this.region.longitude
      }
    })
    this.map.animateToRegion(this.region, 100);
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#5271FF'}}>
         
          <Left style={{flex:1}}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Image source={require('../images/hamburgericon.png')} style={{height:32, width:32,}} />
              </Button>
          </Left>
          <Body style={{flex:3, textAlign:'center', alignItems:'center'}}>
              <Title style={styles.HeaderTitle}>{'Home'.toUpperCase()}</Title>
          </Body>
          <Right style={{flex:1}}>
              
          </Right>
         
       </Header>
       <View style={styles.nestedToggleView}>
          <Text style={styles.toggleText}>{this.state.wAvailable? 'Available':'Unavailable'}</Text>
          <View style={styles.toggleIconstyle}>
          <Switch
              onValueChange = {this._handleToggleSwitch}
              value = {this.state.wAvailable} 
              thumbColor = {'#fff'}
              trackColor = {'#ccc'}
              onTintColor = {'#4fd25d'}
              style={styles.switchBtnStyle} />
          </View>
        </View>
        <View style={{backgroundColor:'#fff'}}>
          <Title style={styles.SubHeaderTitle}>{this.state.hasBookedShiftsToday? 'My booked shifts today':'Trending shifts today'}</Title>
        </View>
       
        <View style={styles.mapContainer}>
          <MapView
          ref={map => (this.map = map)}
          style={styles.map}
          region={this.state.markedLocation}
          //onRegionChangeComplete={region => this.setState({region: region})}
          onRegionChangeComplete={() => this.renderCallout()}
          zoomEnabled={true}
          pitchEnabled={true}
          showsUserLocation={true}
          showsMyLocationButton={true}
          followUserLocation={true}
          zoomControlEnabled={true}
          >

          {this.state.shiftsToday.map((aShift, index) => {
            const coord = {latitude: aShift.lat, longitude: aShift.lng};

            return (
              <MapView.Marker
                ref={(marker) => {this.markersMap[aShift.id] = marker;}}
                key={aShift.id} identifier={`${aShift.id}`}
                coordinate={coord}
                tooltip={true}
                style={{width:60, height: 60}}
                image={this.state.hasBookedShiftsToday? require('../images/markericon-red.png') : require('../images/markericon.png')}
                 onPress={() => {}}
                >


                <MapView.Callout style={{maxWidth:180, maxHeight:80, width:180}} onPress={() => {this.onMarkerPressed(aShift.id);}}>
                  <MarkerCalloutView category={aShift.category} title={aShift.subcategory+' #'+aShift.shiftNo} 
                  date={aShift.startDateTime.getDate()+'/'+(aShift.startDateTime.getMonth()+1)+'/'+aShift.startDateTime.getFullYear()} 
                  time={aShift.startDateTime.getHours() +':'+("0" + aShift.startDateTime.getMinutes()).slice(-2)} 
                  shiftId={aShift.id} 
                  onMarkerPressed={this.onMarkerPressed}/>
                </MapView.Callout>

              </MapView.Marker>
            );
          })}

        </MapView>
         {/* Commenting out zoom buttons as user can do pinch and zoom on device */}
        {/* <View style={{position:'absolute', 
                bottom:10, right:10, zIndex:9 }}>
            <TouchableOpacity
                style={{...styles.zoomButton, ...{borderBottomWidth:1, borderBottomColor:'#efefef',}}}
                onPress={()=>{this.onPressZoomIn()}}
                >
                <Icon 
                  type="FontAwesome" 
                  name="plus" 
                  style={{fontSize: 20, color: '#666', padding:3}}
                />
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.zoomButton}
                onPress={()=>{this.onPressZoomOut()}}
                >
                <Icon 
                  type="FontAwesome" 
                  name="minus"  
                  style={{fontSize: 20, color: '#666', padding:3}}
                />
            </TouchableOpacity>
        </View> */}
        </View>
      </Container>
     );
    }
  }
  const styles = StyleSheet.create({
    container: {
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf:'stretch',
      width:null,
    },
    HeaderTitle:{
      color:'#fff',
     
    },
    mapContainer:{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
  map: {
    marginTop: 1.5,
    ...StyleSheet.absoluteFillObject,
  },
  SubHeaderTitle:{
    color:'#000',
    fontSize:16,
    padding:5,
    paddingTop:6,
  },
  toggleText: {
    color: '#fff',
    alignItems:'flex-end',
    lineHeight:30,
  },
  toggleIconstyle:{
    alignItems:'flex-start'
  },

  nestedToggleView: {
   flexDirection: 'row',
   justifyContent: 'space-between',
   backgroundColor:'#6e87fb',
   padding:2,
   paddingLeft:10, 
   paddingRight:10,
  },
  toggleFontStyle:{
  },
  switchBtnStyle:{
    ...Platform.select({
      ios: {
        transform: [{ scaleX: .6 }, { scaleY: .6 }],
      },
      android: {
        transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }],
      },
  })
  },
  markerContainer:{
    flexDirection: 'row', 
    flex:1,
    paddingRight:5
  },
  markerJobTitle:{
    color:'#000', 
    flexWrap:'wrap',
    fontSize:12,
    fontWeight:'bold',
    lineHeight:14,
  },
  mJobNumbrStyle:{
    color:'#5f7bfa',
    marginTop:5,
    fontSize:12,
    flexWrap:'wrap',
  },
  zoomButton:{
    backgroundColor:'#fff', 
    borderRadius:3, 
    padding:10, 
  },
})


class MarkerCalloutView extends React.Component {
  constructor(props) {

    super(props);
    this.sideIcon = require('../images/plumbingicon.png');


  }

  displayCalloutImage() {

    if(Platform.OS === 'ios') {
      // showing local
      if (this.props.category == "Construction") {
        this.sideIcon = require('../images/cat-construction1.png');
      } else if (this.props.category == "Health and Social Care") {
        this.sideIcon = require('../images/cat-healthcare1.png');
      } else if (this.props.category == "Security and Concierge") {
        this.sideIcon = require('../images/cat-security1.png');
      } else if (this.props.category == "Hospitality") {
        this.sideIcon = require('../images/cat-hospitality1.png');
      }

      return(
        <View style={styles.markerContainer}>
        <Image style={{width:30, height:30, 
          alignItems: 'center', justifyContent:'center',}} 
          source={this.sideIcon}/>
          <View style={{marginLeft:15, marginRight:10}}>
          <Text style={styles.markerJobTitle}>{this.props.title}</Text>
          <View style={{flexDirection: 'row', flex:1,}}>
            <Text style={{...styles.mJobNumbrStyle, ...{marginRight: 10}}}>{this.props.date}</Text>
            <Text style={styles.mJobNumbrStyle}>
              <NativeBaseIcon type="FontAwesome" name="clock-o" 
                  style={{fontSize: 14, color: '#5f7bfa'}}/> {''}
                  {this.props.time}
            </Text>
          </View>
        </View>
        </View>
      );
    } else {

      // showing from web
      if (this.props.category == "Construction") {
        this.sideIcon = {uri: 'https://ultraagent.co.uk/assets/cat-construction1.png'};
      } else if (this.props.category == "Health and Social Care") {
        this.sideIcon = {uri: 'https://ultraagent.co.uk/assets/cat-healthcare1.png'};
      } else if (this.props.category == "Security and Concierge") {
        this.sideIcon = {uri: 'https://ultraagent.co.uk/assets/cat-security1.png'};
      } else if (this.props.category == "Hospitality") {
        this.sideIcon = {uri: 'https://ultraagent.co.uk/assets/cat-hospitality1.png'};
      }

      return(
        <View style={styles.markerContainer}>
        <WebView
            style={{width:30, height:30, 
              alignItems: 'center', justifyContent:'center',}} 
              source={{uri:'https://ultraagent.co.uk/assets/cat-security1.png'}}
            injectedJavaScript={'document.getElementsByTagName("BODY")[0].style.backgroundColor = "#ffffff";'}
            javaScriptEnabledAndroid={true}
          /> 
          <View style={{marginLeft:45, marginRight:10}}>
          <Text style={styles.markerJobTitle}>{this.props.title}</Text>
          <View style={{flexDirection: 'row', flex:1,}}>
            <Text style={{...styles.mJobNumbrStyle, ...{marginRight: 10}}}>{this.props.date}</Text>
            <Text style={styles.mJobNumbrStyle}>
              <NativeBaseIcon type="FontAwesome" name="clock-o" 
                  style={{fontSize: 14, color: '#5f7bfa'}}/> {''}
                  {this.props.time}
            </Text>
          </View>
        </View>
        </View>
      );
    }

  }

  render() {
    return (

      <View style={{backgroundColor: '#fff'}}>
        {/* No Need to add TocuableOpacity here. Callout has onPress()
        <TouchableOpacity onPress={() => {this.props.onMarkerPressed(this.props.shiftId);}}> */}
        {this.displayCalloutImage()} 
        {/* </TouchableOpacity> */}
      </View>
    );
  }
}

// MarkerCalloutView.propTypes = {
//   title: React.PropTypes.string.isRequired
// };
