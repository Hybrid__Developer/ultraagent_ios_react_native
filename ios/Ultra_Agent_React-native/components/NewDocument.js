import React, { Component } from 'react';
import { StyleSheet, Text, View, 
   Image, TextInput, TouchableOpacity, TouchableHighlight, ImageBackground, 
   ScrollView, Platform, Button, Alert, Dimensions} from 'react-native';
import { List, FlatList } from "react-native";
//import  Icon from 'react-native-ionicons';
import { Picker, Icon } from 'native-base';
import DatePicker from 'react-native-datepicker'
import UltraAgentHelpers from '../helpers/UltraAgentHelpers';
import UploadedObjectAsyncModel from '../asyncstoragemodels/UploadedObjectAsyncModel';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import Spinner from 'react-native-loading-spinner-overlay';
import ProgressCircle from 'react-native-progress-circle';
import SingletonClass from '../helpers/SingletonClass';
import moment from 'moment';

import firebase from 'react-native-firebase';

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

// Because this variable remains in global scope, it should always be a 
// unique name, otherwise, as screen push pop it will get overridden if declared by same name in
// another screen
thisNewDocumentInstance = null;

export default class NewDocument extends Component {

    constructor(props) {
        super(props);

        this.documentTypesData = SingletonClass.get('documentTypesData');

        this.state = {
            selectedInsurance: "",
            spinner:false,
            spinnerMessage: '',
            isUploading:false,
            documentType:'',
            documentNumber:'',
            expiryDate: undefined,
            wDocuments:[],
        };
    

        this._uploadImage = this._uploadImage.bind(this);

        thisNewDocumentInstance = this;

        this.uploadProgressUpdate = this.uploadProgressUpdate.bind(this);

    }

    onValueChangeDocumentType(itemValue) {

        if ((itemValue == undefined) || (itemValue.length < 1) || (itemValue == '-none-')) {
        } else {
            this.setState({
                documentType: itemValue,
                documentNumber:'',
                expiryDate:undefined,
            });   
        }
    }

    documentTypesList = () =>{
       
        var arrKeys = Object.keys(this.documentTypesData);
        console.log(arrKeys);
        return( arrKeys.map( (x,i) => { 
            if ((x == undefined) || (x.length < 1) || (x == '-none-')) {
                return( <Picker.Item label={this.documentTypesData[x]} key={i} value={x} disabled={true}/>)
            } else {
                return( <Picker.Item label={this.documentTypesData[x]} key={i} value={x} />)
            } 
        }));

    }


    componentWillMount() {
        // Required for instancing of models objects.
        UploadedObjectAsyncModel.require(UploadedObjectAsyncModel);
 
    }
   

    

    static navigationOptions = {
       // header:null
       headerTitle: 'NEW DOCUMENT',
       headerStyle: {
        backgroundColor: '#5271FF',
      },
      headerBackTitle: null,
      headerTitleStyle: {
        color: "#fff",
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      headerTintColor: '#fff',
    }

    uploadProgressUpdate = (progress, transferred, total) => {

        wDocuments = this.state.wDocuments;
        const aLength = wDocuments.length;
        if (aLength > 0) {
            wDocuments[aLength-1].setProgress(progress);
            wDocuments[aLength-1].setTotalBytes(total);
            wDocuments[aLength-1].setBytesTransferred(transferred);

            this.setState({ wDocuments });
            //someRandom = Math.random();
            //this.setState({someRandom})
            //console.log("uploadProgressUpdate:progress="+progress+", transferred="+transferred+", total="+total);
            //console.log(this.state);
        }
    }

    _uploadImage (uri, fileName, mime='application/octet-stream') {
        return new Promise((resolve, reject) => {
            const uploadUri = Platform.OS === 'ios' ? uri.replace('file://','') : uri;
            const sessionId = new Date().getTime();
            let uploadBlob = null;

            //console.log(uploadUri)
            const fileParts = uploadUri.split('/')
            const lastPart = fileParts[fileParts.length -1];
            const nameParts = lastPart.split('.')
            const fileExtension = nameParts[nameParts.length -1] 
            const someRandom = Math.floor(Math.random() * 10000) + 1 
            const uniqFileName = "worker-"+sessionId+"-"+someRandom+"."+fileExtension;
            //console.log("uniqFileName="+uniqFileName);

            const uploadRefObject = new UploadedObjectAsyncModel();
            uploadRefObject.setUniqId(uniqFileName);
            uploadRefObject.setName(fileName);
            uploadRefObject.setDocumentType(this.state.documentType);
            if (this.state.documentNumber != undefined) {
                uploadRefObject.setDocumentNumber(this.state.documentNumber);
            }
            if (this.state.expiryDate != undefined) {
            
                uploadRefObject.setExpiryDate(this.state.expiryDate);
            }

            this.setState({
                wDocuments: [...this.state.wDocuments,uploadRefObject]
            });

            //create a reference in firebase  storage for the file 
            const uploadRef = firebase.storage().ref('docs').child(uniqFileName);

            //encode data with Bse64 prior to uploading
            fs.readFile(uploadUri, 'base64')
            .then((data) => {
                return Blob.build(data, { type: `${mime};BASE64` })
            })
            //place the blob into your storage reference
            .then((blob) => {
                uploadBlob = blob
                uploadTask = uploadRef.put(blob._ref, blob, { contentType: mime });

                // Register three observers:
                // 1. 'state_changed' observer, called any time the state changes
                // 2. Error observer, called on failure
                // 3. Completion observer, called on successful completion
                uploadTask.on('state_changed', function(snapshot){
                    // Observe state change events such as progress, pause, and resume
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

                    thisNewDocumentInstance.uploadProgressUpdate(progress, snapshot.bytesTransferred, snapshot.totalBytes);

                    //console.log('Upload is ' + progress + '% done');
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                        //console.log('Upload is paused');
                        break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                        //console.log('Upload is running');
                        break;
                    }
                },
                function(error) {
                    // Handle unsuccessful uploads
                    reject(error)
                }, function() {
                    // Handle successful uploads on complete
                    // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                    uploadRef.getDownloadURL().then(function(url) {
                        //console.log('File available at', url);
                        resolve(url)
                        
                    });
                });
            });

        });

    }

    _onPressButtonBrowse = () => {

        if ((this.state.documentType == undefined) || (this.state.documentType == null) ||
            (this.state.documentType.length < 1)) 
        {
            UltraAgentHelpers.showAlertWithOneButton("Document Type",
            "Please select a document type first");
            return;
        }

        if ((this.state.documentType != 'Other') && (this.state.documentType != 'Training')) {
            if ((this.state.documentNumber == undefined) || (this.state.documentNumber == null) ||
                (this.state.documentNumber.length < 1)) 
            {
                UltraAgentHelpers.showAlertWithOneButton("Document Number",
                "Please specify a document number first");
                return;
            }
        }

        if ((this.state.documentType != 'Other') && 
            (this.state.documentType != 'Training') &&
            (this.state.documentType != 'ProfessionBodyCertificate') && 
            (this.state.documentType != 'IdentificationDocument')) {
            if ((this.state.expiryDate == undefined) || (this.state.expiryDate == null)) 
            {
                UltraAgentHelpers.showAlertWithOneButton("Expiry Date",
                "Please select a expiry date first");
                return;
            }
        }

        if (this.state.isUploading == true) {
            UltraAgentHelpers.showAlertWithOneButton("Browse not allowed","Please wait for current upload to complete.\n");
            return;
        }

        if ((this.state.wDocuments != undefined) && (this.state.wDocuments != null) &&
            (this.state.wDocuments.length >= 5)) 
        {
            UltraAgentHelpers.showAlertWithOneButton("Upload Limit Reached",
            "You can upload maximum 5 documents.\n"+
            "You can delete one of the earlier uploads and release quota instead.");
            return;
        }

        ImagePicker.showImagePicker({allowsEditing: true}, (response) => {
            console.log('Response = ', response);
    
            if (response.didCancel) {
                //console.log('User cancelled image picker');
            } else if (response.error) {
                //console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                //console.log('User tapped custom button: ', response.customButton);
            } else {  
                
                this.setState({isUploading: true});
                var aFileName = response.fileName;
                if (aFileName == undefined) {
                    var pieces = response.uri.split('/');
                    aFileName = pieces.pop() || pieces.pop();
                }
                this._uploadImage(response.uri, aFileName)
                .then((url) => {
                    this.setState({isUploading: false});
                    //console.log("Upload complete")
                    //console.log(url)
                    wDocuments = thisNewDocumentInstance.state.wDocuments;
                    const aLength = wDocuments.length;
                    if (aLength > 0) {
                        wDocuments[aLength-1].setBytesTransferred(wDocuments[aLength-1].getTotalBytes());
                        wDocuments[aLength-1].setProgress(100);
                        wDocuments[aLength-1].setUrl(url);

                        thisNewDocumentInstance.setState({ wDocuments });

                    }
                })
                .catch((error) => {
                    this.setState({isUploading: false});
                    //console.log("_onPressButtonBrowse:: Upload failed")
                    console.log(error)
                    UltraAgentHelpers.showAlertWithOneButton("Upload Error",error,"Dismiss");
                })
                
            }
        });
    }
    _onPressButtonDone = () => {

        if (this.state.isUploading == true) {
            UltraAgentHelpers.showAlertWithOneButton("Done not allowed","Please wait for current upload to complete.\n");
            return;
        }

        
        const currentDate = new Date();
        const formatedDateString = moment(currentDate).format('D, MMM YYYY');
        var arrayLength = this.state.wDocuments.length;
        if (arrayLength > 0) {
            const theProfile = SingletonClass.get('loggedinprofile');

            var docArr = [];
            if ((theProfile != undefined) && (theProfile.wDocuments != undefined)) {
                for (const aDoc of theProfile.wDocuments) {
                    docArr.push(aDoc);
                }
            }

            for (var i = 0; i < arrayLength; i++) {
                docArr.push({
                    url: this.state.wDocuments[i]._url,
                    name: this.state.wDocuments[i]._name,
                    totalBytes: this.state.wDocuments[i]._totalBytes,
                    uniqId: this.state.wDocuments[i]._uniqId,
                    uploadedDateStr: formatedDateString,
                    documentType: this.state.wDocuments[i]._documentType,
                    documentNumber: this.state.wDocuments[i]._documentNumber,
                    expiryDate: this.state.wDocuments[i]._expiryDate,
                });
            }
                
            this.setState({spinner: true, spinnerMessage:'Saving...'});

            firebase.firestore().collection('profiles').doc(theProfile.userId).set(
                {wDocuments:docArr}, {merge: true}
            ).then(() => {
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("SUCCESS",'Documents updated successfully');
                this.props.navigation.goBack(this.props.navigation.state.key);
            })
            .catch((error) => {
                console.log(error);
                this.setState({spinner: false, spinnerMessage:''});
                UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
            });
        }
       
    }

    _onPressDeleteDocument = (aDocument) => {

        if (this.state.isUploading == true) {
            UltraAgentHelpers.showAlertWithOneButton("Delete not allowed","Please wait for upload to complete.\n");
            return;
        }

        if (((aDocument.getUrl() == null) || (aDocument.getUrl().length < 1)) &&
            (aDocument.getProgress() > 0) && (aDocument.getProgress() < 100) ) {
            UltraAgentHelpers.showAlertWithOneButton("Delete Not Allowed","Please wait for upload to complete.\n");
            return;
        }

        Alert.alert(
            "Are you sure?",
            "You are attempting to delete "+aDocument.getName()+" of size "+
                UltraAgentHelpers.formatBytesToDisplay(aDocument.getTotalBytes())+".\n" ,
            [
                //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                {text: 'No, Cancel', onPress: () => {/*cancelled*/}, style: 'cancel'},
                //{text: 'OK', onPress: () => console.log('OK Pressed')},*/
                {text: "Yes, Delete", style:'destructive', onPress: () => {
                    //Delete now
                    this.setState({spinner: true, spinnerMessage:'Deleting...'});
                    firebase.storage().ref('docs').child(aDocument.getUniqId()).delete()
                    .then(() => {

                        arr = [];
                        wDocuments = this.state.wDocuments;
                        for (var i=0; i < wDocuments.length; i++) {
                            if (wDocuments[i].getUniqId() == aDocument.getUniqId()) {
                                // do not save
                            } else {
                                arr.push(wDocuments[i]);
                            }
                        }
                        this.setState({wDocuments: arr});
                    })
                    .catch((error) => {
                        console.log(error);
                        this.setState({spinner: false, spinnerMessage:''});
                        UltraAgentHelpers.showAlertWithOneButton("Oops!",error.message);
                    });

                }}
            ],
            { cancelable: false }
        );

        
    }

    renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: "100%",
              backgroundColor: "#CED0CE",
              marginLeft: "0%"
            }}
          />
        );
    };

  render() {
    return (
        <View style={styles.wrapper}>

            <Spinner
                visible={this.state.spinner}
                textContent={this.state.spinnerMessage}
                textStyle={{color: '#FFF'}}
            />
                <View style={{ flex:1, backgroundColor:'#fff'}} >
                    <ScrollView>

                        <View style={styles.logoContainer}>
                            <Text style={styles.iconText}>
                                {'Update Your Profile with a \n New Document'}
                            </Text>

                            <View style={[styles.inputContainer, styles.yearPickerStyle]}>
                                       
                            <Picker
                                mode="dropdown"
                                iconRight
                                // style={{borderColor: 'red', borderWidth:1}}
                                iosIcon={<Icon type="FontAwesome" name="caret-down" 
                                style={{ color: "#5271FF", fontSize: 15, position:'absolute', right:-30}} />}
                                style={ styles.dropdownPickerStyle}
                                itemDisabledTextStyle={{color:'grey'}}
                                placeholder="Select a Document Type"
                                placeholderStyle={{ color: "#838383", fontSize:14, left:0 }}
                                placeholderTextColor = "#838383"
                                selectedValue={this.state.documentType}
                                onValueChange={(documentType) => {this.onValueChangeDocumentType(documentType);}}
                            >
                                {/* <Picker.Item label="WITH EXPIRY DATE" value="Withexpirydate" disabled = {true} /> */}
                                {/* <Picker.Item label="Insurance" value="Insurance" />
                                <Picker.Item label="DBS certificate" value="DBSCertificate" />
                                <Picker.Item label="Driving License (for drivers only)" value="DrivingLicense" /> */}
                                {/* <Picker.Item label="WITHOUT EXPIRY DATE" value="Withoutexpirydate" disabled = {true} /> */}
                                {/* <Picker.Item label="Profession body Certificate" value="ProfessionBodyCertificate" />
                                <Picker.Item label="Identification document" value="IdentificationDocument" />
                                <Picker.Item label="Other (e.g CV)" value="Other" /> */}

                                {this.documentTypesList()}
                            </Picker>
                            {/* <Icon type="FontAwesome" name="caret-down" 
                            style={{fontSize: 15, color: '#5271FF', position:'absolute', right:110, zIndex:9}}/> */}
                            
                        </View>
                           
                        { this.state.documentType != 'Other' && 
                        this.state.documentType != 'Training' &&
                                <View style={styles.inputContainer}>
                                {/* <Image source={require('../images/dbs-icon.png')} 
                                style={styles.InputImageIconsize} /> */}
                                <TextInput 
                                    style={{flex:1}} 
                                    underlineColorAndroid={'transparent'}
                                    placeholder="Enter Document Number" 
                                    placeholderTextColor = "#838383"
                                    keyboardType='default' 
                                    outoCorrect={false}
                                    onChangeText={documentNumber => this.setState({ documentNumber })}
                                    value={this.state.documentNumber}
                                />

                                </View>
                            }
                           
                           { this.state.documentType != 'Other' && 
                           this.state.documentType != 'Training' &&
                           this.state.documentType != 'ProfessionBodyCertificate' && 
                           this.state.documentType != 'IdentificationDocument' &&
                                <View style={[styles.inputContainer, styles.dateInputContainer]}>
                                        
                                    <DatePicker
                                        style={{width: '100%', flex:1, height:30, bottom:5, paddingLeft:8}}
                                        date={this.state.expiryDate}
                                        mode="date"
                                        placeholder="Choose Expiry Date"
                                        placeholderStyle={{ color: "#838383", fontSize:14, left:0 }}
                                        format="DD-MM-YYYY"
                                        minDate={new Date()}
                                        // maxDate="2016-06-01"                               
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        customStyles={{
                                            dateInput: {
                                                alignItems : 'flex-start',
                                                borderColor: 'transparent', 
                                                borderWidth: 0,
                                                paddingLeft: 0,
                                            },
                                            placeholderText: { 
                                                color: '#838383' 
                                            }
                                        }}
                                        onDateChange={(expiryDate) => {this.setState({expiryDate: expiryDate})}}
                                        ref={(ref)=>this.datePickerRef=ref}
                                    />
                                        {/* <Image source={require('../images/datepicker-icon.png')} 
                                        style={[styles.InputImageIcon, styles.imageBg]} /> */}
                                        <TouchableHighlight onPress={() => this.datePickerRef.onPressDate()} 
                                            underlayColor='none'>
                                            <Image source={require('../images/datepicker-icon.png')} 
                                            style={[styles.InputImageIcon, styles.imageBg]}/>
                                        </TouchableHighlight>   
                                </View>
                            }

                            { this.state.documentType != undefined && this.state.documentType.length > 0 &&
                              this.state.documentNumber != undefined && this.state.documentNumber.length > 0 &&
                              this.state.expiryDate != undefined &&
                                <View>
                                    {/* <Image style={styles.logo} 
                                    source={require('../images/down50x50.png')}/> */}
                                    <TouchableHighlight onPress={this._onPressButtonBrowse} 
                                        style={styles.browseButton}>
                                        <Text style={{color:'#2EB2FF', alignItems: 'center', 
                                        textAlign:'center', fontWeight:'bold'}}>
                                            SELECT PHOTO 
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                            }

                            { this.state.documentType != undefined && 
                                (this.state.documentType == 'ProfessionBodyCertificate' || this.state.documentType == 'IdentificationDocument' || this.state.documentType == 'Other' || this.state.documentType == 'Training') &&
                                this.state.documentNumber != undefined &&  this.state.documentNumber.length > 0 &&
                                <View>
                                    {/* <Image style={styles.logo} 
                                    source={require('../images/down50x50.png')}/> */}
                                    <TouchableHighlight onPress={this._onPressButtonBrowse} 
                                        style={styles.browseButton}>
                                        <Text style={{color:'#2EB2FF', alignItems: 'center', 
                                        textAlign:'center', fontWeight:'bold'}}>
                                            SELECT PHOTO
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                            }

                            { this.state.documentType != undefined && this.state.documentType == 'Training' &&
                                <View>
                                    {/* <Image style={styles.logo} 
                                    source={require('../images/down50x50.png')}/> */}
                                    <TouchableHighlight onPress={this._onPressButtonBrowse} 
                                        style={styles.browseButton}>
                                        <Text style={{color:'#2EB2FF', alignItems: 'center', 
                                        textAlign:'center', fontWeight:'bold'}}>
                                            SELECT PHOTO
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                            }

                            { this.state.documentType != undefined && this.state.documentType == 'Other' &&
                                <View>
                                    {/* <Image style={styles.logo} 
                                    source={require('../images/down50x50.png')}/> */}
                                    <TouchableHighlight onPress={this._onPressButtonBrowse} 
                                        style={styles.browseButton}>
                                        <Text style={{color:'#2EB2FF', alignItems: 'center', 
                                        textAlign:'center', fontWeight:'bold'}}>
                                            SELECT PHOTO
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                            }
                        </View>

                            <View style={styles.uploadContainer}>
                            {/* <ScrollView> */}
                                <FlatList
                                    style={styles.listContentStyle}
                                    extraData={this.state} /*IMPORTANT: This line makes the byte counts update*/
                                    data={this.state.wDocuments}
                                    keyExtractor={(item, index) => index+""}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    renderItem={({ item }) => (
                                        <View style={[styles.rowContainer, styles.GrayBgColor]}>
                                            <View style={styles.textContainer}>
                                            <ProgressCircle
                                                percent={item.getProgress()}
                                                radius={16}
                                                borderWidth={4}
                                                color="#3399FF"
                                                shadowColor="#999"
                                                bgColor="#fff"
                                            >
                                                {/* <Text style={{ fontSize: 18 }}>{'30%'}</Text> */}
                                                <Image source={require('../images/pdf-icon1.png')} 
                                                style={styles.pdfImageIcon} />
                                            </ProgressCircle>
                                                {/* <Image source={require('../images/pdf-icon1.png')} 
                                                style={styles.pdfImageIcon} /> */}
                                                <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                                    <Text style={styles.textColor}>
                                                        { this.documentTypesData[item.getDocumentType()] }
                                                    </Text>
                                                    { item.getDocumentNumber().length > 0 && 
                                                        <Text style={styles.textColor}>
                                                            { item.getDocumentNumber() }
                                                        </Text>
                                                    }
                                                     { item.getExpiryDate().length > 0 && 
                                                        <Text style={styles.textColor}>
                                                            Expiry: { item.getExpiryDate() }
                                                        </Text>
                                                    }
                                                    <Text style={styles.textColor}>
                                                        { UltraAgentHelpers.formatBytesToDisplay(item.getBytesTransferred()) } / { UltraAgentHelpers.formatBytesToDisplay(item.getTotalBytes()) }
                                                    </Text>
                                                </View>
                                            </View>
                                            <View style={styles.crossIconContainer}>
                                                <Icon style ={styles.crossIcon} type="FontAwesome" name="times-circle-o" 
                                                    style={{fontSize: 20, color: '#f20028'}} onPress={() => this._onPressDeleteDocument(item)}/>
                                            </View>
                                        </View>
                                    )}
                                />
                            {/* </ScrollView> */}
                                {/* <View style={[styles.rowContainer, styles.GrayBgColor]}>
                                    <View style={styles.textContainer}>
                                        <Image source={require('../images/pdf-icon1.png')} 
                                        style={styles.pdfImageIcon} />
                                        <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                            <Text style={styles.textColor}>
                                                CV.pdf1
                                            </Text>
                                            <Text style={styles.textColor}>
                                                1.36mb/1.35mb
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.crossIconContainer}>
                                        <Icon style ={styles.crossIcon} name="md-close-circle" 
                                            size={20} color="#f20028" />
                                    </View>
                                </View> */}
                                {/* <View style={[styles.rowContainer, styles.WhiteBgColor]}>
                                    <View style={styles.textContainer}>
                                        <Image source={require('../images/pdf-icon2.png')} 
                                        style={styles.pdfImageIcon} />
                                        <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                            <Text style={styles.textColor}>
                                                CV.pdf1
                                            </Text>
                                            <Text style={styles.textColor}>
                                                1.36mb/1.35mb
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.crossIconContainer}>
                                        <Icon style ={styles.crossIcon} name="md-close-circle" 
                                            size={20} color="#f20028" />
                                    </View>
                                </View> */}
                                {/* <View style={[styles.rowContainer, styles.GrayBgColor]}>
                                    <View style={styles.textContainer}>
                                        <Image source={require('../images/pdf-icon3.png')} 
                                        style={styles.pdfImageIcon} />
                                        <View style={{flex:1, marginLeft:10, alignItems : 'flex-start'}}>
                                            <Text style={styles.textColor}>
                                                CV.pdf1
                                            </Text>
                                            <Text style={styles.textColor}>
                                                1.36mb/1.35mb
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.crossIconContainer}>
                                        <Icon style ={styles.crossIcon} name="md-close-circle" 
                                            size={20} color="#f20028" />
                                    </View>
                                </View> */}
                               
                                <TouchableHighlight 
                                style={styles.button} onPress={this._onPressButtonDone} >
                                    <Text style={styles.btntext}>
                                        DONE 
                                    </Text>
                                </TouchableHighlight>
                               
                            </View>
                    </ScrollView>

                </View>

        </View>
    );
  }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container:{
        flex:1,
        alignItems: 'center',
        alignSelf:'stretch',
        backgroundColor:'#fff',
        width: null,
        height:null,
    },
    logoContainer:{
        alignItems: 'center',
        alignSelf:'stretch',
        backgroundColor:'#fff',
        marginTop:20,
        marginBottom:10,
        padding:15,
    },
 
    iconText:{
        color:'#000',
        textAlign:'center',
        alignItems: 'center',
        marginTop:10,
        marginBottom:30,
        lineHeight:20,
    },
    logo:{
        marginTop:20
    },
    browseButton:{
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#2EB2FF',
        backgroundColor:'transparent',
        padding:10,
        marginTop:20,
        width:150,
        alignItems: 'center',
    },
    uploadContainer:{
        alignItems: 'stretch',
        backgroundColor:'transparent',
        marginBottom:20,
        paddingBottom:10,
        paddingTop:10,

    },
    GrayBgColor:{
        backgroundColor:'#f5f5f5',
    },
    WhiteBgColor:{
        backgroundColor:'#fff',
    },
    rowContainer:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft:30,
        paddingRight:30,
        paddingTop:10,
        paddingBottom:10,
    },
    pdfImageIcon:{
        width:40,
        height:40
    },
    textContainer: {
        flex: 1, 
        flexDirection: 'row',
        alignSelf:'stretch',
    },
    textColor:{
        color:'#838383',
    },
    crossIconContainer:{
        alignItems: 'center',
        alignSelf:'stretch',
        justifyContent: 'center',
    },

    button:{
        alignSelf:'stretch',
        marginLeft:70,
        marginRight:70,
        marginTop:20,
        marginBottom:50,
        backgroundColor:'#5271FF',
        padding:12,
        borderRadius:3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btntext:{
        textAlign:'center',
        alignSelf:'stretch',
        alignItems: 'center',
        color:'#fff',
        fontWeight:'500',
        fontSize:14,
        letterSpacing:1,
        color:'#fff',
    },
    linkContainer:{
        alignSelf:'stretch',
        alignItems:'center',
        marginBottom:20,
        //marginTop:20,
        justifyContent:'flex-end',
        position:'absolute',
        bottom:0,
        left:0,
        right:0,
    },
    
    regText:{
        marginTop:5,
        color:'#000',
    },
    dateInputContainer:{
        padding:0,
    },
    inputContainer: {
        flex: 1, 
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'stretch',
        padding:8,
        backgroundColor:'#fff',
        borderRadius:2,
        marginBottom:12,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.1,
                height:40,
            },
            android: {
                elevation:2,
                height:40,
                paddingBottom:0,
                paddingTop:0,
            },
        })
    },
    dropdownPickerStyle:{
        ...Platform.select({
            ios: {
                width: Dimensions.get('window').width-55,
                left:-15,
            },
            android: {
                width: undefined, 
                left:-5,
            },
        })
    },
    InputImageIcon: {
        marginRight:10,
        marginLeft:5,
        height: 20,
        width: 20,
        resizeMode : 'stretch',
        alignItems: 'center'
    },
    imageBg:{
        backgroundColor:'#efefef',
        marginRight:0,
        padding:20,
        height: 10,
        width: 10,
    },
  
});