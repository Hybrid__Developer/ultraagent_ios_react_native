import Model from "react-native-models";
 
export default class UploadedObjectAsyncModel extends Model {
    // className used instead name because babel replaces him at run-time.
    static get className() {
        return "UploadedObjectAsyncModel";
    }
 
    constructor() {
        super({
            uniqId: "String",
            name: "String",
            icon: "String",
            url: "String",
            progress: "Number",
            totalBytes: "Number",
            bytesTransferred: "Number",
            documentType: "String",
            documentNumber: "String",
            expiryDate: "String"
        });

        this.setUniqId("");
        this.setName("");
        this.setIcon("");
        this.setUrl("");
        this.setProgress(0);
        this.setTotalBytes(0);
        this.setBytesTransferred(0);
        this.setDocumentType("");
        this.setDocumentNumber("");
        this.setExpiryDate("");
    }
 
    test() {
        this.setName("AFileName");
        this.setUrl("http://www.google.com");
 
        const a = this.getName(); // a === 1
        const b = this.getUrl(); // b === "bar"
 
        try {
            this.setName("1");
        } catch (error) {
            return "exception";
        }
 
        return "no exception";
    }
}
