import Model from "react-native-models";
import UploadedObjectAsyncModel from "../asyncstoragemodels/UploadedObjectAsyncModel";

export default class ProfileAsyncModel extends Model {
    // className used instead name because babel replaces him at run-time.
    static get className() {
        return "ProfileAsyncModel";
    }
 
    constructor(type="worker") {
        super({
            errorMessage:"String",
            email: "String",
            profileType: "String",
            salutation: "String",
            firstName: "String",
            lastName: "String",
            dateOfBirth: "String",
            phoneNumber: "String",
            wInsuranceNumber: "String",
            address: "String",
            city: "String",
            wProfession: "String",
            wSubCategory: "String",
            wProfessionBodyNumber: "String",
            wDBSNumber: "String",
            wNationalInsurance: "String",
            wWorkExperienceDescr: "String",
            wWorkExperienceYears: "String",
            wDocuments: "Array",
            profilePicture: "UploadedObjectAsyncModel",   // Nested model
            password: "String",
            confirmPassword: "String",
            avatarSource:"String",
            postcode: "String"

        });
 
        this.setErrorMessage("");
        this.setEmail("");
        this.setProfileType(type);
        this.setSalutation("Mr");
        this.setFirstName("");
        this.setLastName("");
        this.setDateOfBirth("");
        this.setPhoneNumber("");
        this.setWInsuranceNumber("");
        this.setAddress("");
        this.setPostcode("");
        this.setCity("");
        this.setWProfession("");
        this.setWSubCategory("");
        this.setWProfessionBodyNumber("");
        this.setWDBSNumber("");
        this.setWNationalInsurance("");
        //this.setWWorkExperienceDescr("Work Experience ( Years )");
        this.setWWorkExperienceDescr("");
        this.setWWorkExperienceYears("");
        arr = new Array();
        //arr[0] = new UploadedObjectAsyncModel();
        this.setWDocuments(arr);


        this.setProfilePicture(new UploadedObjectAsyncModel());
        this.setPassword("");
        this.setConfirmPassword("");
        this.setAvatarSource("");

    }
 
    test() {
        this.setEmail("worker14@mobiona.com");
 
        const a = this.getEmail(); // a === 1
 
        try {
            this.setEmail("abc@xyz.com");
        } catch (error) {
            return "exception";
        }
 
        return "no exception";
    }
}